-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 27, 2016 at 04:30 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_realtynxt`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `last_login` date NOT NULL,
  `last_ip` varchar(255) NOT NULL,
  `last_session` varchar(32) NOT NULL,
  `email` varchar(255) NOT NULL,
  `memberId` int(11) NOT NULL,
  `super_user` smallint(1) NOT NULL,
  `perms` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `login`, `password`, `last_login`, `last_ip`, `last_session`, `email`, `memberId`, `super_user`, `perms`) VALUES
(1, 'admin', 'admin', '2016-01-27', '', '', 'realtynxt_admin@acrobat.co.in', 1, 1, 'ALL'),
(2, 'alex', '123456', '0000-00-00', '', '', 'alex.pierce@yahoo.co.in', 2, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_adventure`
--

CREATE TABLE `tbl_adventure` (
  `adventureId` int(11) NOT NULL,
  `adventureType` varchar(255) NOT NULL,
  `eventName` varchar(255) NOT NULL,
  `date` date DEFAULT NULL,
  `startingTime` varchar(50) DEFAULT NULL,
  `timespan` varchar(255) DEFAULT NULL,
  `frequency` varchar(50) NOT NULL,
  `repeatInterval` varchar(255) DEFAULT NULL,
  `capacity` int(11) NOT NULL,
  `availableEntry` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `contactName` varchar(255) NOT NULL,
  `contactNumber` varchar(15) NOT NULL,
  `meetingAddress` text NOT NULL,
  `safetyPrecautions` text NOT NULL,
  `whereToCome` text NOT NULL,
  `liker` varchar(255) NOT NULL,
  `images` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_adventure`
--

INSERT INTO `tbl_adventure` (`adventureId`, `adventureType`, `eventName`, `date`, `startingTime`, `timespan`, `frequency`, `repeatInterval`, `capacity`, `availableEntry`, `price`, `contactName`, `contactNumber`, `meetingAddress`, `safetyPrecautions`, `whereToCome`, `liker`, `images`, `status`) VALUES
(1, 'Extreme Sports', 'BASE Jumping', '0000-00-00', '', '10:00 to 19:00', 'Weekly', '1,3,7', 15, 14, 10000, 'Chris Gardner', '1234567890', 'Head Office, Bodakdev', 'Brace yourself', 'Mount Everest', 'Everyone', 'http://localhost/realtynxt/uploads/adventures/1452081856_Koala.jpg,http://localhost/realtynxt/uploads/adventures/1452081054_Desert.jpg,http://localhost/realtynxt/uploads/adventures/1452081055_Jellyfish.jpg', 1),
(3, 'Water Sports', 'Jet Skiing', '0000-00-00', '', '07:00 to 19:00', 'Monthly', '01/02/2016,04/02/2016,03/02/2016', 10, 3, 5000, 'Jason Statham', '1234567891', 'Head Office, Bodakdev', 'Hang on', 'Nagva Beach, Diu', 'Everybody', 'http://localhost/realtynxt/uploads/adventures/1452083556_Jellyfish.jpg', 0),
(4, 'Water Sports', 'Aquajogging', '0000-00-00', '', '06:00 to 09:00', 'Daily', 'Daily', 30, 16, 500, 'Steven Smith', '1234567892', 'Head Office, Goa', 'Beware of thorns!', 'Some Beach, Goa', 'Adults', 'http://localhost/realtynxt/uploads/adventures/1452147137_Hydrangeas.jpg,http://localhost/realtynxt/uploads/adventures/1452083786_Jellyfish.jpg,http://localhost/realtynxt/uploads/adventures/1452083786_Penguins.jpg', 1),
(5, 'Extreme Sports', 'Paragliding', '2016-01-31', '07:00', '', '0', NULL, 10, 9, 2500, 'Jade Wilson', '1234567893', 'Head Office, Himachal Pradesh', 'Hang on!', 'Mount Everest', 'Adults', 'http://localhost/realtynxt/uploads/adventures/1452084075_Desert.jpg,http://localhost/realtynxt/uploads/adventures/1452084075_Lighthouse.jpg,http://localhost/realtynxt/uploads/adventures/1452084088_Hydrangeas.jpg', 1),
(6, 'Extreme Sports', 'Bungee Jumping', '0000-00-00', '', '10:00 to 18:00', 'Monthly', '01/01/2016,10/01/2016,20/01/2016,30/01/2016', 10, 10, 2000, 'Steven Spielberg', '1234567894', 'Branch Office, Ahmedabad', 'Hang On!', 'Mount Everest', 'Everyone', 'http://localhost/realtynxt/uploads/adventures/1452260611_bungee-jumping.jpg,http://localhost/realtynxt/uploads/adventures/1452260611_bungee-jumping2.jpg,http://localhost/realtynxt/uploads/adventures/1452147485_Desert.jpg,http://localhost/realtynxt/uploads/adventures/1452147485_Lighthouse.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_adventure_type`
--

CREATE TABLE `tbl_adventure_type` (
  `id` int(11) NOT NULL,
  `eventName` varchar(255) NOT NULL,
  `parentId` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_adventure_type`
--

INSERT INTO `tbl_adventure_type` (`id`, `eventName`, `parentId`, `status`) VALUES
(1, 'Water Sports', 0, 1),
(2, 'Jet Skiing', 1, 1),
(3, 'Rafting', 1, 1),
(4, 'Aquajogging', 1, 1),
(5, 'Extreme Sports', 0, 1),
(6, 'Paragliding', 5, 1),
(7, 'Bungee Jumping', 5, 1),
(8, 'BASE Jumping', 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_area`
--

CREATE TABLE `tbl_area` (
  `areaId` int(11) NOT NULL,
  `areaName` varchar(255) NOT NULL,
  `cityId` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_area`
--

INSERT INTO `tbl_area` (`areaId`, `areaName`, `cityId`, `status`) VALUES
(1, 'Koregaon Park', 1, 1),
(2, 'Kalyani Nagar_NEW', 2, 1),
(3, 'Boat Club Road', 1, 1),
(4, 'Hadapsar', 1, 1),
(5, 'Wagholi', 1, 1),
(6, 'Kalyani Nagar_Old', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_business_category`
--

CREATE TABLE `tbl_business_category` (
  `id` int(11) NOT NULL,
  `categoryName` varchar(255) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_business_category`
--

INSERT INTO `tbl_business_category` (`id`, `categoryName`, `status`) VALUES
(1, 'Food Industry', 1),
(2, 'Hospitality', 1),
(3, 'Adventure trip', 1),
(4, 'Disc.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_city`
--

CREATE TABLE `tbl_city` (
  `cityId` int(11) NOT NULL,
  `cityName` varchar(100) NOT NULL,
  `stateId` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_city`
--

INSERT INTO `tbl_city` (`cityId`, `cityName`, `stateId`, `status`) VALUES
(2, 'Port Blair', 31, 1),
(3, 'Adilabad', 1, 1),
(4, 'Adoni', 1, 1),
(5, 'Amadalavalasa', 1, 1),
(6, 'Amalapuram', 1, 1),
(7, 'Anakapalle', 1, 1),
(8, 'Anantapur', 1, 1),
(9, 'Badepalle', 1, 1),
(10, 'Banganapalle', 1, 1),
(11, 'Bapatla', 1, 1),
(12, 'Bellampalle', 1, 1),
(13, 'Bethamcherla', 1, 1),
(14, 'Bhadrachalam', 1, 1),
(15, 'Bhainsa', 1, 1),
(16, 'Bheemunipatnam', 1, 1),
(17, 'Bhimavaram', 1, 1),
(18, 'Bhongir', 1, 1),
(19, 'Bobbili', 1, 1),
(20, 'Bodhan', 1, 1),
(21, 'Chilakaluripet', 1, 1),
(22, 'Chirala', 1, 1),
(23, 'Chittoor', 1, 1),
(24, 'Cuddapah', 1, 1),
(25, 'Devarakonda', 1, 1),
(26, 'Dharmavaram', 1, 1),
(27, 'Eluru', 1, 1),
(28, 'Farooqnagar', 1, 1),
(29, 'Gadwal', 1, 1),
(30, 'Gooty', 1, 1),
(31, 'Gudivada', 1, 1),
(32, 'Gudur', 1, 1),
(33, 'Guntakal', 1, 1),
(34, 'Guntur', 1, 1),
(35, 'Hanuman Junction', 1, 1),
(36, 'Hindupur', 1, 1),
(37, 'Hyderabad', 1, 1),
(38, 'Ichchapuram', 1, 1),
(39, 'Jaggaiahpet', 1, 1),
(40, 'Jagtial', 1, 1),
(41, 'Jammalamadugu', 1, 1),
(42, 'Jangaon', 1, 1),
(43, 'Kadapa', 1, 1),
(44, 'Kadiri', 1, 1),
(45, 'Kagaznagar', 1, 1),
(46, 'Kakinada', 1, 1),
(47, 'Kalyandurg', 1, 1),
(48, 'Kamareddy', 1, 1),
(49, 'Kandukur', 1, 1),
(50, 'Karimnagar', 1, 1),
(51, 'Kavali', 1, 1),
(52, 'Khammam', 1, 1),
(53, 'Koratla', 1, 1),
(54, 'Kothagudem', 1, 1),
(55, 'Kothapeta', 1, 1),
(56, 'Kovvur', 1, 1),
(57, 'Kurnool', 1, 1),
(58, 'Kyathampalle', 1, 1),
(59, 'Macherla', 1, 1),
(60, 'Machilipatnam', 1, 1),
(61, 'Madanapalle', 1, 1),
(62, 'Mahbubnagar', 1, 1),
(63, 'Mancherial', 1, 1),
(64, 'Mandamarri', 1, 1),
(65, 'Mandapeta', 1, 1),
(66, 'Manuguru', 1, 1),
(67, 'Markapur', 1, 1),
(68, 'Medak', 1, 1),
(69, 'Miryalaguda', 1, 1),
(70, 'Mogalthur', 1, 1),
(71, 'Nagari', 1, 1),
(72, 'Nagarkurnool', 1, 1),
(73, 'Nandyal', 1, 1),
(74, 'Narasapur', 1, 1),
(75, 'Narasaraopet', 1, 1),
(76, 'Narayanpet', 1, 1),
(77, 'Narsipatnam', 1, 1),
(78, 'Nellore', 1, 1),
(79, 'Nidadavole', 1, 1),
(80, 'Nirmal', 1, 1),
(81, 'Nizamabad', 1, 1),
(82, 'Nuzvid', 1, 1),
(83, 'Ongole', 1, 1),
(84, 'Palacole', 1, 1),
(85, 'Palasa Kasibugga', 1, 1),
(86, 'Palwancha', 1, 1),
(87, 'Parvathipuram', 1, 1),
(88, 'Pedana', 1, 1),
(89, 'Peddapuram', 1, 1),
(90, 'Pithapuram', 1, 1),
(91, 'Pondur', 1, 1),
(92, 'Ponnur', 1, 1),
(93, 'Proddatur', 1, 1),
(94, 'Punganur', 1, 1),
(95, 'Puttur', 1, 1),
(96, 'Rajahmundry', 1, 1),
(97, 'Rajam', 1, 1),
(98, 'Ramachandrapuram', 1, 1),
(99, 'Ramagundam', 1, 1),
(100, 'Rayachoti', 1, 1),
(101, 'Rayadurg', 1, 1),
(102, 'Renigunta', 1, 1),
(103, 'Repalle', 1, 1),
(104, 'Sadasivpet', 1, 1),
(105, 'Salur', 1, 1),
(106, 'Samalkot', 1, 1),
(107, 'Sangareddy', 1, 1),
(108, 'Sattenapalle', 1, 1),
(109, 'Siddipet', 1, 1),
(110, 'Singapur', 1, 1),
(111, 'Sircilla', 1, 1),
(112, 'Srikakulam', 1, 1),
(113, 'Srikalahasti', 1, 1),
(115, 'Suryapet', 1, 1),
(116, 'Tadepalligudem', 1, 1),
(117, 'Tadpatri', 1, 1),
(118, 'Tandur', 1, 1),
(119, 'Tanuku', 1, 1),
(120, 'Tenali', 1, 1),
(121, 'Tirupati', 1, 1),
(122, 'Tuni', 1, 1),
(123, 'Uravakonda', 1, 1),
(124, 'Venkatagiri', 1, 1),
(125, 'Vicarabad', 1, 1),
(126, 'Vijayawada', 1, 1),
(127, 'Vinukonda', 1, 1),
(128, 'Visakhapatnam', 1, 1),
(129, 'Vizianagaram', 1, 1),
(130, 'Wanaparthy', 1, 1),
(131, 'Warangal', 1, 1),
(132, 'Yellandu', 1, 1),
(133, 'Yemmiganur', 1, 1),
(134, 'Yerraguntla', 1, 1),
(135, 'Zahirabad', 1, 1),
(137, 'Along', 3, 1),
(138, 'Bomdila', 3, 1),
(139, 'Itanagar', 3, 1),
(140, 'Naharlagun', 3, 1),
(141, 'Pasighat', 3, 1),
(142, 'Abhayapuri', 2, 1),
(143, 'Amguri', 2, 1),
(144, 'Anandnagaar', 2, 1),
(145, 'Barpeta', 2, 1),
(146, 'Barpeta Road', 2, 1),
(147, 'Bilasipara', 2, 1),
(148, 'Bongaigaon', 2, 1),
(149, 'Dhekiajuli', 2, 1),
(150, 'Dhubri', 2, 1),
(151, 'Dibrugarh', 2, 1),
(152, 'Digboi', 2, 1),
(153, 'Diphu', 2, 1),
(154, 'Dispur', 2, 1),
(156, 'Gauripur', 2, 1),
(157, 'Goalpara', 2, 1),
(158, 'Golaghat', 2, 1),
(159, 'Guwahati', 2, 1),
(160, 'Haflong', 2, 1),
(161, 'Hailakandi', 2, 1),
(162, 'Hojai', 2, 1),
(163, 'Jorhat', 2, 1),
(164, 'Karimganj', 2, 1),
(165, 'Kokrajhar', 2, 1),
(166, 'Lanka', 2, 1),
(167, 'Lumding', 2, 1),
(168, 'Mangaldoi', 2, 1),
(169, 'Mankachar', 2, 1),
(170, 'Margherita', 2, 1),
(171, 'Mariani', 2, 1),
(172, 'Marigaon', 2, 1),
(173, 'Nagaon', 2, 1),
(174, 'Nalbari', 2, 1),
(175, 'North Lakhimpur', 2, 1),
(176, 'Rangia', 2, 1),
(177, 'Sibsagar', 2, 1),
(178, 'Silapathar', 2, 1),
(179, 'Silchar', 2, 1),
(180, 'Tezpur', 2, 1),
(181, 'Tinsukia', 2, 1),
(182, 'Amarpur', 5, 1),
(183, 'Araria', 5, 1),
(184, 'Areraj', 5, 1),
(185, 'Arrah', 5, 1),
(186, 'Asarganj', 5, 1),
(187, 'Aurangabad', 5, 1),
(188, 'Bagaha', 5, 1),
(189, 'Bahadurganj', 5, 1),
(190, 'Bairgania', 5, 1),
(191, 'Bakhtiarpur', 5, 1),
(192, 'Banka', 5, 1),
(193, 'Banmankhi Bazar', 5, 1),
(194, 'Barahiya', 5, 1),
(195, 'Barauli', 5, 1),
(196, 'Barbigha', 5, 1),
(197, 'Barh', 5, 1),
(198, 'Begusarai', 5, 1),
(199, 'Behea', 5, 1),
(200, 'Bettiah', 5, 1),
(201, 'Bhabua', 5, 1),
(202, 'Bhagalpur', 5, 1),
(203, 'Bihar Sharif', 5, 1),
(204, 'Bikramganj', 5, 1),
(205, 'Bodh Gaya', 5, 1),
(206, 'Buxar', 5, 1),
(207, 'Chandan Bara', 5, 1),
(208, 'Chanpatia', 5, 1),
(209, 'Chhapra', 5, 1),
(210, 'Colgong', 5, 1),
(211, 'Dalsinghsarai', 5, 1),
(212, 'Darbhanga', 5, 1),
(213, 'Daudnagar', 5, 1),
(214, 'Dehri-on-Sone', 5, 1),
(215, 'Dhaka', 5, 1),
(216, 'Dighwara', 5, 1),
(217, 'Dumraon', 5, 1),
(218, 'Fatwah', 5, 1),
(219, 'Forbesganj', 5, 1),
(220, 'Gaya', 5, 1),
(221, 'Gogri Jamalpur', 5, 1),
(222, 'Gopalganj', 5, 1),
(223, 'Hajipur', 5, 1),
(224, 'Hilsa', 5, 1),
(225, 'Hisua', 5, 1),
(226, 'Islampur', 5, 1),
(227, 'Jagdispur', 5, 1),
(228, 'Jamalpur', 5, 1),
(229, 'Jamui', 5, 1),
(230, 'Jehanabad', 5, 1),
(231, 'Jhajha', 5, 1),
(232, 'Jhanjharpur', 5, 1),
(233, 'Jogabani', 5, 1),
(234, 'Kanti', 5, 1),
(235, 'Katihar', 5, 1),
(236, 'Khagaria', 5, 1),
(237, 'Kharagpur', 5, 1),
(238, 'Kishanganj', 5, 1),
(239, 'Lakhisarai', 5, 1),
(240, 'Lalganj', 5, 1),
(241, 'Madhepura', 5, 1),
(242, 'Madhubani', 5, 1),
(243, 'Maharajganj', 5, 1),
(244, 'Mahnar Bazar', 5, 1),
(245, 'Makhdumpur', 5, 1),
(246, 'Maner', 5, 1),
(247, 'Manihari', 5, 1),
(248, 'Marhaura', 5, 1),
(249, 'Masaurhi', 5, 1),
(250, 'Mirganj', 5, 1),
(251, 'Mokameh', 5, 1),
(252, 'Motihari', 5, 1),
(253, 'Motipur', 5, 1),
(254, 'Munger', 5, 1),
(255, 'Murliganj', 5, 1),
(256, 'Muzaffarpur', 5, 1),
(257, 'Narkatiaganj', 5, 1),
(258, 'Naugachhia', 5, 1),
(259, 'Nawada', 5, 1),
(260, 'Nokha', 5, 1),
(261, 'Patna', 5, 1),
(262, 'Piro', 5, 1),
(263, 'Purnia', 5, 1),
(264, 'Rafiganj', 5, 1),
(265, 'Rajgir', 5, 1),
(266, 'Ramnagar', 5, 1),
(267, 'Raxaul Bazar', 5, 1),
(268, 'Revelganj', 5, 1),
(269, 'Rosera', 5, 1),
(270, 'Saharsa', 5, 1),
(271, 'Samastipur', 5, 1),
(272, 'Sasaram', 5, 1),
(273, 'Sheikhpura', 5, 1),
(274, 'Sheohar', 5, 1),
(275, 'Sherghati', 5, 1),
(276, 'Silao', 5, 1),
(277, 'Sitamarhi', 5, 1),
(278, 'Siwan', 5, 1),
(279, 'Sonepur', 5, 1),
(280, 'Sugauli', 5, 1),
(281, 'Sultanganj', 5, 1),
(282, 'Supaul', 5, 1),
(283, 'Warisaliganj', 5, 1),
(284, 'Ahiwara', 34, 1),
(285, 'Akaltara', 34, 1),
(286, 'Ambagarh Chowki', 34, 1),
(287, 'Ambikapur', 34, 1),
(288, 'Arang', 34, 1),
(289, 'Bade Bacheli', 34, 1),
(290, 'Balod', 34, 1),
(291, 'Baloda Bazar', 34, 1),
(292, 'Bemetra', 34, 1),
(293, 'Bhatapara', 34, 1),
(294, 'Bilaspur', 34, 1),
(295, 'Birgaon', 34, 1),
(296, 'Champa', 34, 1),
(297, 'Chirmiri', 34, 1),
(298, 'Dalli-Rajhara', 34, 1),
(299, 'Dhamtari', 34, 1),
(300, 'Dipka', 34, 1),
(301, 'Dongargarh', 34, 1),
(302, 'Durg-Bhilai Nagar', 34, 1),
(303, 'Gobranawapara', 34, 1),
(304, 'Jagdalpur', 34, 1),
(305, 'Janjgir', 34, 1),
(306, 'Jashpurnagar', 34, 1),
(307, 'Kanker', 34, 1),
(308, 'Kawardha', 34, 1),
(309, 'Kondagaon', 34, 1),
(310, 'Korba', 34, 1),
(311, 'Mahasamund', 34, 1),
(312, 'Mahendragarh', 34, 1),
(313, 'Mungeli', 34, 1),
(314, 'Naila Janjgir', 34, 1),
(315, 'Raigarh', 34, 1),
(316, 'Raipur', 34, 1),
(317, 'Rajnandgaon', 34, 1),
(318, 'Sakti', 34, 1),
(319, 'Tilda Newra', 34, 1),
(320, 'Amli', 29, 1),
(322, 'Daman and Diu', 28, 1),
(323, 'Daman and Diu', 28, 1),
(324, 'Asola', 35, 1),
(325, 'Delhi', 35, 1),
(326, 'Aldona', 25, 1),
(327, 'Curchorem Cacora', 25, 1),
(328, 'Madgaon', 25, 1),
(329, 'Mapusa', 25, 1),
(330, 'Margao', 25, 1),
(331, 'Marmagao', 25, 1),
(332, 'Panaji', 25, 1),
(333, 'Ahmedabad', 4, 1),
(334, 'Amreli', 4, 1),
(335, 'Anand', 4, 1),
(336, 'Ankleshwar', 4, 1),
(337, 'Bharuch', 4, 1),
(338, 'Bhavnagar', 4, 1),
(339, 'Bhuj', 4, 1),
(340, 'Cambay', 4, 1),
(341, 'Dahod', 4, 1),
(342, 'Deesa', 4, 1),
(344, 'Dholka', 4, 1),
(345, 'Gandhinagar', 4, 1),
(346, 'Godhra', 4, 1),
(347, 'Himatnagar', 4, 1),
(348, 'Idar', 4, 1),
(349, 'Jamnagar', 4, 1),
(350, 'Junagadh', 4, 1),
(351, 'Kadi', 4, 1),
(352, 'Kalavad', 4, 1),
(353, 'Kalol', 4, 1),
(354, 'Kapadvanj', 4, 1),
(355, 'Karjan', 4, 1),
(356, 'Keshod', 4, 1),
(357, 'Khambhalia', 4, 1),
(358, 'Khambhat', 4, 1),
(359, 'Kheda', 4, 1),
(360, 'Khedbrahma', 4, 1),
(361, 'Kheralu', 4, 1),
(362, 'Kodinar', 4, 1),
(363, 'Lathi', 4, 1),
(364, 'Limbdi', 4, 1),
(365, 'Lunawada', 4, 1),
(366, 'Mahesana', 4, 1),
(367, 'Mahuva', 4, 1),
(368, 'Manavadar', 4, 1),
(369, 'Mandvi', 4, 1),
(370, 'Mangrol', 4, 1),
(371, 'Mansa', 4, 1),
(372, 'Mehmedabad', 4, 1),
(373, 'Modasa', 4, 1),
(374, 'Morvi', 4, 1),
(375, 'Nadiad', 4, 1),
(376, 'Navsari', 4, 1),
(377, 'Padra', 4, 1),
(378, 'Palanpur', 4, 1),
(379, 'Palitana', 4, 1),
(380, 'Pardi', 4, 1),
(381, 'Patan', 4, 1),
(382, 'Petlad', 4, 1),
(383, 'Porbandar', 4, 1),
(384, 'Radhanpur', 4, 1),
(385, 'Rajkot', 4, 1),
(386, 'Rajpipla', 4, 1),
(387, 'Rajula', 4, 1),
(388, 'Ranavav', 4, 1),
(389, 'Rapar', 4, 1),
(390, 'Salaya', 4, 1),
(391, 'Sanand', 4, 1),
(392, 'Savarkundla', 4, 1),
(393, 'Sidhpur', 4, 1),
(394, 'Sihor', 4, 1),
(395, 'Songadh', 4, 1),
(396, 'Surat', 4, 1),
(397, 'Talaja', 4, 1),
(398, 'Thangadh', 4, 1),
(399, 'Tharad', 4, 1),
(400, 'Umbergaon', 4, 1),
(401, 'Umreth', 4, 1),
(402, 'Una', 4, 1),
(403, 'Unjha', 4, 1),
(404, 'Upleta', 4, 1),
(405, 'Vadnagar', 4, 1),
(406, 'Vadodara', 4, 1),
(407, 'Valsad', 4, 1),
(408, 'Vapi', 4, 1),
(409, 'Vapi', 4, 1),
(410, 'Veraval', 4, 1),
(411, 'Vijapur', 4, 1),
(412, 'Viramgam', 4, 1),
(413, 'Visnagar', 4, 1),
(414, 'Vyara', 4, 1),
(415, 'Wadhwan', 4, 1),
(416, 'Wankaner', 4, 1),
(429, 'Ambala', 6, 1),
(430, 'Ambala', 6, 1),
(431, 'Asankhurd', 6, 1),
(432, 'Assandh', 6, 1),
(433, 'Ateli', 6, 1),
(434, 'Babiyal', 6, 1),
(435, 'Bahadurgarh', 6, 1),
(436, 'Barwala', 6, 1),
(437, 'Bhiwani', 6, 1),
(438, 'Charkhi Dadri', 6, 1),
(439, 'Cheeka', 6, 1),
(440, 'Ellenabad 2', 6, 1),
(441, 'Faridabad', 6, 1),
(442, 'Fatehabad', 6, 1),
(443, 'Ganaur', 6, 1),
(444, 'Gharaunda', 6, 1),
(445, 'Gohana', 6, 1),
(446, 'Gurgaon', 6, 1),
(447, 'Haibat(Yamuna Nagar)', 6, 1),
(448, 'Hansi', 6, 1),
(449, 'Hisar', 6, 1),
(450, 'Hodal', 6, 1),
(451, 'Jhajjar', 6, 1),
(452, 'Jind', 6, 1),
(453, 'Kaithal', 6, 1),
(454, 'Kalan Wali', 6, 1),
(455, 'Kalka', 6, 1),
(456, 'Karnal', 6, 1),
(457, 'Ladwa', 6, 1),
(458, 'Mahendragarh', 6, 1),
(459, 'Mandi Dabwali', 6, 1),
(460, 'Narnaul', 6, 1),
(461, 'Narwana', 6, 1),
(462, 'Palwal', 6, 1),
(463, 'Panchkula', 6, 1),
(464, 'Panipat', 6, 1),
(465, 'Pehowa', 6, 1),
(466, 'Pinjore', 6, 1),
(467, 'Rania', 6, 1),
(468, 'Ratia', 6, 1),
(469, 'Rewari', 6, 1),
(470, 'Rohtak', 6, 1),
(471, 'Safidon', 6, 1),
(472, 'Samalkha', 6, 1),
(473, 'Shahbad', 6, 1),
(474, 'Sirsa', 6, 1),
(475, 'Sohna', 6, 1),
(476, 'Sonipat', 6, 1),
(477, 'Taraori', 6, 1),
(478, 'Thanesar', 6, 1),
(479, 'Tohana', 6, 1),
(480, 'Yamunanagar', 6, 1),
(481, 'Arki', 7, 1),
(482, 'Baddi', 7, 1),
(483, 'Bilaspur', 7, 1),
(484, 'Chamba', 7, 1),
(485, 'Dalhousie', 7, 1),
(486, 'Dharamsala', 7, 1),
(487, 'Hamirpur', 7, 1),
(488, 'Mandi', 7, 1),
(489, 'Nahan', 7, 1),
(490, 'Shimla', 7, 1),
(491, 'Solan', 7, 1),
(492, 'Sundarnagar', 7, 1),
(493, 'Jammu', 8, 1),
(494, 'Achabbal', 8, 1),
(495, 'Akhnoor', 8, 1),
(496, 'Anantnag', 8, 1),
(497, 'Arnia', 8, 1),
(498, 'Awantipora', 8, 1),
(499, 'Bandipore', 8, 1),
(500, 'Baramula', 8, 1),
(501, 'Kathua', 8, 1),
(502, 'Leh', 8, 1),
(503, 'Punch', 8, 1),
(504, 'Rajauri', 8, 1),
(505, 'Sopore', 8, 1),
(506, 'Srinagar', 8, 1),
(507, 'Udhampur', 8, 1),
(508, 'Amlabad', 33, 1),
(509, 'Ara', 33, 1),
(510, 'Barughutu', 33, 1),
(511, 'Bokaro Steel City', 33, 1),
(512, 'Chaibasa', 33, 1),
(513, 'Chakradharpur', 33, 1),
(514, 'Chandrapura', 33, 1),
(515, 'Chatra', 33, 1),
(516, 'Chirkunda', 33, 1),
(517, 'Churi', 33, 1),
(518, 'Daltonganj', 33, 1),
(519, 'Deoghar', 33, 1),
(520, 'Dhanbad', 33, 1),
(521, 'Dumka', 33, 1),
(522, 'Garhwa', 33, 1),
(523, 'Ghatshila', 33, 1),
(524, 'Giridih', 33, 1),
(525, 'Godda', 33, 1),
(526, 'Gomoh', 33, 1),
(527, 'Gumia', 33, 1),
(528, 'Gumla', 33, 1),
(529, 'Hazaribag', 33, 1),
(530, 'Hussainabad', 33, 1),
(531, 'Jamshedpur', 33, 1),
(532, 'Jamtara', 33, 1),
(533, 'Jhumri Tilaiya', 33, 1),
(534, 'Khunti', 33, 1),
(535, 'Lohardaga', 33, 1),
(536, 'Madhupur', 33, 1),
(537, 'Mihijam', 33, 1),
(538, 'Musabani', 33, 1),
(539, 'Pakaur', 33, 1),
(540, 'Patratu', 33, 1),
(541, 'Phusro', 33, 1),
(542, 'Ramngarh', 33, 1),
(543, 'Ranchi', 33, 1),
(544, 'Sahibganj', 33, 1),
(545, 'Saunda', 33, 1),
(546, 'Simdega', 33, 1),
(547, 'Tenu Dam-cum- Kathhara', 33, 1),
(548, 'Arasikere', 9, 1),
(549, 'Bangalore', 9, 1),
(550, 'Belgaum', 9, 1),
(551, 'Bellary', 9, 1),
(552, 'Chamrajnagar', 9, 1),
(553, 'Chikkaballapur', 9, 1),
(554, 'Chintamani', 9, 1),
(555, 'Chitradurga', 9, 1),
(556, 'Gulbarga', 9, 1),
(557, 'Gundlupet', 9, 1),
(558, 'Hassan', 9, 1),
(559, 'Hospet', 9, 1),
(560, 'Hubli', 9, 1),
(561, 'Karkala', 9, 1),
(562, 'Karwar', 9, 1),
(563, 'Kolar', 9, 1),
(564, 'Kota', 9, 1),
(565, 'Lakshmeshwar', 9, 1),
(566, 'Lingsugur', 9, 1),
(567, 'Maddur', 9, 1),
(568, 'Madhugiri', 9, 1),
(569, 'Madikeri', 9, 1),
(570, 'Magadi', 9, 1),
(571, 'Mahalingpur', 9, 1),
(572, 'Malavalli', 9, 1),
(573, 'Malur', 9, 1),
(574, 'Mandya', 9, 1),
(575, 'Mangalore', 9, 1),
(576, 'Manvi', 9, 1),
(577, 'Mudalgi', 9, 1),
(578, 'Mudbidri', 9, 1),
(579, 'Muddebihal', 9, 1),
(580, 'Mudhol', 9, 1),
(581, 'Mulbagal', 9, 1),
(582, 'Mundargi', 9, 1),
(583, 'Mysore', 9, 1),
(584, 'Nanjangud', 9, 1),
(585, 'Pavagada', 9, 1),
(586, 'Puttur', 9, 1),
(587, 'Rabkavi Banhatti', 9, 1),
(588, 'Raichur', 9, 1),
(589, 'Ramanagaram', 9, 1),
(590, 'Ramdurg', 9, 1),
(591, 'Ranibennur', 9, 1),
(592, 'Robertson Pet', 9, 1),
(593, 'Ron', 9, 1),
(594, 'Sadalgi', 9, 1),
(595, 'Sagar', 9, 1),
(596, 'Sakleshpur', 9, 1),
(597, 'Sandur', 9, 1),
(598, 'Sankeshwar', 9, 1),
(599, 'Saundatti-Yellamma', 9, 1),
(600, 'Savanur', 9, 1),
(601, 'Sedam', 9, 1),
(602, 'Shahabad', 9, 1),
(603, 'Shahpur', 9, 1),
(604, 'Shiggaon', 9, 1),
(605, 'Shikapur', 9, 1),
(606, 'Shimoga', 9, 1),
(607, 'Shorapur', 9, 1),
(608, 'Shrirangapattana', 9, 1),
(609, 'Sidlaghatta', 9, 1),
(610, 'Sindgi', 9, 1),
(611, 'Sindhnur', 9, 1),
(612, 'Sira', 9, 1),
(613, 'Sirsi', 9, 1),
(614, 'Siruguppa', 9, 1),
(615, 'Srinivaspur', 9, 1),
(616, 'Talikota', 9, 1),
(617, 'Tarikere', 9, 1),
(618, 'Tekkalakota', 9, 1),
(619, 'Terdal', 9, 1),
(620, 'Tiptur', 9, 1),
(621, 'Tumkur', 9, 1),
(622, 'Udupi', 9, 1),
(623, 'Vijayapura', 9, 1),
(624, 'Wadi', 9, 1),
(625, 'Yadgir', 9, 1),
(626, 'Adoor', 10, 1),
(627, 'Akathiyoor', 10, 1),
(628, 'Alappuzha', 10, 1),
(629, 'Ancharakandy', 10, 1),
(630, 'Aroor', 10, 1),
(631, 'Ashtamichira', 10, 1),
(632, 'Attingal', 10, 1),
(633, 'Avinissery', 10, 1),
(634, 'Chalakudy', 10, 1),
(635, 'Changanassery', 10, 1),
(636, 'Chendamangalam', 10, 1),
(637, 'Chengannur', 10, 1),
(638, 'Cherthala', 10, 1),
(639, 'Cheruthazham', 10, 1),
(640, 'Chittur-Thathamangalam', 10, 1),
(641, 'Chockli', 10, 1),
(642, 'Erattupetta', 10, 1),
(643, 'Guruvayoor', 10, 1),
(644, 'Irinjalakuda', 10, 1),
(645, 'Kadirur', 10, 1),
(646, 'Kalliasseri', 10, 1),
(647, 'Kalpetta', 10, 1),
(648, 'Kanhangad', 10, 1),
(649, 'Kanjikkuzhi', 10, 1),
(650, 'Kannur', 10, 1),
(651, 'Kasaragod', 10, 1),
(652, 'Kayamkulam', 10, 1),
(653, 'Kochi', 10, 1),
(654, 'Kodungallur', 10, 1),
(655, 'Kollam', 10, 1),
(656, 'Koothuparamba', 10, 1),
(657, 'Kothamangalam', 10, 1),
(658, 'Kottayam', 10, 1),
(659, 'Kozhikode', 10, 1),
(660, 'Kunnamkulam', 10, 1),
(661, 'Malappuram', 10, 1),
(662, 'Mattannur', 10, 1),
(663, 'Mavelikkara', 10, 1),
(664, 'Mavoor', 10, 1),
(665, 'Muvattupuzha', 10, 1),
(666, 'Nedumangad', 10, 1),
(667, 'Neyyattinkara', 10, 1),
(668, 'Ottappalam', 10, 1),
(669, 'Palai', 10, 1),
(670, 'Palakkad', 10, 1),
(671, 'Panniyannur', 10, 1),
(672, 'Pappinisseri', 10, 1),
(673, 'Paravoor', 10, 1),
(674, 'Pathanamthitta', 10, 1),
(675, 'Payyannur', 10, 1),
(676, 'Peringathur', 10, 1),
(677, 'Perinthalmanna', 10, 1),
(678, 'Perumbavoor', 10, 1),
(679, 'Ponnani', 10, 1),
(680, 'Punalur', 10, 1),
(681, 'Quilandy', 10, 1),
(682, 'Shoranur', 10, 1),
(683, 'Taliparamba', 10, 1),
(684, 'Thiruvalla', 10, 1),
(685, 'Thiruvananthapuram', 10, 1),
(686, 'Thodupuzha', 10, 1),
(687, 'Thrissur', 10, 1),
(688, 'Tirur', 10, 1),
(689, 'Vadakara', 10, 1),
(690, 'Vaikom', 10, 1),
(691, 'Varkala', 10, 1),
(692, 'Kavaratti', 27, 1),
(693, 'Ashok Nagar', 11, 1),
(694, 'Balaghat', 11, 1),
(695, 'Betul', 11, 1),
(696, 'Bhopal', 11, 1),
(697, 'Burhanpur', 11, 1),
(698, 'Chhatarpur', 11, 1),
(699, 'Dabra', 11, 1),
(700, 'Datia', 11, 1),
(701, 'Dewas', 11, 1),
(702, 'Dhar', 11, 1),
(703, 'Fatehabad', 11, 1),
(704, 'Gwalior', 11, 1),
(705, 'Indore', 11, 1),
(706, 'Itarsi', 11, 1),
(707, 'Jabalpur', 11, 1),
(708, 'Katni', 11, 1),
(709, 'Kotma', 11, 1),
(710, 'Lahar', 11, 1),
(711, 'Lundi', 11, 1),
(712, 'Maharajpur', 11, 1),
(713, 'Mahidpur', 11, 1),
(714, 'Maihar', 11, 1),
(715, 'Malajkhand', 11, 1),
(716, 'Manasa', 11, 1),
(717, 'Manawar', 11, 1),
(718, 'Mandideep', 11, 1),
(719, 'Mandla', 11, 1),
(720, 'Mandsaur', 11, 1),
(721, 'Mauganj', 11, 1),
(722, 'Mhow Cantonment', 11, 1),
(723, 'Mhowgaon', 11, 1),
(724, 'Morena', 11, 1),
(725, 'Multai', 11, 1),
(726, 'Murwara', 11, 1),
(727, 'Nagda', 11, 1),
(728, 'Nainpur', 11, 1),
(729, 'Narsinghgarh', 11, 1),
(730, 'Narsinghgarh', 11, 1),
(731, 'Neemuch', 11, 1),
(732, 'Nepanagar', 11, 1),
(733, 'Niwari', 11, 1),
(734, 'Nowgong', 11, 1),
(735, 'Nowrozabad', 11, 1),
(736, 'Pachore', 11, 1),
(737, 'Pali', 11, 1),
(738, 'Panagar', 11, 1),
(739, 'Pandhurna', 11, 1),
(740, 'Panna', 11, 1),
(741, 'Pasan', 11, 1),
(742, 'Pipariya', 11, 1),
(743, 'Pithampur', 11, 1),
(744, 'Porsa', 11, 1),
(745, 'Prithvipur', 11, 1),
(746, 'Raghogarh-Vijaypur', 11, 1),
(747, 'Rahatgarh', 11, 1),
(748, 'Raisen', 11, 1),
(749, 'Rajgarh', 11, 1),
(750, 'Ratlam', 11, 1),
(751, 'Rau', 11, 1),
(752, 'Rehli', 11, 1),
(753, 'Rewa', 11, 1),
(754, 'Sabalgarh', 11, 1),
(755, 'Sagar', 11, 1),
(756, 'Sanawad', 11, 1),
(757, 'Sarangpur', 11, 1),
(758, 'Sarni', 11, 1),
(759, 'Satna', 11, 1),
(760, 'Sausar', 11, 1),
(761, 'Sehore', 11, 1),
(762, 'Sendhwa', 11, 1),
(763, 'Seoni', 11, 1),
(764, 'Seoni-Malwa', 11, 1),
(765, 'Shahdol', 11, 1),
(766, 'Shajapur', 11, 1),
(767, 'Shamgarh', 11, 1),
(768, 'Sheopur', 11, 1),
(769, 'Shivpuri', 11, 1),
(770, 'Shujalpur', 11, 1),
(771, 'Sidhi', 11, 1),
(772, 'Sihora', 11, 1),
(773, 'Singrauli', 11, 1),
(774, 'Sironj', 11, 1),
(775, 'Sohagpur', 11, 1),
(776, 'Tarana', 11, 1),
(777, 'Tikamgarh', 11, 1),
(778, 'Ujhani', 11, 1),
(779, 'Ujjain', 11, 1),
(780, 'Umaria', 11, 1),
(781, 'Vidisha', 11, 1),
(782, 'Wara Seoni', 11, 1),
(895, 'Uchgaon', 12, 1),
(896, 'Udgir', 12, 1),
(897, 'Umarga', 12, 1),
(898, 'Umarkhed', 12, 1),
(899, 'Umred', 12, 1),
(900, 'Vadgaon Kasba', 12, 1),
(901, 'Vaijapur', 12, 1),
(902, 'Vasai', 12, 1),
(903, 'Virar', 12, 1),
(904, 'Vita', 12, 1),
(905, 'Yavatmal', 12, 1),
(906, 'Yawal', 12, 1),
(907, 'Imphal', 13, 1),
(908, 'Kakching', 13, 1),
(909, 'Lilong', 13, 1),
(910, 'Mayang Imphal', 13, 1),
(911, 'Thoubal', 13, 1),
(912, 'Jowai', 14, 1),
(913, 'Nongstoin', 14, 1),
(914, 'Shillong', 14, 1),
(915, 'Tura', 14, 1),
(916, 'Aizawl', 15, 1),
(917, 'Champhai', 15, 1),
(918, 'Lunglei', 15, 1),
(919, 'Saiha', 15, 1),
(920, 'Dimapur', 16, 1),
(921, 'Kohima', 16, 1),
(922, 'Mokokchung', 16, 1),
(923, 'Tuensang', 16, 1),
(924, 'Wokha', 16, 1),
(925, 'Zunheboto', 16, 1),
(950, 'Anandapur', 17, 1),
(951, 'Anugul', 17, 1),
(952, 'Asika', 17, 1),
(953, 'Balangir', 17, 1),
(954, 'Balasore', 17, 1),
(955, 'Baleshwar', 17, 1),
(956, 'Bamra', 17, 1),
(957, 'Barbil', 17, 1),
(958, 'Bargarh', 17, 1),
(959, 'Bargarh', 17, 1),
(960, 'Baripada', 17, 1),
(961, 'Basudebpur', 17, 1),
(962, 'Belpahar', 17, 1),
(963, 'Bhadrak', 17, 1),
(964, 'Bhawanipatna', 17, 1),
(965, 'Bhuban', 17, 1),
(966, 'Bhubaneswar', 17, 1),
(967, 'Biramitrapur', 17, 1),
(968, 'Brahmapur', 17, 1),
(969, 'Brajrajnagar', 17, 1),
(970, 'Byasanagar', 17, 1),
(971, 'Cuttack', 17, 1),
(972, 'Debagarh', 17, 1),
(973, 'Dhenkanal', 17, 1),
(974, 'Gunupur', 17, 1),
(975, 'Hinjilicut', 17, 1),
(976, 'Jagatsinghapur', 17, 1),
(977, 'Jajapur', 17, 1),
(978, 'Jaleswar', 17, 1),
(979, 'Jatani', 17, 1),
(980, 'Jeypur', 17, 1),
(981, 'Jharsuguda', 17, 1),
(982, 'Joda', 17, 1),
(983, 'Kantabanji', 17, 1),
(984, 'Karanjia', 17, 1),
(985, 'Kendrapara', 17, 1),
(986, 'Kendujhar', 17, 1),
(987, 'Khordha', 17, 1),
(988, 'Koraput', 17, 1),
(989, 'Malkangiri', 17, 1),
(990, 'Nabarangapur', 17, 1),
(991, 'Paradip', 17, 1),
(992, 'Parlakhemundi', 17, 1),
(993, 'Pattamundai', 17, 1),
(994, 'Phulabani', 17, 1),
(995, 'Puri', 17, 1),
(996, 'Rairangpur', 17, 1),
(997, 'Rajagangapur', 17, 1),
(998, 'Raurkela', 17, 1),
(999, 'Rayagada', 17, 1),
(1000, 'Sambalpur', 17, 1),
(1001, 'Soro', 17, 1),
(1002, 'Sunabeda', 17, 1),
(1003, 'Sundargarh', 17, 1),
(1004, 'Talcher', 17, 1),
(1005, 'Titlagarh', 17, 1),
(1006, 'Umarkote', 17, 1),
(1011, 'Ahmedgarh', 18, 1),
(1012, 'Amritsar', 18, 1),
(1013, 'Barnala', 18, 1),
(1014, 'Batala', 18, 1),
(1015, 'Bathinda', 18, 1),
(1016, 'Bhagha Purana', 18, 1),
(1017, 'Budhlada', 18, 1),
(1018, 'Chandigarh', 18, 1),
(1019, 'Dasua', 18, 1),
(1020, 'Dhuri', 18, 1),
(1021, 'Dinanagar', 18, 1),
(1022, 'Faridkot', 18, 1),
(1023, 'Fazilka', 18, 1),
(1024, 'Firozpur', 18, 1),
(1025, 'Firozpur Cantt.', 18, 1),
(1026, 'Giddarbaha', 18, 1),
(1027, 'Gobindgarh', 18, 1),
(1028, 'Gurdaspur', 18, 1),
(1029, 'Hoshiarpur', 18, 1),
(1030, 'Jagraon', 18, 1),
(1031, 'Jaitu', 18, 1),
(1032, 'Jalalabad', 18, 1),
(1033, 'Jalandhar', 18, 1),
(1034, 'Jalandhar Cantt.', 18, 1),
(1035, 'Jandiala', 18, 1),
(1036, 'Kapurthala', 18, 1),
(1037, 'Karoran', 18, 1),
(1038, 'Kartarpur', 18, 1),
(1039, 'Khanna', 18, 1),
(1040, 'Kharar', 18, 1),
(1041, 'Kot Kapura', 18, 1),
(1042, 'Kurali', 18, 1),
(1043, 'Longowal', 18, 1),
(1044, 'Ludhiana', 18, 1),
(1045, 'Malerkotla', 18, 1),
(1046, 'Malout', 18, 1),
(1047, 'Mansa', 18, 1),
(1048, 'Maur', 18, 1),
(1049, 'Moga', 18, 1),
(1050, 'Mohali', 18, 1),
(1051, 'Morinda', 18, 1),
(1052, 'Mukerian', 18, 1),
(1053, 'Muktsar', 18, 1),
(1054, 'Nabha', 18, 1),
(1055, 'Nakodar', 18, 1),
(1056, 'Nangal', 18, 1),
(1057, 'Nawanshahr', 18, 1),
(1058, 'Pathankot', 18, 1),
(1059, 'Patiala', 18, 1),
(1060, 'Patran', 18, 1),
(1061, 'Patti', 18, 1),
(1062, 'Phagwara', 18, 1),
(1063, 'Phillaur', 18, 1),
(1064, 'Qadian', 18, 1),
(1065, 'Raikot', 18, 1),
(1066, 'Rajpura', 18, 1),
(1067, 'Rampura Phul', 18, 1),
(1068, 'Rupnagar', 18, 1),
(1069, 'Samana', 18, 1),
(1070, 'Sangrur', 18, 1),
(1071, 'Sirhind Fatehgarh Sahib', 18, 1),
(1072, 'Sujanpur', 18, 1),
(1073, 'Sunam', 18, 1),
(1074, 'Talwara', 18, 1),
(1075, 'Tarn Taran', 18, 1),
(1076, 'Urmar Tanda', 18, 1),
(1077, 'Zira', 18, 1),
(1078, 'Zirakpur', 18, 1),
(1081, 'Ajmer', 19, 1),
(1082, 'Alwar', 19, 1),
(1083, 'Bandikui', 19, 1),
(1084, 'Baran', 19, 1),
(1085, 'Barmer', 19, 1),
(1086, 'Bikaner', 19, 1),
(1087, 'Fatehpur', 19, 1),
(1088, 'Jaipur', 19, 1),
(1089, 'Jaisalmer', 19, 1),
(1090, 'Jodhpur', 19, 1),
(1091, 'Kota', 19, 1),
(1092, 'Lachhmangarh', 19, 1),
(1093, 'Ladnu', 19, 1),
(1094, 'Lakheri', 19, 1),
(1095, 'Lalsot', 19, 1),
(1096, 'Losal', 19, 1),
(1097, 'Makrana', 19, 1),
(1098, 'Malpura', 19, 1),
(1099, 'Mandalgarh', 19, 1),
(1100, 'Mandawa', 19, 1),
(1101, 'Mangrol', 19, 1),
(1102, 'Merta City', 19, 1),
(1103, 'Mount Abu', 19, 1),
(1104, 'Nadbai', 19, 1),
(1105, 'Nagar', 19, 1),
(1106, 'Nagaur', 19, 1),
(1107, 'Nargund', 19, 1),
(1108, 'Nasirabad', 19, 1),
(1109, 'Nathdwara', 19, 1),
(1110, 'Navalgund', 19, 1),
(1111, 'Nawalgarh', 19, 1),
(1112, 'Neem-Ka-Thana', 19, 1),
(1113, 'Nelamangala', 19, 1),
(1114, 'Nimbahera', 19, 1),
(1115, 'Nipani', 19, 1),
(1116, 'Niwai', 19, 1),
(1117, 'Nohar', 19, 1),
(1118, 'Nokha', 19, 1),
(1119, 'Pali', 19, 1),
(1120, 'Phalodi', 19, 1),
(1121, 'Phulera', 19, 1),
(1122, 'Pilani', 19, 1),
(1123, 'Pilibanga', 19, 1),
(1124, 'Pindwara', 19, 1),
(1125, 'Pipar City', 19, 1),
(1126, 'Prantij', 19, 1),
(1127, 'Pratapgarh', 19, 1),
(1128, 'Raisinghnagar', 19, 1),
(1129, 'Rajakhera', 19, 1),
(1130, 'Rajaldesar', 19, 1),
(1131, 'Rajgarh (Alwar)', 19, 1),
(1132, 'Rajgarh (Churu', 19, 1),
(1133, 'Rajsamand', 19, 1),
(1134, 'Ramganj Mandi', 19, 1),
(1135, 'Ramngarh', 19, 1),
(1136, 'Ratangarh', 19, 1),
(1137, 'Rawatbhata', 19, 1),
(1138, 'Rawatsar', 19, 1),
(1139, 'Reengus', 19, 1),
(1140, 'Sadri', 19, 1),
(1141, 'Sadulshahar', 19, 1),
(1142, 'Sagwara', 19, 1),
(1143, 'Sambhar', 19, 1),
(1144, 'Sanchore', 19, 1),
(1145, 'Sangaria', 19, 1),
(1146, 'Sardarshahar', 19, 1),
(1147, 'Sawai Madhopur', 19, 1),
(1148, 'Shahpura', 19, 1),
(1149, 'Shahpura', 19, 1),
(1150, 'Sheoganj', 19, 1),
(1151, 'Sikar', 19, 1),
(1152, 'Sirohi', 19, 1),
(1153, 'Sojat', 19, 1),
(1154, 'Sri Madhopur', 19, 1),
(1155, 'Sujangarh', 19, 1),
(1156, 'Sumerpur', 19, 1),
(1157, 'Suratgarh', 19, 1),
(1158, 'Taranagar', 19, 1),
(1159, 'Todabhim', 19, 1),
(1160, 'Todaraisingh', 19, 1),
(1161, 'Tonk', 19, 1),
(1162, 'Udaipur', 19, 1),
(1163, 'Udaipurwati', 19, 1),
(1164, 'Vijainagar', 19, 1),
(1165, 'Gangtok', 20, 1),
(1166, 'Calcutta', 24, 1),
(1167, 'Arakkonam', 21, 1),
(1168, 'Arcot', 21, 1),
(1169, 'Aruppukkottai', 21, 1),
(1170, 'Bhavani', 21, 1),
(1171, 'Chengalpattu', 21, 1),
(1172, 'Chennai', 21, 1),
(1173, 'Chinna salem', 21, 1),
(1174, 'Coimbatore', 21, 1),
(1175, 'Coonoor', 21, 1),
(1176, 'Cuddalore', 21, 1),
(1177, 'Dharmapuri', 21, 1),
(1178, 'Dindigul', 21, 1),
(1179, 'Erode', 21, 1),
(1180, 'Gudalur', 21, 1),
(1181, 'Gudalur', 21, 1),
(1182, 'Gudalur', 21, 1),
(1183, 'Kanchipuram', 21, 1),
(1184, 'Karaikudi', 21, 1),
(1185, 'Karungal', 21, 1),
(1186, 'Karur', 21, 1),
(1187, 'Kollankodu', 21, 1),
(1188, 'Lalgudi', 21, 1),
(1189, 'Madurai', 21, 1),
(1190, 'Nagapattinam', 21, 1),
(1191, 'Nagercoil', 21, 1),
(1192, 'Namagiripettai', 21, 1),
(1193, 'Namakkal', 21, 1),
(1194, 'Nandivaram-Guduvancheri', 21, 1),
(1195, 'Nanjikottai', 21, 1),
(1196, 'Natham', 21, 1),
(1197, 'Nellikuppam', 21, 1),
(1198, 'Neyveli', 21, 1),
(1199, 'O\' Valley', 21, 1),
(1200, 'Oddanchatram', 21, 1),
(1201, 'P.N.Patti', 21, 1),
(1202, 'Pacode', 21, 1),
(1203, 'Padmanabhapuram', 21, 1),
(1204, 'Palani', 21, 1),
(1205, 'Palladam', 21, 1),
(1206, 'Pallapatti', 21, 1),
(1207, 'Pallikonda', 21, 1),
(1208, 'Panagudi', 21, 1),
(1209, 'Panruti', 21, 1),
(1210, 'Paramakudi', 21, 1),
(1211, 'Parangipettai', 21, 1),
(1212, 'Pattukkottai', 21, 1),
(1213, 'Perambalur', 21, 1),
(1214, 'Peravurani', 21, 1),
(1215, 'Periyakulam', 21, 1),
(1216, 'Periyasemur', 21, 1),
(1217, 'Pernampattu', 21, 1),
(1218, 'Pollachi', 21, 1),
(1219, 'Polur', 21, 1),
(1220, 'Ponneri', 21, 1),
(1221, 'Pudukkottai', 21, 1),
(1222, 'Pudupattinam', 21, 1),
(1223, 'Puliyankudi', 21, 1),
(1224, 'Punjaipugalur', 21, 1),
(1225, 'Rajapalayam', 21, 1),
(1226, 'Ramanathapuram', 21, 1),
(1227, 'Rameshwaram', 21, 1),
(1228, 'Rasipuram', 21, 1),
(1229, 'Salem', 21, 1),
(1230, 'Sankarankoil', 21, 1),
(1231, 'Sankari', 21, 1),
(1232, 'Sathyamangalam', 21, 1),
(1233, 'Sattur', 21, 1),
(1234, 'Shenkottai', 21, 1),
(1235, 'Sholavandan', 21, 1),
(1236, 'Sholingur', 21, 1),
(1237, 'Sirkali', 21, 1),
(1238, 'Sivaganga', 21, 1),
(1239, 'Sivagiri', 21, 1),
(1240, 'Sivakasi', 21, 1),
(1241, 'Srivilliputhur', 21, 1),
(1242, 'Surandai', 21, 1),
(1243, 'Suriyampalayam', 21, 1),
(1244, 'Tenkasi', 21, 1),
(1245, 'Thammampatti', 21, 1),
(1246, 'Thanjavur', 21, 1),
(1247, 'Tharamangalam', 21, 1),
(1248, 'Tharangambadi', 21, 1),
(1249, 'Theni Allinagaram', 21, 1),
(1250, 'Thirumangalam', 21, 1),
(1251, 'Thirunindravur', 21, 1),
(1252, 'Thiruparappu', 21, 1),
(1253, 'Thirupuvanam', 21, 1),
(1254, 'Thiruthuraipoondi', 21, 1),
(1255, 'Thiruvallur', 21, 1),
(1256, 'Thiruvarur', 21, 1),
(1257, 'Thoothukudi', 21, 1),
(1258, 'Thuraiyur', 21, 1),
(1259, 'Tindivanam', 21, 1),
(1260, 'Tiruchendur', 21, 1),
(1261, 'Tiruchengode', 21, 1),
(1262, 'Tiruchirappalli', 21, 1),
(1263, 'Tirukalukundram', 21, 1),
(1264, 'Tirukkoyilur', 21, 1),
(1265, 'Tirunelveli', 21, 1),
(1266, 'Tirupathur', 21, 1),
(1267, 'Tirupathur', 21, 1),
(1268, 'Tiruppur', 21, 1),
(1269, 'Tiruttani', 21, 1),
(1270, 'Tiruvannamalai', 21, 1),
(1271, 'Tiruvethipuram', 21, 1),
(1272, 'Tittakudi', 21, 1),
(1273, 'Udhagamandalam', 21, 1),
(1274, 'Udumalaipettai', 21, 1),
(1275, 'Unnamalaikadai', 21, 1),
(1276, 'Usilampatti', 21, 1),
(1277, 'Uthamapalayam', 21, 1),
(1278, 'Uthiramerur', 21, 1),
(1279, 'Vadakkuvalliyur', 21, 1),
(1280, 'Vadalur', 21, 1),
(1281, 'Vadipatti', 21, 1),
(1282, 'Valparai', 21, 1),
(1283, 'Vandavasi', 21, 1),
(1284, 'Vaniyambadi', 21, 1),
(1285, 'Vedaranyam', 21, 1),
(1286, 'Vellakoil', 21, 1),
(1287, 'Vellore', 21, 1),
(1288, 'Vikramasingapuram', 21, 1),
(1289, 'Viluppuram', 21, 1),
(1290, 'Virudhachalam', 21, 1),
(1291, 'Virudhunagar', 21, 1),
(1292, 'Viswanatham', 21, 1),
(1293, 'Agartala', 22, 1),
(1294, 'Badharghat', 22, 1),
(1295, 'Dharmanagar', 22, 1),
(1296, 'Indranagar', 22, 1),
(1297, 'Jogendranagar', 22, 1),
(1298, 'Kailasahar', 22, 1),
(1299, 'Khowai', 22, 1),
(1300, 'Pratapgarh', 22, 1),
(1301, 'Udaipur', 22, 1),
(1302, 'Achhnera', 23, 1),
(1303, 'Adari', 23, 1),
(1304, 'Agra', 23, 1),
(1305, 'Aligarh', 23, 1),
(1306, 'Allahabad', 23, 1),
(1307, 'Amroha', 23, 1),
(1308, 'Azamgarh', 23, 1),
(1309, 'Bahraich', 23, 1),
(1310, 'Ballia', 23, 1),
(1311, 'Balrampur', 23, 1),
(1312, 'Banda', 23, 1),
(1313, 'Bareilly', 23, 1),
(1314, 'Chandausi', 23, 1),
(1315, 'Dadri', 23, 1),
(1316, 'Deoria', 23, 1),
(1317, 'Etawah', 23, 1),
(1318, 'Fatehabad', 23, 1),
(1319, 'Fatehpur', 23, 1),
(1320, 'Fatehpur', 23, 1),
(1321, 'Greater Noida', 23, 1),
(1322, 'Hamirpur', 23, 1),
(1323, 'Hardoi', 23, 1),
(1324, 'Jajmau', 23, 1),
(1325, 'Jaunpur', 23, 1),
(1326, 'Jhansi', 23, 1),
(1327, 'Kalpi', 23, 1),
(1328, 'Kanpur', 23, 1),
(1329, 'Kota', 23, 1),
(1330, 'Laharpur', 23, 1),
(1331, 'Lakhimpur', 23, 1),
(1332, 'Lal Gopalganj Nindaura', 23, 1),
(1333, 'Lalganj', 23, 1),
(1334, 'Lalitpur', 23, 1),
(1335, 'Lar', 23, 1),
(1336, 'Loni', 23, 1),
(1337, 'Lucknow', 23, 1),
(1338, 'Mathura', 23, 1),
(1339, 'Meerut', 23, 1),
(1340, 'Modinagar', 23, 1),
(1341, 'Muradnagar', 23, 1),
(1342, 'Nagina', 23, 1),
(1343, 'Najibabad', 23, 1),
(1344, 'Nakur', 23, 1),
(1345, 'Nanpara', 23, 1),
(1346, 'Naraura', 23, 1),
(1347, 'Naugawan Sadat', 23, 1),
(1348, 'Nautanwa', 23, 1),
(1349, 'Nawabganj', 23, 1),
(1350, 'Nehtaur', 23, 1),
(1351, 'NOIDA', 23, 1),
(1352, 'Noorpur', 23, 1),
(1353, 'Obra', 23, 1),
(1354, 'Orai', 23, 1),
(1355, 'Padrauna', 23, 1),
(1356, 'Palia Kalan', 23, 1),
(1357, 'Parasi', 23, 1),
(1358, 'Phulpur', 23, 1),
(1359, 'Pihani', 23, 1),
(1360, 'Pilibhit', 23, 1),
(1361, 'Pilkhuwa', 23, 1),
(1362, 'Powayan', 23, 1),
(1363, 'Pukhrayan', 23, 1),
(1364, 'Puranpur', 23, 1),
(1365, 'Purquazi', 23, 1),
(1366, 'Purwa', 23, 1),
(1367, 'Rae Bareli', 23, 1),
(1368, 'Rampur', 23, 1),
(1369, 'Rampur Maniharan', 23, 1),
(1370, 'Rasra', 23, 1),
(1371, 'Rath', 23, 1),
(1372, 'Renukoot', 23, 1),
(1373, 'Reoti', 23, 1),
(1374, 'Robertsganj', 23, 1),
(1375, 'Rudauli', 23, 1),
(1376, 'Rudrapur', 23, 1),
(1377, 'Sadabad', 23, 1),
(1378, 'Safipur', 23, 1),
(1379, 'Saharanpur', 23, 1),
(1380, 'Sahaspur', 23, 1),
(1381, 'Sahaswan', 23, 1),
(1382, 'Sahawar', 23, 1),
(1383, 'Sahjanwa', 23, 1),
(1385, 'Sambhal', 23, 1),
(1386, 'Samdhan', 23, 1),
(1387, 'Samthar', 23, 1),
(1388, 'Sandi', 23, 1),
(1389, 'Sandila', 23, 1),
(1390, 'Sardhana', 23, 1),
(1391, 'Seohara', 23, 1),
(1394, 'Shahganj', 23, 1),
(1395, 'Shahjahanpur', 23, 1),
(1396, 'Shamli', 23, 1),
(1399, 'Sherkot', 23, 1),
(1401, 'Shikohabad', 23, 1),
(1402, 'Shishgarh', 23, 1),
(1403, 'Siana', 23, 1),
(1404, 'Sikanderpur', 23, 1),
(1405, 'Sikandra Rao', 23, 1),
(1406, 'Sikandrabad', 23, 1),
(1407, 'Sirsaganj', 23, 1),
(1408, 'Sirsi', 23, 1),
(1409, 'Sitapur', 23, 1),
(1410, 'Soron', 23, 1),
(1411, 'Suar', 23, 1),
(1412, 'Sultanpur', 23, 1),
(1413, 'Sumerpur', 23, 1),
(1414, 'Tanda', 23, 1),
(1415, 'Tanda', 23, 1),
(1416, 'Tetri Bazar', 23, 1),
(1417, 'Thakurdwara', 23, 1),
(1418, 'Thana Bhawan', 23, 1),
(1419, 'Tilhar', 23, 1),
(1420, 'Tirwaganj', 23, 1),
(1421, 'Tulsipur', 23, 1),
(1422, 'Tundla', 23, 1),
(1423, 'Unnao', 23, 1),
(1424, 'Utraula', 23, 1),
(1425, 'Varanasi', 23, 1),
(1426, 'Vrindavan', 23, 1),
(1427, 'Warhapur', 23, 1),
(1428, 'Zaidpur', 23, 1),
(1429, 'Zamania', 23, 1),
(1454, 'Alipurduar', 24, 1),
(1455, 'Arambagh', 24, 1),
(1456, 'Asansol', 24, 1),
(1457, 'Baharampur', 24, 1),
(1458, 'Bally', 24, 1),
(1459, 'Balurghat', 24, 1),
(1460, 'Bankura', 24, 1),
(1461, 'Barakar', 24, 1),
(1462, 'Barasat', 24, 1),
(1463, 'Bardhaman', 24, 1),
(1464, 'Bidhan Nagar', 24, 1),
(1465, 'Chinsura', 24, 1),
(1466, 'Contai', 24, 1),
(1467, 'Cooch Behar', 24, 1),
(1468, 'Darjeeling', 24, 1),
(1469, 'Durgapur', 24, 1),
(1470, 'Haldia', 24, 1),
(1471, 'Howrah', 24, 1),
(1472, 'Islampur', 24, 1),
(1473, 'Jhargram', 24, 1),
(1474, 'Kharagpur', 24, 1),
(1475, 'Kolkata', 24, 1),
(1476, 'Mainaguri', 24, 1),
(1477, 'Mal', 24, 1),
(1478, 'Mathabhanga', 24, 1),
(1479, 'Medinipur', 24, 1),
(1480, 'Memari', 24, 1),
(1481, 'Monoharpur', 24, 1),
(1482, 'Murshidabad', 24, 1),
(1483, 'Nabadwip', 24, 1),
(1484, 'Naihati', 24, 1),
(1485, 'Panchla', 24, 1),
(1486, 'Pandua', 24, 1),
(1487, 'Paschim Punropara', 24, 1),
(1488, 'Purulia', 24, 1),
(1489, 'Raghunathpur', 24, 1),
(1490, 'Raiganj', 24, 1),
(1491, 'Rampurhat', 24, 1),
(1492, 'Ranaghat', 24, 1),
(1493, 'Sainthia', 24, 1),
(1494, 'Santipur', 24, 1),
(1495, 'Siliguri', 24, 1),
(1496, 'Sonamukhi', 24, 1),
(1497, 'Srirampore', 24, 1),
(1498, 'Suri', 24, 1),
(1499, 'Taki', 24, 1),
(1500, 'Tamluk', 24, 1),
(1501, 'Tarakeswar', 24, 1),
(1502, 'Chikmagalur', 9, 1),
(1503, 'Davanagere', 9, 1),
(1504, 'Dharwad', 9, 1),
(1505, 'Gadag', 9, 1),
(1506, 'Chennai', 21, 1),
(1507, 'Coimbatore', 21, 1),
(1, 'Pune', 12, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cms_page`
--

CREATE TABLE `tbl_cms_page` (
  `page_id` int(11) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `page_short_desc` text NOT NULL,
  `page_full_desc` text NOT NULL,
  `page_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_cms_page`
--

INSERT INTO `tbl_cms_page` (`page_id`, `page_title`, `page_short_desc`, `page_full_desc`, `page_status`) VALUES
(1, 'About Us - Updated', 'Updated :&nbsp;Lorem ipsum dolor sit amet, assum scripta pertinacia sed an, ne vis putent delicatissimi, ius reque nulla facilisis id. In noster mentitum nam. Sed offendit senserit eu.<br />\r\n...', '<div>\r\n Updated : Lorem ipsum dolor sit amet, ut mazim scaevola pertinacia duo. Ea vel persius percipitur dissentiunt. Ut impedit volumus sapientem his, vel timeam delectus iracundia eu. In duo indoctum urbanitas, cu munere comprehensam vis.</div>\r\n<div>\r\n &nbsp;</div>\r\n<div>\r\n Eu sea dolores mandamus prodesset. Quodsi volutpat accommodare sea cu. Eu tale agam idque has, pro ex simul animal numquam. At zril liberavisse sea, menandri aliquando incorrupte sed ea, qui in probo adolescens contentiones. Ea tibique adolescens per, erant cotidieque referrentur eum ad. Ut est duis viderer suavitate.</div>', 1),
(2, 'Privacy Policy', 'Cum&nbsp;sociis&nbsp;natoque&nbsp;penatibus&nbsp;et&nbsp;magnis&nbsp;dis&nbsp;parturient&nbsp;montes,&nbsp;nascetur&nbsp;ridiculus&nbsp;mus.', '<div>\r\n Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vitae finibus lectus. Cras accumsan dictum nisi ut varius. Phasellus ut libero quam. Quisque molestie mi lectus, eu sagittis ex dictum vel. Vestibulum sed est nulla. Donec posuere ornare turpis, eget convallis magna efficitur ullamcorper. Nulla non vestibulum sapien, ac lacinia neque. Praesent vitae odio dolor. Proin ac fringilla metus. Aenean molestie tortor ut leo efficitur, eu dignissim est ultrices. Donec rhoncus vitae tortor in accumsan. Nunc id laoreet metus. Integer efficitur metus id molestie accumsan. Aliquam blandit, neque eu efficitur interdum, ex est ullamcorper ante, quis volutpat mauris justo ut ante.</div>\r\n<div>\r\n &nbsp;</div>\r\n<div>\r\n Suspendisse volutpat placerat lacinia. Sed quis turpis ultricies est ornare molestie eu eu mi. In semper eros et congue tincidunt. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec leo quam, viverra eget lectus et, aliquam suscipit lorem. Cras ut arcu ut turpis lacinia ultricies a sit amet metus. Donec varius ac leo et pretium. Fusce non tristique nisl, quis ultricies nisi. Vestibulum sed ligula pretium, suscipit orci non, euismod mauris. Nam ipsum ante, rutrum ac nisi ac, vulputate venenatis orci. Morbi ultrices fringilla ligula, eget facilisis enim. Nulla volutpat, arcu eu pharetra placerat, urna metus vestibulum nibh, vel pretium erat odio ac urna. Etiam eleifend sapien eget dapibus eleifend. Etiam at volutpat risus, sit amet dictum leo.</div>\r\n<div>\r\n &nbsp;</div>\r\n<div>\r\n Etiam cursus consequat cursus. Praesent varius felis leo, sed tempor lorem sollicitudin quis. Suspendisse egestas luctus pulvinar. Pellentesque urna est, malesuada quis faucibus eu, fringilla sit amet metus. Phasellus cursus consectetur lorem. Maecenas a diam aliquet, efficitur nunc vel, vestibulum leo. Nullam mi libero, posuere sit amet tempus ac, pellentesque nec dolor. Praesent eros justo, viverra eu pretium at, tincidunt id nisl. Donec sit amet tincidunt nunc, quis elementum ligula. In pulvinar, lorem mattis fringilla viverra, dolor libero varius est, et maximus enim odio eu diam. Etiam et iaculis orci, in ultricies augue. Sed leo nulla, placerat nec nisl nec, elementum congue enim. Etiam laoreet orci nisl, iaculis dictum ex porttitor vel. Donec pretium metus eget commodo ornare. Proin placerat ante vitae egestas lobortis.</div>\r\n<p 0px="" ac="" accumsan="" accumsan.="" aenean="" aliquam="" color:="" convallis="" cras="" cum="" dictum="" dignissim="" dis="" dolor.="" donec="" efficitur="" eget="" est="" et="" etiam="" eu="" ex="" finibus="" font-family:="" font-size:="" fringilla="" id="" in="" integer="" justo="" lacinia="" laoreet="" lectus.="" leo="" libero="" line-height:="" magna="" magnis="" margin:="" mauris="" metus="" metus.="" mi="" molestie="" mus.="" nascetur="" natoque="" neque="" neque.="" nisi="" non="" nulla="" nulla.="" nunc="" odio="" ornare="" p="" padding:="" parturient="" penatibus="" phasellus="" posuere="" praesent="" proin="" quam.="" quis="" quisque="" rhoncus="" ridiculus="" sagittis="" sed="" sociis="" tortor="" ullamcorper="" ullamcorper.="" ultrices.="" ut="" varius.="" vel.="" vestibulum="" vitae="" volutpat="">\r\n &nbsp;</p>', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_country`
--

CREATE TABLE `tbl_country` (
  `countryId` int(11) NOT NULL,
  `countryName` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_country`
--

INSERT INTO `tbl_country` (`countryId`, `countryName`, `status`) VALUES
(1, 'India', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_disc`
--

CREATE TABLE `tbl_disc` (
  `discId` int(11) NOT NULL,
  `discName` varchar(255) NOT NULL,
  `fromDate` date NOT NULL,
  `toDate` date NOT NULL,
  `ageLimit` int(3) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_disc`
--

INSERT INTO `tbl_disc` (`discId`, `discName`, `fromDate`, `toDate`, `ageLimit`, `status`) VALUES
(1, 'Disc 1', '2016-01-03', '2016-01-08', 22, 1),
(2, 'Disc 2', '2016-01-04', '2016-01-10', 23, 1),
(5, 'Disc 5', '2016-01-04', '2016-01-04', 21, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_disc_details`
--

CREATE TABLE `tbl_disc_details` (
  `id` int(11) NOT NULL,
  `discId` int(11) NOT NULL,
  `entryType` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `discDays` varchar(15) NOT NULL,
  `noOfEntry` int(11) NOT NULL,
  `availableEntry` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_disc_details`
--

INSERT INTO `tbl_disc_details` (`id`, `discId`, `entryType`, `price`, `discDays`, `noOfEntry`, `availableEntry`) VALUES
(2, 1, 'Girls', 200, '2,3,5,6,7', 30, 30),
(3, 1, 'Couple', 400, '1,2,6', 20, 20),
(4, 1, 'Stag', 400, '1,7', 20, 20),
(5, 2, 'Stag', 200, '1,2,6,7', 30, 30),
(6, 2, 'Girls', 0, '1,2,6,7', 30, 30),
(10, 5, 'Couple', 100, '2,6', 10, 10);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_emailsettings`
--

CREATE TABLE `tbl_emailsettings` (
  `email_id` int(11) NOT NULL,
  `email_template` varchar(255) NOT NULL,
  `email_subject` varchar(255) NOT NULL,
  `email_body` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_emailsettings`
--

INSERT INTO `tbl_emailsettings` (`email_id`, `email_template`, `email_subject`, `email_body`) VALUES
(10, 'Member - Approved', 'Application to join the Canberra Bushwalking Club Approved', '<p alt="Gudgenby Meadows" height:="" img="" src="http://demo.zealousys.com:81/canberrabushwalking/uploads/images/Gudgenby meadows, very cropped for email header.JPG">\n &nbsp;</p>\n<p>\n <br />\n Hello [USERNAME],</p>\n<p>\n The GTFS&nbsp;Committee has approved your application for membership. Welcome to the Group!<br />\n <br />\n You can now login to the Member&#39;s area, where you can see a list of other members, and track your own walk history.&nbsp;</p>\n<p>\n If you have any questions about the Group, please don&#39;t hesitate to contact me.<br />\n <br />\n with best wishes<br />\n <br />\n Thanks &amp; Regards<br />\n Sagar Jaisur.</p>'),
(11, 'Member - Rejection', 'Application to join the Canberra Bushwalking Club Rejected', '<p>\n Hello [USERNAME],</p>\n<p>\n We regret to inform you that your membership application with<br />\n GTFS Group &nbsp;cannot be approved under the current circumstances.</p>\n<p>\n Thank you.</p>'),
(12, 'Contact Us', 'You have been contacted successfully.', '<p>\r\n Hello [NAME],</p>\r\n<p>\r\n [MESSAGE]</p>\r\n<p>\r\n Thank you.</p>\r\n<p>\r\n &nbsp;</p>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_member`
--

CREATE TABLE `tbl_member` (
  `uid` int(11) NOT NULL,
  `roleId` int(11) NOT NULL COMMENT '3=broker,4=customer',
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `companyName` varchar(255) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `mobileNumber` double NOT NULL,
  `weburl` text NOT NULL,
  `workingHoursFrom` varchar(255) NOT NULL,
  `workingHoursTo` varchar(255) NOT NULL,
  `workingDays` varchar(255) NOT NULL,
  `serviceTaxNumber` varchar(255) NOT NULL,
  `panCardNo` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `gender` int(11) NOT NULL COMMENT '1=male,2=Female,3=Other',
  `address` text NOT NULL,
  `country` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `pincode` int(11) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `deviceType` int(11) NOT NULL COMMENT '1-Android,2-IOS',
  `deviceId` varchar(255) NOT NULL,
  `rating` int(11) NOT NULL COMMENT 'Rating Average Total ',
  `verificationFlag` int(11) NOT NULL DEFAULT '2' COMMENT '1-approved,2-requested,3-rejected',
  `verificationEmail` int(11) NOT NULL DEFAULT '2' COMMENT '1-verified,2-unverified''',
  `token` varchar(255) NOT NULL,
  `otp_code` varchar(255) NOT NULL,
  `registerDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_member`
--

INSERT INTO `tbl_member` (`uid`, `roleId`, `name`, `image`, `email`, `companyName`, `categoryId`, `mobileNumber`, `weburl`, `workingHoursFrom`, `workingHoursTo`, `workingDays`, `serviceTaxNumber`, `panCardNo`, `dob`, `gender`, `address`, `country`, `state`, `city`, `pincode`, `designation`, `deviceType`, `deviceId`, `rating`, `verificationFlag`, `verificationEmail`, `token`, `otp_code`, `registerDate`, `modifiedDate`, `status`) VALUES
(1, 1, 'Jason Statham', 'http://localhost/realtynxt/uploads/users/5689fb3e22180.png', 'realtynxt_admin@acrobat.co.in', 'Acrobat', 0, 9924120209, '', '10 AM', '7 PM', '1,2,4', '1231ST', 'Pan123', '1992-06-01', 1, 'B/1/2', 'India', 'Gujarat', 'Bhavnagar', 364004, 'Software Engineer', 1, '566a7e13ef143', 5, 1, 1, '566a7e13ef143', '', '2015-12-11', '2016-01-23', 1),
(2, 2, 'Alex Pierce', 'http://localhost/realtynxt/uploads/users/566be2ae409c8.jpg', 'alex.pierce@yahoo.co.in', 'Acrobat', 0, 1234567890, '', '', '', '', '', '', '1989-12-31', 1, 'B/1/3', 'India', 'Gujarat', 'Vadodara', 456123, 'Software Engineer', 0, '', 0, 1, 2, '', '', '2015-12-12', '2015-12-18', 1),
(4, 4, 'Scarlett Johansson', 'http://localhost/realtynxt/uploads/users/566be69e00000.jpg', 'scarlet.johansson@acrobat.co.in', 'Acrobat', 0, 1234567891, '', '', '', '', '', '', '1989-01-01', 2, 'Bapunagar', 'India', 'Gujarat', 'Ahmedabad', 380014, 'iPhone Developer', 0, '', 0, 1, 2, '', '', '2015-12-17', '2015-12-18', 1),
(7, 3, 'John Smith', 'http://acrobat.co.in/ftpaccount/gtfs/uploads/users/567bd37091fc0.jpg', 'test123@gmail.com', 'test', 0, 9988775566, 'www.google.com', '10 AM', '7 PM', '1,3,5,6,7', '1231ST', 'Pan123', '2014-11-30', 1, 'Near  by you', 'India', 'Gujarat', 'Ahmedabad', 380015, 'test', 0, '', 0, 1, 2, '', '', '2015-12-23', '2015-12-24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_memberoffline`
--

CREATE TABLE `tbl_memberoffline` (
  `uid` int(11) NOT NULL,
  `roleId` int(11) NOT NULL COMMENT '3=broker,4=customer',
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `companyName` varchar(255) NOT NULL,
  `mobileNumber` int(11) NOT NULL,
  `dob` date NOT NULL,
  `address` int(11) NOT NULL,
  `country` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `pincode` int(11) NOT NULL,
  `profession` varchar(255) NOT NULL COMMENT '1=Salaried,2=Self-employed,3=businessman,4=RetiredStudent,5=Other',
  `deviceType` int(11) NOT NULL COMMENT '1-Android,2-IOS',
  `deviceId` varchar(255) NOT NULL,
  `rating` int(11) NOT NULL COMMENT 'Rating Average Total ',
  `verificationFlag` int(11) NOT NULL DEFAULT '2' COMMENT '1-approved,2-requested,3-rejected',
  `verificationEmail` int(11) NOT NULL DEFAULT '2' COMMENT '1-verified,2-unverified''',
  `token` varchar(255) NOT NULL,
  `registerDate` date NOT NULL,
  `modifiedDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_members_roles`
--

CREATE TABLE `tbl_members_roles` (
  `roleId` int(11) NOT NULL,
  `roleTitle` varchar(255) NOT NULL,
  `rolePermission` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_members_roles`
--

INSERT INTO `tbl_members_roles` (`roleId`, `roleTitle`, `rolePermission`) VALUES
(1, 'ADMIN', 'Enabled'),
(2, 'SUB ADMIN', 'Enabled'),
(3, 'BROKER', 'Enabled'),
(4, 'CUSTOMER', 'Enabled');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_property`
--

CREATE TABLE `tbl_property` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `propertyTitle` varchar(255) NOT NULL,
  `propertyMainType` varchar(255) NOT NULL COMMENT '1=Inventory,2=Requirement',
  `propertyType` varchar(255) NOT NULL COMMENT '1-Residential, 2-Commercial,3-Agriculuter',
  `stateFor` varchar(255) NOT NULL COMMENT ' 1-Sale,  2-Lease,  3-Original Booking',
  `propertyCategory` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `area` varchar(255) NOT NULL,
  `landmark` varchar(255) NOT NULL,
  `bhk` varchar(255) NOT NULL COMMENT '1rk	1BHK	1.5BHK	2BHK	2.5BHK	3BHK	3.5BHK	4BHK	5BHK	>5BHK',
  `servantRoom` varchar(10) NOT NULL COMMENT '1=Yes,0=No',
  `floorSize` double NOT NULL,
  `floorSizeUnit` varchar(255) NOT NULL COMMENT '1=Sq. Feet,2=Sq. Yard,3=Sq. Meter',
  `price` double NOT NULL,
  `priceUnit` varchar(255) NOT NULL COMMENT '1=Thousand,2=Lac,3=Crore',
  `throughDirect` varchar(255) NOT NULL,
  `floor` varchar(255) NOT NULL COMMENT 'BASEMENT	GROUND	FIRST	SECOND 	THIRD	FOURTH',
  `tower` varchar(255) NOT NULL COMMENT 'Text Input',
  `parking` varchar(255) NOT NULL COMMENT 'No Parking	1	2	3	4',
  `facing` varchar(255) NOT NULL COMMENT 'Park	Road	Pool	Corner	ATTRIUM	ROADSIDE',
  `vastu` varchar(255) NOT NULL COMMENT 'North	West	East	South	North East	North West	South East	South West',
  `furnishing` varchar(255) NOT NULL COMMENT 'Furnished	Semi Furnished	Bare ShellFurnished	Bare Shell',
  `lift` varchar(10) NOT NULL COMMENT '1=Yes,0=No',
  `amenities` text NOT NULL COMMENT 'PARKING	GYM	SWIMMING POOL	LIFT	POWER BACKUP	GAS PIPELINE	24 HOURS WATER SUPPLY	INTERCOM	CC TV',
  `plotSize` double NOT NULL,
  `plotUnit` varchar(20) NOT NULL COMMENT 'Feet	Meter	Yard',
  `powerBackup` varchar(255) NOT NULL COMMENT 'No Backup	90%	100%	Inverter',
  `roadSize` varchar(255) NOT NULL COMMENT '16 meter, 20 meter, 24 meter, 28 meter',
  `leaseType` varchar(255) NOT NULL COMMENT 'COMPANY	PERSONAL',
  `isBuildUp` varchar(10) NOT NULL COMMENT '1=yes,2=No',
  `siteReady` varchar(10) NOT NULL COMMENT '1=yes,2=No',
  `status` varchar(255) NOT NULL COMMENT '0=Deleted,1=Show,2=Closed'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_property`
--

INSERT INTO `tbl_property` (`id`, `uid`, `propertyTitle`, `propertyMainType`, `propertyType`, `stateFor`, `propertyCategory`, `city`, `area`, `landmark`, `bhk`, `servantRoom`, `floorSize`, `floorSizeUnit`, `price`, `priceUnit`, `throughDirect`, `floor`, `tower`, `parking`, `facing`, `vastu`, `furnishing`, `lift`, `amenities`, `plotSize`, `plotUnit`, `powerBackup`, `roadSize`, `leaseType`, `isBuildUp`, `siteReady`, `status`) VALUES
(1, 1, 'Palace1', 'Inventory', 'Residential', 'Lease', 'Multistrory Apartment', 'Pune', 'Koregaon Park', 'In front of RIL', '>5BHK', 'Yes', 1024, 'Sq. Feet', 50, 'Lac', 'Direct', 'GROUND', '1', '4', 'Pool', 'North East', 'Furnished', 'Yes', 'PARKING, GYM, SWIMMING POOL, LIFT, POWER BACKUP, GAS PIPELINE, 24 HOURS WATER SUPPLY, INTERCOM, CC TV', 1024, 'Yard', 'Inverter', '16 meter', 'PERSONAL', 'Yes', 'Yes', 'Show'),
(3, 2, 'Palace3', 'Requirement', 'Commercial', 'Original Booking', 'Independent House', 'Pune', 'Boat Club Road', 'Behind RIL', '4BHK', 'No', 512, 'Sq. Feet', 25, 'Lac', 'Direct', 'FIRST', '1', '1', 'Pool', 'East', 'Furnished', 'No', 'PARKING, GYM, SWIMMING POOL, POWER BACKUP, GAS PIPELINE, 24 HOURS WATER SUPPLY', 768, 'Yard', '90%', '16 meter', 'PERSONAL', 'Yes', 'Yes', 'Show');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_property_map_list`
--

CREATE TABLE `tbl_property_map_list` (
  `propertyMapListId` int(11) NOT NULL,
  `propertyName` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `address` text NOT NULL,
  `price` varchar(20) NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `images` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_property_map_list`
--

INSERT INTO `tbl_property_map_list` (`propertyMapListId`, `propertyName`, `description`, `address`, `price`, `latitude`, `longitude`, `images`, `status`) VALUES
(1, '2 BHK Flat 1285 sqft - Applewoods Sorrel', 'Presenting Sorrel Apartments, an abode of luxury for you and your loved ones. Wrapped in the cocoon of comfort, Sorrel apartments are designed for holistic living and conform to global standards of architecture...', 'Near Sardar Patel Ring Road, Ahmedabad', '42.0 Lac', '22.9294641', '72.5918494', 'http://localhost/realtynxt/uploads/propertyMapList/1453454332_Property1.jpg', 1),
(2, '2 BHK Flat 1140 sqft - Orchid Greenfield', 'This property, also a part of Apple wood Township, sits amidst restful, verdant surrounds, just a short hop away from SP Road. Here, 2 and 3 BHK residential apartments come fully supported with Internet and TV connections, fire safety equipment, and much more...', 'Near Sardar Patel Ring Road, Ahmedabad', '31.9 Lac', '21.2700', '81.6000', 'http://localhost/realtynxt/uploads/propertyMapList/1453454629_Property2.jpg', 1),
(4, '2 BHK Flat 782 sqft - Suburbia Estate', 'This Apartment is located in Wagholi, Pune. It is a spacious and well-ventilated apartment loaded with state-of-the-art amenities. The key specifications include 2 feet x 2 feet vitrified tile flooring, internal oil bond distemper, designer bathroom and much more...', 'Near Wagholi, Pune', '29.7 Lac', '17.3700', '78.4800', 'http://localhost/realtynxt/uploads/propertyMapList/1453463223_Property3.jpg', 1),
(5, '3 BHK Flat 1367 sqft - Tinsel Town', 'The project offers 2BHK and 3BHK flats at very competitive and affordable price. It is well planned and is built with all modern amenities. \r\nWell Planned Design Buildings are located in a manner to get maximum daylight and ventilation throughout and much more...', 'Near Phase 2,Hinjewadi, Pune', '73.5 Lac', '18.586622', '73.673744', 'http://localhost/realtynxt/uploads/propertyMapList/1453463363_Property4.jpg', 1),
(6, '4 BHK Flat 1720 sqft - Belvedere Park', 'Belvedere Park is located just 1/5 km from NH-8, on the main 60 meters Sector Road within DLF City. It is a part of the exclusive Belvedere Place with three and four bedroom apartments with an L-shaped living/dining room. The drawing, dining and lob and much more...', 'Near Belvedere Park,DLF City Phase 3, Gurgaon', '1.80 Cr', '28.490837', '77.065514', 'http://localhost/realtynxt/uploads/propertyMapList/1453479706_Property5.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rating`
--

CREATE TABLE `tbl_rating` (
  `id` int(11) NOT NULL,
  `forId` int(11) NOT NULL COMMENT 'Rating getting  user',
  `fromId` int(11) NOT NULL COMMENT 'Rating Given By ',
  `date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_restaurant`
--

CREATE TABLE `tbl_restaurant` (
  `restaurantId` int(11) NOT NULL,
  `restaurantName` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `speciality` varchar(255) NOT NULL,
  `liker` varchar(255) NOT NULL,
  `images` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_restaurant`
--

INSERT INTO `tbl_restaurant` (`restaurantId`, `restaurantName`, `location`, `speciality`, `liker`, `images`, `status`) VALUES
(6, 'Restaurant 2', 'Ahmedabad', '7 Star - Updated', 'Everybody', 'http://localhost/realtynxt/uploads/restaurants/1451911802_Penguins.jpg', 1),
(7, 'Restaurant 3', 'India', '7 Star', 'Everyone', 'http://localhost/realtynxt/uploads/restaurants/1451913504_Desert.jpg,http://localhost/realtynxt/uploads/restaurants/1451913504_Lighthouse.jpg,http://localhost/realtynxt/uploads/restaurants/1451913504_Tulips.jpg', 1),
(8, 'Honest', 'Bodakdev', 'Punjabi', 'Everyone', 'http://localhost/realtynxt/uploads/restaurants/1452147045_Jellyfish.jpg,http://localhost/realtynxt/uploads/restaurants/1452146746_Chrysanthemum.jpg,http://localhost/realtynxt/uploads/restaurants/1452146747_Lighthouse.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_setting`
--

CREATE TABLE `tbl_setting` (
  `settingId` int(11) NOT NULL,
  `settingName` varchar(255) NOT NULL,
  `settingValue` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_setting`
--

INSERT INTO `tbl_setting` (`settingId`, `settingName`, `settingValue`) VALUES
(1, 'BANNER_DESCRIPTION', '<font color="#000000">GTFS</font>'),
(2, 'BANNER_TITLE', '<span style="color:#000000;"><font face="arial, helvetica, sans-serif">Welcome To The</font></span>'),
(6, 'COPYRIGHT_TEXT', 'Copyright © 2015.'),
(12, 'CONTACT_US_EMAIL', 'sagar.jaisur@acrobat.co.in'),
(15, 'SITE_TITLE', 'GTFS'),
(19, 'BANNER_BACKGROUND_COLOR', 'transparent'),
(21, 'BANNER_TITLE_COLOR', ''),
(22, 'BANNER_TITLE_FONT_SIZE', '60'),
(23, 'BANNER_DESCRIPTION_FONT_SIZE', '45'),
(25, 'EMAIL_SMTP_PROTOCOL', 'smtp'),
(26, 'EMAIL_SMTP_HOST', 'smtp.gmail.com'),
(27, 'EMAIL_SMTP_TIMEOUT', '5'),
(28, 'EMAIL_SMTP_USER', 'sagar.jaisur@acrobat.co.in'),
(29, 'EMAIL_SMTP_PASSWORD', 'Sagar123'),
(30, 'EMAIL_SMTP_MAILTYPE', 'html'),
(31, 'EMAIL_SMTP_PORT', '587'),
(32, 'EMAIL_SMTP_CRYPTO', 'tls');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_smssettings`
--

CREATE TABLE `tbl_smssettings` (
  `sms_id` int(11) NOT NULL,
  `sms_template` varchar(255) NOT NULL,
  `sms_subject` varchar(255) NOT NULL,
  `sms_body` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_smssettings`
--

INSERT INTO `tbl_smssettings` (`sms_id`, `sms_template`, `sms_subject`, `sms_body`) VALUES
(10, 'Member - Approved', 'Application to join the RealtyNxt Approved', 'Hello [USERNAME], \r\n \r\n The RealtyNxt Committee has approved your application for membership. Welcome to the Group! \r\n  \r\n You can now login to the Member\'s area, where you can see a list of other members, and track your own walk history.  \r\n \r\n If you have any questions about the Group, please don\'t hesitate to contact me. \r\n  \r\n with best wishes \r\n  \r\n Thanks & Regards \r\n Sagar Jaisur.'),
(11, 'Member - Rejection', 'Application to join the RealtyNxt Rejected', 'Hello [USERNAME]\r\n\r\n We regret to inform you that your membership application with RealtyNxt Group cannot be approved under the current circumstances.\r\n Thank you.'),
(12, 'Contact Us', 'You have been contacted successfully.', 'Hello [NAME], \r\n \r\n [MESSAGE] \r\n \r\n Thank you.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_state`
--

CREATE TABLE `tbl_state` (
  `stateId` int(11) NOT NULL,
  `countryId` int(11) NOT NULL,
  `stateName` varchar(50) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_state`
--

INSERT INTO `tbl_state` (`stateId`, `countryId`, `stateName`, `status`) VALUES
(1, 1, 'Andhra Pradesh', 1),
(2, 1, 'Assam', 1),
(3, 1, 'Arunachal Pradesh', 1),
(4, 1, 'Gujarat', 1),
(5, 1, 'Bihar', 1),
(6, 1, 'Haryana', 1),
(7, 1, 'Himachal Pradesh', 1),
(8, 1, 'Jammu and Kashmir', 1),
(9, 1, 'Karnataka', 1),
(10, 1, 'Kerala', 1),
(11, 1, 'Madhya Pradesh', 1),
(12, 1, 'Maharastra', 1),
(13, 1, 'Manipur', 1),
(14, 1, 'Meghalaya', 1),
(15, 1, 'Mizoram', 1),
(16, 1, 'Nagaland', 1),
(17, 1, 'Orissa', 1),
(18, 1, 'Punjab', 1),
(19, 1, 'Rajasthan', 1),
(20, 1, 'Sikkim', 1),
(21, 1, 'Tamil Nadu', 1),
(22, 1, 'Tripura', 1),
(23, 1, 'Uttar Pradesh', 1),
(24, 1, 'West Bengal', 1),
(25, 1, 'Goa', 1),
(26, 1, 'Pondichery', 1),
(27, 1, 'Lakshadweep', 1),
(28, 1, 'Daman & Diu', 1),
(29, 1, 'Dadra & Nagar Haveli', 1),
(30, 1, 'Chandigarh', 1),
(31, 1, 'Andaman and Nicobar Islands', 1),
(32, 1, 'Uttaranchal', 1),
(33, 1, 'Jharkhand', 1),
(34, 1, 'Chhattisgarh', 1),
(35, 1, 'Delhi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_vehicle_rental`
--

CREATE TABLE `tbl_vehicle_rental` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `city` varchar(255) NOT NULL,
  `landmark` varchar(255) NOT NULL,
  `area` varchar(255) NOT NULL,
  `vehicleType` enum('car','bike') NOT NULL,
  `vehicleModel` varchar(255) NOT NULL,
  `vehicleHaving` int(11) NOT NULL,
  `vehicleAvilable` int(11) NOT NULL,
  `vehicleDays` varchar(255) NOT NULL,
  `vehiclePrice` int(11) NOT NULL COMMENT 'Per vehicle/Per day',
  `vehicleNote` text NOT NULL,
  `vehicleNumber` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_vehicle_rental`
--

INSERT INTO `tbl_vehicle_rental` (`id`, `uid`, `city`, `landmark`, `area`, `vehicleType`, `vehicleModel`, `vehicleHaving`, `vehicleAvilable`, `vehicleDays`, `vehiclePrice`, `vehicleNote`, `vehicleNumber`) VALUES
(1, 1, 'Pune', 'near by you', 'Koregaon Park', 'car', 'Residential', 50, 10, '1,2,5,6', 100, '', 'GJ27M6155'),
(2, 1, 'Pune', 'near by you', 'Koregaon Park', 'car', 'Residential', 50, 10, '3,4,5', 100, 'test  V Note', 'test  V Number');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_adventure`
--
ALTER TABLE `tbl_adventure`
  ADD PRIMARY KEY (`adventureId`);

--
-- Indexes for table `tbl_adventure_type`
--
ALTER TABLE `tbl_adventure_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_area`
--
ALTER TABLE `tbl_area`
  ADD PRIMARY KEY (`areaId`);

--
-- Indexes for table `tbl_business_category`
--
ALTER TABLE `tbl_business_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_city`
--
ALTER TABLE `tbl_city`
  ADD PRIMARY KEY (`cityId`);

--
-- Indexes for table `tbl_cms_page`
--
ALTER TABLE `tbl_cms_page`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `tbl_country`
--
ALTER TABLE `tbl_country`
  ADD PRIMARY KEY (`countryId`);

--
-- Indexes for table `tbl_disc`
--
ALTER TABLE `tbl_disc`
  ADD PRIMARY KEY (`discId`);

--
-- Indexes for table `tbl_disc_details`
--
ALTER TABLE `tbl_disc_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_emailsettings`
--
ALTER TABLE `tbl_emailsettings`
  ADD PRIMARY KEY (`email_id`);

--
-- Indexes for table `tbl_member`
--
ALTER TABLE `tbl_member`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `tbl_memberoffline`
--
ALTER TABLE `tbl_memberoffline`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `tbl_property`
--
ALTER TABLE `tbl_property`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_property_map_list`
--
ALTER TABLE `tbl_property_map_list`
  ADD PRIMARY KEY (`propertyMapListId`);

--
-- Indexes for table `tbl_rating`
--
ALTER TABLE `tbl_rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_restaurant`
--
ALTER TABLE `tbl_restaurant`
  ADD PRIMARY KEY (`restaurantId`);

--
-- Indexes for table `tbl_setting`
--
ALTER TABLE `tbl_setting`
  ADD PRIMARY KEY (`settingId`);

--
-- Indexes for table `tbl_smssettings`
--
ALTER TABLE `tbl_smssettings`
  ADD PRIMARY KEY (`sms_id`);

--
-- Indexes for table `tbl_state`
--
ALTER TABLE `tbl_state`
  ADD PRIMARY KEY (`stateId`);

--
-- Indexes for table `tbl_vehicle_rental`
--
ALTER TABLE `tbl_vehicle_rental`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_adventure`
--
ALTER TABLE `tbl_adventure`
  MODIFY `adventureId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_adventure_type`
--
ALTER TABLE `tbl_adventure_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbl_area`
--
ALTER TABLE `tbl_area`
  MODIFY `areaId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_business_category`
--
ALTER TABLE `tbl_business_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_city`
--
ALTER TABLE `tbl_city`
  MODIFY `cityId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1508;
--
-- AUTO_INCREMENT for table `tbl_cms_page`
--
ALTER TABLE `tbl_cms_page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_country`
--
ALTER TABLE `tbl_country`
  MODIFY `countryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_disc`
--
ALTER TABLE `tbl_disc`
  MODIFY `discId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_disc_details`
--
ALTER TABLE `tbl_disc_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_emailsettings`
--
ALTER TABLE `tbl_emailsettings`
  MODIFY `email_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbl_member`
--
ALTER TABLE `tbl_member`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_memberoffline`
--
ALTER TABLE `tbl_memberoffline`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_property`
--
ALTER TABLE `tbl_property`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_property_map_list`
--
ALTER TABLE `tbl_property_map_list`
  MODIFY `propertyMapListId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_rating`
--
ALTER TABLE `tbl_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_restaurant`
--
ALTER TABLE `tbl_restaurant`
  MODIFY `restaurantId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_smssettings`
--
ALTER TABLE `tbl_smssettings`
  MODIFY `sms_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbl_state`
--
ALTER TABLE `tbl_state`
  MODIFY `stateId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `tbl_vehicle_rental`
--
ALTER TABLE `tbl_vehicle_rental`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
