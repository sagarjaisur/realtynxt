<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "login/index";
$route['login/changepassword'] = "login/changepassword";
$route['stars'] = "stars/index";
$route['banners'] = "banners/index";
$route['dashboard'] = "dashboard/index";
$route['users'] = "users/index";
$route['starposts'] = "starposts/index";
$route['category'] = "category/index";

//Term & Condition Section
$route['termcondition'] = "termcondition/index";

//Gallery Management
$route['gallerycollection'] = "gallerycollection/index";
$route['galleryalbum'] = "galleryalbum/index";
$route['gallerypicture'] = "gallerypicture/index";

$route['404_override'] = '';
