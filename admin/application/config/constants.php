<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/

define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);
define('ADMIN_URL', 'http://localhost/realtynxt/');
define('MAIN_DIR_URL', 'http://localhost/realtynxt/');
define('ASSET_URL', MAIN_DIR_URL . 'admin/');
define('SITE_URL', MAIN_DIR_URL . 'admin/');
define('UPLOAD_URL', MAIN_DIR_URL . 'uploads/');//Upload Directory
define('UPLOAD_PATH', $_SERVER['DOCUMENT_ROOT'] . '/realtynxt/uploads/');//Upload Directory

define('SMTP_EMAIL', 'dump@acrobat.co.in');
define('PROTOCOL', 'smtp');
define('SMTP_HOST', 'ssl://smtp.googlemail.com');
define('SMTP_PORT', '465');
define('SMTP_PASS', 'Naresh1234');
define('MAIL_TYPE', 'html');
define('APP_POST_PATH', MAIN_DIR_URL . 'uploads/posts/');
define('APP_ADMIN_IMAGE_PATH', MAIN_DIR_URL . 'admin/assets/img/');
define('LIMIT', 10);

// Google Anroid Push Notification API access key from Google API's Console
define('API_ACCESS_KEY', 'AIzaSyDZqoX1MWf8QnszcfJu4H1ycmjMUKuypUo');

/*

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');


/* End of file constants.php */
/* Location: ./application/config/constants.php */
