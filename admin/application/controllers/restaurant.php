<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Restaurant extends CI_Controller
{
    public $page_caption = 'Restaurant'; // Added For page Captions

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Restaurant_model');
        $restaurant = $this->session->userdata('memberId');

        if (empty($restaurant)) {
            redirect(base_url());
        }
    }

    public function index()
    {
        $data['restaurants'] = $this->Restaurant_model->get_restaurant();
        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('restaurant/index', $data);
        $this->load->view('elements/footer');
    }

    // Insert Restaurant
    public function add()
    {
        $this->form_validation->set_rules('restaurantName', 'Restaurant Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('location', 'Location', 'required|trim|xss_clean');
        $this->form_validation->set_rules('speciality', 'Speciality', 'required|trim|xss_clean');
        $this->form_validation->set_rules('liker', 'Who will like it?', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('restaurant/add');
            $this->load->view('elements/footer');
        } else {
            $images = @implode(',', $this->input->post('images'));

            $data = array(
                'restaurantName' => $this->input->post('restaurantName'),
                'location' => $this->input->post('location'),
                'speciality' => $this->input->post('speciality'),
                'liker' => $this->input->post('liker'),
                'images' => $images
            );

            $res = $this->Restaurant_model->add($data);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Added Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Added Successfully.</div>');
            }
            redirect(base_url() . 'restaurant/index');
        }
    }

    // Update Records
    public function edit($restaurantId)
    {
        if (empty($restaurantId)) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $data['restaurantId'] = $restaurantId;
        $this->form_validation->set_rules('restaurantName', 'Restaurant Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('location', 'Location', 'required|trim|xss_clean');
        $this->form_validation->set_rules('speciality', 'Speciality', 'required|trim|xss_clean');
        $this->form_validation->set_rules('liker', 'Who will like it?', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
            $data['detail'] = $this->Restaurant_model->get_detail($restaurantId)[0];

            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('restaurant/edit', $data);
            $this->load->view('elements/footer');

        } else {
            $data = array();
            $images = @implode(',', $this->input->post('images'));
            $data = array(
                'restaurantName' => $this->input->post('restaurantName'),
                'location' => $this->input->post('location'),
                'speciality' => $this->input->post('speciality'),
                'liker' => $this->input->post('liker'),
                'images' => $images
            );

            $where = array('restaurantId' => $restaurantId);
            $res = $this->Restaurant_model->update($data, $where);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Updated Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Updated Successfully.</div>');
            }
            redirect(base_url() . 'restaurant/index');
        }
    }

    public function delete($restaurantId)
    {
        if ($this->Restaurant_model->delete($restaurantId)) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Deleted Successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Deleted Successfully.</div>');
        }
        redirect(base_url() . 'restaurant/index');
    }

    public function status($restaurantId = '', $status = '')
    {
        if (!empty($restaurantId) && isset($status)) {

            $res = $this->Restaurant_model->changeStatus($restaurantId, $status);

            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-success"> Sorry, ' . $this->page_caption . 'status could not be changed.</div>');
            }

            $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            redirect($_SERVER['HTTP_REFERER']);
        }
        redirect(base_url() . 'restaurant/index');
    }

}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */