<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Smssettings extends CI_Controller
{
    public $page_caption = 'SMS'; // Added For page Captions

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Smssettings_model');
        $user = $this->session->userdata('memberId');

        /*
                $this->load->library('ckeditor');
                $this->load->library('ckFinder');
                //configure base path of ckeditor folder
                $this->ckeditor->basePath = ASSET_URL . 'assets/ckeditor/';

                //$this->ckeditor->config['toolbar'] = 'Basic';
                //$this->ckeditor->config['toolbar'] = 'Basic';
                $this->ckeditor->config['scayt_autoStartup'] = TRUE;
                $this->ckeditor->config['resize_enabled'] = FALSE;
                $this->ckeditor->config['language'] = 'en';
                $this->ckeditor->config['enterMode'] = 'CKEDITOR.ENTER_BR';
                //CKFinder Config
                $this->ckeditor->config['filebrowserBrowseUrl'] = ASSET_URL . 'assets/ckfinder/ckfinder.html';
                $this->ckeditor->config['filebrowserImageBrowseUrl'] = ASSET_URL . 'assets/ckfinder/ckfinder.html?type=Images';
                $this->ckeditor->config['filebrowserFlashBrowseUrl'] = ASSET_URL . 'assets/ckfinder/ckfinder.html?type=Flash';
                $this->ckeditor->config['filebrowserUploadUrl'] = ASSET_URL . 'assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
                $this->ckeditor->config['filebrowserImageUploadUrl'] = ASSET_URL . 'assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
                $this->ckeditor->config['filebrowserFlashUploadUrl'] = ASSET_URL . 'assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
        */
        if (empty($user)) {
            redirect(base_url());
        }
    }

    public function index()
    {
        $data['details'] = $this->Smssettings_model->getSmssTemplates();
        //print_r($data);exit('ds');
        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('smssettings/index', $data);
        $this->load->view('elements/footer');
    }

    // Add new SMS Template
    public function add()
    {
        $this->form_validation->set_rules('smsTemplate', 'SMS Template', 'required|trim|xss_clean');
        $this->form_validation->set_rules('smsSubject', 'SMS Subject', 'required|trim|xss_clean');
        $this->form_validation->set_rules('smsBody', 'SMS Body', 'required|trim|xss_clean');
        echo $this->form_validation->run();

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('smssettings/add');
            $this->load->view('elements/footer');
        } else {
            $data = array('smsTemplate' => $this->input->post('smsTemplate'),
                'smsSubject' => $this->input->post('smsSubject'),
                'smsBody' => $this->input->post('smsBody'));


            $res = $this->Smssettings_model->add($data);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Added Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Added Successfully.</div>');
            }
            redirect(base_url() . 'smssettings/index');
        }

    }

    // Update Records
    public function edit($id)
    {
        if (empty($id)) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $data['id'] = $id;
        $this->form_validation->set_rules('smsTemplate', 'SMS Template', 'required|trim|xss_clean');
        $this->form_validation->set_rules('smsSubject', 'SMS Subject', 'required|trim|xss_clean');
        $this->form_validation->set_rules('smsBody', 'SMS Body', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
            $data['details'] = $this->Smssettings_model->getSmsTemplateById($id);
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('smssettings/edit', $data);
            $this->load->view('elements/footer');
        } else {
            $data = array();

            $where = array('smsId' => $id);
            $data = array('smsTemplate' => $this->input->post('smsTemplate'),
                'smsSubject' => $this->input->post('smsSubject'),
                'smsBody' => $this->input->post('smsBody'));


            $res = $this->Smssettings_model->update($data, $where);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Updated Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Updated Successfully.</div>');
            }
            redirect(base_url() . 'smssettings/index');
        }

    }

    public function delete($id)
    {
        if ($this->Smssettings_model->delete($id)) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Deleted Successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Deleted Successfully.</div>');
        }

        redirect(base_url() . 'smssettings/index');
    }


}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */