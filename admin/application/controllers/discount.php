<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Discount extends CI_Controller
{
    public $page_caption = 'Special Discount'; // Added For page Captions

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Discount_model');
        $document = $this->session->userdata('memberId');

        if (empty($document)) {
            redirect(base_url());
        }
    }

    public function index()
    {
        $data['documents'] = $this->Discount_model->get_document();
        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('discount/index', $data);
        $this->load->view('elements/footer');
    }

    // Insert Document
    public function add()
    {
        $this->form_validation->set_rules('discountSchemeTitle', 'Special Discount Title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('projectId', 'Select Project', 'required|trim|xss_clean');
        $this->form_validation->set_rules('percentage', 'Percentage', 'required|trim|xss_clean');
        $this->form_validation->set_rules('assignToRoleId', 'Assign Role', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('discount/add');
            $this->load->view('elements/footer');
        } else {
//            $images = @implode(',', $this->input->post('images'));

            $data = array(
                'name' => $this->input->post('name'),
                'type' => $this->input->post('type')
            );

            $res = $this->Discount_model->add($data);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Added Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Added Successfully.</div>');
            }
            redirect(base_url() . 'discount/index');
        }
    }

    // Update Records
    public function edit($documentId)
    {
        if (empty($documentId)) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $data['documentId'] = $documentId;
        $this->form_validation->set_rules('name', 'Document Title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('type', 'Document Type', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
            $data['detail'] = $this->Discount_model->get_detail($documentId)[0];

            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('discount/edit', $data);
            $this->load->view('elements/footer');

        } else {
            $data = array();
            $images = @implode(',', $this->input->post('images'));
            $data = array(
                'name' => $this->input->post('name'),
                'type' => $this->input->post('type')
            );

            $where = array('documentId' => $documentId);
            $res = $this->Discount_model->update($data, $where);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Updated Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Updated Successfully.</div>');
            }
            redirect(base_url() . 'discount/index');
        }
    }

    public function delete($documentId)
    {
        if ($this->Discount_model->delete($documentId)) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Deleted Successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Deleted Successfully.</div>');
        }
        redirect(base_url() . 'discount/index');
    }

    public function status($documentId = '', $status = '')
    {
        if (!empty($documentId) && isset($status)) {

            $res = $this->Discount_model->changeStatus($documentId, $status);

            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-success"> Sorry, ' . $this->page_caption . 'status could not be changed.</div>');
            }

            $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            redirect($_SERVER['HTTP_REFERER']);
        }
        redirect(base_url() . 'discount/index');
    }

}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */