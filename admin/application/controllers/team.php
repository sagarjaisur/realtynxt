<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Team extends CI_Controller
{
    public $page_caption = 'User'; // Added For page Captions
    private $siteDefaultData = array();

    public function __construct()
    {
        parent::__construct();

        $this->siteDefaultData['setting'] = $this->db->query('SELECT *  FROM tbl_setting')->result_array();
        foreach ($this->siteDefaultData['setting'] as $key => $value) {
            $formatedData[$value['settingName']] = $value['settingValue'];
        }
        $this->siteDefaultData['setting'] = $formatedData;

        $this->load->model('Team_model');
        $this->load->model('Region_model');

        $user = $this->session->userdata('memberId');
        $this->load->library('pagination');
        $this->load->helper('url');

        // For email management
        $this->load->library('email');
        //Start -  Configuration Setting For Emails
        $this->email->protocol = $this->siteDefaultData['setting']['EMAIL_SMTP_PROTOCOL'];
        $this->email->smtp_host = $this->siteDefaultData['setting']['EMAIL_SMTP_HOST'];
        $this->email->smtp_timeout = $this->siteDefaultData['setting']['EMAIL_SMTP_TIMEOUT'];
        $this->email->smtp_user = $this->siteDefaultData['setting']['EMAIL_SMTP_USER'];
        $this->email->smtp_pass = $this->siteDefaultData['setting']['EMAIL_SMTP_PASSWORD'];
        $this->email->smtp_port = $this->siteDefaultData['setting']['EMAIL_SMTP_PORT'];
        $this->email->mailtype = $this->siteDefaultData['setting']['EMAIL_SMTP_MAILTYPE'];
        $this->email->smtp_crypto = $this->siteDefaultData['setting']['EMAIL_SMTP_CRYPTO'];

        // End  -  Configuration Setting For Emails

        $this->email->initialize();

        if (empty($user)) {
            redirect(base_url());
        }
    }

    public function index()
    {
        $data['default_roles'] = $this->roleTitle();
        /* $search = '';
         if(isset($_POST['search']) && !empty($_POST['search'])){
             $data['search'] = $_POST['search'];
             $search = $_POST['search'];
             $this->session->set_userdata('search', $_POST['search']);
         }else{
             $data['search'] = $this->session->userdata('search');
             $search = $data['search'];
         }

         if(isset($_POST['clearsearch']) && empty($_POST['clearsearch'])){
             $this->session->unset_userdata('search');
             $data['search'] = '' ;
             $search = '';
         }
         */
        //Start - Pagination Setup
        $totalRecodeCount = $this->Team_model->recode_count();
        $config['base_url'] = base_url() . 'team/index/';
        $config['total_rows'] = $totalRecodeCount;
        $config['per_page'] = 25;
        $config["uri_segment"] = 3;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm inline">';
        $config['full_tag_close'] = '</ul>';
        $config['prev_link'] = '&lt;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&gt;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="current"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

//            $config['first_link'] = '&lt;&lt;';
        $config['first_link'] = '&laquo;';
//            $config['last_link'] = '&gt;&gt;';
        $config['last_link'] = '&raquo;';

        $data['links'] = $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data["users"] = $this->Team_model->getUsersRequest(25, $page, '');

        $data["links"] = $this->pagination->create_links();
        //End - Pagination Setup


        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('team/index', $data);
        $this->load->view('elements/footer');
    }

    public function logout()
    {
        $this->session->unset_userdata('user');
        $this->session->sess_destroy();
        redirect(base_url());
    }

    /*    public function status($userId = '', $status = '')
        {
            if (!empty($userId) && !empty($status)) {
                $data = $this->Team_model->changeStatus($userId, $status);
                $this->session->set_flashdata('success', '<div class="alert alert-success">Account status has been changed successfully.</div>');
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    */

    /** For Guest Managemnt **/
    public function guestmgmt()
    {

        $data['default_roles'] = $this->roleTitle();

        $data['users'] = $this->Team_model->getGuestUser();
        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('team/index', $data);
        $this->load->view('elements/footer');
    }

    /** For Memeber Managemnt **/
    public function membermgmt()
    {
        $data['default_roles'] = $this->roleTitle();

        $data['users'] = $this->Team_model->getMemberUser();
        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('team/index', $data);
        $this->load->view('elements/footer');
    }

    /** Function to add User **/
    public function addUser($roleId)
    {
        // Form Validation Start
        //$this->form_validation->set_rules('membershipPlanId', 'Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('firstName', 'First Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('lastName', 'Last Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('contactAddress', 'Contact Address', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('country', 'Country', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('state', 'State', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('city', 'City', 'required|trim|xss_clean');
        $this->form_validation->set_rules('mobileNumber', 'Mobile Number', 'required|trim|xss_clean');
        $this->form_validation->set_rules('email', 'Email ID', 'required|trim|xss_clean|valid_email|is_unique[tbl_member.email]');
//        $this->form_validation->set_rules('alternativeEmail', 'Alternative Email ID', 'required|trim|xss_clean|valid_email|is_unique[tbl_member.alternativeEmail]');

//        $this->form_validation->set_rules('mobileNumForSms', 'Mobile Number for SMS', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('mobileVerifiedStatus', 'Mobile Number Verified Status', 'required|trim|xss_clean');

//        $this->form_validation->set_rules('landlineNumber', 'Landline Number', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('fax', 'Fax', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('hideContactDetailsStatus', 'Hide Contact Details Status', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('receiveEmailsOnAltEmailStatus', 'Receive Emails On Alternative Email Status', 'required|trim|xss_clean');

        /*        $this->form_validation->set_rules('mobileNumber',
                        array(
                            function($mobileNumber)
                            {
                                // Check $value
                                $count = $this->db->query('SELECT * FROM `tbl_member` WHERE mobileNumber ='.$mobileNumber)->num_rows();
                                die($count);
                                if ($count > 0)
                                {
                                    $this->form_validation->set_message('mobileNumberCheck', "A user is already registered with this mobile number!");
                                    return FALSE;
                                }
                                else
                                {
                                    return TRUE;
                                }
                            }
                        )
                    );
        */

        /*
                $allCitiesData = $this->Region_model->get_Region('tbl_city','','', array('status' => 1, 'stateId' => 1));
                $dataX['CitiesList'] = array();
                foreach ($allCitiesData as $key => $CityData) {
                    $dataX['CitiesList'] += array(
                        $CityData['cityName'] => $CityData['cityName']
                    );
                }
        */
        $dataX['CitiesList'] = array('' => ' Please Select State');

//        if ($roleId == 1 || $roleId == 2) {
        $this->form_validation->set_rules('username', 'Username', 'required|trim|xss_clean|is_unique[tbl_member.username]|alpha_dash');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('password1', 'Confirm Password', 'required|matches[password]');
//        }

        if ($this->form_validation->run() === FALSE) {
            //$data['countries'] = $this->db->query('SELECT *  FROM tbl_country')->result_array();
            $dataX['states'] = $this->db->query('SELECT *  FROM tbl_state')->result_array();

            $dataX['roleId'] = $roleId;
            $dataX['default_roles'] = $this->roleTitle();

            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('team/add_user', $dataX);
            $this->load->view('elements/footer');
        } else {
            if (!empty($_FILES) and $_FILES['image']['name'] != '') {
                $uploadResult = $this->do_upload();

                if (!array_key_exists('error', $uploadResult)) {
                    $image = UPLOAD_URL . 'team/' . $uploadResult['upload_data']['file_name'];
                } else {
                    //$data['countries'] = $this->db->query('SELECT *  FROM tbl_country')->result_array();
                    $data['states'] = $this->db->query('SELECT *  FROM tbl_state')->result_array();

                    $data['roleId'] = $roleId;
                    $data['default_roles'] = $this->roleTitle();
                    $data['error'] = $uploadResult['error'];

                    $this->load->view('elements/header');
                    $this->load->view('elements/page_header_sidebar');
                    $this->load->view('team/add_user', $data);
                    $this->load->view('elements/footer');
                }
            } else {
                $image = $this->input->post('imageName');
            }
            //  Add RM Section
            $dataOfRm  =  $this->input->post('rmIds');            
            
            if($dataOfRm && count($dataOfRm) >  0  ){
                $rmIds =  implode(',',$dataOfRm);
            }else {
                $rmIds = '';
            }

            $data = array(
                'roleId' => $roleId,
//                'membershipPlanId' => $this->input->post('membershipPlanId'),
                "rmIds" => $rmIds,
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password'),
                'email' => $this->input->post('email'),
                'alternativeEmail' => $this->input->post('alternativeEmail'),
                'firstName' => $this->input->post('firstName'),
                'lastName' => $this->input->post('lastName'),
                'contactAddress' => $this->input->post('contactAddress'),

//                'country' => $this->input->post('country'),
                'country' => $this->input->post('country_default'),

                'state' => $this->input->post('state'),
                'city' => $this->input->post('city'),
                'mobileNumber' => $this->input->post('mobileNumber'),

//                'mobileNumForSms' => $this->input->post('mobileNumForSms'),
//                'mobileVerifiedStatus' => $this->input->post('mobileVerifiedStatus'),

                'landlineNumber' => $this->input->post('landlineNumber'),
                'fax' => $this->input->post('fax'),
                'hideContactDetailsStatus' => ($this->input->post('hideContactDetailsStatus') == false) ? 0 : 1,
                'receiveEmailsOnAltEmailStatus' => ($this->input->post('receiveEmailsOnAltEmailStatus') == false) ? 0 : 1,
                'image' => $image
            );


            if ($roleId == 1 || $roleId == 2) {
                $adminData = array(
                    'login' => $this->input->post('login'),
                    'password' => $this->input->post('password'),
                    'email' => $this->input->post('email')
                );

                $res = $this->Team_model->addAdmin($data, $adminData);
            } else {
                $res = $this->Team_model->addUser($data);
            }


            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Added Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">Sorry, ' . $this->page_caption . '  could not be added. Please try again.</div>');
            }
            $redirectURL = "team/userrequest/" . $roleId;
            redirect(base_url() . $redirectURL);
        }
    }

    // Callback function for unique mobile number validation
    public function mobilenumber_check($mobileNumber)
    {
        $count = $this->db->query('SELECT * FROM `tbl_member` WHERE mobileNumber =' . $mobileNumber)->num_rows();
        die($count);
        if ($count > 0) {
            $this->form_validation->set_message('mobileNumberCheck', "A user is already registered with this mobile number!");
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /** Function to get all users for dashboard **/
    public function userdetails($roleId, $uid) // $roleId : For highlighting active submenu item in Sidebar
    {

        //$data['countries'] = $this->db->query('SELECT *  FROM tbl_country')->result_array();
        $data['states'] = $this->db->query('SELECT *  FROM tbl_state')->result_array();
        $data['user'] = $this->Team_model->getUserById($uid);
//        $data['admin'] = $this->Team_model->getAdminByEmail($data['user'][0]['email']);
        $data['admin'] = $this->Team_model->getAdminById($uid);

        $stateId = $this->db->query("SELECT stateId FROM tbl_state WHERE stateName LIKE '" . $data['user'][0]['state'] . "';")->result_array()[0]['stateId'];
        $relatedCitiesData = $this->Region_model->get_Region('tbl_city', '', '', array('status' => 1, 'stateId' => $stateId));
        $data['CitiesList'] = array('' => 'Select City');
        foreach ($relatedCitiesData as $key => $CityData) {
            $data['CitiesList'] += array(
                $CityData['cityName'] => $CityData['cityName']
            );
        }


        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('team/edit_user', $data);
        $this->load->view('elements/footer');
    }

    /** Function update user details from admin **/
    public function updateUser($uid)
    {
//        $id = $this->uri->segment(3);
        $users = $this->Team_model->getUserById($uid);
        $admin = $this->Team_model->getAdminById($uid);

//        print_r($users);
//        die('updateUser');

        // Form Validation Start
        //$this->form_validation->set_rules('membershipPlanId', 'Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('firstName', 'First Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('lastName', 'Last Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('contactAddress', 'Contact Address', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('country', 'Country', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('state', 'State', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('city', 'City', 'required|trim|xss_clean');
        $this->form_validation->set_rules('mobileNumber', 'Mobile Number', 'required|trim|xss_clean');
        $this->form_validation->set_rules('email', 'Email ID', 'required|trim|xss_clean|valid_email');
//        $this->form_validation->set_rules('alternativeEmail', 'Alternative Email ID', 'required|trim|xss_clean|valid_email|is_unique[tbl_member.alternativeEmail]');

//        $this->form_validation->set_rules('mobileNumForSms', 'Mobile Number for SMS', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('mobileVerifiedStatus', 'Mobile Number Verified Status', 'required|trim|xss_clean');

//        $this->form_validation->set_rules('landlineNumber', 'Landline Number', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('fax', 'Fax', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('hideContactDetailsStatus', 'Hide Contact Details Status', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('receiveEmailsOnAltEmailStatus', 'Receive Emails On Alternative Email Status', 'required|trim|xss_clean');

//        if ($users[0]['roleId'] == 1 || $users[0]['roleId'] == 2) {
        $this->form_validation->set_rules('username', 'Username', 'required|trim|xss_clean|alpha_dash');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('password1', 'Confirm Password', 'required|matches[password]');
//        }

        $allCitiesData = $this->Region_model->get_Region('tbl_city', '', '', array('status' => 1));
        $dataX['CitiesList'] = array('' => 'Select City');
        foreach ($allCitiesData as $key => $CityData) {
            $dataX['CitiesList'] += array(
                $CityData['cityName'] => $CityData['cityName']
            );
        }

        if ($this->form_validation->run() === FALSE) {
            //$data['countries'] = $this->db->query('SELECT *  FROM tbl_country')->result_array();
            $dataX['states'] = $this->db->query('SELECT *  FROM tbl_state')->result_array();
            $dataX['roleId'] = $users[0]['roleId'];
            $dataX['default_roles'] = $this->roleTitle();

            $dataX['user'] = $users;
            $dataX['admin'] = $admin;

            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('team/edit_user', $dataX);
            $this->load->view('elements/footer');
        } else {
            if (!empty($_FILES) and $_FILES['image']['name'] != '') {
                $uploadResult = $this->do_upload();
                
                if (!array_key_exists('error', $uploadResult)) {
                    $image = UPLOAD_URL . 'team/' . $uploadResult['upload_data']['file_name'];
                } else {
                    //$data['countries'] = $this->db->query('SELECT *  FROM tbl_country')->result_array();
                    $data['states'] = $this->db->query('SELECT *  FROM tbl_state')->result_array();
                    $data['roleId'] = $users[0]['roleId'];
                    $data['default_roles'] = $this->roleTitle();
                    $data['error'] = $uploadResult['error'];

                    $data['user'] = $users;

                    $this->load->view('elements/header');
                    $this->load->view('elements/page_header_sidebar');
                    $this->load->view('team/edit_user', $data);
                    $this->load->view('elements/footer');
                }
            } else {
                $image = $this->input->post('imageName');
            }
             
             //  Add RM Section
            $dataOfRm  =  $this->input->post('rmIds');            
            
            if($dataOfRm && count($dataOfRm) >  0  ){
                $rmIds =  implode(',',$dataOfRm);
            }else {
                $rmIds = '';
            }
            $data = array(
                'roleId' => $users[0]['roleId'],
//                'membershipPlanId' => $this->input->post('membershipPlanId'),
                'rmIds' =>$rmIds,
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password'),
                'email' => $this->input->post('email'),
                'alternativeEmail' => $this->input->post('alternativeEmail'),
                'firstName' => $this->input->post('firstName'),
                'lastName' => $this->input->post('lastName'),
                'contactAddress' => $this->input->post('contactAddress'),
                'image' => $image,
                'country' => $this->input->post('country_default'),

                'state' => $this->input->post('state'),
                'city' => $this->input->post('city'),
                'mobileNumber' => $this->input->post('mobileNumber'),

//                'mobileNumForSms' => $this->input->post('mobileNumForSms'),
//                'mobileVerifiedStatus' => $this->input->post('mobileVerifiedStatus'),

                'landlineNumber' => $this->input->post('landlineNumber'),
                'fax' => $this->input->post('fax'),
                'hideContactDetailsStatus' => ($this->input->post('hideContactDetailsStatus') == false) ? 0 : 1,
                'receiveEmailsOnAltEmailStatus' => ($this->input->post('receiveEmailsOnAltEmailStatus') == false) ? 0 : 1,
                'modifiedDate' => date('Y-m-d H:i:s')
            );

//                die('roleId:'.$users['roleId']);
            if ($users[0]['roleId'] == 1 || $users[0]['roleId'] == 2) {
                $adminData = array('login' => $this->input->post('login'),
                    'password' => $this->input->post('password'),
                    'email' => $this->input->post('email')
                );

                $res = $this->Team_model->updateAdminDetails($users[0]['email'], $data, $adminData);
//                    die("res:$res");
            } else {
//                die('wrong');
                $res = $this->Team_model->updateMemberDetails($uid, $data);
            }

            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">Profile Updated Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">Sorry, profile could not be updated. Please try again.</div>');
            }
            $redirectURL = "team/userrequest/" . $users[0]['roleId'];

            redirect(base_url() . $redirectURL);


            /*            //$data['countries'] = $this->db->query('SELECT *  FROM tbl_country')->result_array();
                        $data['states'] = $this->db->query('SELECT *  FROM tbl_state')->result_array();
                        $data['roleId'] = $users[0]['roleId'];
                        $data['default_roles'] = $this->roleTitle();
                        $data['error'] = $uploadResult['error'];

                        $data['user'] = $users;

                        $this->load->view('elements/header');
                        $this->load->view('elements/page_header_sidebar');
                        $this->load->view('team/edit_user', $data);
                        $this->load->view('elements/footer');
            */
        }
    }


    // Update member role
    public function updateRoleMember()
    {
        $id = $this->uri->segment(3);
        $role = $this->uri->segment(4);

        $res = $this->Team_model->updateRoleMember($id, $role);

        if ($res) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">Role updated successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">Role could not be updated. Please try again.</div>');
        }
        redirect(base_url() . 'team/userrequest/' . $role);
    }

    // Upload  Proccess
    public function do_upload()
    {
        if (!empty($_FILES['image']['name'])) {
            $ext = pathinfo($_FILES['image']['name']);
            $pic = uniqid() . '.' . $ext['extension'];
        }

        $config['file_name'] = $pic;
        $config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . '/realtynxt/uploads/team/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('image')) {
            $error = array('error' => $this->upload->display_errors());
            return $error;
        } else {
            $data = array('upload_data' => $this->upload->data());
            return $data;
        }
    }

    public function changeuserstatus()
    {
        $data = $this->siteDefaultData;

        $id = $this->uri->segment(3);
        $status = $this->uri->segment(4);

        $data['users'] = $this->Team_model->get_User_Record($id);

        $res = $this->Team_model->changeUserStatus($id, $status);
        if ($res) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">Action updated successfully.</div>');

            // Mail Sending to admin and User
            if ($status && $status == "1") {
                $admin_data = $this->db->query('SELECT *  FROM tbl_emailsettings where email = 10')->result_array();

                $this->email->from($data["setting"]["DISPLAY_EMAIL"], $data["setting"]["SITE_TITLE"]);
                $this->email->to($data['users'][0]['email']);
                $this->email->cc("sagar@acrobat.co.in");

                $subject = $admin_data[0]["emailSubject"];
                $this->email->subject($subject);

                $message = $admin_data[0]["emailBody"];
                // Replace Specific keywords constant content
                $message = str_replace('[USERNAME]', $data['users'][0]["name"], $message);

                $this->email->message($message);
                $this->email->send();
                // Mail Sending here
            } else if ($status && $status == "2") {
                $admin_data = $this->db->query('SELECT *  FROM tbl_emailsettings where email = 11')->result_array();

                $this->email->from($data["setting"]["DISPLAY_EMAIL"], $data["setting"]["SITE_TITLE"]);
                $this->email->to($data['users'][0]['email']);
                $this->email->cc("sagar.jaisur@acrobat.co.in");

                $subject = $admin_data[0]["emailSubject"];
                $this->email->subject($subject);

                $message = $admin_data[0]["emailBody"];
                // Replace Specific keywords constant content
                $message = str_replace('[USERNAME]', $data['users'][0]['name'], $message);

                $this->email->message($message);
                $this->email->send();
                // Mail Sending here
            }
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">Action does not updated.</div>');
        }
        $redirectURL = "team/userrequest/" . $data['users'][0]['roleId'];
        redirect(base_url() . $redirectURL);
    }

    public function userrequest($roleId = NULL)
    {
        $data['roleId'] = $roleId;
        $data['default_roles'] = $this->roleTitle();

        //Start - Pagination Setup
        $totalRecodeCount = $this->Team_model->recode_count();
        $config['base_url'] = base_url() . 'team/userrequest/' . $roleId . '/';
        $config['total_rows'] = $totalRecodeCount;
        $config['per_page'] = 25;
        $config["uri_segment"] = 4;
//            $config['full_tag_open'] = '<ul class="pagination pagination-sm inline">';
        $config['full_tag_open'] = '<ul class="pagination pagination-sm inline">';
        $config['full_tag_close'] = '</ul>';
        $config['prev_link'] = '&lt;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&gt;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="current active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['first_link'] = '&laquo;';
        $config['first_link'] = '&raquo;';

        $data['links'] = $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $data["users"] = $this->Team_model->getUsersRequest(25, $page, array('roleId' => $roleId));

        $data["links"] = $this->pagination->create_links();

        //$data['users'] = $this->Team_model->getUsersRequest();
        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('team/index', $data);
        $this->load->view('elements/footer');
    }

    public function roleTitle()
    {
        $this->db->select('roleId,roleTitle');
        $this->db->where('rolePermission', 'Enabled');
        $this->db->from('tbl_members_roles');
        $query = $this->db->get();
        if ($query->num_rows()) {
            $data = $query->result_array();
            $finalData = array();
            foreach ($data as $key => $value) {
                $finalData[$value['roleId']] = $value['roleTitle'];
            }
            return $finalData;
        } else {
            return false;
        }
    }

    function checkForWebname($str = '', $uid = '')
    {
        if ($str == '')
            $str = $_POST['text'];

        if ($uid == '')
            $uid = $_POST['memberId'];

        $data = $this->Team_model->checkForUniqueWsername($str, $uid);

        if ($data == $str) {
            echo 0;
        } else {
            echo $data;
        }
        exit;
    }

    function checkForNominee()
    {
        if (isset($_POST['keyword']) && $_POST['keyword'] != '') {
            $data = $this->Team_model->checkForNomineeList($_POST['keyword']);
            if ($data > 0) {
                echo 'success';
            } else {
                echo 'error';
            }
            exit;
        }
    }

    function stateFind()
    {
        $countryId = $_POST['countryId'];
        if (isset($countryId) && $countryId != '') {
            $data = $this->Team_model->checkForState($countryId);

            if (count($data) > 1) {
                echo json_encode($data);
                //return $data;
            } else {
                echo json_encode("error");
            }
            exit;
        }
    }

    //AJAX call for dynamically populating Cities according to selected State
    function cityFind()
    {
        $stateName = $_POST['stateName'];
        $stateResId = $this->db->query("SELECT stateId FROM tbl_state WHERE stateName LIKE '" . $stateName . "'")->result_array();
        $stateId = $stateResId[0]['stateId'];

        if (isset($stateId) && $stateId != '') {
            $data = $this->Team_model->checkForCity($stateId);
            if (count($data) > 0 && !empty($data)) {
                echo json_encode($data);
                //return $data;
            } else {
                echo json_encode("error");
            }
            exit;
        }
    }


    //Delete User
    public function delete($userId, $roleId)
    {
        if ($this->Team_model->delete($userId)) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Deleted Successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Deleted Successfully.</div>');
        }
        redirect(base_url() . 'team/userrequest/' . $roleId);
    }

    //  Send  Push On reject  Status
    public function sendPushNotification($deviceIds = "APA91bFOWL5cS1WdWfL-mi5BwoQ_Kx_VTMI5NKXzdtpB9fhFadUv-tWI-ly1gIEb9fhEt_vE93-aWydELuFHGs-RmrjCo10OOoYsC-lcfluhaZ5Mm4IsIo4LzxLzs90vKXAV5tqMtxyO")
    {

        $registrationIds = array($deviceIds);
        // prep the bundle
        $msg = array
        (
            'message' => 'User Rejected',
            'title' => 'User Rejected',
            'subtitle' => 'User Rejected',
            'tickerText' => 'User Rejected',
            'vibrate' => 1,
            'sound' => 1,
            'largeIcon' => 'large_icon',
            'type' => '6',
            'smallIcon' => 'small_icon'
        );
        $fields = array
        (
            'registrationIds' => $registrationIds,
            'data' => $msg
        );

        $headers = array
        (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);

    }

}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */