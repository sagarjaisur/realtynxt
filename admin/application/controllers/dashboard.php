<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller
{


    public function __construct()
    {

        parent::__construct();

        $this->load->model('login_model');
        //$this->load->model('star_model');
        //$this->load->model('starposts_model');
        $user = $this->session->all_userdata();
        if (empty($user['memberId'])) {
            redirect(SITE_URL);
        }


        //For Active Roles to be displayed in "User Management" in Sidebar
        $this->load->model('Role_model');
        $activeRoles = $this->Role_model->get_role(array('rolePermission' => 'Enabled'));
        $this->session->set_userdata('activeRoles', $activeRoles);


        //For Regions to be displayed in "Region Management" in Sidebar
        $regionTypes = array(
            1 => 'area',
            2 => 'city',
            3 => 'state',
            4 => 'country'
        );
        $this->session->set_userdata('regionTypes', $regionTypes);


        //For New User Notification in Header
        $uid = $user['memberId'];
        $newUsersArr = $this->db->query("SELECT mem.email FROM tbl_member AS mem, tbl_admin AS adm where adm.memberId = $uid AND mem.registerDate >= adm.lastLogin AND mem.registerDate != '0000-00-00' AND adm.lastLogin != '0000-00-00'")->result_array();
        // $newUsersArr = $this->db->query("SELECT mem.email FROM tbl_member AS mem, tbl_admin AS adm where mem.registerDate >= adm.lastLogin AND mem.registerDate != '0000-00-00' AND adm.lastLogin != '0000-00-00'")->result_array();
        $newUsers = array();
        foreach ($newUsersArr AS $key => $value) {
            $newUsers[$key] = $value['email'];
        }
        $this->session->set_userdata('newUsers', $newUsers);
    }

    public function index()
    {
        $data = array();

        $data['fan'] = $this->db->query('SELECT count(*) as totaluser FROM tbl_member where status = 1')->result_array();
        //$data['facebook_fan'] = $this->db->query('SELECT count(*) as total_fb_user FROM login where eStatus = "Active" and vLogin_type= "facebook"')->result_array();
        //$data['star'] = $this->db->query('SELECT count(*) as totalstar FROM stars where eStatus = "Active"')->result_array();
        //$data['post_text'] = $this->db->query("SELECT count(*) as total_text_post FROM post_master where eStatus = 'Active' and vPost_type = 'text'")->result_array();
        //$data['post_image'] = $this->db->query("SELECT count(*) as total_img_post FROM post_master where eStatus = 'Active' and vPost_type = 'image'")->result_array();
        //$data['post_audio'] = $this->db->query("SELECT count(*) as total_audio_post FROM post_master where eStatus = 'Active' and vPost_type = 'audio'")->result_array();
        //$data['post_video'] = $this->db->query("SELECT count(*) as total_video_post FROM post_master where eStatus = 'Active' and vPost_type = 'video'")->result_array();
        //$data['post_total'] = $this->db->query("SELECT count(*) as total_post FROM post_master where eStatus = 'Active'")->result_array();

        /*$data['most_likes_celebs'] = $this->db->query("SELECT vStar_name, vStar_lastName, vStar_phone, vStar_like_count as starlikes "
                                                    . "FROM stars where vStar_like_count > 0 "
                                                    . "order by vStar_like_count DESC LIMIT 0, 5")->result_array();

        $data['top_post_likes'] = $this->db->query("SELECT s.vStar_name, s.vStar_lastName, s.vStar_phone ,SUM(`iPost_like_count`) as iPost_like_count
                                                    FROM post_master
                                                    LEFT JOIN stars as s ON s.`iStarId` = post_master.`iStarId`
                                                    WHERE iPost_like_count > 0
                                                    GROUP BY post_master.`iStarId` ORDER BY iPost_like_count DESC LIMIT 0, 5")->result_array();
        */
        //$data['recent_join'] = $this->db->query("SELECT vStar_name, vStar_lastName,vStar_phone, dDate_time from stars ORDER BY dDate_time DESC LIMIT 0, 5")->result_array();
        /*$data['most_liking_post'] = $this->db->query("SELECT vStar_name, vStar_lastName, iPost_like_count, lPost_text, vPost_image, vPost_audio_url, vPost_video_url
                                    FROM post_master
                                    LEFT JOIN stars ON stars.`iStarId` = post_master.`iStarId`
                                    WHERE iPost_like_count > 0
                                    ORDER BY iPost_like_count DESC
                                    LIMIT 0, 5")->result_array();
        */

        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('dashboard/index', $data);
        $this->load->view('elements/footer');
    }

    public function analytics()
    {
        $data['countries'] = $this->db->query('SELECT id, location  FROM country_state_city where type = "CO" ORDER BY location ASC')->result_array();

        $sql = '';
        $groupby = '';
        $statewhere = '';
        $starid = $countryid = $stateid = '';

        if (isset($_POST['starname']) && !empty($_POST['starname'])) {
            $data['select_star'] = $_POST['starname'];
            $starid = $_POST['starname'];
        }

        if (isset($_POST['countyname']) && !empty($_POST['countyname'])) {
            $countryid = $_POST['countyname'];
        }

        if (isset($_POST['statename']) && !empty($_POST['statename'])) {
            $stateid = $_POST['statename'];
        }


        if (isset($_POST['starname']) && !empty($_POST['starname'])) {   //$_POST['starname'] is star id

            $data['select_county'] = $_POST['countyname'];

            $xSQL1 = ' (vStar_list LIKE "' . $_POST['starname'] . ',%" '
                . 'OR vStar_list LIKE "%,' . $_POST['starname'] . '" '
                . 'OR vStar_list LIKE "%,' . $_POST['starname'] . ',%" '
                . 'OR vStar_list = "' . $_POST['starname'] . '") ';
            $sql = $xSQL1;
        }

        if (isset($_POST['countyname']) && !empty($_POST['countyname'])) {
            $data['select_county'] = $_POST['countyname'];

            $countryId = $this->db->query('SELECT id, location  FROM country_state_city where type = "CO" AND id = "' . $_POST['countyname'] . '" ORDER BY location ASC')->result_array();
            $statewhere = " AND in_location= '" . $_POST['countyname'] . "'";
            $groupby = ' group by state';

            if (!empty($xSQL1)) {
                $xSQL2 = ' AND country = "' . trim($countryId[0]['location']) . '" ';
            } else {
                $xSQL2 = ' country = "' . trim($countryId[0]['location']) . '" ';
            }
            $sql .= $xSQL2;
        }

        if (isset($_POST['statename']) && !empty($_POST['statename'])) {
            $data['select_state'] = $_POST['statename'];
            $stateId = $this->db->query('SELECT id, location  FROM country_state_city where type = "RE" AND id = "' . $_POST['statename'] . '" ORDER BY location ASC')->result_array();
            $groupby = 'group by city';

            if (!empty($xSQL2)) {
                $xSQL3 = ' AND state = "' . trim($stateId[0]['location']) . '" ';
            } else {
                $xSQL3 = ' state = "' . trim($stateId[0]['location']) . '" ';
            }

            $sql .= $xSQL3;
        }


        if (!empty($starid) && empty($countryid) && empty($stateid)) {

            $data['stars'] = $this->db->query('SELECT COUNT(iUserId) as follower, country, '
                . '(SELECT iStarId FROM stars WHERE iStarId =' . $_POST['starname'] . ') AS starid,'
                . '(SELECT vStar_name FROM stars WHERE iStarId =' . $_POST['starname'] . ') AS starname,'
                . '(SELECT vStar_email FROM stars WHERE iStarId =' . $_POST['starname'] . ') AS staremail,'
                . '(SELECT vStar_phone FROM stars WHERE iStarId =' . $_POST['starname'] . ') AS starphone '
                . ' FROM login '
                . ' WHERE  ' . $sql
                . $groupby
                . ' ORDER BY vFirst_name ASC')->result_array();
        }

        if (!empty($starid) && !empty($countryid) && empty($stateid)) {

            $data['stars'] = $this->db->query('SELECT COUNT(iUserId) as follower, country, state, '
                . '(SELECT iStarId FROM stars WHERE iStarId =' . $_POST['starname'] . ') AS starid,'
                . '(SELECT vStar_name FROM stars WHERE iStarId =' . $_POST['starname'] . ') AS starname,'
                . '(SELECT vStar_email FROM stars WHERE iStarId =' . $_POST['starname'] . ') AS staremail,'
                . '(SELECT vStar_phone FROM stars WHERE iStarId =' . $_POST['starname'] . ') AS starphone '
                . ' FROM login '
                . ' WHERE  ' . $sql
                . $groupby
                . ' ORDER BY vFirst_name ASC')->result_array();

        }

        if (!empty($starid) && !empty($countryid) && !empty($stateid)) {
            $data['stars'] = $this->db->query('SELECT COUNT(iUserId) as follower, country, state, city,'
                . '(SELECT iStarId FROM stars WHERE iStarId =' . $_POST['starname'] . ') AS starid,'
                . '(SELECT vStar_name FROM stars WHERE iStarId =' . $_POST['starname'] . ') AS starname,'
                . '(SELECT vStar_email FROM stars WHERE iStarId =' . $_POST['starname'] . ') AS staremail,'
                . '(SELECT vStar_phone FROM stars WHERE iStarId =' . $_POST['starname'] . ') AS starphone '
                . ' FROM login '
                . ' WHERE  ' . $sql
                . $groupby
                . ' ORDER BY vFirst_name ASC')->result_array();

        }

        if (empty($starid) && empty($countryid) && empty($stateid)) {
            $data['stars'] = $this->db->query('SELECT iStarId as starid, vStar_name AS starname, vStar_email AS staremail, vStar_phone AS starphone, vStar_like_count AS follower '
                . ' FROM stars '
                . ' WHERE vStar_like_count != 0 '
                . ' ORDER BY vStar_like_count DESC')->result_array();
        }


        $data['stardetails'] = $this->db->query('SELECT iStarId as starid, vStar_name AS starname, vStar_email AS staremail, vStar_phone AS starphone, vStar_like_count AS follower '
            . ' FROM stars '
            . ' WHERE vStar_like_count != 0 '
            . ' ORDER BY vStar_like_count DESC')->result_array();
        if (!empty($statewhere)) {
            $data['states'] = $this->db->query('SELECT id, location  FROM country_state_city where type = "RE" ' . $statewhere . ' ORDER BY location ASC')->result_array();
        } else {
            $data['states'] = array();
        }


        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('dashboard/analytics', $data);
        $this->load->view('elements/footer');
    }


    public function logout()
    {
        $this->session->unset_userdata('user');
        $this->session->sess_destroy();
        redirect(base_url());
    }

    public function getstate()
    {
        $id = $_POST['id'];
        $data = $this->db->query('SELECT id, location  FROM country_state_city where type = "RE" AND in_location="' . $id . '" ORDER BY location ASC')->result_array();
        echo json_encode($data);
    }

    public function getcity()
    {
        $id = $_POST['id'];
        $data = $this->db->query('SELECT id, location  FROM country_state_city where type = "CI" AND in_location=' . $id . ' ORDER BY location ASC')->result_array();
        echo json_encode($data);
    }

    public function getcountry()
    {
        $id = $_POST['id'];
        $data = $this->db->query('SELECT id, location, '
            . '(SELECT vStar_name FROM stars WHERE iStarId =' . $id . ') AS starname,'
            . '(SELECT vStar_email FROM stars WHERE iStarId =' . $id . ') AS staremail,'
            . '(SELECT vStar_phone FROM stars WHERE iStarId =' . $id . ') AS starphone '
            . ' FROM country_state_city '
            . ' WHERE vStar_list IN(' . $id . ') '
            . ' ORDER BY vFirst_name ASC')->result_array();
        echo json_encode($data);

    }

    public function followersDetail($starId = '', $country = '', $state = '', $city = '')
    {


        if (isset($starId) && !empty($starId)) {   //$_POST['starname'] is star id

            $xSQL1 = ' (vStar_list LIKE "' . $starId . ',%" '
                . 'OR vStar_list LIKE "%,' . $starId . '" '
                . 'OR vStar_list LIKE "%,' . $starId . ',%" '
                . 'OR vStar_list = "' . $starId . '") ';
            $sql = $xSQL1;
        }

        if (isset($country) && !empty($country)) {
            if (!empty($xSQL1)) {
                $xSQL2 = ' AND country = "' . trim($country) . '" ';
            }
            $sql .= $xSQL2;
        }

        if (isset($state) && !empty($state)) {
            if (!empty($xSQL2)) {
                $xSQL3 = ' AND state = "' . trim($state) . '" ';
            } else {
                $xSQL3 = ' state = "' . trim($state) . '" ';
            }

            $sql .= $xSQL3;
        }

        if (isset($city) && !empty($city)) {
            if (!empty($xSQL2)) {
                $xSQL4 = ' AND city = "' . trim($city) . '" ';
            } else {
                $xSQL4 = ' city = "' . trim($city) . '" ';
            }

            $sql .= $xSQL4;
        }


        $data['followers'] = $this->db->query('SELECT vFirst_name, vLast_name, vEmailId, vPhone_no, country, state, city'
            . ' FROM login '
            . ' WHERE  ' . $sql
            . ' ORDER BY vFirst_name ASC')->result_array();

        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('dashboard/follower', $data);
        $this->load->view('elements/footer');

    }

    public function remainder()
    {
        $data['stars_remainder'] = $this->db->query('SELECT vStar_name, vStar_email,vStar_phone
                                                   FROM stars
                                                   WHERE iStarId NOT IN(  SELECT GROUP_CONCAT(iStarId) as starId
                                                                           FROM `post_master`
                                                                           WHERE `dDate_time` > (CURDATE() - INTERVAL 2 DAY )
                                                                           GROUP BY iStarId)
                                                                           AND eStatus="Active"')->result_array();

        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('dashboard/remainder', $data);
        $this->load->view('elements/footer');
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
