<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller
{

    // Call parent constructor
    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        // $this->load->model('Login_model');
        $this->load->helper('form');

    }

    // Call login page.
    public function index()
    {
        $this->form_validation->set_rules('username', 'username', 'required|trim|xss_clean');
        $this->form_validation->set_rules('password', 'password', 'required|trim|xss_clean');
        if ($this->form_validation->run() === FALSE) {
//                $this->load->view('elements/header');
            $this->load->view('login/index');
//                $this->load->view('elements/footer');
        } else {
            $where = array("login" => $this->input->post("username"),
                "password" => $this->input->post("password"),
                "superUser" => 1
            );

            $res = $this->User_model->check_login($where);

            if ($res) {
//                $adminData = $this->User_model->getAdminByEmail($res[0]['email']);
                $adminData = $this->User_model->getAdminById($res[0]['memberId']);
                $this->session->set_userdata('lastLogin', $adminData[0]['lastLogin']);

                $userData = $this->User_model->getUserByEmail($res[0]['email']);
                $memberName = $userData[0]['firstName'] . " " . $userData[0]['lastName'];
                $this->session->set_userdata('memberId', $userData[0]['memberId']);
                $this->session->set_userdata('roleId', $userData[0]['roleId']);
                $this->session->set_userdata('name', $memberName);
                $this->session->set_userdata('image', $userData[0]['image']);

                redirect(base_url() . 'dashboard/index');
            } else {
                $this->session->set_flashdata('error', '<div class="err">Please check Username or Password.</div>');
                redirect(base_url() . 'login/index');
            }
        }

    }

    public function changepassword()
    {
        $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean');
        $this->form_validation->set_rules('oldpassword', 'Username', 'required|trim|xss_clean|callback_password_check');
        $this->form_validation->set_rules('newpassword', 'Password', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('login/changepassword');
            $this->load->view('elements/footer');
        } else {
            $res = $this->User_model->changepassword();

            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">New password has been update.</div>');
            } else {
                $this->session->set_flashdata('error', '<div class="alert alert-danger">New password not assign successfully.</div>');
            }
            redirect($_SERVER['HTTP_REFERER']);

        }
    }

    public function password_check($passwd)
    {
        if ($_POST['newpassword'] != $_POST['retypepassword']) {
            $this->form_validation->set_message('password_check', '<div class="alert alert-danger">The Re type password field not match with New password.</div>');
            return FALSE;
        }


        $pw = $this->User_model->check_pasword($passwd);
        if (empty($pw)) {
            $this->form_validation->set_message('password_check', '<div class="alert alert-danger">Please enter correct Old Password.</div>');
            return FALSE;
        }

        if ($_POST['email'] != $pw[0]['email']) {
            $this->form_validation->set_message('password_check', '<div class="alert alert-danger">Please enter correct Email to change password.</div>');
            return FALSE;
        }
    }


}