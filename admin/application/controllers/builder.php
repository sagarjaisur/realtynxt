<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Builder extends CI_Controller
{
    public $page_caption = 'Builder'; // Added For page Captions

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Builder_model');
        $builder = $this->session->userdata('memberId');

        if (empty($builder)) {
            redirect(base_url());
        }
    }

    public function index()
    {
        $data['builders'] = $this->Builder_model->get_builder();
        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('builder/index', $data);
        $this->load->view('elements/footer');
    }

    // Insert Builder
    public function add()
    {
        $this->form_validation->set_rules('name', 'Builder Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('mobileNumber', 'Mobile Number', 'required|trim|xss_clean');
        $this->form_validation->set_rules('altMobileNumber', 'Alternate Mobile Number', 'trim|xss_clean');
        $this->form_validation->set_rules('email', 'Email ID', 'required|trim|xss_clean');
        $this->form_validation->set_rules('altEmail', 'Email ID', 'trim|xss_clean');
        $this->form_validation->set_rules('contactAddress', 'Contact Address', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('builder/add');
            $this->load->view('elements/footer');
        } else {
//            $images = @implode(',', $this->input->post('images'));

            $data = array(
                'name' => $this->input->post('name'),
                'mobileNumber' => $this->input->post('mobileNumber'),
                'altMobileNumber' => $this->input->post('altMobileNumber'),
                'email' => $this->input->post('email'),
                'altEmail' => $this->input->post('altEmail'),
                'contactAddress' => $this->input->post('contactAddress')
//                'images' => $images
            );

            $res = $this->Builder_model->add($data);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Added Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Added Successfully.</div>');
            }
            redirect(base_url() . 'builder/index');
        }
    }

    // Update Records
    public function edit($builderId)
    {
        if (empty($builderId)) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $data['builderId'] = $builderId;
        $this->form_validation->set_rules('name', 'Builder Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('mobileNumber', 'Mobile Number', 'required|trim|xss_clean');
        $this->form_validation->set_rules('altMobileNumber', 'Alternate Mobile Number', 'trim|xss_clean');
        $this->form_validation->set_rules('email', 'Email ID', 'required|trim|xss_clean');
        $this->form_validation->set_rules('altEmail', 'Email ID', 'trim|xss_clean');
        $this->form_validation->set_rules('contactAddress', 'Contact Address', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
            $data['detail'] = $this->Builder_model->get_detail($builderId)[0];

            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('builder/edit', $data);
            $this->load->view('elements/footer');

        } else {
            $data = array();
//            $images = @implode(',', $this->input->post('images'));
            $data = array(
                'name' => $this->input->post('name'),
                'mobileNumber' => $this->input->post('mobileNumber'),
                'altMobileNumber' => $this->input->post('altMobileNumber'),
                'email' => $this->input->post('email'),
                'altEmail' => $this->input->post('altEmail'),
                'contactAddress' => $this->input->post('contactAddress')
//                'images' => $images
            );

            $where = array('builderId' => $builderId);
            $res = $this->Builder_model->update($data, $where);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Updated Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Updated Successfully.</div>');
            }
            redirect(base_url() . 'builder/index');
        }
    }

    public function delete($builderId)
    {
        if ($this->Builder_model->delete($builderId)) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Deleted Successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Deleted Successfully.</div>');
        }
        redirect(base_url() . 'builder/index');
    }

    public function status($builderId = '', $status = '')
    {
        if (!empty($builderId) && isset($status)) {

            $res = $this->Builder_model->changeStatus($builderId, $status);

            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-success"> Sorry, ' . $this->page_caption . 'status could not be changed.</div>');
            }

            $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            redirect($_SERVER['HTTP_REFERER']);
        }
        redirect(base_url() . 'builder/index');
    }

}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */