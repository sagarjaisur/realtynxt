<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Vehiclerental extends CI_Controller
{
    public $page_caption = 'Vehicle Rental'; // Added For page Captions

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Vehiclerental_model');
        $this->load->model('User_model');
        $this->load->model('Category_model');
        $this->load->model('Region_model');

        $property = $this->session->userdata('memberId');

        if (empty($property)) {
            redirect(base_url());
        }
    }

    public function index()
    {
        $data['properties'] = $this->Vehiclerental_model->get_property();
        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('vehiclerental/index', $data);
        $this->load->view('elements/footer');
    }

    // Insert Property
    public function add()
    {
        $allUsersData = $this->User_model->get_User('', '', '');
        $dataX['usersList'] = array('' => 'Select User');
        foreach ($allUsersData as $key => $userData) {
            $dataX['usersList'] += array(
                $userData['memberId'] => $userData['name']
            );
        }

        $allCitiesData = $this->Region_model->get_Region('tbl_city', '', '', array('status' => 1));
        $dataX['CitiesList'] = array('' => 'Select City');
        foreach ($allCitiesData as $key => $CityData) {
            $dataX['CitiesList'] += array(
                $CityData['cityName'] => $CityData['cityName']
            );
        }
//        print_r($dataX['CitiesList']); die();

        $dataX['AreasList'] = array('' => 'Select City');
        $defaultAreasData = $this->Region_model->get_Region('tbl_area', '', '', array('status' => 1, 'cityId' => 1));
        $dataX['AreasList'] = array('' => 'Select Area');
        foreach ($defaultAreasData as $key => $AreaData) {
            $dataX['AreasList'] += array(
                $AreaData['areaName'] => $AreaData['areaName']
            );
        }
//        print_r($dataX['AreasList']); die();


        $this->form_validation->set_rules('vehicleType', 'Vehicle Type', 'required|trim|xss_clean');
        $this->form_validation->set_rules('vehicleModel', 'Vehicle Model', 'required|trim|xss_clean');
        $this->form_validation->set_rules('area', 'Area', 'required|trim|xss_clean');
        $this->form_validation->set_rules('landmark', 'Landmark', 'required|trim|xss_clean');
        $this->form_validation->set_rules('vehicleHaving', 'Vehicle Having', 'required|trim|xss_clean|numeric');
        $this->form_validation->set_rules('vehiclePrice', 'Vehicle Price', 'required|trim|xss_clean|numeric');
        $this->form_validation->set_rules('vehicleNumber', 'Vehicle Registration Number', 'required|trim|xss_clean');
        $this->form_validation->set_rules('vehicleAvilable', 'Vehicle Avilable', 'required|trim|xss_clean|numeric');
        $this->form_validation->set_rules('vehicleDays', 'Vehicle Days', 'required');
        //echo $this->form_validation->run();

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('vehiclerental/add', $dataX);
            $this->load->view('elements/footer');
        } else {

            $data = array(
                'memberId' => $this->input->post('memberId'),
                'vehicleType' => $this->input->post('vehicleType'),
                'vehicleModel' => $this->input->post('vehicleModel'),
                'city' => $this->input->post('city'),
                'area' => $this->input->post('area'),
                'landmark' => $this->input->post('landmark'),
                'vehicleHaving' => $this->input->post('vehicleHaving'),
                'vehiclePrice' => $this->input->post('vehiclePrice'),
                'vehicleNumber' => $this->input->post('vehicleNumber'),
                'vehicleAvilable' => $this->input->post('vehicleAvilable'),
                'vehicleNote' => $this->input->post('vehicleNote'),
                'vehicleDays' => @implode(',', $this->input->post('vehicleDays'))
            );

            $res = $this->Vehiclerental_model->add($data);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Added Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Added Successfully.</div>');
            }
            redirect(base_url() . 'vehiclerental/index');
        }
    }

    // Update Records
    public function edit($propertyId)
    {
        if (empty($propertyId)) {
            redirect($_SERVER['HTTP_REFERER']);
        }

        $allUsersData = $this->User_model->get_User('', '', '');
        $dataX['usersList'] = array('' => 'Select User');
        foreach ($allUsersData as $key => $userData) {
            $dataX['usersList'] += array(
                $userData['memberId'] => $userData['name']
            );
        }


        $allCitiesData = $this->Region_model->get_Region('tbl_city', '', '', array('status' => 1));
        $dataX['CitiesList'] = array('' => 'Select City');
        foreach ($allCitiesData as $key => $CityData) {
            $dataX['CitiesList'] += array(
                $CityData['cityName'] => $CityData['cityName']
            );
        }

        $dataX['property'] = $this->Vehiclerental_model->get_detail($propertyId)[0];

        $cityId = $this->db->query("SELECT cityId FROM tbl_city WHERE cityName LIKE '" . $dataX['property']['city'] . "';")->result_array()[0]['cityId'];

        $relatedAreasData = $this->Region_model->get_Region('tbl_area', '', '', array('status' => 1, 'cityId' => $cityId));
        $dataX['AreasList'] = array('' => 'Select City');
        foreach ($relatedAreasData as $key => $AreaData) {
            $dataX['AreasList'] += array(
                $AreaData['areaName'] => $AreaData['areaName']
            );
        }


        $data['id'] = $propertyId;
        $this->form_validation->set_rules('vehicleType', 'Vehicle Type', 'required|trim|xss_clean');
        $this->form_validation->set_rules('vehicleModel', 'Vehicle Model', 'required|trim|xss_clean');
        $this->form_validation->set_rules('area', 'Area', 'required|trim|xss_clean');
        $this->form_validation->set_rules('landmark', 'Landmark', 'required|trim|xss_clean');
        $this->form_validation->set_rules('vehicleHaving', 'Vehicle Having', 'required|trim|xss_clean|numeric');
        $this->form_validation->set_rules('vehiclePrice', 'Vehicle Price', 'required|trim|xss_clean|numeric');
        $this->form_validation->set_rules('vehicleNumber', 'Vehicle Registration Number', 'required|trim|xss_clean');
        $this->form_validation->set_rules('vehicleAvilable', 'Vehicle Avilable', 'required|trim|xss_clean|numeric');
        $this->form_validation->set_rules('vehicleDays', 'Vehicle Days', 'required');

        if ($this->form_validation->run() === FALSE) {
//            $dataX['property'] = $this->Vehiclerental_model->get_detail($propertyId)[0];

            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('vehiclerental/edit', $dataX);
            $this->load->view('elements/footer');

        } else {
//            $data = array();
            $where = array('id' => $propertyId);


            $data = array(
                'memberId' => $this->input->post('memberId'),
                'vehicleType' => $this->input->post('vehicleType'),
                'vehicleModel' => $this->input->post('vehicleModel'),
                'city' => $this->input->post('city'),
                'area' => $this->input->post('area'),
                'landmark' => $this->input->post('landmark'),
                'vehicleHaving' => $this->input->post('vehicleHaving'),
                'vehiclePrice' => $this->input->post('vehiclePrice'),
                'vehicleNumber' => $this->input->post('vehicleNumber'),
                'vehicleAvilable' => $this->input->post('vehicleAvilable'),
                'vehicleNote' => $this->input->post('vehicleNote'),
                'vehicleDays' => @implode(',', $this->input->post('vehicleDays'))
            );

            $res = $this->Vehiclerental_model->update($data, $where);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Updated Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Updated Successfully.</div>');
            }
            redirect(base_url() . 'vehiclerental/index');
        }
    }


    public function delete($propertyId)
    {
        if ($this->Vehiclerental_model->delete($propertyId)) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Deleted Successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Deleted Successfully.</div>');
        }
        redirect(base_url() . 'vehiclerental/index');
    }

    public function changePropertyStatus($propertyId = '', $status = '')
    {
        $res = $this->Vehiclerental_model->changeStatus($propertyId, $status);
        if ($res) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">Action updated successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">Action does not updated.</div>');
        }

        redirect(base_url() . "property");
    }

    //AJAX call for dynamically populating Areas according to selected City
    function areaFind()
    {
        $cityName = $_POST['cityName'];
        $cityResId = $this->db->query("SELECT cityId FROM tbl_city WHERE cityName LIKE '" . $cityName . "'")->result_array();
        $cityId = $cityResId[0]['cityId'];

        if (isset($cityId) && $cityId != '') {
            $data = $this->Vehiclerental_model->checkForArea($cityId);
            if (count($data) > 0 && !empty($data)) {
                echo json_encode($data);
                //return $data;
            } else {
                echo json_encode("error");
            }
            exit;
        }
    }

}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */