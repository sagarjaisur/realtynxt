<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class PropertyMapList extends CI_Controller
{
    public $page_caption = 'Property'; // Added For page Captions

    public function __construct()
    {
        parent::__construct();

        $this->load->model('PropertyMapList_model');
        $uid = $this->session->userdata('memberId');

        if (empty($uid)) {
            redirect(base_url());
        }
    }

    public function index()
    {
        $data['propertyMapList'] = $this->PropertyMapList_model->get_propertyMapList();
        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('propertyMapList/index', $data);
        $this->load->view('elements/footer');
    }

    public function map()
    {
        $data['propertyMapList'] = $this->PropertyMapList_model->get_propertyMapList();

        $propertyMapList = $data['propertyMapList'];
        $jsonArr = "";
        foreach ($propertyMapList as $property) {
            $jsonArr .= " { title:'" . $property['propertyName'] . "',
                url:'http://aletheme.com/wordpress/luster/properties/luxury-villa-av-mozart-paris/',
                currency:'$',
                areasize:'450',
                bath:'3',
                bad:'9',
                thumb:'" . $property['images'] . "',
                price:'" . $property['price'] . "',
                pricetype:'',
                lat:" . $property['latitude'] . ",
                lng:" . $property['longitude'] . ",
                icon:'http://localhost/realtynxt/admin/assets/map/mapIcon.png'} ,";

        }
        $jsonArr = rtrim($jsonArr, ',');

        $data['mapJson'] = $jsonArr;

        //echo "<pre>";     print_r($data); die();


        $this->load->view('propertyMapList/map', $data);

    }

    // Insert PropertyMapList
    public function add()
    {
        $this->form_validation->set_rules('propertyName', 'Property Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('price', 'Price', 'required|trim|xss_clean');
        $this->form_validation->set_rules('description', 'Property Description', 'required|trim|xss_clean');
        $this->form_validation->set_rules('address', 'Address', 'required|trim|xss_clean');
        $this->form_validation->set_rules('latitude', 'Latitude', 'required|trim|xss_clean');
        $this->form_validation->set_rules('longitude', 'Longitude', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('propertyMapList/add');
            $this->load->view('elements/footer');
        } else {
            $images = @implode(',', $this->input->post('images'));

            $data = array(
                'propertyName' => $this->input->post('propertyName'),
                'price' => $this->input->post('price'),
                'description' => $this->input->post('description'),
                'address' => $this->input->post('address'),
                'latitude' => $this->input->post('latitude'),
                'longitude' => $this->input->post('longitude'),
                'images' => $images
            );

            $res = $this->PropertyMapList_model->add($data);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Added Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">Sorry, ' . $this->page_caption . ' could not be added.</div>');
            }
            redirect(base_url() . 'propertyMapList/index');
        }
    }

    // Update Records
    public function edit($propertyMapListId)
    {
        if (empty($propertyMapListId)) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $data['propertyMapListId'] = $propertyMapListId;
        $this->form_validation->set_rules('propertyName', 'Property Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('price', 'Price', 'required|trim|xss_clean');
        $this->form_validation->set_rules('description', 'Property Description', 'required|trim|xss_clean');
        $this->form_validation->set_rules('address', 'Address', 'required|trim|xss_clean');
        $this->form_validation->set_rules('latitude', 'Latitude', 'required|trim|xss_clean');
        $this->form_validation->set_rules('longitude', 'Longitude', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
            $data['detail'] = $this->PropertyMapList_model->get_detail($propertyMapListId)[0];

            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('propertyMapList/edit', $data);
            $this->load->view('elements/footer');

        } else {
            $data = array();
            $images = @implode(',', $this->input->post('images'));
            $data = array(
                'propertyName' => $this->input->post('propertyName'),
                'price' => $this->input->post('price'),
                'description' => $this->input->post('description'),
                'address' => $this->input->post('address'),
                'latitude' => $this->input->post('latitude'),
                'longitude' => $this->input->post('longitude'),
                'images' => $images
            );

            $where = array('propertyMapListId' => $propertyMapListId);
            $res = $this->PropertyMapList_model->update($data, $where);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Updated Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">Sorry, ' . $this->page_caption . ' could not be updated.</div>');
            }
            redirect(base_url() . 'propertyMapList/index');
        }
    }

    public function delete($propertyMapListId)
    {
        if ($this->PropertyMapList_model->delete($propertyMapListId)) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Deleted Successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">Sorry, ' . $this->page_caption . ' could not be deleted.</div>');
        }
        redirect(base_url() . 'propertyMapList/index');
    }

    public function status($propertyMapListId = '', $status = '')
    {
        if (!empty($propertyMapListId) && isset($status)) {

            $res = $this->PropertyMapList_model->changeStatus($propertyMapListId, $status);

            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger"> Sorry, ' . $this->page_caption . 'status could not be changed.</div>');
            }

//            $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            redirect($_SERVER['HTTP_REFERER']);
        }
        redirect(base_url() . 'propertyMapList/index');
    }

}
/* End of file welcome.php */
/* Property Description: ./application/controllers/welcome.php */