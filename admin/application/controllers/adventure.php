<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Adventure extends CI_Controller
{
    public $page_caption = 'Adventure'; // Added For page Captions

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Adventure_model');
        $adventure = $this->session->userdata('memberId');

        if (empty($adventure)) {
            redirect(base_url());
        }
    }

    public function index()
    {
        $data['adventures'] = $this->Adventure_model->get_adventure();
        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('adventure/index', $data);
        $this->load->view('elements/footer');
    }

    public function types()
    {
        $data['adventures'] = $this->Adventure_model->get_adventure_types();
        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('adventure/types/index', $data);
        $this->load->view('elements/footer');
    }

    // Insert Adventure
    public function add()
    {
        $this->form_validation->set_rules('capacity', 'Capacity', 'required|trim|xss_clean|is_natural');
        $this->form_validation->set_rules('price', 'Price', 'required|trim|xss_clean|numeric');
        $this->form_validation->set_rules('contactName', 'Contact Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('contactNumber', 'Contact Number', 'required|trim|xss_clean|is_natural');
        $this->form_validation->set_rules('meetingAddress', 'Meeting Address', 'required|trim|xss_clean');
        $this->form_validation->set_rules('safetyPrecautions', 'Safety Precautions', 'required|trim|xss_clean');
        $this->form_validation->set_rules('whereToCome', 'Where To Come', 'required|trim|xss_clean');
        $this->form_validation->set_rules('liker', 'Who will like it?', 'required|trim|xss_clean');

        $dataX['eventNamesList'] = array('' => 'Please Select Adventure Type');

        $allAdventureTypes = $this->Adventure_model->get_adventure_types('parentId = 0');

        $dataX['adventureTypesList'] = array('' => 'Select Adventure Type');
        foreach ($allAdventureTypes as $key => $adventureType) {
            $dataX['adventureTypesList'] += array(
                $adventureType['eventName'] => $adventureType['eventName']
            );
        }


        if ($this->form_validation->run() === FALSE) {
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('adventure/add', $dataX);
            $this->load->view('elements/footer');
        } else {
            $images = @implode(',', $this->input->post('images'));

            $frequency = $this->input->post('frequency');
            if ($frequency == 'Daily') {
                $repeatInterval = $frequency;
            } elseif ($frequency == 'Weekly') {
                $repeatInterval = @implode(',', $this->input->post('weekDays'));
            } elseif ($frequency == 'Monthly') {
                $repeatInterval = $this->input->post('repetitiveDate');
            }
            $data = array(
                'adventureType' => $this->input->post('adventureType'),
                'eventName' => $this->input->post('eventName'),

                'date' => $this->input->post('date'),
                'startingTime' => $this->input->post('startingTime'),

                'timespan' => $this->input->post('timespan'),
                'frequency' => $frequency,
                'repeatInterval' => $repeatInterval,

                'capacity' => $this->input->post('capacity'),
                'availableEntry' => $this->input->post('capacity'),
                'price' => $this->input->post('price'),
                'contactName' => $this->input->post('contactName'),
                'contactNumber' => $this->input->post('contactNumber'),
                'meetingAddress' => $this->input->post('meetingAddress'),
                'safetyPrecautions' => $this->input->post('safetyPrecautions'),
                'whereToCome' => $this->input->post('whereToCome'),
                'liker' => $this->input->post('liker'),
                'images' => $images
            );

            $res = $this->Adventure_model->add($data);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Added Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Added Successfully.</div>');
            }
            redirect(base_url() . 'adventure/index');
        }
    }

    // Update Records
    public function edit($adventureId)
    {
        if (empty($adventureId)) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $dataX['adventureId'] = $adventureId;
        $this->form_validation->set_rules('capacity', 'Capacity', 'required|trim|xss_clean|is_natural');
        $this->form_validation->set_rules('price', 'Price', 'required|trim|xss_clean|numeric');
        $this->form_validation->set_rules('contactName', 'Contact Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('contactNumber', 'Contact Number', 'required|trim|xss_clean|is_natural');
        $this->form_validation->set_rules('meetingAddress', 'Meeting Address', 'required|trim|xss_clean');
        $this->form_validation->set_rules('safetyPrecautions', 'Safety Precautions', 'required|trim|xss_clean');
        $this->form_validation->set_rules('whereToCome', 'Where To Come', 'required|trim|xss_clean');
        $this->form_validation->set_rules('liker', 'Who will like it?', 'required|trim|xss_clean');


        $dataX['detail'] = $this->Adventure_model->get_detail($adventureId)[0];
        $allAdventureTypes = $this->Adventure_model->get_adventure_types('parentId = 0');

        $dataX['adventureTypesList'] = array('' => 'Select Adventure Type');
        foreach ($allAdventureTypes as $key => $adventureType) {
            $dataX['adventureTypesList'] += array(
                $adventureType['eventName'] => $adventureType['eventName']
            );
        }

        $parentId = $this->db->query("SELECT id FROM tbl_adventure_type WHERE eventName LIKE '" . $dataX['detail']['adventureType'] . "'")->result_array()[0]['id'];
        $relatedEventNames = $this->Adventure_model->get_adventure_types("parentId = $parentId");
        $dataX['eventNamesList'] = array('' => 'Select Event Name');
        foreach ($relatedEventNames as $key => $eventName) {
            $dataX['eventNamesList'] += array(
                $eventName['eventName'] => $eventName['eventName']
            );
        }

        if ($this->form_validation->run() === FALSE) {

            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('adventure/edit', $dataX);
            $this->load->view('elements/footer');

        } else {
            $data = array();
            $images = @implode(',', $this->input->post('images'));

            $frequency = $this->input->post('frequency');
            if ($frequency == 'Daily') {
                $repeatInterval = $frequency;
            } elseif ($frequency == 'Weekly') {
                $repeatInterval = @implode(',', $this->input->post('weekDays'));
            } elseif ($frequency == 'Monthly') {
                $repeatInterval = $this->input->post('repetitiveDate');
            }
            $data = array(
                'adventureType' => $this->input->post('adventureType'),
                'eventName' => $this->input->post('eventName'),

                'date' => $this->input->post('date'),
                'startingTime' => $this->input->post('startingTime'),

                'timespan' => $this->input->post('timespan'),
                'frequency' => $frequency,
                'repeatInterval' => $repeatInterval,

                'capacity' => $this->input->post('capacity'),
                'price' => $this->input->post('price'),
                'contactName' => $this->input->post('contactName'),
                'contactNumber' => $this->input->post('contactNumber'),
                'meetingAddress' => $this->input->post('meetingAddress'),
                'safetyPrecautions' => $this->input->post('safetyPrecautions'),
                'whereToCome' => $this->input->post('whereToCome'),
                'liker' => $this->input->post('liker'),
                'images' => $images
            );

            $where = array('adventureId' => $adventureId);
            $res = $this->Adventure_model->update($data, $where);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Updated Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Updated Successfully.</div>');
            }
            redirect(base_url() . 'adventure/index');
        }
    }

    public function delete($adventureId)
    {
        if ($this->Adventure_model->delete($adventureId)) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Deleted Successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Deleted Successfully.</div>');
        }
        redirect(base_url() . 'adventure/index');
    }

    public function status($adventureId = '', $status = '')
    {
        if (!empty($adventureId) && isset($status)) {

            $res = $this->Adventure_model->changeStatus($adventureId, $status);

            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-success"> Sorry, ' . $this->page_caption . 'status could not be changed.</div>');
            }

            $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            redirect($_SERVER['HTTP_REFERER']);
        }
        redirect(base_url() . 'adventure/index');
    }


    // For managing adventure types
    // Add New Adventure Type
    public function addType()
    {
        $this->form_validation->set_rules('adventureType', 'Adventure Type', 'required|trim|xss_clean');
        echo $this->form_validation->run();

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('adventure/types/add_type');
            $this->load->view('elements/footer');
        } else {
            $data = array(
                'eventName' => $this->input->post('adventureType'),
                'parentId' => 0
            );

            $res = $this->Adventure_model->addType($data);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">Adventure Type Added Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">Sorry, Adventure Type could not be added.</div>');
            }
            redirect(base_url() . 'adventure/types');
        }
    }

    // Update Records
    public function editType($id)
    {
        if (empty($id)) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $data['id'] = $id;
        $this->form_validation->set_rules('adventureType', 'Adventure Type', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
            $data['detail'] = $this->Adventure_model->get_detail_type($id)[0];

            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('adventure/types/edit_type', $data);
            $this->load->view('elements/footer');

        } else {
            $data = array();

            $where = array('id' => $id);
            $data = array('eventName' => $this->input->post('adventureType'));

            $res = $this->Adventure_model->updateType($data, $where);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Updated Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Updated Successfully.</div>');
            }
            redirect(base_url() . 'adventure/types');
        }
    }

    public function deleteType($adventureId, $isParent = 0)
    {
        if ($this->Adventure_model->deleteType($adventureId, $isParent)) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Deleted Successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Deleted Successfully.</div>');
        }
        redirect(base_url() . 'adventure/types');
    }

    public function statusType($id = '', $status = '', $isParent = 0)
    {
        if (isset($id) && isset($status)) {

            $res = $this->Adventure_model->changeStatusType($id, $status, $isParent);

            $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    // For managing Event Name
    // Add New Event Name
    public function addEventName()
    {
        $this->form_validation->set_rules('eventName', 'Event Name', 'required|trim|xss_clean');

        $allAdventureTypes = $this->Adventure_model->get_adventure_types('parentId = 0');

        $dataX['adventureTypesList'] = array('' => 'Select Adventure Type');
        foreach ($allAdventureTypes as $key => $adventureType) {
            $dataX['adventureTypesList'] += array(
                $adventureType['id'] => $adventureType['eventName']
            );
        }

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('adventure/types/add_eventName', $dataX);
            $this->load->view('elements/footer');
        } else {
            $data = array(
                'eventName' => $this->input->post('eventName'),
                'parentId' => $this->input->post('adventureType')
            );

            $res = $this->Adventure_model->addEventName($data);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">Adventure EventName Added Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">Adventure EventName Not Added Successfully.</div>');
            }
            redirect(base_url() . 'adventure/types');
        }
    }

    // AJAX call for dynamically populating Event Names according to selected Adventure Type
    function eventNameFind()
    {
        $adventureType = $_POST['adventureType'];
        $parentId = $this->db->query("SELECT id FROM tbl_adventure_type WHERE eventName LIKE '" . $adventureType . "'")->result_array()[0]['id'];

        $data = $this->db->query('SELECT `eventName` FROM `tbl_adventure_type` WHERE parentId = "' . $parentId . '"')->result_array();
        if (count($data) > 0 && !empty($data)) {
            echo json_encode($data);
//                return $data;
        } else {
            echo json_encode("error");
        }
        exit;
    }
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */