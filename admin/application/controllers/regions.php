<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Regions extends CI_Controller
{
    public $page_caption = 'Region'; // Added For page Captions

//    private $siteDefaultData = array();

    public function __construct()
    {
        parent::__construct();

        /*$this->siteDefaultData['setting'] = $this->db->query('SELECT *  FROM tbl_setting ')->result_array();
        foreach ($this->siteDefaultData['setting'] as $key => $value) {
            $formatedData[$value['settingName']] = $value['settingValue'];
        }
        $this->siteDefaultData['setting'] = $formatedData;*/

        $this->load->model('Region_model');
        $user = $this->session->userdata('memberId');
        $this->load->library('pagination');
        $this->load->helper('url');

        /*
                // For email management
                $this->load->library('email');
                //Start -  Configuration Setting For Emails
                $this->email->protocol = $this->siteDefaultData['setting']['EMAIL_SMTP_PROTOCOL'];
                $this->email->smtp_host = $this->siteDefaultData['setting']['EMAIL_SMTP_HOST'];
                $this->email->smtp_timeout = $this->siteDefaultData['setting']['EMAIL_SMTP_TIMEOUT'];
                $this->email->smtp_region = $this->siteDefaultData['setting']['EMAIL_SMTP_USER'];
                $this->email->smtp_pass = $this->siteDefaultData['setting']['EMAIL_SMTP_PASSWORD'];
                $this->email->smtp_port = $this->siteDefaultData['setting']['EMAIL_SMTP_PORT'];
                $this->email->mailtype = $this->siteDefaultData['setting']['EMAIL_SMTP_MAILTYPE'];
                $this->email->smtp_crypto = $this->siteDefaultData['setting']['EMAIL_SMTP_CRYPTO'];

                // End  -  Configuration Setting For Emails

                $this->email->initialize();
        */
        if (empty($user)) {
            redirect(base_url());
        }
    }

    public function index()
    {
        $data['regionTypeId'] = $this->uri->segment(3);
        $tableName = 'tbl_' . $this->uri->segment(4);
        $id = $this->uri->segment(4) . 'Id';

        //Start - Pagination Setup
        $totalRecodeCount = $this->Region_model->recode_count($tableName, $id);
        $config['base_url'] = base_url() . 'regions/index/';
        $config['total_rows'] = $totalRecodeCount;
        $config['per_page'] = 25;
        $config["uri_segment"] = 3;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm inline">';
        $config['full_tag_close'] = '</ul>';
        $config['prev_link'] = '&lt;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&gt;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="current"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

//            $config['first_link'] = '&lt;&lt;';
        $config['first_link'] = '&laquo; First';
//            $config['last_link'] = '&gt;&gt;';
        $config['last_link'] = 'Last &raquo;';

        $data['links'] = $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data["regions"] = $this->Region_model->getRegionsRequest(25, $page, '');

        $data["links"] = $this->pagination->create_links();
        //End - Pagination Setup

        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('regions/index', $data);
        $this->load->view('elements/footer');
    }

    /** Function to add Region **/
    public function add($regionTypeId, $regionType)
    {
        $data['regionTypeId'] = $regionTypeId;
        $data['regionType'] = $regionType;

        if ($regionTypeId == 1) {
            $tableName = 'tbl_area';
            $joinTable = 'tbl_city';
            $regionParent = 'city';

            $data['regionParent'] = $regionParent;

            $allRegionsData = $this->Region_model->get_Region($joinTable, '', '', '');

            $data['regionParentList'] = array();
            foreach ($allRegionsData as $key => $regionData) {
                $data['regionParentList'] += array(
                    $regionData['cityId'] => $regionData['cityName']
                );
            }
        } elseif ($regionTypeId == 2) {
            $tableName = 'tbl_city';
            $joinTable = 'tbl_state';
            $regionParent = 'state';
            $data['regionParent'] = $regionParent;

            $allRegionsData = $this->Region_model->get_Region($joinTable, '', '', '');

            $data['regionParentList'] = array();
            foreach ($allRegionsData as $key => $regionData) {
                $data['regionParentList'] += array(
                    $regionData['stateId'] => $regionData['stateName']
                );
            }

        } elseif ($regionTypeId == 3) {
            $tableName = 'tbl_state';
            $joinTable = 'tbl_country';
            $regionParent = 'country';
            $data['regionParent'] = $regionParent;

            $allRegionsData = $this->Region_model->get_Region($joinTable, '', '', '');

            $data['regionParentList'] = array();
            foreach ($allRegionsData as $key => $regionData) {
                $data['regionParentList'] += array(
                    $regionData['countryId'] => $regionData['countryName']
                );
            }

        } elseif ($regionTypeId == 4) {
            $tableName = 'tbl_country';
            $joinTable = '';
            $regionParent = 'continent';
            $data['regionParent'] = $regionParent;

            $data['regionParentList'] = array(
                '1' => 'Asia'
            );
        }


        if (empty($regionTypeId)) {
            redirect($_SERVER['HTTP_REFERER']);
        }

        $this->form_validation->set_rules('regionName', ucwords($regionType) . ' Name', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
//            $data['region'] = $this->Region_model->get_detail($where, $tableName, $id, $joinTable, $column)[0];

            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('regions/add_region', $data);
            $this->load->view('elements/footer');

        } else {
            $insertData = array();

            $insertData = array(
                $regionType . 'Name' => $this->input->post('regionName'),
                $regionParent . 'Id' => $this->input->post('regionParentId')
            );

            $res = $this->Region_model->addRegion($tableName, $insertData);

            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Added Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  could not be added!</div>');
            }

            $redirectURL = "regions/regionrequest/$regionTypeId";
            redirect(base_url() . $redirectURL);
        }
    }

    /** Function to get all regions for dashboard **/
    public function regiondetails($uid)
    {
//        $id = $this->uri->segment(3);
        //$data['countries'] = $this->db->query('SELECT *  FROM tbl_country')->result_array();
        $data['states'] = $this->db->query('SELECT *  FROM tbl_state')->result_array();
        $data['region'] = $this->Region_model->getRegionById($uid);
        $data['admin'] = $this->Region_model->getAdminByEmail($data['region'][0]['email']);

        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('regions/edit_region', $data);
        $this->load->view('elements/footer');
    }

    /** Function update region details from admin **/
    public function edit($regionTypeId, $regionType, $regionId)
    {
        $data['regionTypeId'] = $regionTypeId;
        $data['regionType'] = $regionType;

        if ($regionTypeId == 1) {
            $tableName = 'tbl_area';
            $id = 'areaId';
            $parentId = 'cityId';
            $joinTable = 'tbl_city';
            $where = "$tableName.$parentId = $joinTable.$parentId  AND $tableName.$id = $regionId";
            $column = 'cityName';

            $regionParent = 'city';
            $data['regionParent'] = $regionParent;

            $allRegionsData = $this->Region_model->get_Region($joinTable, '', '', '');

            $data['regionParentList'] = array();
            foreach ($allRegionsData as $key => $regionData) {
                $data['regionParentList'] += array(
                    $regionData['cityId'] => $regionData['cityName']
                );
            }
        } elseif ($regionTypeId == 2) {
            $tableName = 'tbl_city';
            $id = 'cityId';
            $parentId = 'stateId';
//            $joinTable = ''; //No need for Join
//            $where = '';
//            $column = '';
            $joinTable = 'tbl_state';
            $where = "$tableName.$parentId = $joinTable.$parentId  AND $tableName.$id = $regionId";
            $column = 'stateName';

            $regionParent = 'state';
            $data['regionParent'] = $regionParent;

            $allRegionsData = $this->Region_model->get_Region($joinTable, '', '', '');

            $data['regionParentList'] = array();
            foreach ($allRegionsData as $key => $regionData) {
                $data['regionParentList'] += array(
                    $regionData['stateId'] => $regionData['stateName']
                );
            }
        } elseif ($regionTypeId == 3) {
            $tableName = 'tbl_state';
            $id = 'stateId';
            $parentId = 'countryId';
            $joinTable = 'tbl_country';
            $where = "$tableName.$parentId = $joinTable.$parentId  AND $tableName.$id = $regionId";
            $column = 'countryName';

            $regionParent = 'country';
            $data['regionParent'] = $regionParent;

            $allRegionsData = $this->Region_model->get_Region($joinTable, '', '', '');

            $data['regionParentList'] = array();
            foreach ($allRegionsData as $key => $regionData) {
                $data['regionParentList'] += array(
                    $regionData['countryId'] => $regionData['countryName']
                );
            }
        } elseif ($regionTypeId == 4) {
            $tableName = 'tbl_country';
            $id = 'countryId';
            $joinTable = '';
            $where = '';
            $column = '';

            $regionParent = 'continent';
            $data['regionParent'] = $regionParent;

            $data['regionParentList'] = array(
                '1' => 'Asia'
            );
        }


        if (empty($id)) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $data['regionId'] = $regionId;
        $this->form_validation->set_rules('regionName', ucwords($regionType) . ' Name', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
            $data['region'] = $this->Region_model->get_detail($where, $tableName, $id, $joinTable, $column)[0];

            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('regions/edit_region', $data);
            $this->load->view('elements/footer');

        } else {
            $insertData = array();

            $where = array($id => $regionId);
            $insertData = array(
                $regionParent . 'Id' => $this->input->post('regionParentId'),
                $regionType . 'Name' => $this->input->post('regionName')
            );

            $res = $this->Region_model->update($insertData, $where, $tableName);

            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Updated Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Updated Successfully.</div>');
            }

            $redirectURL = "regions/regionrequest/$regionTypeId";
            redirect(base_url() . $redirectURL);
        }
    }


    public function changeregionstatus($regionId, $status, $regionTypeId, $regionType)
    {
        $tableName = 'tbl_' . $regionType;
        $id = $regionType . 'Id';

        $res = $this->Region_model->changeRegionStatus($regionId, $status, $tableName, $id);
        if ($res) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">Action updated successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">Action does not updated.</div>');
        }
        $redirectURL = "regions/regionrequest/" . $regionTypeId;
        redirect(base_url() . $redirectURL);
    }

    public function regionrequest($regionTypeId = NULL)
    {
        $data['regionTypeId'] = $regionTypeId;

        if ($regionTypeId == 1) {
            $tableName = 'tbl_area';
            $id = 'areaId';
            $joinTable = 'tbl_city';
            $where = 'tbl_area.cityId = tbl_city.cityId';
            $column = 'cityName';
        } elseif ($regionTypeId == 2) {
            $tableName = 'tbl_city';
            $id = 'cityId';
//            $joinTable = ''; //No need for Join
//            $where = '';
//            $column = '';
            $joinTable = 'tbl_state';
            $where = 'tbl_city.stateId = tbl_state.stateId';
            $column = 'stateName';
        } elseif ($regionTypeId == 3) {
            $tableName = 'tbl_state';
            $id = 'stateId';
            $joinTable = 'tbl_country';
            $where = 'tbl_state.countryId = tbl_country.countryId';
            $column = 'countryName';
        } elseif ($regionTypeId == 4) {
            $tableName = 'tbl_country';
            $id = 'countryId';
            $joinTable = '';
            $where = '';
            $column = '';
        }

        //Start - Pagination Setup
        $totalRecodeCount = $this->Region_model->recode_count($tableName, $id);
        $config['base_url'] = base_url() . 'regions/regionrequest/' . $regionTypeId . '/';
        $config['total_rows'] = $totalRecodeCount;
        $config['per_page'] = 25;
        $config["uri_segment"] = 4;
//            $config['full_tag_open'] = '<ul class="pagination pagination-sm inline">';
        $config['full_tag_open'] = '<ul class="pagination pagination-sm inline">';
        $config['full_tag_close'] = '</ul>';
        $config['prev_link'] = '&lt;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&gt;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="current active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['first_link'] = '&laquo; First';
        $config['last_link'] = 'Last &raquo;';

        $data['links'] = $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;


        $data["regions"] = $this->Region_model->getRegionsRequest(25, $page, $where, $tableName, $id, $joinTable, $column);

        $data["links"] = $this->pagination->create_links();

        //$data['regions'] = $this->Region_model->getRegionsRequest();
        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('regions/index', $data);
        $this->load->view('elements/footer');
    }

    function stateFind()
    {
        $countryId = $_POST['countryId'];
        if (isset($countryId) && $countryId != '') {
            $data = $this->Region_model->checkForState($countryId);

            if (count($data) > 1) {
                echo json_encode($data);
                //return $data;
            } else {
                echo json_encode("error");
            }
            exit;
        }
    }

    //Delete Region
    public function delete($regionId, $regionTypeId, $regionType)
    {
        if ($regionTypeId == 1) {
            $tableName = 'tbl_area';
            $id = 'areaId';
        } elseif ($regionTypeId == 2) {
            $tableName = 'tbl_city';
            $id = 'cityId';
        }

        $res = $this->Region_model->delete($regionId, $tableName, $id);

        if ($res) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">' . ucwords($regionType) . '  Deleted Successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">' . ucwords($regionType) . ' could not be deleted.</div>');
        }
        redirect(base_url() . 'regions/regionrequest/' . $regionTypeId);

    }
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */