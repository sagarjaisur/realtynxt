<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ostestimonial extends CI_Controller
{
    public $page_caption = 'Testimonial'; // Added For page Captions

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Ostestimonial_model');
        $this->load->library('pagination');

        $this->load->library('ckeditor');
        $this->load->library('ckFinder');
        //configure base path of ckeditor folder
        $this->ckeditor->basePath = ASSET_URL . 'assets/ckeditor/';
        //$this->ckeditor->config['toolbar'] = 'Basic';
        //$this->ckeditor->config['toolbar'] = 'Basic';
        $this->ckeditor->config['scayt_autoStartup'] = TRUE;
        $this->ckeditor->config['resize_enabled'] = FALSE;
        $this->ckeditor->config['language'] = 'en';
        $this->ckeditor->config['enterMode'] = 'CKEDITOR.ENTER_BR';
        //CKFinder Config
        $this->ckeditor->config['filebrowserBrowseUrl'] = ASSET_URL . 'assets/ckfinder/ckfinder.html';
        $this->ckeditor->config['filebrowserImageBrowseUrl'] = ASSET_URL . 'assets/ckfinder/ckfinder.html?type=Images';
        $this->ckeditor->config['filebrowserFlashBrowseUrl'] = ASSET_URL . 'assets/ckfinder/ckfinder.html?type=Flash';
        $this->ckeditor->config['filebrowserUploadUrl'] = ASSET_URL . 'assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
        $this->ckeditor->config['filebrowserImageUploadUrl'] = ASSET_URL . 'assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
        $this->ckeditor->config['filebrowserFlashUploadUrl'] = ASSET_URL . 'assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';


        $user = $this->session->userdata('memberId');
        if (empty($user)) {
            redirect(base_url());
        }
    }

    public function index()
    {
        //Start - Pagination Setup
        $totalRecodeCount = $this->Ostestimonial_model->recode_count();
        $config['base_url'] = base_url() . 'ourstory/testimonial/index/';
        $config['total_rows'] = $totalRecodeCount;
        $config['per_page'] = LIMIT;
        $config["uri_segment"] = 3;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm inline">';
        $config['full_tag_close'] = '</ul>';
        $config['prev_link'] = '&lt;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&gt;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="current"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['first_link'] = '&lt;&lt;';
        $config['last_link'] = '&gt;&gt;';

        $data['links'] = $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data["allostestimonial"] = $this->Ostestimonial_model->getAllOsTestimonial(LIMIT, $page, '');

        $data["links"] = $this->pagination->create_links();
        //End - Pagination Setup

        //    $data['allostestimonial'] = $this->Ostestimonial_model->getAllOsTestimonial();
        //print_r($data);exit('ds');
        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('ourstory/testimonial/index', $data);
        $this->load->view('elements/footer');
    }


    // Insert new Testimonial
    public function add()
    {
        $this->form_validation->set_rules('testimonialTitle', 'Testimonial Title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('clientName', 'Client Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('testimonialShortDesc', 'Short Description', 'required|trim|xss_clean');
        $this->form_validation->set_rules('testimonialFullDesc', 'Full Description', 'required|trim|xss_clean');


        if ($this->form_validation->run() === FALSE) {
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('ourstory/testimonial/add');
            $this->load->view('elements/footer');
        } else {
            $data = array(
                'testimonialTitle' => $this->input->post('testimonialTitle'),
                'clientName' => $this->input->post('clientName'),
                'testimonialShortDesc' => $this->input->post('testimonialShortDesc'),
                'testimonialFullDesc' => $this->input->post('testimonialFullDesc')
            );


            $res = $this->Ostestimonial_model->add($data);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Added Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Added Successfully.</div>');
            }
            redirect(base_url() . 'ostestimonial');
        }

    }

    // Update Records
    public function edit($id)
    {
        if (empty($id)) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $data['id'] = $id;
        $this->form_validation->set_rules('testimonialTitle', 'Testimonial Title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('clientName', 'Client Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('testimonialShortDesc', 'Short Description', 'required|trim|xss_clean');
        $this->form_validation->set_rules('testimonialFullDesc', 'Full Description', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
            $data['ostestimonial'] = $this->Ostestimonial_model->getOsTestimonialById($id);
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('ourstory/testimonial/edit', $data);
            $this->load->view('elements/footer');
        } else {
            $data = array();

            $where = array('testimonialId' => $id);
            $data = array(
                'testimonialTitle' => $this->input->post('testimonialTitle'),
                'clientName' => $this->input->post('clientName'),
                'testimonialShortDesc' => $this->input->post('testimonialShortDesc'),
                'testimonialFullDesc' => $this->input->post('testimonialFullDesc')
            );


            $res = $this->Ostestimonial_model->update($data, $where);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Updated Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Updated Successfully.</div>');
            }
            redirect(base_url() . 'ostestimonial');
        }

    }

    public function delete($id)
    {
        if ($this->Ostestimonial_model->delete($id)) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Deleted Successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Deleted Successfully.</div>');
        }

        redirect(base_url() . 'ostestimonial');
    }

    public function status($testimonialId = '', $status = '')
    {
        if (isset($testimonialId) && isset($status)) {
            $data = $this->Ostestimonial_model->changeStatus($testimonialId, $status);

            $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
