<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Amenity extends CI_Controller
{
    public $page_caption = 'Amenity'; // Added For page Captions

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Amenity_model');
        $amenity = $this->session->userdata('memberId');

        if (empty($amenity)) {
            redirect(base_url());
        }
    }

    public function index()
    {
        $data['amenitys'] = $this->Amenity_model->get_amenity();
        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('amenity/index', $data);
        $this->load->view('elements/footer');
    }

    // Insert Amenity
    public function add()
    {
        $this->form_validation->set_rules('amenityName', 'Amenity Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('description', 'Description', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('amenity/add');
            $this->load->view('elements/footer');
        } else {
            if (!empty($_FILES) and $_FILES['image']['name'] != '') {
                $uploadResult = $this->do_upload();

                if (!array_key_exists('error', $uploadResult)) {
                    $image = UPLOAD_URL . 'amenities/' . $uploadResult['upload_data']['file_name'];
                } else {
                    $data['error'] = $uploadResult['error'];

                    $this->load->view('elements/header');
                    $this->load->view('elements/page_header_sidebar');
                    $this->load->view('amenity/add', $data);
                    $this->load->view('elements/footer');
                }
            } else {
                $image = $this->input->post('imageName');
            }

            $data = array(
                'amenityName' => $this->input->post('amenityName'),
                'description' => $this->input->post('description'),
                'image' => $image
            );

            $res = $this->Amenity_model->add($data);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Added Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Added Successfully.</div>');
            }
            redirect(base_url() . 'amenity/index');
        }
    }

    // Update Records
    public function edit($amenityId)
    {
        if (empty($amenityId)) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $data['amenityId'] = $amenityId;
        $this->form_validation->set_rules('amenityName', 'Amenity Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('description', 'Description', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
            $data['detail'] = $this->Amenity_model->get_detail($amenityId)[0];

            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('amenity/edit', $data);
            $this->load->view('elements/footer');

        } else {
            if (!empty($_FILES) and $_FILES['image']['name'] != '') {
                $uploadResult = $this->do_upload();

                if (!array_key_exists('error', $uploadResult)) {
                    $image = UPLOAD_URL . 'amenities/' . $uploadResult['upload_data']['file_name'];
                } else {
                    $data['error'] = $uploadResult['error'];

                    $this->load->view('elements/header');
                    $this->load->view('elements/page_header_sidebar');
                    $this->load->view('amenity/add', $data);
                    $this->load->view('elements/footer');
                }
            } else {
                $image = $this->input->post('imageName');
            }

            $data = array();

            $data = array(
                'amenityName' => $this->input->post('amenityName'),
                'description' => $this->input->post('description'),
                'image' => $image
            );

            $where = array('amenityId' => $amenityId);
            $res = $this->Amenity_model->update($data, $where);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Updated Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Updated Successfully.</div>');
            }
            redirect(base_url() . 'amenity/index');
        }
    }

    // Upload Process
    public function do_upload()
    {
        if (!empty($_FILES['image']['name'])) {
            $ext = pathinfo($_FILES['image']['name']);
            $pic = uniqid() . '.' . $ext['extension'];
        }

        $config['file_name'] = $pic;
        $config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . '/realtynxt/uploads/amenities/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('image')) {
            $error = array('error' => $this->upload->display_errors());
            return $error;
        } else {
            $data = array('upload_data' => $this->upload->data());
            return $data;
        }
    }

    public function delete($amenityId)
    {
        if ($this->Amenity_model->delete($amenityId)) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Deleted Successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Deleted Successfully.</div>');
        }
        redirect(base_url() . 'amenity/index');
    }

    public function status($amenityId = '', $status = '')
    {
        if (!empty($amenityId) && isset($status)) {

            $res = $this->Amenity_model->changeStatus($amenityId, $status);

            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-success"> Sorry, ' . $this->page_caption . 'status could not be changed.</div>');
            }

            $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            redirect($_SERVER['HTTP_REFERER']);
        }
        redirect(base_url() . 'amenity/index');
    }

}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */