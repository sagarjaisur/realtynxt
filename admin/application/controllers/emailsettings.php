<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Emailsettings extends CI_Controller
{
    public $page_caption = 'Email'; // Added For page Captions

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Emailsettings_model');
        $user = $this->session->userdata('memberId');

        $this->load->library('ckeditor');
        $this->load->library('ckFinder');
        //configure base path of ckeditor folder
        $this->ckeditor->basePath = ASSET_URL . 'assets/ckeditor/';

        //$this->ckeditor->config['toolbar'] = 'Basic';
        //$this->ckeditor->config['toolbar'] = 'Basic';
        $this->ckeditor->config['scayt_autoStartup'] = TRUE;
        $this->ckeditor->config['resize_enabled'] = FALSE;
        $this->ckeditor->config['language'] = 'en';
        $this->ckeditor->config['enterMode'] = 'CKEDITOR.ENTER_BR';
        //CKFinder Config
        $this->ckeditor->config['filebrowserBrowseUrl'] = ASSET_URL . 'assets/ckfinder/ckfinder.html';
        $this->ckeditor->config['filebrowserImageBrowseUrl'] = ASSET_URL . 'assets/ckfinder/ckfinder.html?type=Images';
        $this->ckeditor->config['filebrowserFlashBrowseUrl'] = ASSET_URL . 'assets/ckfinder/ckfinder.html?type=Flash';
        $this->ckeditor->config['filebrowserUploadUrl'] = ASSET_URL . 'assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
        $this->ckeditor->config['filebrowserImageUploadUrl'] = ASSET_URL . 'assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
        $this->ckeditor->config['filebrowserFlashUploadUrl'] = ASSET_URL . 'assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';

        if (empty($user)) {
            redirect(base_url());
        }
    }

    public function index()
    {
        $data['details'] = $this->Emailsettings_model->getEmailsTemplates();
        //print_r($data);exit('ds');
        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('emailsettings/index', $data);
        $this->load->view('elements/footer');
    }

    // Insert Banner
    public function add()
    {
        $this->form_validation->set_rules('emailTemplate', 'Email Template', 'required|trim|xss_clean');
        $this->form_validation->set_rules('emailSubject', 'Email Subject', 'required|trim|xss_clean');
        $this->form_validation->set_rules('emailBody', 'Email Body', 'required|trim|xss_clean');
        echo $this->form_validation->run();

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('emailsettings/add');
            $this->load->view('elements/footer');
        } else {
            $data = array('emailTemplate' => $this->input->post('emailTemplate'),
                'emailSubject' => $this->input->post('emailSubject'),
                'emailBody' => $this->input->post('emailBody'));


            $res = $this->Emailsettings_model->add($data);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Added Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Added Successfully.</div>');
            }
            redirect(base_url() . 'emailsettings/index');
        }

    }

    // Update Records
    public function edit($id)
    {
        if (empty($id)) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $data['id'] = $id;
        $this->form_validation->set_rules('emailTemplate', 'Email Template', 'required|trim|xss_clean');
        $this->form_validation->set_rules('emailSubject', 'Email Subject', 'required|trim|xss_clean');
        $this->form_validation->set_rules('emailBody', 'Email Body', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
            $data['details'] = $this->Emailsettings_model->getEmailTemplateById($id);
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('emailsettings/edit', $data);
            $this->load->view('elements/footer');
        } else {
            $data = array();

            $where = array('emailId' => $id);
            $data = array('emailTemplate' => $this->input->post('emailTemplate'),
                'emailSubject' => $this->input->post('emailSubject'),
                'emailBody' => $this->input->post('emailBody'));


            $res = $this->Emailsettings_model->update($data, $where);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Updated Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Updated Successfully.</div>');
            }
            redirect(base_url() . 'emailsettings/index');
        }

    }

    public function delete($id)
    {
        if ($this->Emailsettings_model->delete($id)) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Deleted Successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Deleted Successfully.</div>');
        }

        redirect(base_url() . 'emailsettings/index');
    }


}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */