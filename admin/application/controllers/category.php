<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Category extends CI_Controller
{
    public $page_caption = 'Services'; // Added For page Captions

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Category_model');
        $category = $this->session->userdata('memberId');

        if (empty($category)) {
            redirect(base_url());
        }
    }

    public function index()
    {
        $data['categories'] = $this->Category_model->get_category();
        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('category/index', $data);
        $this->load->view('elements/footer');
    }

    // Insert Category
    public function add()
    {
        $this->form_validation->set_rules('categoryName', 'Category Name', 'required|trim|xss_clean');
        echo $this->form_validation->run();

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('category/add');
            $this->load->view('elements/footer');
        } else {
            $data = array('categoryName' => $this->input->post('categoryName'),
                'status' => 1);

            $res = $this->Category_model->add($data);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Added Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Added Successfully.</div>');
            }
            redirect(base_url() . 'category/index');
        }

    }

    // Update Records
    public function edit($id)
    {
        if (empty($id)) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $data['id'] = $id;
        $this->form_validation->set_rules('categoryName', 'Category Title', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
            $data['detail'] = $this->Category_model->get_detail($id);

            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('category/edit', $data);
            $this->load->view('elements/footer');

        } else {
            $data = array();

            $where = array('id' => $id);
            $data = array('categoryName' => $this->input->post('categoryName'));

            $res = $this->Category_model->update($data, $where);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Updated Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Updated Successfully.</div>');
            }
            redirect(base_url() . 'category/index');
        }
    }

    public function delete($id)
    {
        if ($this->Category_model->delete($id)) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Deleted Successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Deleted Successfully.</div>');
        }
        redirect(base_url() . 'category/index');
    }

    public function status($id = '', $status = '')
    {
        if (isset($id) && isset($status)) {
            $res = $this->Category_model->changeStatus($id, $status);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  status could not be updated!</div>');
            }
            redirect(base_url() . 'category');
        } else {
            redirect(base_url() . 'category');
        }
    }

}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */