<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Role extends CI_Controller
{
    public $page_caption = 'Role'; // Added For page Captions

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Role_model');
        $role = $this->session->userdata('memberId');

        if (empty($role)) {
            redirect(base_url());
        }
    }

    public function index()
    {
        $data['roles'] = $this->Role_model->get_role();
        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('role/index', $data);
        $this->load->view('elements/footer');
    }

    // Insert Role
    public function add()
    {
        $this->form_validation->set_rules('roleTitle', 'Role Title', 'required|trim|xss_clean');
        echo $this->form_validation->run();

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('role/add');
            $this->load->view('elements/footer');
        } else {
            $data = array(
                'roleTitle' => $this->input->post('roleTitle'),
                'rolePermission' => 'enabled'
            );

            $res = $this->Role_model->add($data);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Added Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Added Successfully.</div>');
            }
            redirect(base_url() . 'role/index');
        }
    }

    // Update Records
    public function edit($roleId)
    {
        if (empty($roleId)) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $data['roleId'] = $roleId;
        $this->form_validation->set_rules('roleTitle', 'Role Title', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
            $data['detail'] = $this->Role_model->get_detail($roleId);

            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('role/edit', $data);
            $this->load->view('elements/footer');

        } else {
            $data = array();

            $where = array('roleId' => $roleId);
            $data = array('roleTitle' => $this->input->post('roleTitle'));

            $res = $this->Role_model->update($data, $where);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Updated Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Updated Successfully.</div>');
            }
            redirect(base_url() . 'role/index');
        }
    }

    public function delete($roleId)
    {
        if ($this->Role_model->delete($roleId)) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Deleted Successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Deleted Successfully.</div>');
        }
        redirect(base_url() . 'role/index');
    }

    public function status($roleId = '', $status = '')
    {
        if (!empty($roleId) && !empty($status)) {

            $data = $this->Role_model->changeStatus($roleId, $status);

            $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */