<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Currency extends CI_Controller
{
    public $page_caption = 'Currency'; // Added For page Captions

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Currency_model');
        $currency = $this->session->userdata('memberId');

        if (empty($currency)) {
            redirect(base_url());
        }
    }

    public function index()
    {
        $data['currencys'] = $this->Currency_model->get_currency();
        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('currency/index', $data);
        $this->load->view('elements/footer');
    }

    // Insert Currency
    public function add()
    {
        $this->form_validation->set_rules('currency', 'Currency', 'required|trim|xss_clean');
        $this->form_validation->set_rules('icon', 'Currency Icon', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('currency/add');
            $this->load->view('elements/footer');
        } else {
//            $icon = @implode(',', $this->input->post('icon'));

            $data = array(
                'currency' => $this->input->post('currency'),
                'icon' => $this->input->post('icon')
            );

            $res = $this->Currency_model->add($data);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Added Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Added Successfully.</div>');
            }
            redirect(base_url() . 'currency/index');
        }
    }

    // Update Records
    public function edit($currencyId)
    {
        if (empty($currencyId)) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $data['currencyId'] = $currencyId;
        $this->form_validation->set_rules('currency', 'Currency', 'required|trim|xss_clean');
        $this->form_validation->set_rules('icon', 'Currency Icon', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
            $data['detail'] = $this->Currency_model->get_detail($currencyId)[0];

            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('currency/edit', $data);
            $this->load->view('elements/footer');

        } else {
            $data = array();
//            $icon = @implode(',', $this->input->post('icon'));
            $data = array(
                'currency' => $this->input->post('currency'),
                'icon' => $this->input->post('icon')
            );

            $where = array('currencyId' => $currencyId);
            $res = $this->Currency_model->update($data, $where);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Updated Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Updated Successfully.</div>');
            }
            redirect(base_url() . 'currency/index');
        }
    }

    public function delete($currencyId)
    {
        if ($this->Currency_model->delete($currencyId)) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Deleted Successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Deleted Successfully.</div>');
        }
        redirect(base_url() . 'currency/index');
    }

    public function status($currencyId = '', $status = '')
    {
        if (!empty($currencyId) && isset($status)) {

            $res = $this->Currency_model->changeStatus($currencyId, $status);

            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-success"> Sorry, ' . $this->page_caption . 'status could not be changed.</div>');
            }

            $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            redirect($_SERVER['HTTP_REFERER']);
        }
        redirect(base_url() . 'currency/index');
    }

}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */