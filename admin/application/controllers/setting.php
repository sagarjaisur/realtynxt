<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends CI_Controller
{
    public $page_caption = 'Setting'; // Added For page Captions

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Setting_model');
        //configure base path of ckeditor folder
        $this->load->library('ckeditor');
        $this->load->library('ckFinder');
        $this->ckeditor->basePath = ASSET_URL . 'assets/ckeditor/';
        //$this->ckeditor->config['toolbar'] = 'Basic';
        //$this->ckeditor->config['toolbar'] = 'Basic';
        $this->ckeditor->config['scayt_autoStartup'] = TRUE;
        $this->ckeditor->config['resize_enabled'] = FALSE;
        $this->ckeditor->config['language'] = 'en';
        $this->ckeditor->config['enterMode'] = 'CKEDITOR.ENTER_BR';
        //CKFinder Config
        $this->ckeditor->config['filebrowserBrowseUrl'] = ASSET_URL . 'assets/ckfinder/ckfinder.html';
        $this->ckeditor->config['filebrowserImageBrowseUrl'] = ASSET_URL . 'assets/ckfinder/ckfinder.html?type=Images';
        $this->ckeditor->config['filebrowserFlashBrowseUrl'] = ASSET_URL . 'assets/ckfinder/ckfinder.html?type=Flash';
        $this->ckeditor->config['filebrowserUploadUrl'] = ASSET_URL . 'assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
        $this->ckeditor->config['filebrowserImageUploadUrl'] = ASSET_URL . 'assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
        $this->ckeditor->config['filebrowserFlashUploadUrl'] = ASSET_URL . 'assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';

        $user = $this->session->userdata('memberId');
        if (empty($user)) {
            redirect(base_url());
        }
    }

    public function index()
    {
        $data['stars'] = $this->Setting_model->get_banners();
        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('setting/index', $data);
        $this->load->view('elements/footer');
    }

    public function status($starId = '', $status = '')
    {
        if (!empty($starId) && !empty($status)) {
            $data = $this->Setting_model->changeStatus($starId, $status);

            $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            redirect($_SERVER['HTTP_REFERER']);
        }

    }


    // Upload  Prroccess
    public function do_upload()
    {

        if (!empty($_FILES['banner_image']['name'])) {
            $ext = pathinfo($_FILES['banner_image']['name']);
            $pic = uniqid() . '.' . $ext['extension'];
        }
        $config['file_name'] = $pic;
        $config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . '/canberr/uploads/banners/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $this->load->library('Upload', $config);

        if (!$this->upload->do_upload('banner_image')) {
            $error = array('error' => $this->upload->display_errors());
            $this->form_validation->rules('Image not uploaded');
        } else {
            $data = array('upload_data' => $this->upload->data());
            return $data;
        }

    }

    // Insert Banner
    public function add()
    {
        $this->form_validation->set_rules('settingName', 'Settin Name', 'required|trim|xss_clean');
        echo $this->form_validation->run();

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('setting/add');
            $this->load->view('elements/footer');
        } else {
            $data = array('settingName' => $this->input->post('settingName'),
                'settingValue' => $this->input->post('settingValue'));


            $res = $this->Setting_model->add($data);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Added Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Added Successfully.</div>');
            }
            redirect(base_url() . 'setting/index');
        }

    }

    // Update Records
    public function edit($settingId)
    {
        if (empty($settingId)) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $data['settingId'] = $settingId;
        $this->form_validation->set_rules('settingName', 'Setting Name', 'required|trim|xss_clean');

        if ($this->form_validation->run() === FALSE) {
            $data['detail'] = $this->Setting_model->get_single_banner_detail($settingId);

            $this->load->view('elements/header');

            $this->load->view('elements/page_header_sidebar');
            $this->load->view('setting/edit', $data);
            $this->load->view('elements/footer');

        } else {
            $data = array();

            $where = array('settingId' => $settingId);
            $data = array('settingName' => $this->input->post('settingName'),
                'settingValue' => $this->input->post('settingValue'));


            $res = $this->Setting_model->update($data, $where);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Updated Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Updated Successfully.</div>');
            }
            redirect(base_url() . 'setting/index');
        }

    }

    public function delete($settingId)
    {
        if ($this->Setting_model->delete($settingId)) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Deleted Successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Deleted Successfully.</div>');
        }

        redirect(base_url() . 'setting/index');
    }

}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
