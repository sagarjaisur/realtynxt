<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Career extends CI_Controller
{
    public $page_caption = 'Career '; // Added For page Captions

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Career_model');
        $this->load->library('pagination');

        $this->load->library('ckeditor');
        $this->load->library('ckFinder');
        //configure base path of ckeditor folder
        $this->ckeditor->basePath = ASSET_URL . 'assets/ckeditor/';
        //$this->ckeditor->config['toolbar'] = 'Basic';
        //$this->ckeditor->config['toolbar'] = 'Basic';
        $this->ckeditor->config['scayt_autoStartup'] = TRUE;
        $this->ckeditor->config['resize_enabled'] = FALSE;
        $this->ckeditor->config['language'] = 'en';
        $this->ckeditor->config['enterMode'] = 'CKEDITOR.ENTER_BR';
        //CKFinder Config
        $this->ckeditor->config['filebrowserBrowseUrl'] = ASSET_URL . 'assets/ckfinder/ckfinder.html';
        $this->ckeditor->config['filebrowserImageBrowseUrl'] = ASSET_URL . 'assets/ckfinder/ckfinder.html?type=Images';
        $this->ckeditor->config['filebrowserFlashBrowseUrl'] = ASSET_URL . 'assets/ckfinder/ckfinder.html?type=Flash';
        $this->ckeditor->config['filebrowserUploadUrl'] = ASSET_URL . 'assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
        $this->ckeditor->config['filebrowserImageUploadUrl'] = ASSET_URL . 'assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
        $this->ckeditor->config['filebrowserFlashUploadUrl'] = ASSET_URL . 'assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';


       $user = $this->session->all_userdata();
        if (empty($user['memberId'])) {
            redirect(SITE_URL);
        }

    }

    public function index()
    {
        //Start - Pagination Setup
        $totalRecodeCount = $this->Career_model->recode_count();
        $config['base_url'] = base_url() . 'career/index/';
        $config['total_rows'] = $totalRecodeCount;
        $config['per_page'] = LIMIT;
        $config["uri_segment"] = 3;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm inline">';
        $config['full_tag_close'] = '</ul>';
        $config['prev_link'] = '&lt;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&gt;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="current"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['first_link'] = '&lt;&lt;';
        $config['last_link'] = '&gt;&gt;';

        $data['links'] = $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data["cmspages"] = $this->Career_model->getCmsPages(LIMIT, $page, '');

        $data["links"] = $this->pagination->create_links();
        //End - Pagination Setup

        //    $data['cmspages'] = $this->Career_model->getCmsPages();
        //print_r($data);exit('ds');
        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('career/index', $data);
        $this->load->view('elements/footer');
    }


    // Insert new CMS Page
    public function add()
    {
        $this->form_validation->set_rules('careerTitle', 'Career Title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('careerExperience', 'Career Experience', 'required|trim|xss_clean');
        $this->form_validation->set_rules('careerLocations', 'Career Locations', 'required|trim|xss_clean');
        $this->form_validation->set_rules('careerResponsibilities', 'Career Responsibilities', 'required|trim|xss_clean');
        $this->form_validation->set_rules('careerSkils', 'Career Skils', 'required|trim|xss_clean');


        if ($this->form_validation->run() === FALSE) {
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('career/add');
            $this->load->view('elements/footer');
        } else {
            $data = array(
                'careerTitle' => $this->input->post('careerTitle'),
                'careerExperience' => $this->input->post('careerExperience'),
                'careerLocations' => $this->input->post('careerLocations'),
                'careerResponsibilities' =>$this->input->post('careerSkils'),
                'careerSkils' =>$this->input->post('careerResponsibilities')
            );


            $res = $this->Career_model->add($data);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Added Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Added Successfully.</div>');
            }
            redirect(base_url() . 'career/index');
        }

    }

    // Update Records
    public function edit($id)
    {
        if (empty($id)) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $data['id'] = $id;
        $this->form_validation->set_rules('careerTitle', 'Career Title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('careerExperience', 'Career Experience', 'required|trim|xss_clean');
        $this->form_validation->set_rules('careerLocations', 'Career Locations', 'required|trim|xss_clean');
        $this->form_validation->set_rules('careerResponsibilities', 'Career Responsibilities', 'required|trim|xss_clean');
        $this->form_validation->set_rules('careerSkils', 'Career Skils', 'required|trim|xss_clean');


        if ($this->form_validation->run() === FALSE) {
            $data['cmspage'] = $this->Career_model->getCmsPageById($id);
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('career/edit', $data);
            $this->load->view('elements/footer');
        } else {
            $data = array();

            $where = array('careerId' => $id);
            $data = array(
                'careerTitle' => $this->input->post('careerTitle'),
                'careerExperience' => $this->input->post('careerExperience'),
                'careerLocations' => $this->input->post('careerLocations'),
                'careerResponsibilities' =>$this->input->post('careerSkils'),
                'careerSkils' =>$this->input->post('careerResponsibilities')
            );


            $res = $this->Career_model->update($data, $where);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Updated Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Updated Successfully.</div>');
            }
            redirect(base_url() . 'career/index');
        }

    }

    public function delete($id)
    {
        if ($this->Career_model->delete($id)) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . '  Deleted Successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">' . $this->page_caption . '  Not Deleted Successfully.</div>');
        }

        redirect(base_url() . 'career/index');
    }

    public function status($page_id = '', $status = '')
    {
        if (isset($page_id) && isset($status)) {
            $data = $this->Career_model->changeStatus($page_id, $status);

            $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
