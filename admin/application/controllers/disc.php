<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Disc extends CI_Controller
{
    public $page_caption = 'Disc'; // Added For page Captions

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Disc_model');

        $disc = $this->session->userdata('memberId');

        if (empty($disc)) {
            redirect(base_url());
        }
    }

    public function index()
    {
        $data['discs'] = $this->Disc_model->get_disc();
        $this->load->view('elements/header');
        $this->load->view('elements/page_header_sidebar');
        $this->load->view('disc/index', $data);
        $this->load->view('elements/footer');
    }

    // Add New Disc
    public function add()
    {
        $this->form_validation->set_rules('discName', 'Disc Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('entryType', 'Entry Type', 'required');
//        $this->form_validation->set_rules('price', 'Price', 'required');
//        $this->form_validation->set_rules('discDaysET0', 'Disc Days', 'required');
//        $this->form_validation->set_rules('discDaysET1', 'Disc Days', 'required');
//        $this->form_validation->set_rules('discDaysET2', 'Disc Days', 'required');
//        $this->form_validation->set_rules('noOfEntry', 'Number Of Entry', 'required');
        $this->form_validation->set_rules('fromDate', 'From Date', 'required');
        $this->form_validation->set_rules('toDate', 'To Date', 'required');
        $this->form_validation->set_rules('ageLimit', 'Age Limit', 'required|numeric');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('disc/add');
            $this->load->view('elements/footer');
        } else {

            $entryTypes = $this->input->post('entryType');
            $prices = $this->input->post('price');
//            $discDays = @implode(',',$this->input->post('discDays'));
            $discDays[] = @implode(',', $this->input->post('discDaysET0'));
            $discDays[] = @implode(',', $this->input->post('discDaysET1'));
            $discDays[] = @implode(',', $this->input->post('discDaysET2'));
            $noOfEntries = $this->input->post('noOfEntry');
            $availableEntries = $this->input->post('noOfEntry');

            foreach ($entryTypes as $key => $entryType) {
                $discDetails[] = array(
                    'entryType' => $entryType,
                    'price' => $prices[$key],
                    'discDays' => $discDays[$key],
                    'noOfEntry' => $noOfEntries[$key],
                    'availableEntry' => $availableEntries[$key],
                );
            }

            $data = array(
                'discName' => $this->input->post('discName'),
                'fromDate' => $this->input->post('fromDate'),
                'toDate' => $this->input->post('toDate'),
                'ageLimit' => $this->input->post('ageLimit')
            );

            $res = $this->Disc_model->add($data, $discDetails);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . ' Added Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">Sorry, ' . $this->page_caption . ' could not be added. Please try again.</div>');
            }
            redirect(base_url() . 'disc/index');
        }
    }

    // Update Disc
    public function edit($discId)
    {
        if (empty($discId)) {
            redirect($_SERVER['HTTP_REFERER']);
        }

        $data['discId'] = $discId;
        $dataX['disc'] = $this->Disc_model->get_detail($discId)[0];

        $discDetailsArr = $this->Disc_model->get_disc_details($discId);

        foreach ($discDetailsArr as $key => $discDetail) {
            $dataX['discDetails'][$discDetail['entryType']] = $discDetail;
        }

        $this->form_validation->set_rules('discName', 'Disc Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('entryType', 'Entry Type', 'required');
//        $this->form_validation->set_rules('price', 'Price', 'required');
//        $this->form_validation->set_rules('discDaysET0', 'Disc Days', 'required');
//        $this->form_validation->set_rules('discDaysET1', 'Disc Days', 'required');
//        $this->form_validation->set_rules('discDaysET2', 'Disc Days', 'required');
//        $this->form_validation->set_rules('noOfEntry', 'Number Of Entry', 'required');
        $this->form_validation->set_rules('fromDate', 'From Date', 'required');
        $this->form_validation->set_rules('toDate', 'To Date', 'required');
        $this->form_validation->set_rules('ageLimit', 'Age Limit', 'required|trim|xss_clean|numeric');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('elements/header');
            $this->load->view('elements/page_header_sidebar');
            $this->load->view('disc/edit', $dataX);
            $this->load->view('elements/footer');

        } else {

            $entryTypes = $this->input->post('entryType');
            $prices = $this->input->post('price');
            $discDays[0] = @implode(',', $this->input->post('discDaysET0'));
            $discDays[1] = @implode(',', $this->input->post('discDaysET1'));
            $discDays[2] = @implode(',', $this->input->post('discDaysET2'));
            $noOfEntries = $this->input->post('noOfEntry');
            $availableEntries = $this->input->post('noOfEntry');

            foreach ($entryTypes as $key => $entryType) {
                $discDetails[$key] = array(
                    'discId' => $discId,
                    'entryType' => $entryType,
                    'price' => $prices[$key],
                    'discDays' => $discDays[$key],
                    'noOfEntry' => $noOfEntries[$key],
                    'availableEntry' => $availableEntries[$key],
                );
            }

            $data = array(
                'discName' => $this->input->post('discName'),
                'fromDate' => $this->input->post('fromDate'),
                'toDate' => $this->input->post('toDate'),
                'ageLimit' => $this->input->post('ageLimit')
            );

            $res = $this->Disc_model->update($data, $discDetails, $discId);
            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . ' Updated Successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-danger">Sorry,' . $this->page_caption . ' could not be updated..</div>');
            }
            redirect(base_url() . 'disc/index');
        }
    }


    // Delete Disc
    public function delete($discId)
    {
        if ($this->Disc_model->delete($discId)) {
            $this->session->set_flashdata('success', '<div class="alert alert-success">' . $this->page_caption . ' Deleted Successfully.</div>');
        } else {
            $this->session->set_flashdata('success', '<div class="alert alert-danger">Sorry, ' . $this->page_caption . ' could not be deleted.</div>');
        }
        redirect(base_url() . 'disc/index');
    }

    // Change Disc Status
    public function status($discId = '', $status = '')
    {
        if (!empty($discId) && isset($status)) {

            $res = $this->Disc_model->changeStatus($discId, $status);

            if ($res) {
                $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            } else {
                $this->session->set_flashdata('success', '<div class="alert alert-success"> Sorry, ' . $this->page_caption . 'status could not be changed.</div>');
            }

            $this->session->set_flashdata('success', '<div class="alert alert-success"> ' . $this->page_caption . ' status has been changed successfully.</div>');
            redirect($_SERVER['HTTP_REFERER']);
        }
        redirect(base_url() . 'disc/index');
    }
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */