<?php

class C_Loader extends CI_Loader
{

    //get header and footer for admin particular view
    public function template($template_name, $vars = array(), $return = FALSE)
    {
        $content = $this->view('elements/header', $vars, $return);
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('elements/footer', $vars, $return);

        if ($return) {
            return $content;
        }
    }
}

?>