<?php
$page_caption = 'Feature Page';
$cancel_url = base_url() . 'feature/index';
$submitForm_url = 'feature/add/';
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header featureTitle">Add <?php echo $page_caption; ?></h1>
                </section>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <?php if (isset($error)) echo '<div class="alert alert-danger">' . $error . '</div>'; ?>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php echo $page_caption; ?> Detail
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-10">
                                <?php echo form_open_multipart($submitForm_url) ?>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <label>Feature Title *</label>
                                        <input class="form-control validate[required] featureTitle"
                                               name="featureTitle"
                                               type="text"
                                               value="<?php echo set_value("featureTitle") ? set_value("featureTitle") : ""; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>
                                     <div class="col-md-12">
                                        <br/>
                                        <label>Icon Text *</label>
                                         <input class="form-control validate[required] featureTitle"
                                               name="featureIcon"
                                               type="text"
                                               value="<?php echo set_value("featureIcon") ? set_value("featureIcon") : ""; ?>"
                                               required>
                                    </div>
                                    <div class="col-md-12">
                                        <br/>
                                        <label>Short Description *</label>
<!--                                        <textarea class="form-control" name="featureShortDescription" required>--><?php //echo set_value('featureShortDescription'); ?><!--</textarea>-->
                                        <?php echo $this->ckeditor->editor('featureShortDescription', set_value('featureShortDescription')); ?>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-12">
                                        <br/>
                                        <label>Full Description *</label>
<!--                                        <textarea class="form-control" name="featureDescription" required>--><?php //echo set_value('featureDescription'); ?><!--</textarea>-->
                                        <?php echo $this->ckeditor->editor('featureDescription', set_value('featureDescription')); ?>
                                    </div>
                                    <div class="clearfix"></div>

                                </div>

                                <input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
                                <input type="button" name="Cancel" value="Cancel" class="btn btn-success"
                                       onclick="javascript: window.location.href='<?php echo $cancel_url; ?>';"/>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
</div>
