<?php
$page_caption = 'Document';
$cancel_url = base_url() . 'document/index';
$submitForm_url = 'document/edit/' . $documentId;
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title">Edit <?php echo $page_caption; ?></h1>
                </section>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <?php if (isset($error)) echo '<div class="alert alert-danger">' . $error . '</div>'; ?>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <?php echo form_open_multipart($submitForm_url) ?>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label>Document Title *</label>
                                        <input class="form-control validate[required] name"
                                               name="name"
                                               type="text"
                                               value="<?php echo set_value("name") ? set_value("name") : $detail['name']; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label>Document Type *</label>
                                        <?php
                                        $options = array(
                                            'normal' => 'Normal',
                                            'exclusive' => 'Exclusive'
                                        );

                                        $selected = ($this->input->post('type')) ? $this->input->post('type') : $detail['type'];

                                        echo form_dropdown('type', $options, $selected, ' class="form-control"');
                                        ?>
                                    </div>
                                    <div class="clearfix"></div>
                                    <!--
                                    <div class="col-md-6">
                                        <label class="" id="uploadImageData">Upload Images</label>
                                        <div id="imagesData"></div>
                                        <input type="file" name="file_upload" id="file_upload" required/>
                                    </div>
                                    <div class="clearfix"></div>
                                    -->
                                </div>
                                <input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
                                <input type="button" name="Cancel" value="Cancel" class="btn btn-success"
                                       onclick="javascript: window.location.href='<?php echo $cancel_url; ?>';"/>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->
</div>