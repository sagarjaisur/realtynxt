<?php
$page_caption = 'Adventure';
$cancel_url = base_url() . 'adventure/index';
$submitForm_url = 'adventure/edit/' . $adventureId;
$eventName_url = base_url() . 'adventure/';
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title">Edit <?php echo $page_caption; ?></h1>
                </section>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <?php if (isset($error)) echo '<div class="alert alert-danger">' . $error . '</div>'; ?>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php echo form_open_multipart($submitForm_url) ?>
                                <input type="hidden" id="eventName_url" name="eventName_url"
                                       value="<?php echo $eventName_url; ?>">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <label>Adventure Type *</label>
                                        <?php
                                        $selected = ($this->input->post('adventureType')) ? $this->input->post('adventureType') : $detail['adventureType'];
                                        echo form_dropdown('adventureType', $adventureTypesList, $selected, ' class="form-control validate[required]" id="adventureType" required');
                                        ?>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-4">
                                        <label>Event Name *</label>
                                        <?php
                                        $selected = ($this->input->post('eventName')) ? $this->input->post('eventName') : $detail['eventName'];
                                        echo form_dropdown('eventName', $eventNamesList, $selected, ' class="form-control validate[required]" id="eventName" required');
                                        ?>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-12" style="text-align: center">
                                        <br/>
                                        <h3>Nature of Event</h3><br/><br/>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <?php
                                            $isOneTime = $isRepetitive = false;
                                            if (!empty($detail['frequency'])) {
                                                $isRepetitive = true;
                                            } else {
                                                $isOneTime = true;
                                            }
                                            echo form_radio('natureOfEvent', 'One Time', $isOneTime); ?><?php echo form_label('One Time', 'natureOfEvent'); ?>
                                            <div class="clearfix"></div>

                                            <div class="col-md-6">
                                                <input type="date" name="date" id="date"
                                                       value="<?php echo set_value("date") ? set_value("date") : $detail['date']; ?>"
                                                       class="form-control">
                                            </div>
                                            <div class="clearfix"></div>

                                            <div class="col-md-6">
                                                <lable>Starting Time :</lable>
                                                <input type="text" id="startingTime"
                                                       value="<?php echo set_value("startingTime") ? set_value("startingTime") : $detail['startingTime']; ?>"
                                                       name="startingTime" class="form-control">
                                            </div>

                                        </div>

                                        <div class="col-md-6" style="border-left: 2px solid">
                                            <?php echo form_radio('natureOfEvent', 'Repetitive', $isRepetitive); ?><?php echo form_label('Repetitive', 'natureOfEvent'); ?>
                                            <div class="clearfix"></div>

                                            <div class="col-md-6">
                                                <lable>TimeSpan :</lable>
                                                <input type="text" name="timespan" id="timespan"
                                                       value="<?php echo set_value("timespan") ? set_value("timespan") : $detail['timespan']; ?>"
                                                       class="form-control">
                                            </div>
                                            <div class="clearfix"></div>

                                            <div class="col-md-3">
                                                <?php
                                                $isDaily = $isMonthly = $isWeekly = false;
                                                if ($detail['frequency'] == 'Daily') {
                                                    $isDaily = true;
                                                } elseif ($detail['frequency'] == 'Weekly') {
                                                    $isWeekly = true;
                                                } elseif ($detail['frequency'] == 'Monthly') {
                                                    $isMonthly = true;
                                                }
                                                echo form_radio('frequency', 'Daily', $isDaily); ?><?php echo form_label('Daily', 'repeatInterval'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <?php echo form_radio('frequency', 'Weekly', $isWeekly); ?><?php echo form_label('Weekly', 'repeatInterval'); ?>
                                                <div class="clearfix"></div>

                                                <?php
                                                if ($detail['frequency'] == 'Weekly' && $detail['repeatInterval'] != '') {
                                                    $weekDays = @explode(',', $detail['repeatInterval']);
                                                } else {
                                                    $weekDays = '';
                                                }
                                                ?>

                                                <input class="validate[required] weekDays"
                                                       name="weekDays[]" type="checkbox"
                                                       value="1" <?php if ($weekDays != '' && in_array(1, $weekDays)) {
                                                    echo 'checked="checked"';
                                                } ?>>Sunday<br/>
                                                <input class="validate[required] weekDays"
                                                       name="weekDays[]" type="checkbox"
                                                       value="2" <?php if ($weekDays != '' && in_array(2, $weekDays)) {
                                                    echo 'checked="checked"';
                                                } ?>>Monday<br/>
                                                <input class="validate[required] weekDays"
                                                       name="weekDays[]" type="checkbox"
                                                       value="3" <?php if ($weekDays != '' && in_array(3, $weekDays)) {
                                                    echo 'checked="checked"';
                                                } ?>>Tuesday<br/>
                                                <input class="validate[required] weekDays"
                                                       name="weekDays[]" type="checkbox"
                                                       value="4" <?php if ($weekDays != '' && in_array(4, $weekDays)) {
                                                    echo 'checked="checked"';
                                                } ?>>Wednesday<br/>
                                                <input class="validate[required] weekDays"
                                                       name="weekDays[]" type="checkbox"
                                                       value="5" <?php if ($weekDays != '' && in_array(5, $weekDays)) {
                                                    echo 'checked="checked"';
                                                } ?>>Thursday<br/>
                                                <input class="validate[required] weekDays"
                                                       name="weekDays[]" type="checkbox"
                                                       value="6" <?php if ($weekDays != '' && in_array(6, $weekDays)) {
                                                    echo 'checked="checked"';
                                                } ?>>Friday<br/>
                                                <input class="validate[required] weekDays"
                                                       name="weekDays[]" type="checkbox"
                                                       value="7" <?php if ($weekDays != '' && in_array(7, $weekDays)) {
                                                    echo 'checked="checked"';
                                                } ?>>Saturday
                                            </div>
                                            <div class="col-md-5">
                                                <?php
                                                if ($detail['frequency'] == 'Monthly' && $detail['repeatInterval'] != '') {
                                                    $repetitiveDates = $detail['repeatInterval'];
                                                } else {
                                                    $repetitiveDates = '';
                                                }
                                                echo form_radio('frequency', 'Monthly', $isMonthly); ?><?php echo form_label('Monthly', 'repeatInterval'); ?>
                                                <div class="clearfix"></div>

                                                <input type="text" id="repetitiveDate"
                                                       value="<?php echo set_value("repetitiveDate") ? set_value("repetitiveDate") : $repetitiveDates ?>"
                                                       name="repetitiveDate" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <br/>
                                    <div class="col-md-6">
                                        <label>Capacity *</label>
                                        <input class="form-control validate[required] capacity"
                                               name="capacity"
                                               type="text"
                                               value="<?php echo set_value("capacity") ? set_value("capacity") : $detail['capacity']; ?>"
                                               required>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Price Per Person *</label>
                                        <input class="form-control validate[required] price" name="price"
                                               type="text"
                                               value="<?php echo set_value("price") ? set_value("price") : $detail['price']; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label>Contact Person Name *</label>
                                        <input class="form-control validate[required] contactName" name="contactName"
                                               type="text"
                                               value="<?php echo set_value("contactName") ? set_value("contactName") : $detail['contactName']; ?>"
                                               required>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Contact Number *</label>
                                        <input class="form-control validate[required] contactNumber"
                                               name="contactNumber"
                                               type="text"
                                               value="<?php echo set_value("contactNumber") ? set_value("contactNumber") : $detail['contactNumber']; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label>Meeting Address *</label>
                                        <textarea class="form-control validate[required] meetingAddress"
                                                  name="meetingAddress"
                                                  required><?php echo set_value("meetingAddress") ? set_value("meetingAddress") : $detail['meetingAddress']; ?></textarea>
                                    </div>
                                    <div class="clearfix"></div>

                                    <hr>
                                    <br/>

                                    <div class="col-md-4">
                                        <label>Safety Precautions *</label>
                                        <textarea class="form-control validate[required] safetyPrecautions"
                                                  name="safetyPrecautions"
                                                  required><?php echo set_value("safetyPrecautions") ? set_value("safetyPrecautions") : $detail['safetyPrecautions']; ?></textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Where to Come? *</label>
                                        <textarea class="form-control validate[required] whereToCome" name="whereToCome"
                                                  name="whereToCome"
                                                  required><?php echo set_value("whereToCome") ? set_value("whereToCome") : $detail['whereToCome']; ?></textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Who will like it? *</label>
                                        <textarea class="form-control validate[required] liker" name="liker"
                                                  name="liker"
                                                  required><?php echo set_value("liker") ? set_value("liker") : $detail['liker']; ?></textarea>
                                    </div>


                                    <div class="col-md-6">
                                        <label class="" id="uploadImageData">Upload Images</label>
                                        <div id="imagesData">
                                        </div>
                                        <input type="file" name="adventure_file_upload" id="adventure_file_upload"
                                               required/>

                                        <?php if ($detail['images'] != '') {
                                            $images = explode(",", $detail['images']);
                                            ?>
                                            <span id="imagediv">
                                                 <?php
                                                 foreach ($images AS $image) { ?>
                                                     <input type="hidden" name="images[]"
                                                            value="<?php echo $image; ?>"/>
                                                     <img id="upload_image" class="upload_side_img2"
                                                          src="<?php echo $image; ?>"
                                                          height="150px" width="150px" style="margin-bottom: 10px">

                                                 <?php } ?>
                                                <input type="button" value="Hide Preview" id="remove_upload"
                                                       class="remove_upload btn btn-success btn-md submit_btn">
                                                </span>
                                        <?php } else { ?>
                                            <span id="imagediv"></span>
                                        <?php } ?>

                                    </div>
                                    <div class="clearfix"></div>

                                </div>

                                <input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
                                <input type="button" name="Cancel" value="Cancel" class="btn btn-success"
                                       onclick="javascript: window.location.href='<?php echo $cancel_url; ?>';"/>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->
</div>