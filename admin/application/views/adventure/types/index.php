<?php
$page_caption = 'Adventure';
$cancel_url = base_url() . 'adventure/types';
$addType_url = base_url() . 'adventure/addType/';
$addEventName_url = base_url() . 'adventure/addEventName/';
?>
<link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/dataTables/dataTables.bootstrap.css">
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">

        <div class="row">
            <!--  page header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title"><?php echo $page_caption; ?> Management [<span
                            style="font-size: 90%">Manage Event Types</span>]</h1>
                    <input type="button"
                           onClick="location.href='<?php echo $addType_url; ?>'"
                           name="submit"
                           value="Add Adventure Type"
                           class="btn btn-success btn-md submit">

                    <input type="button"
                           onClick="location.href='<?php echo $addEventName_url; ?>'"
                           name="submit"
                           value="Add Event Name"
                           class="btn btn-success btn-md submit"
                           style="margin-left: 50px">

                    <ol class="breadcrumb">
                        <li><a href="<?php echo MAIN_DIR_URL . 'admin/index.php/dashboard/'; ?>"><i
                                    class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active"><a href="#"><?php echo $page_caption; ?> Management</a></li>
                        <li class="active"><?php echo "Manage Event Types"; ?></li>
                    </ol>

                </section>
            </div>
            <!-- end  page header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <br/>
        <div class="row">
            <div class="col-lg-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>Adventure Type</th>
                                    <th>Event Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (isset($adventures) && $adventures != "") {
//                                    foreach ($adventures as $adventure) if($adventure['adventureType'] != ""){
                                    foreach ($adventures as $adventure) {

                                        $status = $adventure['status'];
                                        $editLink = base_url() . 'adventure/editType/' . $adventure['id'];

                                        if ($adventure['parentId'] != 0) {
                                            $deleteLink = base_url() . 'adventure/deleteType/' . $adventure['id'] . '/' . $status . '/0';
                                            $statusLink = base_url() . 'adventure/statusType/' . $adventure['id'] . '/' . $status . '/0';
                                        } else {
                                            $deleteLink = base_url() . 'adventure/deleteType/' . $adventure['id'] . '/' . $status . '/1';
                                            $statusLink = base_url() . 'adventure/statusType/' . $adventure['id'] . '/' . $status . '/1';
                                        }

                                        ?>
                                        <tr class="odd gradeX">
                                            <?php
                                            if ($adventure['adventureType'] != "") {
                                                ?>
                                                <td></td>
                                                <td>
                                                    <?php echo $adventure['eventName']; ?>
                                                </td>

                                                <?php
                                            } else {
                                                ?>
                                                <td colspan="2">
                                                    <b><?php echo $adventure['eventName'] . " .:"; ?></b>
                                                </td>
                                                <?php
                                            }
                                            ?>
                                            <td>
                                                <button
                                                    class="<?php echo $status ? 'btn btn-success' : 'btn btn-danger'; ?>"
                                                    type="button"
                                                    onClick="javascipt:window.location.href='<?php echo $statusLink; ?>';"
                                                    title="Click here to Toggle (Active / Inactive)">
                                                    <?php echo $status ? 'Active' : 'Inactive'; ?>
                                                </button>
                                            </td>
                                            <td class="center">
                                                <button class="btn btn-default" type="button"
                                                        onClick="javascipt:window.location.href='<?php echo $editLink; ?>';"
                                                        title="EDIT">
                                                    <i class="fa fa-edit fa-fw"></i>Edit
                                                </button>

                                                <button class="btn btn-danger" type="button"
                                                        onClick="return deleteRecode('<?php echo $deleteLink; ?>','<?php echo $page_caption; ?>');"
                                                        title="DELETE">
                                                    <i class="fa fa-edit fa-fw"></i>Delete
                                                </button>
                                            </td>
                                        </tr>
                                    <?php }
                                } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.panel-body -->

                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->
</div>