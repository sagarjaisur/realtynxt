<?php
$user = $this->session->all_userdata();
$parentAdventureName = (isset($user['adventureTypes'][$adventureTypeId + 1])) ? ucwords($user['adventureTypes'][$adventureTypeId + 1]) : 'Continent';
$childAdventureName = $adventureType;
//$adventureParentId = array_search($adventureParent, $user['adventureTypes']);
$adventureParentId = $adventureParent . 'Id';
$adventureId = $adventureType . 'Id';
$db_AdventureName = $adventureType . 'Name';

$page_caption = 'Edit Adventure';
$cancel_url = base_url() . 'adventure/types';
//$submitForm_url = 'adventure/edit/' . $adventureTypeId;
$submitForm_url = 'adventure/edit/' . $adventureId . '/' . $adventureTypeId . '/' . $adventureType;
$ajax_url = base_url() . 'adventure/';
$state_url = base_url() . 'adventure/';
//$adventureTypeId = $adventure[0]['adventureTypeId']

?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <h1 class="page-header page_title">&nbsp;<?php echo $page_caption; ?> Details</h1>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <?php if (isset($error)) echo '<div class="alert alert-danger">' . $error . '</div>'; ?>
        <div class="row">
            <div class="col-lg-12">
                <?php echo form_open_multipart($submitForm_url, 'id="edit_adventure"') ?>
                <input type="hidden" id="ajax_url" name="ajax_url" value="<?php echo $ajax_url; ?>">
                <input type="hidden" id="state_url" name="state_url" value="<?php echo $state_url; ?>">
                <input type="hidden" id="<?php echo $adventureId; ?>" name="<?php echo $adventureId; ?>"
                       value="<?php echo $adventureId; ?>">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php echo $page_caption; ?> Details
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <?php echo ucwords($childAdventureName); ?> Details
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label><?php echo $parentAdventureName; ?></label>
                                            <?php
                                            $selected = ($this->input->post('adventureParentId')) ? $this->input->post('adventureParentId') : (isset($adventure[$adventureParentId])) ? $adventure[$adventureParentId] : 'Asia';
                                            echo form_dropdown('adventureParentId', $adventureParentList, $selected, ' class="form-control"');
                                            ?>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label><?php echo ucwords($childAdventureName); ?> Name *</label>
                                            <input class="form-control" name="adventureName" type="text"
                                                   value="<?php print set_value('adventureName') ? set_value('adventureName') : $adventure[$db_AdventureName]; ?>"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <hr>
                            <div class="col-md-6 col-xs-4">
                                <div class="col-md-2 putright">
                                    <input type="submit" name="submit" value="Submit"
                                           class="btn btn-success btn-md submit">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-4">
                                <div class="col-md-2 putleft">
                                    <input type="button" name="Cancel" value="Cancel" class="btn btn-default btn-md"
                                           onclick="javascript: window.location.href='<?php echo base_url() . 'adventure/types/' . $adventureTypeId; ?>';">
                                </div>
                            </div>
                            <br/>&nbsp;

                        </div>


                        <!--End Form Elements -->

                        </form>
                    </div>
                </div>
            </div>
            <br/>
        </div>
    </div>
    <!-- end page-wrapper -->
</div>