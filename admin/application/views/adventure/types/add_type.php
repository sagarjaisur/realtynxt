<?php
$page_caption = 'Adventure Type';
$cancel_url = base_url() . 'adventure/types';
$submitForm_url = 'adventure/addType/';
$ajax_url = base_url() . 'adventure/';
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title">Add <?php echo $page_caption; ?></h1>
                </section>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <div class="row">
            <div class="col-lg-12">
                <?php echo form_open_multipart($submitForm_url) ?>
                <input type="hidden" id="ajax_url" name="ajax_url" value="<?php echo $ajax_url; ?>">

                <div class="form-group">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo $page_caption; ?> Details
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Adventure Type :</label>
                                    <input class="form-control validate[required] adventureType" name="adventureType"
                                           type="text"
                                           value="<?php echo set_value("adventureType") ? set_value("adventureType") : ""; ?>"
                                           required>
                                </div>
                            </div>

                            <div class="col-md-12 mb50">
                                <hr>
                                <input type="submit" name="submit" value="Submit" class="btn btn-success"/>
                                <input type="button" name="Cancel" value="Cancel" class="btn btn-primary"
                                       onclick="javascript: window.location.href='<?php echo $cancel_url; ?>';"/>
                            </div>
                        </div>
                    </div>
                    <!-- End Form Elements -->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>