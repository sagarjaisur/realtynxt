<?php
$page_caption = 'Adventure';
$cancel_url = base_url() . 'adventure/index';
$submitForm_url = 'adventure/add/';
$eventName_url = base_url() . 'adventure/';
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title">Add <?php echo $page_caption; ?></h1>
                </section>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <?php if (isset($error)) echo '<div class="alert alert-danger">' . $error . '</div>'; ?>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php echo $page_caption; ?> Detail
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php echo form_open_multipart($submitForm_url) ?>
                                <input type="hidden" id="eventName_url" name="eventName_url"
                                       value="<?php echo $eventName_url; ?>">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <label>Adventure Type *</label>
                                        <?php
                                        $selected = ($this->input->post('adventureType')) ? $this->input->post('adventureType') : reset($adventureTypesList);
                                        echo form_dropdown('adventureType', $adventureTypesList, $selected, ' class="form-control validate[required]" id="adventureType" required');
                                        ?>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-4">
                                        <label>Event Name *</label>
                                        <?php
                                        $selected = ($this->input->post('eventName')) ? $this->input->post('eventName') : reset($eventNamesList);
                                        echo form_dropdown('eventName', $eventNamesList, $selected, ' class="form-control validate[required]" id="eventName" required');
                                        ?>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-12" style="text-align: center">
                                        <br/>
                                        <h3>Nature of Event</h3><br/><br/>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <!--                                            <div class="iradio_minimal-blue checked" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="natureOfEvent" class="minimal" value="One Time" checked="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>-->
                                            <!--                                            <lable>One Time</lable>-->
                                            <?php echo form_radio('natureOfEvent', 'One Time', TRUE); ?><?php echo form_label('One Time', 'natureOfEvent'); ?>
                                            <div class="clearfix"></div>

                                            <div class="col-md-6">
                                                <input type="date" name="date" id="date"
                                                       value="<?php echo set_value("date") ? set_value("date") : ""; ?>"
                                                       class="form-control">
                                            </div>
                                            <div class="clearfix"></div>

                                            <div class="col-md-6">
                                                <lable>Starting Time :</lable>
                                                <input type="text" id="startingTime"
                                                       value="<?php echo set_value("startingTime") ? set_value("startingTime") : ""; ?>"
                                                       name="startingTime" class="form-control">
                                            </div>

                                        </div>

                                        <div class="col-md-6" style="border-left: 2px solid">
                                            <?php echo form_radio('natureOfEvent', 'Repetitive', FALSE); ?><?php echo form_label('Repetitive', 'natureOfEvent'); ?>
                                            <div class="clearfix"></div>

                                            <div class="col-md-6">
                                                <lable>TimeSpan :</lable>
                                                <input type="text" name="timespan" id="timespan"
                                                       value="<?php echo set_value("timespan") ? set_value("timespan") : ""; ?>"
                                                       class="form-control">
                                            </div>
                                            <div class="clearfix"></div>

                                            <div class="col-md-3">
                                                <?php echo form_radio('frequency', 'Daily', FALSE); ?><?php echo form_label('Daily', 'repeatInterval'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <?php echo form_radio('frequency', 'Weekly', FALSE); ?><?php echo form_label('Weekly', 'repeatInterval'); ?>
                                                <div class="clearfix"></div>

                                                <input class="validate[required] repeatInterval"
                                                       name="weekDays[]" type="checkbox"
                                                       value="1">Sunday<br>
                                                <input class="validate[required] repeatInterval"
                                                       name="weekDays[]" type="checkbox"
                                                       value="2">Monday<br>
                                                <input class="validate[required] repeatInterval"
                                                       name="weekDays[]" type="checkbox"
                                                       value="3">Tuesday<br>
                                                <input class="validate[required] repeatInterval"
                                                       name="weekDays[]" type="checkbox"
                                                       value="4">Wednesday<br>
                                                <input class="validate[required] repeatInterval"
                                                       name="weekDays[]" type="checkbox"
                                                       value="5">Thursday<br>
                                                <input class="validate[required] repeatInterval"
                                                       name="weekDays[]" type="checkbox"
                                                       value="6">Friday<br>
                                                <input class="validate[required] repeatInterval"
                                                       name="weekDays[]" type="checkbox"
                                                       value="7">Saturday
                                            </div>
                                            <div class="col-md-5">
                                                <?php echo form_radio('frequency', 'Monthly', FALSE); ?><?php echo form_label('Monthly', 'repeatInterval'); ?>
                                                <div class="clearfix"></div>

                                                <input type="text" id="repetitiveDate"
                                                       value="<?php echo set_value("repetitiveDate") ? set_value("repetitiveDate") : ""; ?>"
                                                       name="repetitiveDate" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <br/>
                                    <div class="col-md-6">
                                        <label>Capacity *</label>
                                        <input class="form-control validate[required] capacity"
                                               name="capacity"
                                               type="text"
                                               value="<?php echo set_value("capacity") ? set_value("capacity") : ""; ?>"
                                               required>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Price Per Person *</label>
                                        <input class="form-control validate[required] price" name="price"
                                               type="text"
                                               value="<?php echo set_value("price") ? set_value("price") : ""; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label>Contact Person Name *</label>
                                        <input class="form-control validate[required] contactName" name="contactName"
                                               type="text"
                                               value="<?php echo set_value("contactName") ? set_value("contactName") : ""; ?>"
                                               required>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Contact Number *</label>
                                        <input class="form-control validate[required] contactNumber"
                                               name="contactNumber"
                                               type="text"
                                               value="<?php echo set_value("contactNumber") ? set_value("contactNumber") : ""; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label>Meeting Address *</label>
                                        <textarea class="form-control validate[required] meetingAddress"
                                                  name="meetingAddress"
                                                  required><?php echo set_value("meetingAddress") ? set_value("meetingAddress") : ""; ?></textarea>
                                    </div>
                                    <div class="clearfix"></div>

                                    <hr>
                                    <br/>

                                    <div class="col-md-4">
                                        <label>Safety Precautions *</label>
                                        <textarea class="form-control validate[required] safetyPrecautions"
                                                  name="safetyPrecautions"
                                                  required><?php echo set_value("safetyPrecautions") ? set_value("safetyPrecautions") : ""; ?></textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Where to Come? *</label>
                                        <textarea class="form-control validate[required] whereToCome" name="whereToCome"
                                                  name="whereToCome"
                                                  required><?php echo set_value("whereToCome") ? set_value("whereToCome") : ""; ?></textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Who will like it? *</label>
                                        <textarea class="form-control validate[required] liker" name="liker"
                                                  name="liker"
                                                  required><?php echo set_value("liker") ? set_value("liker") : ""; ?></textarea>
                                    </div>


                                    <div class="col-md-6">
                                        <label class="" id="uploadImageData">Upload Images</label>
                                        <div id="imagesData"></div>
                                        <input type="file" name="adventure_file_upload" id="adventure_file_upload"
                                               required/>
                                    </div>
                                    <div class="clearfix"></div>

                                </div>

                                <input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
                                <input type="button" name="Cancel" value="Cancel" class="btn btn-success"
                                       onclick="javascript: window.location.href='<?php echo $cancel_url; ?>';"/>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
</div>