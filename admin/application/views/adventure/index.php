<?php
$page_caption = 'Adventure';
$cancel_url = base_url() . 'adventure/index';
$add_url = base_url() . 'adventure/add/';
?>
<link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/dataTables/dataTables.bootstrap.css">
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">

        <div class="row">
            <!--  page header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title">&nbsp;<?php echo $page_caption; ?> Management<input type="button"
                                                                                                           onClick="location.href='<?php echo $add_url; ?>'"
                                                                                                           name="submit"
                                                                                                           value="Add <?php echo $page_caption; ?>"
                                                                                                           class="btn btn-success btn-md submit"
                                                                                                           style="margin-left: 50px">
                    </h1>

                    <ol class="breadcrumb">
                        <li><a href="<?php echo MAIN_DIR_URL . 'admin/index.php/dashboard/'; ?>"><i
                                    class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#"><?php echo $page_caption; ?> Management</a></li>
                        <li class="active"><?php echo "Manage Events"; ?></li>
                    </ol>
                </section>
            </div>
            <!-- end  page header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>

        <div class="row">
            <div class="col-lg-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>Event ID</th>
                                    <th>Event Name</th>
                                    <th>Frequency</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Vacancy</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (isset($adventures) && $adventures != "") {
                                    foreach ($adventures as $adventure) {

                                        $editLink = base_url() . 'adventure/edit/' . $adventure['adventureId'];
                                        $deleteLink = base_url() . 'adventure/delete/' . $adventure['adventureId'];

                                        $status = $adventure['status'];
                                        $statusLink = base_url() . 'adventure/status/' . $adventure['adventureId'] . '/' . $status;

                                        ?>
                                        <tr class="odd gradeX">
                                            <td>
                                                <?php echo $adventure['adventureId']; ?>
                                            </td>
                                            <td>
                                                <?php echo $adventure['eventName']; ?>
                                            </td>
                                            <td>
                                                <?php echo $adventure['frequency'] ? $adventure['frequency'] : 'One Time'; ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($adventure['frequency'] == 0 && $adventure['date'] != "0000-00-00") {
                                                    $date = date_create($adventure['date']);
                                                    echo date_format($date, "d F, Y");
                                                } else {
                                                    if ($adventure['frequency'] == 'Daily') {
                                                        echo 'Daily';
                                                    } elseif ($adventure['frequency'] == 'Weekly') {
                                                        $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

                                                        $count = 0;
                                                        foreach ($days as $key => $day) if (in_array($key + 1, explode(',', $adventure['repeatInterval']))) {
                                                            echo $day . '<br/>';
                                                            $count++;
                                                        }

                                                        if ($count == 0) {
                                                            echo "-NA-";
                                                        }
                                                    } elseif ($adventure['frequency'] == 'Monthly') {
                                                        $dates = explode(',', $adventure['repeatInterval']);

                                                        foreach ($dates as $date) {
                                                            echo $date . "<br/>";
                                                        }
                                                    }
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($adventure['frequency'] == 0 && $adventure['date'] != "0000-00-00") {
                                                    echo $adventure['startingTime'];
                                                } else {
                                                    echo $adventure['timespan'];
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo $adventure['availableEntry'] . ' / ' . $adventure['capacity'];
                                                ?>
                                            </td>
                                            <td class="center">
                                                <button
                                                    class="<?php echo $status ? 'btn btn-success' : 'btn btn-danger'; ?>"
                                                    type="button"
                                                    onClick="javascipt:window.location.href='<?php echo $statusLink; ?>';"
                                                    title="Click here to Toggle (Active / Inactive)">
                                                    <?php echo $status ? 'Active' : 'Inactive'; ?>
                                                </button>

                                                <button class="btn btn-default" type="button"
                                                        onClick="javascipt:window.location.href='<?php echo $editLink; ?>';"
                                                        title="EDIT">
                                                    <i class="fa fa-edit fa-fw"></i>Edit
                                                </button>

                                                <button class="btn btn-danger" type="button"
                                                        onClick="return deleteRecode('<?php echo $deleteLink; ?>','<?php echo $page_caption; ?>');"
                                                        title="DELETE">
                                                    <i class="fa fa-edit fa-fw"></i>Delete
                                                </button>
                                            </td>
                                        </tr>
                                    <?php }
                                } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.panel-body -->

                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->
</div>