<?php
$page_caption = 'Restaurant';
$cancel_url = base_url() . 'restaurant/index';
$submitForm_url = 'restaurant/add/';
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title">Add <?php echo $page_caption; ?></h1>
                </section>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <?php if (isset($error)) echo '<div class="alert alert-danger">' . $error . '</div>'; ?>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php echo $page_caption; ?> Detail
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <?php echo form_open_multipart($submitForm_url) ?>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label>Restaurant Name *</label>
                                        <input class="form-control validate[required] restaurantName"
                                               name="restaurantName"
                                               type="text"
                                               value="<?php echo set_value("restaurantName") ? set_value("restaurantName") : ""; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label>Location *</label>
                                        <input class="form-control validate[required] location" name="location"
                                               type="text"
                                               value="<?php echo set_value("location") ? set_value("location") : ""; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label>Speciality *</label>
                                        <input class="form-control validate[required] speciality" name="speciality"
                                               type="text"
                                               value="<?php echo set_value("speciality") ? set_value("speciality") : ""; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label>Who will like it? *</label>
                                        <input class="form-control validate[required] liker" name="liker"
                                               type="text"
                                               value="<?php echo set_value("liker") ? set_value("liker") : ""; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label class="" id="uploadImageData">Upload Images</label>
                                        <div id="imagesData"></div>
                                        <input type="file" name="file_upload" id="file_upload" required/>
                                    </div>
                                    <div class="clearfix"></div>

                                </div>

                                <input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
                                <input type="button" name="Cancel" value="Cancel" class="btn btn-success"
                                       onclick="javascript: window.location.href='<?php echo $cancel_url; ?>';"/>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
</div>
