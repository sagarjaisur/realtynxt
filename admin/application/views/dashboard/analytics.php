<link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/dataTables/dataTables.bootstrap.css">
<link href="<?php echo ASSET_URL; ?>assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet"/>
<script src="<?php echo ASSET_URL; ?>assets/plugins/dataTables/jquery.dataTables.js"></script>
<script src="<?php echo ASSET_URL; ?>assets/plugins/dataTables/dataTables.bootstrap.js"></script>
<script>
    $(document).ready(function () {
        $('#dataTables-example').dataTable();
        $('#dataTables-example1').dataTable();
    });
</script>
<div id="page-wrapper">

    <div class="row">
        <!-- Page Header -->
        <div class="col-lg-12">
            <h1 class="page-header">Analytics</h1>
        </div>
        <!--End Page Header -->
    </div>

    <div class="row">
        <form name="search" method="post">
            <?php form_open('dashboard/analytics'); ?>
            <!--quick info section -->
            <div class="col-lg-3">
                <div class="alert alert-success text-center">
                    <select name="starname" id="starname" class="form-control">
                        <option value="">Stars</option>
                        <?php foreach ($stardetails as $stardetail) { ?>
                            <option
                                value="<?php echo $stardetail['starid']; ?>" <?php if (isset($select_star) && $stardetail['starid'] == $select_star) { ?> selected="selected" <?php } ?>><?php echo $stardetail['starname']; ?></option>
                        <?php } ?>


                    </select>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="alert alert-warning text-center">
                    <select name="countyname" id="countryname" class="form-control">
                        <option value="">Country</option>
                        <?php foreach ($countries as $country) { ?>
                            <option
                                value="<?php echo trim($country['id']); ?>" <?php if (isset($select_county) && trim($country['id']) == $select_county) { ?> selected="selected" <?php } ?>><?php echo trim($country['location']); ?> </option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="alert alert-danger text-center">
                    <select name="statename" id="statename" class="form-control">
                        <option value="">State</option>
                        <?php foreach ($states as $state) { ?>
                            <option
                                value="<?php echo trim($state['id']); ?>" <?php if (isset($select_state) && trim($state['id']) == $select_state) { ?> selected="selected" <?php } ?>><?php echo trim($state['location']); ?> </option>
                        <?php } ?>

                    </select>

                </div>
            </div>
            <div class="col-lg-3">
                <div class="alert alert-success text-center">
                    <button class="btn btn-primary" type="submit" id="search">Search</button>
                </div>
            </div>
        </form>
        <!--<div class="col-lg-3">
            <div class="alert alert-success text-center">
                <select name="cityname" id="cityname" class="form-control">
                    <option value="">City</option>

                </select>
            </div>
        </div> -->

        <!--end quick info section -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <!--Simple table example -->
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i>&nbsp;Display value based on selected values from Drop down.
                    <br/>( NOTE: If you found state column empty based on search it means user comes from FACEBOOK
                    because FACEBOOK not given state detail and Star followers only contains user followers NOT star to
                    star follower count.)
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <?php
                                if (!isset($select_state)) {
                                    if (!isset($select_county)) {
                                        $state = '';
                                    } else {
                                        $state = $select_county;
                                    }

                                } else {
                                    $state = $select_state;
                                }
                                ?>
                                <table class="table table-bordered table-hover table-striped" id="dataTables-example1">
                                    <thead>
                                    <tr>
                                        <th>Star Name</th>
                                        <th>Contact No#</th>
                                        <?php if (isset($select_county)) { ?>
                                            <th>Country</th>
                                        <?php } ?>
                                        <?php if (isset($state) && !empty($state)) { ?>
                                            <th>State</th>
                                        <?php } ?>
                                        <?php if (isset($select_state)) { ?>
                                            <th>City</th>
                                        <?php } ?>
                                        <th>Star Followers</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php


                                    if (!empty($stars)) {
                                        foreach ($stars as $star) { ?>
                                            <tr>
                                                <td><?php echo $star['starname']; ?></td>
                                                <td><?php echo $star['starphone']; ?></td>
                                                <?php if (isset($star['country'])) { ?>
                                                    <td><?php if (isset($star['country'])) { ?> <a
                                                            href="<?php echo base_url(); ?>dashboard/followersDetail/<?php echo $star['starid']; ?>/"
                                                            target="blank"><?php echo $star['country']; ?></a><?php } ?>
                                                    </td>
                                                <?php } ?>
                                                <?php if (isset($star['state']) && !empty($star['state'])) { ?>
                                                    <td><?php if (isset($star['state'])){ ?><a
                                                            href="<?php echo base_url(); ?>dashboard/followersDetail/<?php echo $select_star; ?>/<?php echo $star['country']; ?>/<?php echo $star['state']; ?>"
                                                            target="blank">  <?php echo $star['state'];
                                                            } ?></a></td>
                                                <?php } ?>
                                                <?php if (isset($star['city']) && !empty($star['city'])) { ?>
                                                    <td><?php if (isset($star['city'])) { ?><a
                                                            href="<?php echo base_url(); ?>dashboard/followersDetail/<?php echo $select_star; ?>/<?php echo $star['country']; ?>/<?php echo $star['state']; ?>/<?php echo $star['city']; ?>"
                                                            target="blank"><?php echo $star['city']; ?></a> <?php } ?>
                                                    </td>
                                                <?php } ?>
                                                <td><?php echo $star['follower']; ?></td>
                                            </tr>
                                        <?php }
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="3">Record Not Available.</td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!--End simple table example -->
        </div>
    </div>


</div>
<script>
    $(document).ready(function () {
        $('#countryname').change(function () {
            $('#statename').empty();
            $("#statename").prepend("<option value='' selected='selected'>State</option>");
            var request = $.ajax({
                url: "<?php echo base_url();?>dashboard/getstate",
                type: "POST",
                data: {id: this.value},
                dataType: "html"
            });

            request.done(function (data) {
                $($.parseJSON(data)).each(function () {
                    var option = $('<option />');
                    option.attr('value', this.id).text(this.location);
                    $('#statename').append(option);
                });
            });
        });

        $('#search').click(function () {
            var starname = $('#starname').val();
            if (starname == '') {
                alert('Please Select Star To Get Analytics.');
                return false;
            }
        });

    });
</script>