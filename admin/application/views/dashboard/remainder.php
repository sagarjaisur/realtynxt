<link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/dataTables/dataTables.bootstrap.css">
<link href="<?php echo ASSET_URL; ?>assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet"/>
<script src="<?php echo ASSET_URL; ?>assets/plugins/dataTables/jquery.dataTables.js"></script>
<script src="<?php echo ASSET_URL; ?>assets/plugins/dataTables/dataTables.bootstrap.js"></script>
<script>
    $(document).ready(function () {
        $('#dataTables-example').dataTable();
        $('#dataTables-example1').dataTable();
    });
</script>
<div id="page-wrapper">
    <div class="row">
        <!-- Page Header -->
        <div class="col-lg-12">
            <h1 class="page-header">Remainder To Star.</h1>
        </div>
        <!--End Page Header -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <!--Simple table example -->
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i>&nbsp; Star list who is not posted any post from last 3 days.
                    ( NOTE : We are calculating current day in difference.)
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped"
                                           id="dataTables-example">
                                        <thead>
                                        <tr>
                                            <th>Star Name</th>
                                            <th>Email Address</th>
                                            <th>Contact No#</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if (!empty($stars_remainder)) {
                                            foreach ($stars_remainder as $remainder) { ?>
                                                <tr>
                                                    <td><?php echo $remainder['vStar_name']; ?></td>
                                                    <td><?php echo $remainder['vStar_email']; ?></td>
                                                    <td><?php echo $remainder['vStar_phone']; ?></td>
                                                </tr>
                                            <?php }
                                        } else {
                                            ?>
                                            <tr>
                                                <td colspan="3">Record Not Available.</td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!--End simple table example -->
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#countryname').change(function () {
            $('#statename').empty();
            $("#statename").prepend("<option value='' selected='selected'>State</option>");
            var request = $.ajax({
                url: "<?php echo base_url();?>dashboard/getstate",
                type: "POST",
                data: {id: this.value},
                dataType: "html"
            });

            request.done(function (data) {
                $($.parseJSON(data)).each(function () {
                    var option = $('<option />');
                    option.attr('value', this.id).text(this.location);
                    $('#statename').append(option);
                });
            });
        });

        $('#search').click(function () {
            var starname = $('#starname').val();
            if (starname == '') {
                alert('Please Select Star To Get Analytics.');
                return false;
            }
        });

    });
</script>