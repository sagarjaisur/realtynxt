<link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/dataTables/dataTables.bootstrap.css">
<link href="<?php echo ASSET_URL; ?>assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet"/>
<script src="<?php echo ASSET_URL; ?>assets/plugins/dataTables/jquery.dataTables.js"></script>
<script src="<?php echo ASSET_URL; ?>assets/plugins/dataTables/dataTables.bootstrap.js"></script>
<script>
    $(document).ready(function () {
        $('#dataTables-example').dataTable();
        $('#dataTables-example1').dataTable();
    });
</script>
<div id="page-wrapper">

    <div class="row">
        <!-- Page Header -->
        <div class="col-lg-12">
            <h1 class="page-header">Followers</h1>
        </div>
        <!--End Page Header -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <!--Simple table example -->
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i>&nbsp; Followers Detail.
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">

                                <table class="table table-bordered table-hover table-striped" id="dataTables-example1">
                                    <thead>
                                    <tr>
                                        <th>Fan Name</th>
                                        <th>Contact No#</th>
                                        <th>Email</th>
                                        <th>Country</th>
                                        <th>State</th>
                                        <th>City</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if (!empty($followers)) { ?>
                                        <?php foreach ($followers as $follower) { ?>
                                            <tr>
                                                <td><?php echo $follower['vFirst_name'] . ' ' . $follower['vLast_name']; ?></td>
                                                <td><?php echo $follower['vPhone_no']; ?></td>
                                                <td><?php echo $follower['vEmailId']; ?></td>
                                                <td><?php echo $follower['country']; ?></td>
                                                <td><?php echo $follower['state']; ?></td>
                                                <td><?php echo $follower['city']; ?></td>
                                            </tr>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <tr>
                                            <td colspan="6">Record Not Available.</td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!--End simple table example -->
        </div>
    </div>
</div>
