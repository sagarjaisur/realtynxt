<?php
$page_caption = 'Disc';
$cancel_url = base_url() . 'disc/index';
$add_url = base_url() . 'disc/add/';
?>
<link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/dataTables/dataTables.bootstrap.css">
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">

        <div class="row">
            <!--  page header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title"><?php echo $page_caption; ?> Management<input type="button"
                                                                                                     onClick="location.href='<?php echo $add_url; ?>'"
                                                                                                     name="submit"
                                                                                                     value="Add <?php echo $page_caption; ?>"
                                                                                                     class="btn btn-success btn-md submit"
                                                                                                     style="margin-left: 50px">
                    </h1>

                    <ol class="breadcrumb">
                        <li><a href="<?php echo MAIN_DIR_URL . 'admin/index.php/dashboard/'; ?>"><i
                                    class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active"><a href="#"><?php echo $page_caption; ?> Management</a></li>
                    </ol>
                </section>
            </div>
            <!-- end  page header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>

        <div class="row">
            <div class="col-lg-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>Day</th>
                                    <th>Entry Type</th>
                                    <th>Price</th>
                                    <th>Total Entries</th>
                                    <th>Available Entries</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (isset($discs) && $discs != "") { ?>
                                    <?php
                                    foreach ($discs as $key => $disc) {
                                        $editLink = base_url() . 'disc/edit/' . $disc['discId'];
                                        $deleteLink = base_url() . 'disc/delete/' . $disc['discId'];

                                        $status = $disc['status'];
                                        $statusLink = base_url() . 'disc/status/' . $disc['discId'] . '/' . $status;

                                        if ($disc['discId'] != @$discs[$key - 1]['discId']) {
                                            ?>
                                            <tr class="odd gradeX">
                                                <th colspan="7">
                                                    <?php
                                                    echo "Disc Event ID : " . $disc['discId'] . " | Disc Name : " . $disc['discName'];
                                                    ?>
                                                </th>
                                                <td class="center">
                                                    <button
                                                        class="<?php echo $status ? 'btn btn-success' : 'btn btn-danger'; ?>"
                                                        type="button"
                                                        onClick="javascipt:window.location.href='<?php echo $statusLink; ?>';"
                                                        title="Click here to Toggle (Active / Inactive)">
                                                        <?php echo $status ? 'Active' : 'Inactive'; ?>
                                                    </button>

                                                    <button class="btn btn-default" type="button"
                                                            onClick="javascipt:window.location.href='<?php echo $editLink; ?>';"
                                                            title="Edit">
                                                        <i class="fa fa-edit fa-fw"></i>Edit
                                                    </button>

                                                    <button class="btn btn-danger" type="button"
                                                            onClick="return deleteRecode('<?php echo $deleteLink; ?>','<?php echo $page_caption; ?>');"
                                                            title="DELETE">
                                                        <i class="fa fa-edit fa-fw"></i>Delete
                                                    </button>
                                                </td>
                                            </tr>
                                            <?php
                                        } ?>

                                        <tr class="odd gradeX">
                                            <td>
                                                <?php
                                                $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

                                                $count = 0;
                                                foreach ($days as $key => $day) if (in_array($key + 1, explode(',', $disc['discDays']))) {
                                                    echo $day . '<br/>';
                                                    $count++;
                                                }

                                                if ($count == 0) {
                                                    echo "-NA-";
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php echo $disc['entryType'];
                                                /*
                                                                                                $entryTypes = array(
                                                                                                    '1' => 'Couple',
                                                                                                    '2' => 'Girls',
                                                                                                    '3' => 'Stag'
                                                                                                );

                                                                                                foreach ($entryTypes as $key => $entryType) if ($key == $disc['entryType']) {
                                                                                                    echo $entryType;
                                                                                                }
                                                */
                                                ?>
                                            </td>
                                            <td>
                                                <?php echo $disc['price'] ? $disc['price'] : 'Free!'; ?>
                                            </td>
                                            <td>
                                                <?php echo $disc['noOfEntry']; ?>
                                            </td>
                                            <td>
                                                <?php echo $disc['availableEntry']; ?>
                                            </td>
                                            <td>
                                                <?php echo $disc['fromDate']; ?>
                                            </td>
                                            <td>
                                                <?php echo $disc['toDate']; ?>
                                            </td>
                                        </tr>
                                    <?php }
                                } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.panel-body -->

                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->
</div>