<?php
$page_caption = 'Disc';
$cancel_url = base_url() . 'disc/index';
$submitForm_url = 'disc/edit/' . $disc['discId'];
$ajax_url = base_url() . 'disc/';
$area_url = base_url() . 'disc/';
//$area_url = 'http://localhost/brokit/admin/index.php/disc/';
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title">Edit <?php echo $page_caption; ?></h1>
                </section>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <div class="row">
            <div class="col-lg-12">
                <?php echo form_open_multipart($submitForm_url) ?>
                <input type="hidden" id="ajax_url" name="ajax_url" value="<?php echo $ajax_url; ?>">
                <input type="hidden" id="area_url" name="state_url" value="<?php echo $area_url; ?>">

                <div class="form-group">
                    <!-- Form Elements -->
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <div class="row">

                                <div class="col-md-4">
                                    <label>Disc Name :</label>
                                    <input class="form-control validate[required] discName" name="discName" type="text"
                                           value="<?php echo set_value("discName") ? set_value("discName") : $disc['discName'] ?>"
                                           required>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-6">
                                    <label>Select Entry Type</label>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-4">
                                    <input class="validate[required] entryType"
                                           name="entryType[0]" type="checkbox"
                                           value="Couple"
                                        <?php if (array_key_exists('Couple', $discDetails)) {
                                            echo 'checked="checked"';
                                        } ?>
                                    >Couple
                                </div>
                                <div class="col-md-4">
                                    <input class="validate[required] entryType"
                                           name="entryType[1]" type="checkbox"
                                           value="Girls"
                                        <?php if (array_key_exists('Girls', $discDetails)) {
                                            echo 'checked="checked"';
                                        } ?>
                                    >Girls
                                </div>
                                <div class="col-md-4">
                                    <input class="validate[required] entryType"
                                           name="entryType[2]" type="checkbox"
                                           value="Stag"
                                        <?php if (array_key_exists('Stag', $discDetails)) {
                                            echo 'checked="checked"';
                                        } ?>
                                    >Stag
                                </div>
                                <div class="clearfix"></div>

                                <?php
                                $entryTypes = array('Couple', 'Girls', 'Stag');

                                foreach ($entryTypes AS $key => $entryType) { ?>
                                    <div class="col-md-4">
                                        <label>Price: </label>
                                        <input class="form-control price validate[required]"
                                               name="price[<?php echo $key; ?>]" type="text"
                                               value="<?php if (set_value("price[$key]")) {
                                                   echo set_value('price');
                                               } else {
                                                   echo @$discDetails[$entryType]['price'] ? $discDetails[$entryType]['price'] : 0;
                                               } ?>">
                                    </div>
                                <?php } ?>
                                <div class="clearfix"></div>

                                <?php
                                foreach ($entryTypes AS $key => $entryType) { ?>
                                    <div class="col-md-4">
                                        <label>Number Of Days *</label><br>

                                        <?php
                                        if (isset($discDetails[$entryType]['discDays']) && $discDetails[$entryType]['discDays'] != '') {
                                            $discDays = @explode(',', $discDetails[$entryType]['discDays']);
                                        } else {
                                            $discDays = '';
                                        }
                                        ?>

                                        <input class="validate[required] discDaysET<?php echo $key; ?>"
                                               name="discDaysET<?php echo $key; ?>[]" type="checkbox"
                                               value="1" <?php if ($discDays != '' && in_array(1, $discDays)) {
                                            echo 'checked="checked"';
                                        } ?>>Sunday
                                        <input class="validate[required] discDaysET<?php echo $key; ?>"
                                               name="discDaysET<?php echo $key; ?>[]" type="checkbox"
                                               value="2" <?php if ($discDays != '' && in_array(2, $discDays)) {
                                            echo 'checked="checked"';
                                        } ?>>Monday
                                        <input class="validate[required] discDaysET<?php echo $key; ?>"
                                               name="discDaysET<?php echo $key; ?>[]" type="checkbox"
                                               value="3" <?php if ($discDays != '' && in_array(3, $discDays)) {
                                            echo 'checked="checked"';
                                        } ?>>Tuesday
                                        <input class="validate[required] discDaysET<?php echo $key; ?>"
                                               name="discDaysET<?php echo $key; ?>[]" type="checkbox"
                                               value="4" <?php if ($discDays != '' && in_array(4, $discDays)) {
                                            echo 'checked="checked"';
                                        } ?>>Wednesday<br/>
                                        <input class="validate[required] discDaysET<?php echo $key; ?>"
                                               name="discDaysET<?php echo $key; ?>[]" type="checkbox"
                                               value="5" <?php if ($discDays != '' && in_array(5, $discDays)) {
                                            echo 'checked="checked"';
                                        } ?>>Thursday
                                        <input class="validate[required] discDaysET<?php echo $key; ?>"
                                               name="discDaysET<?php echo $key; ?>[]" type="checkbox"
                                               value="6" <?php if ($discDays != '' && in_array(6, $discDays)) {
                                            echo 'checked="checked"';
                                        } ?>>Friday
                                        <input class="validate[required] discDaysET<?php echo $key; ?>"
                                               name="discDaysET<?php echo $key; ?>[]" type="checkbox"
                                               value="7" <?php if ($discDays != '' && in_array(7, $discDays)) {
                                            echo 'checked="checked"';
                                        } ?>>Saturday
                                    </div>
                                <?php } ?>
                                <div class="clearfix"></div>

                                <?php
                                foreach ($entryTypes AS $key => $entryType) { ?>
                                    <div class="col-md-4">
                                        <label>No of Entry: </label>
                                        <input class="form-control noOfEntry validate[required]"
                                               name="noOfEntry[<?php echo $key; ?>]" type="text"
                                               value="<?php if (set_value("noOfEntry[$key]")) {
                                                   echo set_value('noOfEntry');
                                               } else {
                                                   echo @$discDetails[$entryType]['noOfEntry'] ? $discDetails[$entryType]['noOfEntry'] : "";
                                               } ?>">
                                    </div>
                                <?php } ?>
                                <div class="clearfix"></div>


                                <div class="col-md-6">
                                    <label>Repeat Registration From </label>
                                    <input class="form-control fromDate validate[required]"
                                           name="fromDate" type="date" value="<?php if (set_value('fromDate')) {
                                        echo set_value('fromDate');
                                    } else {
                                        echo $disc['fromDate'];
                                    } ?>" required>
                                </div>
                                <div class="col-md-6">
                                    <label>To Date</label>
                                    <input class="form-control toDate validate[required]"
                                           name="toDate" type="date" value="<?php if (set_value('toDate')) {
                                        echo set_value('toDate');
                                    } else {
                                        echo $disc['toDate'];
                                    } ?>" required>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-6">
                                    <label>Age Limit</label>
                                    <input class="form-control ageLimit validate[required]"
                                           name="ageLimit" type="number" value="<?php if (set_value('ageLimit')) {
                                        echo set_value('ageLimit');
                                    } else {
                                        echo $disc['ageLimit'];
                                    } ?>">
                                </div>
                            </div>

                            <div class="col-md-12 mb50">
                                <hr>
                                <input type="submit" name="submit" value="Submit" class="btn btn-success"/>
                                <input type="button" name="Cancel" value="Cancel" class="btn btn-primary"
                                       onclick="javascript: window.location.href='<?php echo $cancel_url; ?>';"/>
                                <!-- <div class="col-md-6 col-xs-4">
                                    <div class="col-md-2 putright">
                                        <input type="submit" name="submit" value="Submit"
                                               class="btn btn-success btn-md submit">
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-4">
                                    <div class="col-md-2 putleft">
                                        <input type="button" name="Cancel" value="Cancel" class="btn btn-default btn-md"
                                               onclick="javascript: window.location.href='<?php echo $cancel_url; ?>';">
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->
</div>