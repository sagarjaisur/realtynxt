<?php
$page_caption = 'Disc';
$cancel_url = base_url() . 'disc/index';
$submitForm_url = 'disc/add/';
$ajax_url = base_url() . 'disc/';
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title">Add <?php echo $page_caption; ?></h1>
                </section>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <div class="row">
            <div class="col-lg-12">
                <?php echo form_open_multipart($submitForm_url) ?>
                <input type="hidden" id="ajax_url" name="ajax_url" value="<?php echo $ajax_url; ?>">

                <div class="form-group">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo $page_caption; ?> Details
                        </div>
                        <div class="panel-body">
                            <div class="row">

                                <div class="col-md-4">
                                    <label>Disc Name :</label>
                                    <input class="form-control validate[required] discName" name="discName" type="text"
                                           value="<?php echo set_value("discName") ? set_value("discName") : ""; ?>"
                                           required>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-6">
                                    <label>Select Entry Type</label>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-4">
                                    <input class="validate[required] entryType"
                                           name="entryType[0]" type="checkbox"
                                           value="Couple">Couple
                                </div>
                                <div class="col-md-4">
                                    <input class="validate[required] entryType"
                                           name="entryType[1]" type="checkbox"
                                           value="Girls">Girls
                                </div>
                                <div class="col-md-4">
                                    <input class="validate[required] entryType"
                                           name="entryType[2]" type="checkbox"
                                           value="Stag">Stag
                                </div>
                                <div class="clearfix"></div>

                                <?php
                                for ($i = 0; $i < 3; $i++) { ?>
                                    <div class="col-md-4">
                                        <label>Price: </label>
                                        <input class="form-control price validate[required]"
                                               name="price[<?php echo $i; ?>]" type="text"
                                               value="<?php if (set_value("price[$i]")) {
                                                   echo set_value('price');
                                               } else {
                                                   echo "";
                                               } ?>">
                                    </div>
                                <?php } ?>
                                <div class="clearfix"></div>

                                <?php
                                for ($i = 0; $i < 3; $i++) { ?>
                                    <div class="col-md-4">
                                        <label>Number Of Days *</label><br>
                                        <input class="validate[required] discDays"
                                               name="discDaysET<?php echo $i; ?>[]" type="checkbox"
                                               value="1">Sunday
                                        <input class="validate[required] discDays"
                                               name="discDaysET<?php echo $i; ?>[]" type="checkbox"
                                               value="2">Monday
                                        <input class="validate[required] discDays"
                                               name="discDaysET<?php echo $i; ?>[]" type="checkbox"
                                               value="3">Tuesday
                                        <input class="validate[required] discDays"
                                               name="discDaysET<?php echo $i; ?>[]" type="checkbox"
                                               value="4">Wednesday<br>
                                        <input class="validate[required] discDays"
                                               name="discDaysET<?php echo $i; ?>[]" type="checkbox"
                                               value="5">Thursday
                                        <input class="validate[required] discDays"
                                               name="discDaysET<?php echo $i; ?>[]" type="checkbox"
                                               value="6">Friday
                                        <input class="validate[required] discDays"
                                               name="discDaysET<?php echo $i; ?>[]" type="checkbox"
                                               value="7">Saturday
                                    </div>
                                <?php } ?>
                                <div class="clearfix"></div>

                                <?php
                                for ($i = 0; $i < 3; $i++) { ?>
                                    <div class="col-md-4">
                                        <label>No of Entry: </label>
                                        <input class="form-control noOfEntry validate[required]"
                                               name="noOfEntry[<?php echo $i; ?>]" type="text"
                                               value="<?php if (set_value("noOfEntry[$i]")) {
                                                   echo set_value('noOfEntry');
                                               } else {
                                                   echo "";
                                               } ?>">
                                    </div>
                                <?php } ?>
                                <div class="clearfix"></div>


                                <div class="col-md-6">
                                    <label>Repeat Registration From </label>
                                    <input class="form-control fromDate validate[required]"
                                           name="fromDate" type="date" value="<?php if (set_value('fromDate')) {
                                        echo set_value('fromDate');
                                    } else {
                                        echo "";
                                    } ?>" required>
                                </div>
                                <div class="col-md-6">
                                    <label>To Date</label>
                                    <input class="form-control toDate validate[required]"
                                           name="toDate" type="date" value="<?php if (set_value('toDate')) {
                                        echo set_value('toDate');
                                    } else {
                                        echo "";
                                    } ?>" required>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-6">
                                    <label>Age Limit</label>
                                    <input class="form-control ageLimit validate[required]"
                                           name="ageLimit" type="number" value="<?php if (set_value('ageLimit')) {
                                        echo set_value('ageLimit');
                                    } else {
                                        echo "";
                                    } ?>">
                                </div>
                            </div>

                            <div class="col-md-12 mb50">
                                <hr>
                                <input type="submit" name="submit" value="Submit" class="btn btn-success"/>
                                <input type="button" name="Cancel" value="Cancel" class="btn btn-primary"
                                       onclick="javascript: window.location.href='<?php echo $cancel_url; ?>';"/>
                            </div>
                        </div>
                    </div>
                    <!-- End Form Elements -->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>