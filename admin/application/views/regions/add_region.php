<?php
$user = $this->session->all_userdata();
$parentRegionName = (isset($user['regionTypes'][$regionTypeId + 1])) ? ucwords($user['regionTypes'][$regionTypeId + 1]) : 'Continent';
$childRegionName = $regionType;
$regionParentId = $regionParent . 'Id';
$regionId = $regionType . 'Id';
$db_RegionName = $regionType . 'Name';

$page_caption = 'Add Region';
$cancel_url = base_url() . 'regions/regionrequest';
$submitForm_url = 'regions/add/' . '/' . $regionTypeId . '/' . $regionType;
$ajax_url = base_url() . 'regions/';
$state_url = base_url() . 'regions/';


?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <h1 class="page-header page_title">&nbsp;<?php echo $page_caption; ?> Details</h1>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <?php if (isset($error)) echo '<div class="alert alert-danger">' . $error . '</div>'; ?>
        <div class="row">
            <div class="col-lg-12">
                <?php echo form_open_multipart($submitForm_url, 'id="add_region"') ?>
                <input type="hidden" id="ajax_url" name="ajax_url" value="<?php echo $ajax_url; ?>">
                <input type="hidden" id="state_url" name="state_url" value="<?php echo $state_url; ?>">

                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php echo $page_caption; ?> Details
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <?php echo ucwords($childRegionName); ?> Details
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label><?php echo $parentRegionName; ?></label>
                                            <?php
                                            $selected = ($this->input->post('regionParentId')) ? $this->input->post('regionParentId') : reset($regionParentList);
                                            echo form_dropdown('regionParentId', $regionParentList, $selected, ' class="form-control"');
                                            ?>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label><?php echo ucwords($childRegionName); ?> Name *</label>
                                            <input class="form-control" name="regionName" type="text"
                                                   value="<?php print set_value('regionName') ? set_value('regionName') : ""; ?>"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <hr>
                            <div class="col-md-6 col-xs-4">
                                <div class="col-md-2 putright">
                                    <input type="submit" name="submit" value="Submit"
                                           class="btn btn-success btn-md submit">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-4">
                                <div class="col-md-2 putleft">
                                    <input type="button" name="Cancel" value="Cancel" class="btn btn-default btn-md"
                                           onclick="javascript: window.location.href='<?php echo base_url() . 'regions/regionrequest/' . $regionTypeId; ?>';">
                                </div>
                            </div>
                            <br/>&nbsp;

                        </div>


                        <!--End Form Elements -->

                        </form>
                    </div>
                </div>
            </div>
            <br/>
        </div>
    </div>
    <!-- end page-wrapper -->
</div>