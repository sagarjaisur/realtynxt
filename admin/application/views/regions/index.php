<?php
$user = $this->session->all_userdata();
$page_caption = 'Region';
$cancel_url = base_url() . 'regions/index';
$add_url = base_url() . 'regions/add/' . $regionTypeId . '/' . $user['regionTypes'][$regionTypeId];
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!--  page header -->
            <div class="col-lg-12">
                <!--                <h1 class="page-header page_title">&nbsp;-->
                <?php //echo $page_caption; ?><!-- Dashboard</h1>-->
                <section class="content-header">
                    <h1 class="page-header page_title">&nbsp;<?php echo $page_caption; ?> Management
                        <?php
                        if ($regionTypeId == 1 || $regionTypeId == 2) {
                            ?>
                            <input type="button"
                                   onClick="location.href='<?php echo $add_url; ?>'"
                                   name="submit"
                                   value="Add <?php echo ucwords(strtolower($user['regionTypes'][$regionTypeId])); ?>"
                                   class="btn btn-success btn-md submit"
                                   style="margin-left: 50px">
                        <?php } ?>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo MAIN_DIR_URL . 'admin/index.php/dashboard/'; ?>"><i
                                    class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#"><?php echo $page_caption; ?> Management</a></li>
                        <li class="active"><?php echo ucwords(strtolower($user['regionTypes'][$regionTypeId])); ?></li>
                    </ol>
                </section>
            </div>
            <!-- end  page header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <div class="row">
            <div class="col-lg-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th><?php echo ucwords($user['regionTypes'][$regionTypeId]); ?></th>
                                    <th><?php echo (isset($user['regionTypes'][$regionTypeId + 1])) ? ucwords($user['regionTypes'][$regionTypeId + 1]) : 'Continent'; ?></th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (!empty($regions)) {
                                    foreach ($regions as $region) {

                                        $status = $region['status'];
                                        $editLink = base_url() . 'regions/edit/' . $regionTypeId . '/' . $user['regionTypes'][$regionTypeId] . '/' . $region[$user['regionTypes'][$regionTypeId] . 'Id'];
                                        $deleteLink = base_url() . 'regions/delete/' . $region[$user['regionTypes'][$regionTypeId] . 'Id'] . '/' . $regionTypeId . '/' . $user['regionTypes'][$regionTypeId];
                                        $statusLink = base_url() . 'regions/changeregionstatus/' . $region[$user['regionTypes'][$regionTypeId] . 'Id'] . '/' . $status . '/' . $regionTypeId . '/' . $user['regionTypes'][$regionTypeId];
//                                        $submitForm_url = 'regions/updateRoleMember/' . $region['memberId'];
                                        ?>
                                        <tr class="odd gradeX">
                                            <td><?php echo $region[$user['regionTypes'][$regionTypeId] . 'Name']; ?></td>
                                            <td><?php echo (isset($user['regionTypes'][$regionTypeId + 1])) ? $region[$user['regionTypes'][$regionTypeId + 1] . 'Name'] : 'Asia'; ?></td>
                                            <td><a href="<?php echo $statusLink; ?>"
                                                   title="Click here to Toggle (Enable/Disable)"><?php print ($status == 1) ? 'Enabled' : 'Disabled'; ?></a>
                                            </td>
                                            <td class="center">
                                                <button class="btn btn-default" type="button"
                                                        onClick="javascipt:window.location.href='<?php echo $editLink; ?>';"
                                                        title="EDIT">
                                                    <i class="fa fa-edit fa-fw"></i>Edit
                                                </button>

                                                <?php
                                                if ($regionTypeId == 1 || $regionTypeId == 2) {
                                                    ?>
                                                    <button class="btn btn-danger" type="button"
                                                            onClick="return deleteRecode('<?php echo $deleteLink; ?>','<?php echo ucwords($user['regionTypes'][$regionTypeId]); ?>');"
                                                            title="DELETE">
                                                        <i class="fa fa-edit fa-fw"></i>Delete
                                                    </button>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                    <?php }
                                } else { ?>
                                    <tr>
                                        <td colspan="5">Record Not Available.</td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <p><?php echo $links; ?></p>
                        </div>
                    </div>
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .odd.gradeX a {
        float: none !important;
    }
</style>
