<?php
$page_caption = 'Add User';
$cancel_url = base_url() . 'team/userrequest/' . $roleId;
$submitForm_url = 'team/addUser/' . $roleId;
$ajax_url = base_url() . 'team/';
$state_url = base_url() . 'team/';
$city_url = base_url() . 'team/';

?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title"><?php echo $page_caption; ?></h1>
                </section>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <?php if (isset($error)) echo '<div class="alert alert-danger">' . $error . '</div>'; ?>
        <div class="row">
            <div class="col-lg-12">
                <?php echo form_open_multipart($submitForm_url, 'id="add_user"') ?>
                <input type="hidden" id="ajax_url" name="ajax_url" value="<?php echo $ajax_url; ?>">
                <input type="hidden" id="state_url" name="state_url" value="<?php echo $state_url; ?>">
                <input type="hidden" id="city_url" name="city_url" value="<?php echo $city_url; ?>">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php echo $page_caption . ' [<b>Role:</b> ' . ucwords(strtolower($default_roles[$roleId])) . ' ]'; ?>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Personal Details
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>First Name *</label>
                                            <input
                                                class="form-control validate[required,custom[onlyLetterSp]] firstName"
                                                name="firstName" type="text"
                                                value="<?php if (set_value('firstName')) {
                                                    echo set_value('firstName');
                                                } else {
                                                    echo "";
                                                } ?>" required>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Last Name *</label>
                                            <input
                                                class="form-control validate[required,custom[onlyLetterSp]] lastName"
                                                name="lastName" type="text" value="<?php if (set_value('lastName')) {
                                                echo set_value('lastName');
                                            } else {
                                                echo "";
                                            } ?>" required>
                                        </div>
                                        <!--
                                        <div class="col-md-6">
                                            <label class="">Gender</label>
                                            <?php /*$dataGender = set_value('gender'); */ ?>
                                            <select class="form-control" name="gender">
                                                <option value="">Select Gender</option>

                                                <option
                                                    value="1" <?php /*print ($dataGender == 1) ? 'selected="selected"' : ''; */ ?>>
                                                    Male
                                                </option>
                                                <option
                                                    value="2" <?php /*print ($dataGender == 2) ? 'selected="selected"' : ''; */ ?>>
                                                    Female
                                                </option>
                                                <option
                                                    value="3" <?php /*print ($dataGender == 3) ? 'selected="selected"' : ''; */ ?>>
                                                    Unspecified
                                                </option>
                                            </select>
                                        </div>
-->
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Upload Your Image</label>
                                            <input type="hidden" id="imageName" name="imageName"
                                                   value="<?php if (set_value('image')) {
                                                       echo set_value('image');
                                                   } else {
                                                       echo UPLOAD_URL . "users/0.jpg";
                                                   } ?>">
                                            <input type="file" id="imgMember" name="image">

                                            <span id="imagediv"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Postal Information
                                </div>
                                <div class="panel-body">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <label>Address *</label>
                                            <input class="form-control  contactAddress validate[required]"
                                                   name="contactAddress" type="text"
                                                   value="<?php if (set_value('contactAddress')) {
                                                       echo set_value('contactAddress');
                                                   } else {
                                                       echo "";
                                                   } ?>">
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-6">
                                            <label>Country *</label>
                                            <select id="member_country" name="country"
                                                    class="form-control country validate[required]" disabled>
                                                <option value="1" selected="selected">India</option>
                                            </select>

                                            <input type="hidden" name="country_default" value="India"/>
                                            <!--
                                            <?php /*$dataCountry = set_value('country'); */ ?>
                                            <select id="member_country" name="country"
                                                    class="form-control country">
                                                <option value="">Select Country</option>
                                                <?php /*if (!empty($countries)): foreach ($countries as $country): */ ?>
                                                    <option
                                                        value="<?php /*echo $country['country']; */ ?>" <?php /*print ($dataCountry == $country['country']) ? 'selected="selected"' : ''; */ ?>><?php /*echo $country['title']; */ ?></option>
                                                <?php /*endforeach; endif; */ ?>
                                            </select>
        -->
                                        </div>
                                        <div class="col-md-6">
                                            <label>State *</label>
                                            <?php $dataState = set_value('state'); ?>
                                            <select id="user_state" name="state"
                                                    class="form-control state validate[required]" required>
                                                <option value="">Select State</option>
                                                <?php if (!empty($states)): foreach ($states as $state): ?>
                                                    <option
                                                        value="<?php echo $state['stateName']; ?>" <?php print ($dataState == $state['stateName']) ? 'selected="selected"' : ''; ?>><?php echo $state['stateName']; ?></option>
                                                <?php endforeach; endif; ?>
                                            </select>
                                        </div>
                                        <div class="clearfix"></div>

                                        <div class="col-md-6">
                                            <label>City *</label>
                                            <?php
                                            $selected = ($this->input->post('city')) ? $this->input->post('city') : 'Pune';
                                            echo form_dropdown('city', $CitiesList, $selected, ' class="form-control validate[required]" id="user_city"');
                                            ?>
                                        </div>
                                        <!--
                                        <div class="col-md-6">
                                            <label>Postcode *</label>
                                            <input
                                                class="form-control validate[required,custom[number],maxsize[9] pincode"
                                                name="pincode" type="text" value="<?php /*if (set_value('pincode')) {
                                                echo set_value('pincode');
                                            } else {
                                                echo "";
                                            } */ ?>">
                                        </div>
-->
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Phone and Email
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Mobile Number*</label>
                                    <input class="form-control validate[maxSize[15]]" name="mobileNumber" type="text"
                                           value="<?php if (set_value('mobileNumber')) {
                                               echo set_value('mobileNumber');
                                           } else {
                                               echo "";
                                           } ?>" required>
                                </div>
                                <div class="col-md-6">
                                    <label>Landline Number</label>
                                    <input class="form-control validate[maxSize[15]]" name="landlineNumber" type="text"
                                           value="<?php if (set_value('landlineNumber')) {
                                               echo set_value('landlineNumber');
                                           } else {
                                               echo "";
                                           } ?>">
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-6">
                                    <label>Fax</label>
                                    <input class="form-control validate[maxSize[15]]" name="fax" type="text"
                                           value="<?php if (set_value('fax')) {
                                               echo set_value('fax');
                                           } else {
                                               echo "";
                                           } ?>">
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-6">
                                    <label>Email address *</label>
                                    <input class="form-control validate[required,email]" name="email" type="text"
                                           value="<?php if (set_value('email')) {
                                               echo set_value('email');
                                           } else {
                                               echo "";
                                           } ?>" required>
                                </div>
                                <div class="col-md-6">
                                    <label>Alternative Email address</label>
                                    <input class="form-control" name="alternativeEmail" type="text"
                                           value="<?php if (set_value('alternativeEmail')) {
                                               echo set_value('alternativeEmail');
                                           } else {
                                               echo "";
                                           } ?>">
                                </div>
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <br/>
                                        <label>
                                            <input type="checkbox" name="receiveEmailsOnAltEmailStatus"> Receive
                                            emails on alternative email address?
                                        </label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="hideContactDetailsStatus"> Hide contact
                                            details from other users?
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--                    --><?php //if ($roleId == 1 || $roleId == 2) { ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            User Account
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>User Name *</label>
                                    <input class="form-control validate[required]" name="username" type="text"
                                           value="<?php if (set_value('username')) {
                                               echo set_value('username');
                                           } else {
                                               echo "";
                                           } ?>" required>
                                </div>
                                <div class="col-md-6">
                                    <label>Password *</label>
                                    <input class="form-control validate[required,minSize[4]]" name="password"
                                           type="password" id="password" required>
                                </div>
                                <div class="col-md-6">
                                    <label>Confirm Password *</label>
                                    <input class="form-control validate[required,equals[password]]" name="password1"
                                           type="password" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if ($roleId == 4) { ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Rm Assign Management
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                   
                                    <div class="col-md-6">
                                        <label>Assign RM*</label>
                                        <select class="form-control validate[required]" name="rmIds[]" multiple="multiple" >
                                        <?php foreach ($rmDetail as $key => $value) {
                                            echo  '<option value="'.$value['uid'].'">'.$value['name'].'</option>';
                                        } ?>
                                        
                                        </select>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    <!--                    --><?php //} ?>

                    <input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
                    <input type="button" name="Cancel" value="Cancel" class="btn btn-success"
                           onclick="javascript: window.location.href='<?php echo $cancel_url; ?>';"/>
                </div>
                <!--End Form Elements -->
                </form>
            </div>
        </div>
    </div>
    <br/>
</div>
</div>
<!-- end page-wrapper -->
</div>