<?php
$page_caption = 'Currency';
$cancel_url = base_url() . 'currency/index';
$add_url = base_url() . 'currency/add/';
?>
<link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/dataTables/dataTables.bootstrap.css">
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">

        <div class="row">
            <!--  page header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title">&nbsp;<?php echo $page_caption; ?> Management<input type="button"
                                                                                                           onClick="location.href='<?php echo $add_url; ?>'"
                                                                                                           name="submit"
                                                                                                           value="Add <?php echo $page_caption; ?>"
                                                                                                           class="btn btn-success btn-md submit"
                                                                                                           style="margin-left: 50px">
                    </h1>

                    <ol class="breadcrumb">
                        <li><a href="<?php echo MAIN_DIR_URL . 'admin/index.php/dashboard/'; ?>"><i
                                    class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active"><a href="#"><?php echo $page_caption; ?> Management</a></li>
                    </ol>
                </section>
            </div>
            <!-- end  page header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>

        <div class="row">
            <div class="col-lg-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>Currency</th>
                                    <th>Icon</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (isset($currencys) && $currencys != "") {
                                    foreach ($currencys as $currency) {

                                        $editLink = base_url() . 'currency/edit/' . $currency['currencyId'];
                                        $deleteLink = base_url() . 'currency/delete/' . $currency['currencyId'];

                                        $status = $currency['status'];
                                        $statusLink = base_url() . 'currency/status/' . $currency['currencyId'] . '/' . $status;

                                        ?>
                                        <tr class="odd gradeX">
                                            <td>
                                                <?php echo $currency['currency']; ?>
                                            </td>
                                            <td>
                                                <?php echo $currency['icon'] ? $currency['icon'] : ""; ?>

                                                <?php /*if ($currency['icon'] != '') {
                                                    $icon = explode(",", $currency['icon']);
                                                    */?><!--
                                                    <span id="imagediv">
                                                 <?php
/*                                                 foreach ($icon AS $image) { */?>
                                                     <img id="upload_image" class="upload_side_img2"
                                                          src="<?php /*echo $image; */?>"
                                                          height="50px" width="50px" style="margin-bottom: 10px">

                                                 <?php /*} */?>
                                                </span>
                                                <?php /*} else { */?>
                                                    <span id="imagediv"></span>
                                                --><?php /*} */?>
                                            </td>
                                            <td class="center">
                                                <button
                                                    class="<?php echo $status ? 'btn btn-success' : 'btn btn-danger'; ?>"
                                                    type="button"
                                                    onClick="javascipt:window.location.href='<?php echo $statusLink; ?>';"
                                                    title="Click here to Toggle (Active / Inactive)">
                                                    <?php echo $status ? 'Active' : 'Inactive'; ?>
                                                </button>

                                                <button class="btn btn-default" type="button"
                                                        onClick="javascipt:window.location.href='<?php echo $editLink; ?>';"
                                                        title="EDIT">
                                                    <i class="fa fa-edit fa-fw"></i>Edit
                                                </button>

                                                <button class="btn btn-danger" type="button"
                                                        onClick="return deleteRecode('<?php echo $deleteLink; ?>','<?php echo $page_caption; ?>');"
                                                        title="DELETE">
                                                    <i class="fa fa-edit fa-fw"></i>Delete
                                                </button>
                                            </td>
                                        </tr>
                                    <?php }
                                } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.panel-body -->

                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->
</div>