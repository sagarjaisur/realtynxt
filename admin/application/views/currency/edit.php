<?php
$page_caption = 'Currency';
$cancel_url = base_url() . 'currency/index';
$submitForm_url = 'currency/edit/' . $currencyId;
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title">Edit <?php echo $page_caption; ?></h1>
                </section>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <?php if (isset($error)) echo '<div class="alert alert-danger">' . $error . '</div>'; ?>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <?php echo form_open_multipart($submitForm_url) ?>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label>Currency *</label>
                                        <input class="form-control validate[required] currency"
                                               name="currency"
                                               type="text"
                                               value="<?php echo set_value("currency") ? set_value("currency") : $detail['currency']; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label>Currency Icon *</label>
                                        <input class="form-control validate[required] icon"
                                               name="icon"
                                               type="text"
                                               value="<?php echo htmlentities(set_value("icon") ? set_value("icon") : $detail['icon']); ?>"
                                               required>
                                        <br/>
                                        Preview : <?php echo $detail['icon'] ? $detail['icon'] : ""; ?>
                                        <br/><br/>
                                    </div>
                                    <div class="clearfix"></div>
                                    <!--
                                    <div class="col-md-6">
                                        <label class="">Upload Currency Icon</label>
                                        <div id="imagesData"></div>
                                        <input type="file" name="file_upload" id="file_upload" required/><br/>

                                        <?php /*if ($detail['images'] != '') {
                                            $images = explode(",", $detail['images']);
                                            */ ?>
                                            <span id="imagediv">
                                                 <?php
                                    /*                                                 foreach ($images AS $image) { */ ?>
                                                     <input type="hidden" name="images[]"
                                                            value="<?php /*echo $image; */ ?>"/>
                                                     <img id="upload_image" class="upload_side_img2"
                                                          src="<?php /*echo $image; */ ?>"
                                                          height="150px" width="150px" style="margin-bottom: 10px">

                                                 <?php /*} */ ?>
                                                <input type="button" value="Hide Preview" id="remove_upload"
                                                       class="remove_upload btn btn-success btn-md submit_btn">
                                                </span>
                                        <?php /*} else { */ ?>
                                            <span id="imagediv"></span>
                                        <?php /*} */ ?>
                                    </div>
                                    <div class="clearfix"></div>
-->

                                </div>
                                <input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
                                <input type="button" name="Cancel" value="Cancel" class="btn btn-success"
                                       onclick="javascript: window.location.href='<?php echo $cancel_url; ?>';"/>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->
</div>