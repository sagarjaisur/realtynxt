<?php
$page_caption = 'Property to Map-List';
$cancel_url = base_url() . 'propertyMapList/index';
$submitForm_url = 'propertyMapList/add/';
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title">Add <?php echo $page_caption; ?></h1>
                </section>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <?php if (isset($error)) echo '<div class="alert alert-danger">' . $error . '</div>'; ?>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Property Detail
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8">
                                <?php echo form_open_multipart($submitForm_url) ?>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label>Property Name *</label>
                                        <input class="form-control validate[required] propertyName"
                                               name="propertyName"
                                               type="text"
                                               value="<?php echo set_value("propertyName") ? set_value("propertyName") : ""; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label>Price *</label>
                                        <input class="form-control validate[required] price"
                                               name="price"
                                               type="text"
                                               value="<?php echo set_value("price") ? set_value("price") : ""; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label>Property Description *</label>
                                        <textarea class="form-control validate[required] description" name="description"
                                                  name="description"
                                                  required><?php echo set_value("description") ? set_value("description") : ""; ?></textarea>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label>Address *</label>
                                        <textarea class="form-control validate[required] address" name="address"
                                                  name="address"
                                                  required><?php echo set_value("address") ? set_value("address") : ""; ?></textarea>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label>Latitude *</label>
                                        <input class="form-control validate[required] latitude" name="latitude"
                                               type="text"
                                               value="<?php echo set_value("latitude") ? set_value("latitude") : ""; ?>"
                                               required>
                                    </div>
                                    <!--                                    <div class="clearfix"></div>-->

                                    <div class="col-md-6">
                                        <label>Longitude *</label>
                                        <input class="form-control validate[required] longitude" name="longitude"
                                               type="text"
                                               value="<?php echo set_value("longitude") ? set_value("longitude") : ""; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label class="" id="uploadImageData">Upload Image</label>
                                        <div id="imagesData"></div>
                                        <input type="file" name="propertyMapList_file_upload"
                                               id="propertyMapList_file_upload" required/>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
                                <input type="button" name="Cancel" value="Cancel" class="btn btn-success"
                                       onclick="javascript: window.location.href='<?php echo $cancel_url; ?>';"/>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
</div>
