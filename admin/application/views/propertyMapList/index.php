<?php
$page_caption = 'Property Map-List';
$cancel_url = base_url() . 'propertyMapList/index';
$add_url = base_url() . 'propertyMapList/add/';
$map_url = base_url() . 'propertyMapList/map/';
?>
<link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/dataTables/dataTables.bootstrap.css">
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">

        <div class="row">
            <!--  page header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title"><?php echo $page_caption; ?> Management
                        <input type="button"
                               onClick="location.href='<?php echo $add_url; ?>'"
                               name="submit"
                               value="Add Property to Map-List"
                               class="btn btn-success btn-md submit"
                               style="margin-left: 50px">

                        <input type="button"
                               onClick="location.href='<?php echo $map_url; ?>'"
                               name="submit"
                               value="View Map"
                               class="btn btn-success btn-md submit"
                               style="margin-left: 50px">
                    </h1>

                    <ol class="breadcrumb">
                        <li><a href="<?php echo MAIN_DIR_URL . 'admin/index.php/dashboard/'; ?>"><i
                                    class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active"><a href="#"><?php echo $page_caption; ?> Management</a></li>
                    </ol>
                </section>
            </div>
            <!-- end  page header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>

        <div class="row">
            <div class="col-lg-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>Property Name</th>
                                    <th>Price</th>
                                    <th>Address</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (isset($propertyMapList) && $propertyMapList != "") {
                                    foreach ($propertyMapList as $property) {

                                        $editLink = base_url() . 'propertyMapList/edit/' . $property['propertyMapListId'];
                                        $deleteLink = base_url() . 'propertyMapList/delete/' . $property['propertyMapListId'];

                                        $status = $property['status'];
                                        $statusLink = base_url() . 'propertyMapList/status/' . $property['propertyMapListId'] . '/' . $status;

                                        ?>
                                        <tr class="odd gradeX">
                                            <td>
                                                <?php echo $property['propertyName']; ?>
                                            </td>
                                            <td>
                                                <?php echo $property['price']; ?>
                                            </td>
                                            <td>
                                                <?php echo $property['address']; ?>
                                            </td>
                                            <td>
                                                <button
                                                    class="<?php echo $status ? 'btn btn-success' : 'btn btn-danger'; ?>"
                                                    type="button"
                                                    onClick="javascipt:window.location.href='<?php echo $statusLink; ?>';"
                                                    title="Click here to Toggle (Active / Inactive)">
                                                    <?php echo $status ? 'Active' : 'Inactive'; ?>
                                                </button>
                                            </td>
                                            <td class="center">
                                                <button class="btn btn-default" type="button"
                                                        onClick="javascipt:window.location.href='<?php echo $editLink; ?>';"
                                                        title="EDIT">
                                                    <i class="fa fa-edit fa-fw"></i>Edit
                                                </button>

                                                <button class="btn btn-danger" type="button"
                                                        onClick="return deleteRecode('<?php echo $deleteLink; ?>','Property');"
                                                        title="DELETE">
                                                    <i class="fa fa-edit fa-fw"></i>Delete
                                                </button>
                                            </td>
                                        </tr>
                                    <?php }
                                } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.panel-body -->

                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->
</div>