<!DOCTYPE html>
<!-- saved from url=(0037)http://aletheme.com/wordpress/luster/ -->
<html
    class=" js no-flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths no-ipad no-iphone no-ipod no-appleios positionfixed"
    lang="en-US"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">.gm-style .gm-style-mtc label, .gm-style .gm-style-mtc div {
            font-weight: 400
        }</style>
    <link type="text/css" rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/map/css">
    <style type="text/css">.gm-style .gm-style-cc span, .gm-style .gm-style-cc a, .gm-style .gm-style-mtc div {
            font-size: 10px
        }</style>
    <style type="text/css">@media print {
            .gm-style .gmnoprint, .gmnoprint {
                display: none
            }
        }

        @media screen {
            .gm-style .gmnoscreen, .gmnoscreen {
                display: none
            }
        }</style>
    <style type="text/css">.gm-style {
            font-family: Roboto, Arial, sans-serif;
            font-size: 11px;
            font-weight: 400;
            text-decoration: none
        }

        .gm-style img {
            max-width: none
        }</style>
    <meta charset="UTF-8">
    <title>Luster Real Estate Theme</title>

    <link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/map/language-selector.css" type="text/css" media="all">
    <link rel="alternate" type="application/rss+xml" title="Luster Real Estate Theme » Feed"
          href="http://aletheme.com/wordpress/luster/feed/">
    <link rel="alternate" type="application/rss+xml" title="Luster Real Estate Theme » Comments Feed"
          href="http://aletheme.com/wordpress/luster/comments/feed/">
    <link rel="alternate" type="application/rss+xml" title="Luster Real Estate Theme » Home Comments Feed"
          href="http://aletheme.com/wordpress/luster/home/feed/">
    <script src="<?php echo ASSET_URL; ?>assets/map/cb=gapi.loaded_0" async=""></script>
    <script id="twitter-wjs" src="<?php echo ASSET_URL; ?>assets/map/widgets.js"></script>
    <script type="text/javascript" async="" src="<?php echo ASSET_URL; ?>assets/map/plusone.js"
            gapi_processed="true"></script>
    <script type="text/javascript">
        window._wpemojiSettings = {
            "baseUrl": "http:\/\/s.w.org\/images\/core\/emoji\/72x72\/",
            "ext": ".png",
            "source": {"concatemoji": "http:\/\/aletheme.com\/wordpress\/luster\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.3.2"}
        };
        !function (a, b, c) {
            function d(a) {
                var c = b.createElement("canvas"), d = c.getContext && c.getContext("2d");
                return d && d.fillText ? (d.textBaseline = "top", d.font = "600 32px Arial", "flag" === a ? (d.fillText(String.fromCharCode(55356, 56812, 55356, 56807), 0, 0), c.toDataURL().length > 3e3) : (d.fillText(String.fromCharCode(55357, 56835), 0, 0), 0 !== d.getImageData(16, 16, 1, 1).data[0])) : !1
            }

            function e(a) {
                var c = b.createElement("script");
                c.src = a, c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
            }

            var f, g;
            c.supports = {simple: d("simple"), flag: d("flag")}, c.DOMReady = !1, c.readyCallback = function () {
                c.DOMReady = !0
            }, c.supports.simple && c.supports.flag || (g = function () {
                c.readyCallback()
            }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", g, !1), a.addEventListener("load", g, !1)) : (a.attachEvent("onload", g), b.attachEvent("onreadystatechange", function () {
                "complete" === b.readyState && c.readyCallback()
            })), f = c.source || {}, f.concatemoji ? e(f.concatemoji) : f.wpemoji && f.twemoji && (e(f.twemoji), e(f.wpemoji)))
        }(window, document, window._wpemojiSettings);
    </script>
    <script src="<?php echo ASSET_URL; ?>assets/map/wp-emoji-release.min.js" type="text/javascript"></script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel="stylesheet" id="dsidx-css" href="<?php echo ASSET_URL; ?>assets/map/43e53d" type="text/css" media="all">
    <link rel="stylesheet" id="dsidxpress-icons-css" href="<?php echo ASSET_URL; ?>assets/map/dsidx-icons.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="dsidxpress-unconditional-css" href="<?php echo ASSET_URL; ?>assets/map/client.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="dsidxwidgets-unconditional-css" href="<?php echo ASSET_URL; ?>assets/map/client(1).css"
          type="text/css" media="all">
    <link rel="stylesheet" id="ale-shortcodes-css" href="<?php echo ASSET_URL; ?>assets/map/shortcodes.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="wp-postratings-css" href="<?php echo ASSET_URL; ?>assets/map/postratings-css.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="wpml-cms-nav-css-css" href="<?php echo ASSET_URL; ?>assets/map/navigation.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="cms-navigation-style-base-css"
          href="<?php echo ASSET_URL; ?>assets/map/cms-navigation-base.css" type="text/css" media="screen">
    <link rel="stylesheet" id="cms-navigation-style-css" href="<?php echo ASSET_URL; ?>assets/map/cms-navigation.css"
          type="text/css" media="screen">
    <link rel="stylesheet" id="mailchimp-for-wp-checkbox-css" href="<?php echo ASSET_URL; ?>assets/map/checkbox.min.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="aletheme_general_css-css" href="<?php echo ASSET_URL; ?>assets/map/general.min.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="aletheme_fontawesome_css-css" href="<?php echo ASSET_URL; ?>assets/map/fontawesome.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="aletheme_woocommerce_css-css" href="<?php echo ASSET_URL; ?>assets/map/woocommerce.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="alethemeIdx_css-css" href="<?php echo ASSET_URL; ?>assets/map/idx.css" type="text/css"
          media="all">
    <link rel="stylesheet" id="ale_colorpicker-css" href="<?php echo ASSET_URL; ?>assets/map/colorpicker.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="ale_demopreview-css" href="<?php echo ASSET_URL; ?>assets/map/general.css"
          type="text/css" media="all">
    <script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/jquery.js"></script>
    <script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/4fc9f4"></script>
    <script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/core.min.js"></script>
    <script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/widget.min.js"></script>
    <script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/accordion.min.js"></script>
    <script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/tabs.min.js"></script>
    <script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/ale-shortcodes-lib.js"></script>
    <script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/modernizr-2.5.3.min.js"></script>
    <!--[if lt IE 9]>
    <script type='text/javascript' src='http://html5shim.googlecode.com/svn/trunk/html5.js?ver=1.8'></script>
    <![endif]-->

    <style type="text/css">
        .ale_map_canvas img {
            max-width: none;
        }
    </style>
    <script type="text/javascript">
        if (typeof localdsidx == "undefined" || !localdsidx) {
            var localdsidx = {};
        }
        ;
        localdsidx.pluginUrl = "/wordpress/luster/wp-content/plugins/dsidxpress/";
        localdsidx.homeUrl = "http://aletheme.com/wordpress/luster/";
    </script>
    <script type="text/javascript">
        if (typeof localdsidx == "undefined" || !localdsidx) {
            var localdsidx = {};
        }
        ;
        localdsidx.pluginUrl = "/wordpress/luster/wp-content/plugins/dsidxpress/";
        localdsidx.homeUrl = "http://aletheme.com/wordpress/luster/";
    </script>
    <script type="text/javascript" charset="UTF-8" src="<?php echo ASSET_URL; ?>assets/map/common.js"></script>
    <script type="text/javascript" charset="UTF-8" src="<?php echo ASSET_URL; ?>assets/map/map.js"></script>
    <script type="text/javascript" charset="UTF-8" src="<?php echo ASSET_URL; ?>assets/map/util.js"></script>
    <script type="text/javascript" charset="UTF-8" src="<?php echo ASSET_URL; ?>assets/map/marker.js"></script>
    <script type="text/javascript" charset="UTF-8" src="<?php echo ASSET_URL; ?>assets/map/onion.js"></script>
    <script type="text/javascript" charset="UTF-8" src="<?php echo ASSET_URL; ?>assets/map/infowindow.js"></script>
    <script type="text/javascript" charset="UTF-8" src="<?php echo ASSET_URL; ?>assets/map/stats.js"></script>
    <script type="text/javascript" charset="UTF-8" src="<?php echo ASSET_URL; ?>assets/map/controls.js"></script>
    <style type="text/css">@-webkit-keyframes _gm6773 {
                               0% {
                                   -webkit-transform: translate3d(0px, -500px, 0);
                                   -webkit-animation-timing-function: ease-in;
                               }
                               50% {
                                   -webkit-transform: translate3d(0px, 0px, 0);
                                   -webkit-animation-timing-function: ease-out;
                               }
                               75% {
                                   -webkit-transform: translate3d(0px, -20px, 0);
                                   -webkit-animation-timing-function: ease-in;
                               }
                               100% {
                                   -webkit-transform: translate3d(0px, 0px, 0);
                                   -webkit-animation-timing-function: ease-out;
                               }
                           }
    </style>
</head>
<body class="home page page-id-6 page-template page-template-page-home page-template-page-home-php" data-min_price="400"
      data-max_price="700000">
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/js"></script>


<div class="" style="width:100%;">

    <section class="" style="float:left;width:28%;margin-left:10px;height: 538px;overflow-y: scroll;">
        <div class="archive-title">
            <h2>Properties</h2>
            <?php foreach ($propertyMapList as $key => $row) { ?>
                <div class="articles">
                    <article class="clearfix property-item-box">
                        <div class="image">
                            <img width="241" height="219" src="<?php echo $row["images"]; ?>"
                                 class="attachment-properties-mini wp-post-image" alt="39"
                                 onClick="myClick(<?php echo $key; ?>);">
                        </div>
                        <div class="text">
                            <h3>
                                <a href="http://aletheme.com/wordpress/luster/properties/luxury-villa-av-mozart-paris/"><?php echo $row["propertyName"]; ?></a>
                            </h3>
                            <div class="inner">
                                <p><?php echo $row["description"]; ?></p>
                            </div>
                            <hr>
                            <span class="item price">$ <?php echo $row["price"]; ?></span>

                                                <span class="item sale blue">
                                                    Sale            </span>

                            <span class="item typeap">Villa</span>

                            <span class="item area">450</span>
                        </div>
                    </article>
                </div>
            <?php } ?>
        </div>
    </section>
    <div id="map-head col-9" style="float:right;width:70%;">
        <div id="listing-map"></div>
    </div>
</div>

<script type="text/javascript">
    (function () {
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = true;
        po.src = 'https://apis.google.com/js/plusone.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();
</script>
<script>!function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = "//platform.twitter.com/widgets.js";
            fjs.parentNode.insertBefore(js, fjs);
        }
    }(document, "script", "twitter-wjs");</script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('.twitter-block .twitter-slider-box').tweecool({
            username: 'envato',
            limit: 4
        });
        $(window).load(function () {
            $('.twitter-slider-box').flexslider({
                controlNav: false,
                animation: "slide",
                pauseOnHover: true,
                animationLoop: false,
                itemWidth: 319,
                minItems: 1,
                maxItems: 1,
                slideshow: false
            });
        });
    });
</script>
<script type="text/javascript">

    var markers2 = [];
    // The array where to store the markers
    function initializePropertiesMap() {

        var properties = [<?php echo $mapJson; ?>];
        var location_center = new google.maps.LatLng(properties[0].lat, properties[0].lng);
        var mapOptions = {
            zoom: 50,
            scrollwheel: false,
            styles: [{
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [{"color": "#d3d3d3"}]
            }, {
                "featureType": "transit",
                "stylers": [{"color": "#808080"}, {"visibility": "off"}]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [{"visibility": "on"}, {"color": "#b3b3b3"}]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [{"color": "#ffffff"}]
            }, {
                "featureType": "road.local",
                "elementType": "geometry.fill",
                "stylers": [{"visibility": "on"}, {"color": "#ffffff"}, {"weight": 1.8}]
            }, {
                "featureType": "road.local",
                "elementType": "geometry.stroke",
                "stylers": [{"color": "#d7d7d7"}]
            }, {
                "featureType": "poi",
                "elementType": "geometry.fill",
                "stylers": [{"visibility": "on"}, {"color": "#ebebeb"}]
            }, {
                "featureType": "administrative",
                "elementType": "geometry",
                "stylers": [{"color": "#a7a7a7"}]
            }, {
                "featureType": "road.arterial",
                "elementType": "geometry.fill",
                "stylers": [{"color": "#ffffff"}]
            }, {
                "featureType": "road.arterial",
                "elementType": "geometry.fill",
                "stylers": [{"color": "#ffffff"}]
            }, {
                "featureType": "landscape",
                "elementType": "geometry.fill",
                "stylers": [{"visibility": "on"}, {"color": "#efefef"}]
            }, {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#696969"}]
            }, {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [{"visibility": "on"}, {"color": "#737373"}]
            }, {
                "featureType": "poi",
                "elementType": "labels.icon",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "poi",
                "elementType": "labels",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "road.arterial",
                "elementType": "geometry.stroke",
                "stylers": [{"color": "#d6d6d6"}]
            }, {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [{"visibility": "off"}]
            }, {}, {"featureType": "poi", "elementType": "geometry.fill", "stylers": [{"color": "#dadada"}]}]
        }
        var map = new google.maps.Map(document.getElementById("listing-map"), mapOptions);
        var bounds = new google.maps.LatLngBounds();
        var markers = new Array();
        var info_windows = new Array();


        for (var i = 0; i < properties.length; i++) {

            markers[i] = new google.maps.Marker({
                position: new google.maps.LatLng(properties[i].lat, properties[i].lng),
                map: map,
                icon: properties[i].icon,
                title: properties[i].title,
                animation: google.maps.Animation.DROP
            });

            bounds.extend(markers[i].getPosition());

            info_windows[i] = new google.maps.InfoWindow({
                content: '<div class="map-info-window cf"><div class="left-part">' +
                '<a class="thumb-link" href="' + properties[i].url + '"><img class="thumba" src="' + properties[i].thumb + '" alt="' + properties[i].title + '"/></a>' +
                '</div><div class="right-part">' +
                '<h3 class="title"><a class="title-link" href="' + properties[i].url + '">' + properties[i].title + '</a></h3>' +
                '<p><strong>Area:</strong> ' + properties[i].areasize + '<br />' +
                '<strong>Bathrooms:</strong> ' + properties[i].bath + '<br />' +
                '<strong>Bedrooms:</strong> ' + properties[i].bad + '<br />' +
                '<span class="price">' + properties[i].price + ' ' + properties[i].currency + ' ' + properties[i].pricetype + '</span></p>' +
                '<a class="more-link" href="' + properties[i].url + '">Take a look</a>' +
                '</div></div>'
            });
            // Push the marker to the 'markers' array
            markers2.push(markers[i]);
            attachInfoWindowToMarker(map, markers[i], info_windows[i]);

        }
        map.fitBounds(bounds);

        function attachInfoWindowToMarker(map, marker, infoWindow) {
            google.maps.event.addListener(marker, 'click', function () {

                for (var i = 0; i < properties.length; i++) {
                    info_windows[i].close();
                }
                infoWindow.open(map, marker);
            });
        }

    }

    google.maps.event.addDomListener(window, 'load', initializePropertiesMap);
    function myClick(id) {
        google.maps.event.trigger(markers2[id], 'click');
    }
</script>

<script type="text/javascript">
    /* <![CDATA[ */
    var wc_add_to_cart_params = {
        "ajax_url": "\/wordpress\/luster\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/wordpress\/luster\/?wc-ajax=%%endpoint%%",
        "i18n_view_cart": "View Cart",
        "cart_url": "http:\/\/aletheme.com\/wordpress\/luster\/cart\/",
        "is_cart": "",
        "cart_redirect_after_add": "no"
    };
    /* ]]> */
</script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/add-to-cart.min.js"></script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/jquery.blockUI.min.js"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    var woocommerce_params = {
        "ajax_url": "\/wordpress\/luster\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/wordpress\/luster\/?wc-ajax=%%endpoint%%"
    };
    /* ]]> */
</script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/woocommerce.min.js"></script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/jquery.cookie.min.js"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    var wc_cart_fragments_params = {
        "ajax_url": "\/wordpress\/luster\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/wordpress\/luster\/?wc-ajax=%%endpoint%%",
        "fragment_name": "wc_fragments"
    };
    /* ]]> */
</script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/cart-fragments.min.js"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    var ratingsL10n = {
        "plugin_url": "http:\/\/aletheme.com\/wordpress\/luster\/wp-content\/plugins\/wp-postratings",
        "ajax_url": "http:\/\/aletheme.com\/wordpress\/luster\/wp-admin\/admin-ajax.php",
        "text_wait": "Please rate only 1 post at a time.",
        "image": "luster",
        "image_ext": "png",
        "max": "5",
        "show_loading": "0",
        "show_fading": "1",
        "custom": "0"
    };
    var ratings_mouseover_image = new Image();
    ratings_mouseover_image.src = ratingsL10n.plugin_url + "/images/" + ratingsL10n.image + "/rating_over." + ratingsL10n.image_ext;
    ;
    /* ]]> */
</script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/postratings-js.js"></script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/modules.js"></script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/ui.js"></script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/jquery.jscrollpane.js"></script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/jquery.easydropdown.min.js"></script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/isotope.js"></script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/modal-form.js"></script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/tweecool.js"></script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/jquery.form.js"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    var ale = {
        "template_dir": "http:\/\/aletheme.com\/wordpress\/luster\/wp-content\/themes\/luster",
        "ajax_load_url": "http:\/\/aletheme.com\/wordpress\/luster\/wp-admin\/admin-ajax.php",
        "ajax_comments": "1",
        "ajax_posts": "0",
        "ajax_open_single": "0",
        "is_mobile": "0",
        "msg_thankyou": "Thank you for your comment!"
    };
    var ale = {
        "template_dir": "http:\/\/aletheme.com\/wordpress\/luster\/wp-content\/themes\/luster",
        "ajax_load_url": "http:\/\/aletheme.com\/wordpress\/luster\/wp-admin\/admin-ajax.php",
        "ajax_comments": "1",
        "ajax_posts": "0",
        "ajax_open_single": "0",
        "is_mobile": "0",
        "msg_thankyou": "Thank you for your comment!"
    };
    var ale = {
        "template_dir": "http:\/\/aletheme.com\/wordpress\/luster\/wp-content\/themes\/luster",
        "ajax_load_url": "http:\/\/aletheme.com\/wordpress\/luster\/wp-admin\/admin-ajax.php",
        "ajax_comments": "1",
        "ajax_posts": "0",
        "ajax_open_single": "0",
        "is_mobile": "0",
        "msg_thankyou": "Thank you for your comment!"
    };
    /* ]]> */
</script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/scripts.js"></script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/comment-reply.min.js"></script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/colorpicker.js"></script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/jquery.cookie.js"></script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/scripts(1).js"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    var icl_vars = {"current_language": "en", "icl_home": "http:\/\/aletheme.com\/wordpress\/luster\/"};
    /* ]]> */
</script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/sitepress.js"></script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/position.min.js"></script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/menu.min.js"></script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/autocomplete.min.js"></script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/map/autocomplete.js"></script>
<script type="text/javascript">
    (function () {
        function addSubmittedClassToFormContainer(e) {
            var form = e.target.form.parentNode;
            var className = 'mc4wp-form-submitted';
            (form.classList) ? form.classList.add(className) : form.className += ' ' + className;
        }

        var forms = document.querySelectorAll('.mc4wp-form');
        for (var i = 0; i < forms.length; i++) {
            (function (f) {

                /* add class on submit */
                var b = f.querySelector('[type="submit"]');
                if (b.length > 0) {
                    if (b.addEventListener) {
                        b.addEventListener('click', addSubmittedClassToFormContainer);
                    } else {
                        b.attachEvent('click', addSubmittedClassToFormContainer);
                    }
                }

            })(forms[i]);
        }
    })();

</script>

<div id="dsidx_cboxOverlay" style="display: none;"></div>
<div id="dsidx_colorbox" class="" role="dialog" tabindex="-1" style="display: none;">
    <div id="dsidx_cboxWrapper">
        <div>
            <div id="dsidx_cboxTopLeft" style="float: left;"></div>
            <div id="dsidx_cboxTopCenter" style="float: left;"></div>
            <div id="dsidx_cboxTopRight" style="float: left;"></div>
        </div>
        <div style="clear: left;">
            <div id="dsidx_cboxMiddleLeft" style="float: left;"></div>
            <div id="dsidx_cboxContent" style="float: left;">
                <div id="dsidx_cboxTitle" style="float: left;"></div>
                <div id="dsidx_cboxCurrent" style="float: left;"></div>
                <button type="button" id="dsidx_cboxPrevious"></button>
                <button type="button" id="dsidx_cboxNext"></button>
                <button id="dsidx_cboxSlideshow"></button>
                <div id="dsidx_cboxLoadingOverlay" style="float: left;"></div>
                <div id="dsidx_cboxLoadingGraphic" style="float: left;"></div>
            </div>
            <div id="dsidx_cboxMiddleRight" style="float: left;"></div>
        </div>
        <div style="clear: left;">
            <div id="dsidx_cboxBottomLeft" style="float: left;"></div>
            <div id="dsidx_cboxBottomCenter" style="float: left;"></div>
            <div id="dsidx_cboxBottomRight" style="float: left;"></div>
        </div>
    </div>
    <div style="position: absolute; width: 9999px; visibility: hidden; display: none;"></div>
</div>
<iframe id="rufous-sandbox" scrolling="no" frameborder="0" allowtransparency="true" allowfullscreen="true"
        style="position: absolute; visibility: hidden; display: none; width: 0px; height: 0px; padding: 0px; border: none;"></iframe>
</body>
</html>