<?php
//$page_caption = 'Property Map-List';
$cancel_url = base_url() . 'propertyMapList/index';
$submitForm_url = 'propertyMapList/edit/' . $propertyMapListId;
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title">Edit Property Details</h1>
                </section>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <?php if (isset($error)) echo '<div class="alert alert-danger">' . $error . '</div>'; ?>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8">
                                <?php echo form_open_multipart($submitForm_url) ?>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label>Property Name *</label>
                                        <input class="form-control validate[required] propertyName"
                                               name="propertyName"
                                               type="text"
                                               value="<?php echo set_value("propertyName") ? set_value("propertyName") : $detail['propertyName']; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label>Price *</label>
                                        <input class="form-control validate[required] price"
                                               name="price"
                                               type="text"
                                               value="<?php echo set_value("price") ? set_value("price") : $detail['price']; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label>Property Description *</label>
                                        <textarea class="form-control validate[required] description" name="description"
                                                  name="description"
                                                  required><?php echo set_value("description") ? set_value("description") : $detail['description']; ?></textarea>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label>Address *</label>
                                        <textarea class="form-control validate[required] address" name="address"
                                                  name="address"
                                                  required><?php echo set_value("address") ? set_value("address") : $detail['address']; ?></textarea>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label>Latitude *</label>
                                        <input class="form-control validate[required] latitude" name="latitude"
                                               type="text"
                                               value="<?php echo set_value("latitude") ? set_value("latitude") : $detail['latitude']; ?>"
                                               required>
                                    </div>
                                    <!--                                    <div class="clearfix"></div>-->

                                    <div class="col-md-6">
                                        <label>Longitude *</label>
                                        <input class="form-control validate[required] longitude" name="longitude"
                                               type="text"
                                               value="<?php echo set_value("longitude") ? set_value("longitude") : $detail['longitude']; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label class="">Upload Images</label>
                                        <div id="imagesData"></div>
                                        <input type="file" name="propertyMapList_file_upload"
                                               id="propertyMapList_file_upload" required/><br/>

                                        <?php if ($detail['images'] != '') {
                                            $images = explode(",", $detail['images']);
                                            ?>
                                            <span id="imagediv">
                                                 <?php
                                                 foreach ($images AS $image) { ?>
                                                     <input type="hidden" name="images[]"
                                                            value="<?php echo $image; ?>"/>
                                                     <img id="upload_image" class="upload_side_img2"
                                                          src="<?php echo $image; ?>"
                                                          height="150px" width="150px" style="margin-bottom: 10px">

                                                 <?php } ?>
                                                <input type="button" value="Hide Preview" id="remove_upload"
                                                       class="remove_upload btn btn-success btn-md submit_btn">
                                                </span>
                                        <?php } else { ?>
                                            <span id="imagediv"></span>
                                        <?php } ?>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
                                <input type="button" name="Cancel" value="Cancel" class="btn btn-success"
                                       onclick="javascript: window.location.href='<?php echo $cancel_url; ?>';"/>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->
</div>