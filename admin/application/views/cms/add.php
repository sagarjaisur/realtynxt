<?php
$page_caption = 'CMS Page';
$cancel_url = base_url() . 'cms/index';
$submitForm_url = 'cms/add/';
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title">Add <?php echo $page_caption; ?></h1>
                </section>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <?php if (isset($error)) echo '<div class="alert alert-danger">' . $error . '</div>'; ?>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php echo $page_caption; ?> Detail
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-10">
                                <?php echo form_open_multipart($submitForm_url) ?>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <label>CMS Page Name *</label>
                                        <input class="form-control validate[required] page_title"
                                               name="pageTitle"
                                               type="text"
                                               value="<?php echo set_value("pageTitle") ? set_value("pageTitle") : ""; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-12">
                                        <br/>
                                        <label>Short Description *</label>
                                        <!--                                        <textarea class="form-control" name="pageShortDesc" required>-->
                                        <?php //echo set_value('pageShortDesc'); ?><!--</textarea>-->
                                        <?php echo $this->ckeditor->editor('pageShortDesc', set_value('pageShortDesc')); ?>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-12">
                                        <br/>
                                        <label>Full Description *</label>
                                        <!--                                        <textarea class="form-control" name="pageFullDesc" required>-->
                                        <?php //echo set_value('pageFullDesc'); ?><!--</textarea>-->
                                        <?php echo $this->ckeditor->editor('pageFullDesc', set_value('pageFullDesc')); ?>
                                    </div>
                                    <div class="clearfix"></div>

                                </div>

                                <input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
                                <input type="button" name="Cancel" value="Cancel" class="btn btn-success"
                                       onclick="javascript: window.location.href='<?php echo $cancel_url; ?>';"/>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
</div>
