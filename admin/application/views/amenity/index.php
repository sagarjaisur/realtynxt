<?php
$page_caption = 'Amenity';
$cancel_url = base_url() . 'amenity/index';
$add_url = base_url() . 'amenity/add/';
?>
<link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/dataTables/dataTables.bootstrap.css">
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">

        <div class="row">
            <!--  page header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title">&nbsp;<?php echo $page_caption; ?> Management<input type="button"
                                                                                                           onClick="location.href='<?php echo $add_url; ?>'"
                                                                                                           name="submit"
                                                                                                           value="Add <?php echo $page_caption; ?>"
                                                                                                           class="btn btn-success btn-md submit"
                                                                                                           style="margin-left: 50px">
                    </h1>

                    <ol class="breadcrumb">
                        <li><a href="<?php echo MAIN_DIR_URL . 'admin/index.php/dashboard/'; ?>"><i
                                    class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active"><a href="#"><?php echo $page_caption; ?> Management</a></li>
                    </ol>
                </section>
            </div>
            <!-- end  page header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>

        <div class="row">
            <div class="col-lg-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>Amenity</th>
                                    <th>Image</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (isset($amenitys) && $amenitys != "") {
                                    foreach ($amenitys as $amenity) {

                                        $editLink = base_url() . 'amenity/edit/' . $amenity['amenityId'];
                                        $deleteLink = base_url() . 'amenity/delete/' . $amenity['amenityId'];

                                        $status = $amenity['status'];
                                        $statusLink = base_url() . 'amenity/status/' . $amenity['amenityId'] . '/' . $status;

                                        ?>
                                        <tr class="odd gradeX">
                                            <td>
                                                <?php echo $amenity['amenityName']; ?>
                                            </td>
                                            <td>
                                                <img src="<?php echo $amenity['image']; ?>" height="128px" width="128px" />
                                            </td>
                                            <td>
                                                <?php echo $amenity['description']; ?>
                                            </td>
                                            <td class="center">
                                                <button
                                                    class="<?php echo $status ? 'btn btn-success' : 'btn btn-danger'; ?>"
                                                    type="button"
                                                    onClick="javascipt:window.location.href='<?php echo $statusLink; ?>';"
                                                    title="Click here to Toggle (Active / Inactive)">
                                                    <?php echo $status ? 'Active' : 'Inactive'; ?>
                                                </button>

                                                <button class="btn btn-default" type="button"
                                                        onClick="javascipt:window.location.href='<?php echo $editLink; ?>';"
                                                        title="EDIT">
                                                    <i class="fa fa-edit fa-fw"></i>Edit
                                                </button>

                                                <button class="btn btn-danger" type="button"
                                                        onClick="return deleteRecode('<?php echo $deleteLink; ?>','<?php echo $page_caption; ?>');"
                                                        title="DELETE">
                                                    <i class="fa fa-edit fa-fw"></i>Delete
                                                </button>
                                            </td>
                                        </tr>
                                    <?php }
                                } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.panel-body -->

                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->
</div>