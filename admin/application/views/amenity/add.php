<?php
$page_caption = 'Amenity';
$cancel_url = base_url() . 'amenity/index';
$submitForm_url = 'amenity/add/';
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title">Add <?php echo $page_caption; ?></h1>
                </section>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <?php if (isset($error)) echo '<div class="alert alert-danger">' . $error . '</div>'; ?>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php echo $page_caption; ?> Detail
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <?php echo form_open_multipart($submitForm_url) ?>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label>Amenity Name *</label>
                                        <input class="form-control validate[required] amenityName"
                                               name="amenityName"
                                               type="text"
                                               value="<?php echo set_value("amenityName") ? set_value("amenityName") : ""; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label>Description *</label>
                                        <input class="form-control validate[required] description" name="description"
                                               type="text"
                                               value="<?php echo set_value("description") ? set_value("description") : ""; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label class="" id="uploadImageData">Upload Image</label>
                                        <input type="hidden" id="imageName" name="imageName"
                                               value="<?php if (set_value('image')) {
                                                   echo set_value('image');
                                               } else {
                                                   echo UPLOAD_URL . "amenities/0.jpg";
                                               } ?>">
                                        <input type="file" id="imgMember" name="image">

                                        <span id="imagediv"></span>
                                    </div>
                                    <div class="clearfix"></div>

                                </div>

                                <input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
                                <input type="button" name="Cancel" value="Cancel" class="btn btn-success"
                                       onclick="javascript: window.location.href='<?php echo $cancel_url; ?>';"/>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
</div>
