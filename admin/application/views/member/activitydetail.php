<?php
$user[0] = $this->session->all_userdata();
$page_caption = 'Offer Activity View';
$cancel_url = base_url() . 'activity/';
$detail = $detail[0];
$submitForm_url = 'activity/updateactivity/' . $detail['activityId'];
$state_url = base_url() . 'activity/checkLeader/';

$detail['activity_type'] = @explode(',', $detail['activity_type']);
$lat = '';
$long = '';

if ($detail['activity_location_mappoints'] != '') {
    $locationData = explode(',', $detail['activity_location_mappoints']);
    $lat = $locationData[0];
    $long = $locationData[1];
} else {
    $lat = '-33.865143';
    $long = '151.209900';
}
//echo '<pre>';   print_r($leaderdetail ); exit;
?>
<!--  page-wrapper -->
<div id="page-wrapper">
    <div class="row">
        <!-- Page Header -->
        <div class="col-lg-12">
            <h1 class="page-header  page_title"><?php echo $page_caption; ?></h1>
        </div>
        <!--End Page Header -->
    </div>
    <?php echo $this->session->flashdata('success'); ?>
    <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
    <div class="row">
        <div class="col-lg-12">
            <!-- Form Elements -->
            <div class="">
                <div class="">
                    <div class="">
                        <div class="">
                            <?php echo form_open_multipart($submitForm_url, 'id="member_register" '); ?>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group fg">
                                                <label>Activity Name *</label>
                                                <input class="form-control validate[required]" name="activity_name"
                                                       type="text" value="<?php echo $detail['activity_name']; ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group fg">
                                                <label>Activity Length Type *</label>
                                                <select name="activity_lenght_type" class="validate[required]">
                                                    <option value="">-Select-</option>
                                                    <option
                                                        value="part-day" <?php if ($detail['activity_lenght_type'] == 'part-day') {
                                                        echo 'selected="selected"';
                                                    } ?> >Part-Day
                                                    </option>
                                                    <option
                                                        value="single-day" <?php if ($detail['activity_lenght_type'] == 'single-day') {
                                                        echo 'selected="selected"';
                                                    } ?> >Single-Day
                                                    </option>
                                                    <option
                                                        value="multi-day" <?php if ($detail['activity_lenght_type'] == 'multi-day') {
                                                        echo 'selected="selected"';
                                                    } ?>>Multi-Day
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="form-group fg">
                                                <label style="float:left;">Activity Type</label>
                                                <ul class="ul_set">
                                                    <li>
                                                        <div class="checkbox"><label><input type="checkbox"
                                                                                            value="Walk" <?php if (in_array('Walk', $detail['activity_type'])) {
                                                                    echo 'checked="checked"';
                                                                } ?> name="activity_type[]" class="check_side">Walk
                                                    </li>
                                                    <li>
                                                        <div class="checkbox">
                                                            <label><input type="checkbox"
                                                                          value="Back-pack" <?php if (in_array('Back-pack', $detail['activity_type'])) {
                                                                    echo 'checked="checked"';
                                                                } ?> name="activity_type[]" class="check_side">Back-pack
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox">
                                                            <label><input type="checkbox"
                                                                          value="Canoeing" <?php if (in_array('Canoeing', $detail['activity_type'])) {
                                                                    echo 'checked="checked"';
                                                                } ?> name="activity_type[]" class="check_side">Canoeing
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox">
                                                            <label><input type="checkbox"
                                                                          value="Canyoning" <?php if (in_array('Canyoning', $detail['activity_type'])) {
                                                                    echo 'checked="checked"';
                                                                } ?> name="activity_type[]" class="check_side">Canyoning
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox">
                                                            <label><input type="checkbox"
                                                                          value="Car Camp" <?php if (in_array('Car Camp', $detail['activity_type'])) {
                                                                    echo 'checked="checked"';
                                                                } ?> name="activity_type[]" class="check_side">Car Camp
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox">
                                                            <label><input type="checkbox"
                                                                          value="Cycling" <?php if (in_array('Cycling', $detail['activity_type'])) {
                                                                    echo 'checked="checked"';
                                                                } ?> name="activity_type[]" class="check_side">Cycling
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox">
                                                            <label><input type="checkbox"
                                                                          value="Family" <?php if (in_array('Family', $detail['activity_type'])) {
                                                                    echo 'checked="checked"';
                                                                } ?> name="activity_type[]" class="check_side">Family
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox">
                                                            <label><input type="checkbox"
                                                                          value="Liloing" <?php if (in_array('Liloing', $detail['activity_type'])) {
                                                                    echo 'checked="checked"';
                                                                } ?> name="activity_type[]" class="check_side">Liloing
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul class="ul_set">
                                                    <li>
                                                        <div class="checkbox">
                                                            <label><input type="checkbox"
                                                                          value="Meeting" <?php if (in_array('Meeting', $detail['activity_type'])) {
                                                                    echo 'checked="checked"';
                                                                } ?> name="activity_type[]" class="check_side">Meeting
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox">
                                                            <label><input type="checkbox"
                                                                          value="Skiing" <?php if (in_array('Skiing', $detail['activity_type'])) {
                                                                    echo 'checked="checked"';
                                                                } ?> name="activity_type[]" class="check_side">Skiing
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox">
                                                            <label><input type="checkbox"
                                                                          value="Snow-shoeing" <?php if (in_array('Snow-shoeing', $detail['activity_type'])) {
                                                                    echo 'checked="checked"';
                                                                } ?> name="activity_type[]" class="check_side">Snow-shoeing
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox">
                                                            <label><input type="checkbox"
                                                                          value="Social" <?php if (in_array('Social', $detail['activity_type'])) {
                                                                    echo 'checked="checked"';
                                                                } ?> name="activity_type[]" class="check_side">Social
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox">
                                                            <label><input type="checkbox"
                                                                          value="Training" <?php if (in_array('Traning', $detail['activity_type'])) {
                                                                    echo 'checked="checked"';
                                                                } ?> name="activity_type[]" class="check_side">Training
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox">
                                                            <label><input type="checkbox"
                                                                          value="Work Party" <?php if (in_array('Work Party', $detail['activity_type'])) {
                                                                    echo 'checked="checked"';
                                                                } ?> name="activity_type[]" class="check_side">Work
                                                                Party</div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox">
                                                            <label><input type="checkbox"
                                                                          value="Other" <?php if (in_array('Other', $detail['activity_type'])) {
                                                                    echo 'checked="checked"';
                                                                } ?> name="activity_type[]" class="check_side">Other
                                                        </div>
                                                    </li>
                                                </ul>

                                            </div>
                                        </div>
                                        <div class="col-lg-12 pl0">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Travelling Date</label>
                                                    <label><i class="validate_txt">Its most common use is for weekend
                                                            activities, where the participants drive to the activity
                                                            location on Friday evening, and start the activity staurday
                                                            morning.</i></label>
                                                    <input class="form-control" name="activity_travelling_date"
                                                           type="text"
                                                           value="<?php echo $detail['activity_travelling_date']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Start Date Of Activity *</label>
                                                    <input class="form-control validate[required]"
                                                           name="activity_start_date" id="activity_start_date"
                                                           type="text"
                                                           value="<?php echo $detail['activity_start_date']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>End Date Of Activity *</label>
                                                    <input class="form-control validate[required]"
                                                           name="activity_end_date" id="activity_end_date" type="text"
                                                           value="<?php echo $detail['activity_end_date']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group fg_size fg col-md-6 col-xs-6">
                                            <label class="ga">Grading Of Activity</label>

                                            <div class="radio-main rd_main">
                                                <ul class="ul_set">
                                                    <li>
                                                        <div class="col-xs-3 radio_set">Length</div>
                                                        <div class="col-xs-9 radiio_res_grading">
                                                            <div class="checkbox1">
                                                                <label><input type="radio"
                                                                              value="short" <?php if ($detail['activity_grading_length'] == 'short') {
                                                                        echo 'checked="checked"';
                                                                    } ?> name="activity_grading_length[]"
                                                                              class="check_side">Short</div>
                                                            <div class="checkbox1">
                                                                <label><input type="radio"
                                                                              value="medium" <?php if ($detail['activity_grading_length'] == 'medium') {
                                                                        echo 'checked="checked"';
                                                                    } ?> name="activity_grading_length[]"
                                                                              class="check_side">Medium</div>
                                                            <div class="checkbox1">
                                                                <label><input type="radio"
                                                                              value="long" <?php if ($detail['activity_grading_length'] == 'long') {
                                                                        echo 'checked="checked"';
                                                                    } ?> name="activity_grading_length[]"
                                                                              class="check_side">Long</div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="col-xs-3 radio_set">Terrain</div>
                                                        <div class="col-xs-9 radio_set radiio_res_grading">
                                                            <div class="checkbox1">
                                                                <label><input type="radio"
                                                                              value="easy" <?php if ($detail['activity_grading_terrain'] == 'easy') {
                                                                        echo 'checked="checked"';
                                                                    } ?> name="activity_grading_terrain[]"
                                                                              class="check_side">Easy</div>
                                                            <div class="checkbox1">
                                                                <label><input type="radio"
                                                                              value="medium" <?php if ($detail['activity_grading_terrain'] == 'medium') {
                                                                        echo 'checked="checked"';
                                                                    } ?> name="activity_grading_terrain[]"
                                                                              class="check_side">Medium</div>

                                                            <div class="checkbox1">
                                                                <label><input type="radio"
                                                                              value="rough" <?php if ($detail['activity_grading_terrain'] == 'rough') {
                                                                        echo 'checked="checked"';
                                                                    } ?> name="activity_grading_terrain[]"
                                                                              class="check_side">Rough</div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="col-xs-3 radio_set">Exploratory</div>
                                                        <div class="col-xs-9 radio_set radiio_res_grading">
                                                            <div class="checkbox1">

                                                                <label><input type="radio"
                                                                              value="yes" <?php if ($detail['activity_grading_exploratory'] == 'yes') {
                                                                        echo 'checked="checked"';
                                                                    } ?> name="activity_grading_exploratory[]"
                                                                              class="check_side">Yes</div>
                                                            <div class="checkbox1">
                                                                <label><input type="radio"
                                                                              value="no" <?php if ($detail['activity_grading_exploratory'] == 'no') {
                                                                        echo 'checked="checked"';
                                                                    } ?> name="activity_grading_exploratory[]"
                                                                              class="check_side">No</div>
                                                            <div class="checkbox1">
                                                                <label><input type="radio"
                                                                              value="partly" <?php if ($detail['activity_grading_exploratory'] == 'partly') {
                                                                        echo 'checked="checked"';
                                                                    } ?> name="activity_grading_exploratory[]"
                                                                              class="check_side">Partly</div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="col-xs-3 radio_set">Wet</div>
                                                        <div class="col-xs-9 radio_set radiio_res_grading">
                                                            <div class="checkbox1">
                                                                <label><input type="radio" value="yes"
                                                                              name="activity_grading_wet[]" <?php if ($detail['activity_grading_wet'] == 'yes') {
                                                                        echo 'checked="checked"';
                                                                    } ?> class="check_side">Yes</div>
                                                            <div class="checkbox1">
                                                                <label><input type="radio" value="no"
                                                                              name="activity_grading_wet[]" <?php if ($detail['activity_grading_wet'] == 'no') {
                                                                        echo 'checked="checked"';
                                                                    } ?> class="check_side">No</div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="form-group fg col-md-6">
                                            <label class="description_tp">Description *</label>
                                            <textarea cols="50" rows="5" max-lenght="250" name="activityDescription"
                                                      class="validate[required,maxSize[250]]"><?php echo $detail['activityDescription']; ?></textarea>
                                            <i class="validate_txt">Limit 250 words</i>
                                        </div>
                                        <div class="form-group fg col-md-6">
                                            <label class="map_name">Map Name(s) *</label>
                                            <label><i class="validate_txt">(eg 'Corin Dam')</i></label>
                                            <input type="text" name="activity_map_name"
                                                   value="<?php echo $detail['activity_map_name']; ?>"
                                                   class="validate[required,maxSize[25]]"/>
                                            <i class="validate_txt">Limit 50 Words</i>
                                        </div>
                                        <div class="form-group fg col-md-6">
                                            <label>Map Scale(s) *</label>
                                            <input type="text" name="activity_map_scale"
                                                   value="<?php echo $detail['activity_map_scale'] ?>"
                                                   class="validate[required]"/>
                                        </div>
                                    </div>
                                </div>
                                <!--  Start  Leader Section  -->
                                <div class="panel panel-default" style="margin:15px;">
                                    <div class="panel-heading fnt_title">
                                        Leader 1
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="form-group col-lg-6 fg">
                                                <label>Name *</label>
                                                <input class="form-control validate[required] "
                                                       name="activity_leader1_name" type="text"
                                                       value="<?php if (isset($leaderdetail[0]['webname'])) echo $leaderdetail[0]['webname']; ?>">
                                                <input class=" " name="activity_leader1Id" type="hidden"
                                                       value="<?php if (isset($leaderdetail[0]['leaderId'])) echo $leaderdetail[0]['leaderId']; ?>">
                                            </div>
                                            <div class="form-group col-lg-6 fg">
                                                <label>Email *</label>
                                                <input class="form-control validate[required,email]"
                                                       name="activity_leader1_email" type="text"
                                                       value="<?php if (isset($leaderdetail[0]['email'])) echo $leaderdetail[0]['email'] ?>">
                                            </div>
                                            <div class="form-group fg">
                                                <i class="validate_txt ml15">Choose how much of your data should appear
                                                    in the publicly accessible ‘What’s On’ on the Club’s web site. </i>

                                                <div class="other clr">
                                                    <div class="col-xs-12 col-xs-push-0 mb15">
                                                        <div class="radio-main rd_main">
                                                            <ul>
                                                                <li>
                                                                    <div class="col-xs-12 col-md-12">
                                                                        <div class="radio col-md-6 col-xs-6 pl0">
                                                                            <label><input
                                                                                    type="radio" <?php if (isset($leaderdetail[0]['whatson_name']) && $leaderdetail[0]['whatson_name'] == 'yes') {
                                                                                    echo 'checked=""';
                                                                                } ?> value="name"
                                                                                    name="leader1_whatson_name"
                                                                                    class="radi check_side">Name</label>
                                                                        </div>
                                                                        <div class="radio col-md-6 col-xs-12 pl0">
                                                                            <label><input
                                                                                    type="radio" <?php if (isset($leaderdetail[0]['whatson_name']) && $leaderdetail[0]['whatson_name'] == 'yes') {
                                                                                    echo 'checked=""';
                                                                                } ?> value="webname"
                                                                                    name="leader1_whatson_name"
                                                                                    class="check_side">Webname</label>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <div class="clr"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-xs-push-0 mb15">
                                                        <div class="radio-main rd_main">
                                                            <ul class="mb5">
                                                                <li>
                                                                    <div class="col-xs-12 radio_set col-md-6">Home
                                                                        Phone
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6">
                                                                        <div class="col-xs-6 col-md-6">
                                                                            <div class="radio radio-n">
                                                                                <input
                                                                                    type="radio" <?php if (isset($leaderdetail[0]['whatson_phone']) && $leaderdetail[0]['whatson_phone'] == 'yes') {
                                                                                    echo 'checked=""';
                                                                                } ?> value="yes" id="rd1"
                                                                                    name="leader1_whatson_phone">
                                                                                <label for="rd1">Yes</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-6 col-md-6">
                                                                            <div class="radio radio-n">
                                                                                <input
                                                                                    type="radio" <?php if (isset($leaderdetail[0]['whatson_phone']) && $leaderdetail[0]['whatson_phone'] == 'no') {
                                                                                    echo 'checked=""';
                                                                                } ?> name="leader1_whatson_phone"
                                                                                    id="rd2" value="no">
                                                                                <label for="rd2">No</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="col-xs-12 radio_set col-md-6">Mobile
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6">
                                                                        <div class="col-xs-6 col-md-6">
                                                                            <div class="radio radio-n">
                                                                                <input
                                                                                    type="radio" <?php if (isset($leaderdetail[0]['whatson_mobile']) && $leaderdetail[0]['whatson_mobile'] == 'yes') {
                                                                                    echo 'checked=""';
                                                                                } ?> id="rd3"
                                                                                    name="leader1_whatson_mobile"
                                                                                    value="yes">
                                                                                <label for="rd3">Yes</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-6 col-md-6">
                                                                            <div class="radio radio-n">
                                                                                <input
                                                                                    type="radio" <?php if (isset($leaderdetail[0]['whatson_mobile']) && $leaderdetail[0]['whatson_mobile'] == 'no') {
                                                                                    echo 'checked=""';
                                                                                } ?> id="rd4"
                                                                                    name="leader1_whatson_mobile"
                                                                                    value="no">
                                                                                <label for="rd4">No</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="col-xs-12 radio_set col-md-6">Work
                                                                        Phone
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6">
                                                                        <div class="col-xs-6 col-md-6">
                                                                            <div class="radio radio-n">
                                                                                <input
                                                                                    type="radio" <?php if (isset($leaderdetail[0]['whatson_workphone']) && $leaderdetail[0]['whatson_workphone'] == 'yes') {
                                                                                    echo 'checked=""';
                                                                                } ?> id="rd5"
                                                                                    name="leader1_whatson_workphone"
                                                                                    value="yes">
                                                                                <label for="rd5">Yes</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-6 col-md-6">
                                                                            <div class="radio radio-n">
                                                                                <input
                                                                                    type="radio" <?php if (isset($leaderdetail[0]['whatson_workphone']) && $leaderdetail[0]['whatson_workphone'] == 'no') {
                                                                                    echo 'checked=""';
                                                                                } ?> name="leader1_whatson_workphone"
                                                                                    id="rd6" value="no">
                                                                                <label for="rd6">No</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="col-xs-12 radio_set col-md-6">Email
                                                                        Address1
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6">
                                                                        <div class="col-xs-6 col-md-6">
                                                                            <div class="radio radio-n">
                                                                                <input type="radio"
                                                                                <input
                                                                                    type="radio" <?php if (isset($leaderdetail[0]['whatson_email']) && $leaderdetail[0]['whatson_email'] == 'yes') {
                                                                                    echo 'checked=""';
                                                                                } ?> name="leader1_whatson_email"
                                                                                    id="rd07" value="yes">
                                                                                <label for="rd07">Yes</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-6 col-md-6">
                                                                            <div class="radio radio-n">
                                                                                <input
                                                                                    type="radio" <?php if (isset($leaderdetail[0]['whatson_email']) && $leaderdetail[0]['whatson_email'] == 'no') {
                                                                                    echo 'checked=""';
                                                                                } ?> name="leader1_whatson_email"
                                                                                    id="rd8" value="no">
                                                                                <label for="rd8">No</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <div class="clr"></div>
                                                        </div>
                                                        <div class="clr"></div>
                                                    </div>
                                                </div>
                                                <div class="form-group fg">
                                                    <label class="label_spc">
                                                        Choose how this leader’s name should appear in the publicly
                                                        accessible ‘What’s On’ on the Club’s web site.
                                                    </label>

                                                    <div class="other clr">
                                                        <div class="col-xs-12 col-xs-push-0 mb15">
                                                            <div class="radio-main rd_main">
                                                                <ul>
                                                                    <li>
                                                                        <div class="col-xs-12 col-md-6">
                                                                            <div class="radio col-md-6 col-xs-12 pl0">
                                                                                <label><input type="radio"
                                                                                              class="radi check_side"
                                                                                              name="leader1_newsletter_name"
                                                                                              value="name" <?php if (isset($leaderdetail[0]['newsletter_name']) && $leaderdetail[0]['newsletter_name'] == 'name') {
                                                                                        echo 'checked=""';
                                                                                    } ?> >Name</label>
                                                                            </div>
                                                                            <div class="radio col-md-6 col-xs-12 pl0">
                                                                                <label><input type="radio"
                                                                                              class="check_side"
                                                                                              name="leader1_newsletter_name"
                                                                                              value="webname" <?php if (isset($leaderdetail[0]['newsletter_name']) && $leaderdetail[0]['newsletter_name'] == 'webname') {
                                                                                        echo 'checked=""';
                                                                                    } ?>>Webname</label>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                                <div class="clr"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-xs-push-0 ">
                                                            <div class="radio-main rd_main">
                                                                <ul class="mb5">
                                                                    <li>
                                                                        <div class="col-xs-12 radio_set col-md-6">Home
                                                                            Phone
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-6">
                                                                            <div class="col-xs-6 col-md-6">
                                                                                <div class="radio radio-n">
                                                                                    <input
                                                                                        type="radio" <?php if (isset($leaderdetail[0]['newsletter_phone']) && $leaderdetail[0]['newsletter_phone'] == 'yes') {
                                                                                        echo 'checked=""';
                                                                                    } ?> value="yes" id="rd30"
                                                                                        name="leader1_newsletter_phone">
                                                                                    <label for="rd30">Yes</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-6 col-md-6">
                                                                                <div class="radio radio-n">
                                                                                    <input type="radio"
                                                                                           name="leader1_newsletter_phone"
                                                                                           id="rd31"
                                                                                           value="no" <?php if (isset($leaderdetail[0]['newsletter_phone']) && $leaderdetail[0]['newsletter_phone'] == 'no') {
                                                                                        echo 'checked=""';
                                                                                    } ?> >
                                                                                    <label for="rd31">No</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="col-xs-12 radio_set col-md-6">
                                                                            Mobile
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-6">
                                                                            <div class="col-xs-6 col-md-6">
                                                                                <div class="radio radio-n">
                                                                                    <input
                                                                                        type="radio" <?php if (isset($leaderdetail[0]['newsletter_mobile']) && $leaderdetail[0]['newsletter_mobile'] == 'yes') {
                                                                                        echo 'checked=""';
                                                                                    } ?> id="rd32"
                                                                                        name="leader1_newsletter_mobile"
                                                                                        value="yes">
                                                                                    <label for="rd32">Yes</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-6 col-md-6">
                                                                                <div class="radio radio-n">
                                                                                    <input type="radio" id="rd34"
                                                                                           name="leader1_newsletter_mobile"
                                                                                           value="no" <?php if (isset($leaderdetail[0]['newsletter_mobile']) && $leaderdetail[0]['newsletter_mobile'] == 'no') {
                                                                                        echo 'checked=""';
                                                                                    } ?> >
                                                                                    <label for="rd34">No</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="col-xs-12 radio_set col-md-6">Work
                                                                            Phone
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-6">
                                                                            <div class="col-xs-6 col-md-6">
                                                                                <div class="radio radio-n">
                                                                                    <input
                                                                                        type="radio" <?php if (isset($leaderdetail[0]['newsletter_workphone']) && $leaderdetail[0]['newsletter_workphone'] == 'yes') {
                                                                                        echo 'checked=""';
                                                                                    } ?> id="rd35"
                                                                                        name="leader1_newsletter_workphone"
                                                                                        value="yes">
                                                                                    <label for="rd35">Yes</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-6 col-md-6">
                                                                                <div class="radio radio-n">
                                                                                    <input type="radio"
                                                                                           name="leader1_newsletter_workphone"
                                                                                           id="rd36"
                                                                                           value="no" <?php if (isset($leaderdetail[0]['newsletter_workphone']) && $leaderdetail[0]['newsletter_workphone'] == 'no') {
                                                                                        echo 'checked=""';
                                                                                    } ?> >
                                                                                    <label for="rd36">No</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="col-xs-12 radio_set col-md-6">Email
                                                                            Address1
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-6">
                                                                            <div class="col-xs-6 col-md-6">
                                                                                <div class="radio radio-n">
                                                                                    <input
                                                                                        type="radio" <?php if (isset($leaderdetail[0]['newsletter_email']) && $leaderdetail[0]['newsletter_email'] == 'yes') {
                                                                                        echo 'checked=""';
                                                                                    } ?> name="leader1_newsletter_email"
                                                                                        id="rd37" value="yes">
                                                                                    <label for="rd37">Yes</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-6 col-md-6">
                                                                                <div class="radio radio-n">
                                                                                    <input type="radio"
                                                                                           name="leader1_newsletter_email"
                                                                                           id="rd38"
                                                                                           value="no" <?php if (isset($leaderdetail[0]['newsletter_email']) && $leaderdetail[0]['newsletter_email'] == 'no') {
                                                                                        echo 'checked=""';
                                                                                    } ?>>
                                                                                    <label for="rd38">No</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                                <div class="clr"></div>
                                                            </div>
                                                            <div class="clr"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default" style="margin:15px;">
                                    <label class="pl15 fnt_title panel_bg">
                                        Leader 2
                                    </label>

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="form-group col-lg-6 fg">
                                                <label>Name </label>
                                                <input class="form-control" name="activity_leader2_name" type="text"
                                                       value="<?php if (isset($leaderdetail[1]['webname'])) echo $leaderdetail[1]['webname']; ?>">
                                                <input class=" " name="activity_leader2Id" type="hidden"
                                                       value="<?php if (isset($leaderdetail[1]['leaderId'])) echo $leaderdetail[1]['leaderId']; ?>">
                                            </div>
                                            <div class="form-group col-lg-6 fg">
                                                <label>Email </label>
                                                <input class="form-control validate[email]"
                                                       name="activity_leader2_email" type="text"
                                                       value="<?php if (isset($leaderdetail[1]['email'])) echo $leaderdetail[1]['email']; ?>">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-xs-push-0 mb15 pl0">
                                            <label>Choose how this leader’s name should appear in ‘What’s On’ in the
                                                Club Newsletter</label>

                                            <div class="radio-main rd_main">
                                                <ul class="mb5">
                                                    <li>
                                                        <div class="col-xs-12 col-md-6">
                                                            <div class="radio col-md-6  col-xs-12 pl0">
                                                                <label><input
                                                                        type="radio" <?php if (isset($leaderdetail[1]['whatson_name']) && $leaderdetail[1]['whatson_name'] == 'yes') {
                                                                        echo 'checked=""';
                                                                    } ?> value="name" name="leader2_whatson_name"
                                                                        class="radi check_side">Name</label>
                                                            </div>
                                                            <div class="radio col-md-6 col-xs-12 pl0">
                                                                <label><input
                                                                        type="radio" <?php if (isset($leaderdetail[1]['whatson_name']) && $leaderdetail[1]['whatson_name'] == 'yes') {
                                                                        echo 'checked=""';
                                                                    } ?> value="webname" name="leader2_whatson_name"
                                                                        class="check_side">Webname</label>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <div class="clr"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default" style="margin:15px;">
                                    <label class="pl15 fnt_title panel_bg">
                                        Leader 3
                                    </label>

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="form-group col-lg-6 fg">
                                                <label>Name </label>
                                                <input class="form-control" name="activity_leader3_name" type="text"
                                                       value="<?php if (isset($leaderdetail[2]['webname'])) echo $leaderdetail[2]['webname']; ?>">
                                                <input class="form-control" name="activity_leader3Id" type="hidden"
                                                       value="<?php if (isset($leaderdetail[2]['leaderId'])) echo $leaderdetail[2]['leaderId']; ?>">
                                            </div>
                                            <div class="form-group col-lg-6 fg">
                                                <label>Email </label>
                                                <input class="form-control validate[email]"
                                                       name="activity_leader3_email" type="text"
                                                       value="<?php if (isset($leaderdetail[2]['email'])) echo $leaderdetail[2]['email']; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-xs-push-0 pl0 mb15">
                                            <label>Choose how this leader’s name should appear in ‘What’s On’ in the
                                                Club Newsletter</label>

                                            <div class="radio-main rd_main">
                                                <ul class="mb5">
                                                    <li>
                                                        <div class="col-xs-12 col-md-6">
                                                            <div class="radio col-md-6 col-xs-12 pl0">
                                                                <label><input type="radio" class="radi check_side"
                                                                              name="leader3_whatson_name"
                                                                              value="name" <?php if (isset($leaderdetail[2]['whatson_name']) && $leaderdetail[2]['whatson_name'] == 'name') {
                                                                        echo 'checked=""';
                                                                    } ?>>Name</label>
                                                            </div>
                                                            <div class="radio col-md-6 col-xs-12 pl0">
                                                                <label><input type="radio" class="check_side"
                                                                              name="leader3_whatson_name"
                                                                              value="webname" <?php if (isset($leaderdetail[2]['whatson_name']) && $leaderdetail[2]['whatson_name'] == 'webname') {
                                                                        echo 'checked=""';
                                                                    } ?>>Webname</label>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <div class="clr"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group fg">
                                                <label>Transport *</label>
                                                <textarea cols="50" rows="5" max-lenght="250"
                                                          name="activity_transport_detail"
                                                          class="validate[required]"><?php echo $detail['activity_transport_detail']; ?></textarea>
                                            </div>
                                            <div class="form-group fg">
                                                <label>Extended Activity Description </label>
                                                <textarea cols="50" rows="5" max-lenght="250"
                                                          name="activity_extended_detail"><?php echo $detail['activity_extended_detail']; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6 fg">
                                            <label>Length of Activity</label>
                                            <label><i class="validate_txt">(In hours or kilometres)</i></label>
                                            <input type="text" name="activity_length"
                                                   value="<?php echo $detail['activity_length']; ?>"/>
                                        </div>
                                        <div class="form-group col-md-6 fg">
                                            <label>Ascent</label>
                                            <label><i class="validate_txt">(In metres)</i></label>
                                            <input type="text" name="activity_ascent"
                                                   value="<?php echo $detail['activity_ascent']; ?>"/>
                                        </div>
                                        <div class="form-group col-md-6 fg">
                                            <label>Descent</label>
                                            <label><i class="validate_txt">(In metres)</i></label>
                                            <input type="text" name="activityDescent"
                                                   value="<?php echo $detail['activityDescent']; ?>"/>
                                        </div>
                                        <div class="col-xs-12" style="margin: 0;padding: 0;">
                                            <div class="form-group col-md-6 fg">
                                                <label>Type Of Terrain</label>
                                                <input type="text" name="activity_terrain_text"
                                                       value="<?php echo $detail['activity_terrain_text']; ?>"/>
                                            </div>

                                            <div class="form-group col-md-6 fg">
                                                <label>Booking End Date & Time</label>
                                                <input type="text" name="activity_booking_enddate"
                                                       id="activity_booking_enddate"
                                                       value="<?php if (isset($detail['activity_booking_enddate']) && $detail['activity_booking_enddate'] != '1970-01-01 00:00:00') {
                                                           echo date('d-m-Y, l:i:s a', strtotime($detail['activity_booking_enddate']));
                                                       } ?>"/>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12 fg">
                                            <label>Add Activity Location Map</label>
                                            <input type="hidden" name="activity_location_mappoints"
                                                   value="<?php echo $detail['activity_location_mappoints']; ?>">

                                            <div class="col-md-12" id="dvMap" style="height:200px ; "></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group fg col-md-6 col-xs-6">
                                            <label>Accommodation Information</label>
                                            <textarea cols="50" rows="5" max-lenght="250"
                                                      name="activity_accommodation_info"><?php echo $detail['activity_accommodation_info']; ?></textarea>
                                        </div>
                                        <div class="form-group col-md-6  fg ">
                                            <label>Activity Limit</label>
                                            <label><i class="validate_txt">(i.e. maximum number of
                                                    participants)</i></label>
                                            <input type="text" name="activity_participant_limit"
                                                   value="<?php echo $detail['activity_participant_limit']; ?>"
                                                   class="pl5"/>
                                        </div>
                                        <div class="form-group col-md-6 fg ">
                                            <label>Target Audience</label>
                                            <label><i class="validate_txt">(for email distribution lists)</i></label>
                                            <select class="form-control postal_country select_box pc"
                                                    name="activity_target_audience">
                                                <option
                                                    value="Wednesday-Walks" <?php if ($detail['activity_target_audience'] == 'Wednesday-Walks') {
                                                    echo 'selected="selected"';
                                                } ?> >Wednesday Walks
                                                </option>
                                                <option
                                                    value="Short-Notice-Walks" <?php if ($detail['activity_target_audience'] == 'Short-Notice-Walks') {
                                                    echo 'selected="selected"';
                                                } ?> >Short Notice Walks
                                                </option>
                                                <option
                                                    value="Family-Activities" <?php if ($detail['activity_target_audience'] == 'Family-Activities') {
                                                    echo 'selected="selected"';
                                                } ?>>Family Activities
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6 fg">
                                        <div class="row">
                                            <div class="col-md-7 pl0">
                                                <label>Do participants need to book ?</label>
                                            </div>
                                            <div class="col-xs-4 pl0 nd_book col-md-2">
                                                <input type="radio" style="width:auto;" name="activity_booking_require"
                                                       value="yes" <?php if ($detail['activity_booking_require'] == 'yes') {
                                                    echo 'checked="checked"';
                                                } ?> /> Yes
                                            </div>
                                            <div class="col-xs-4 pl0 col-md-2"><input type="radio" style="width:auto;"
                                                                                      name="activity_booking_require"
                                                                                      value="no" <?php if ($detail['activity_booking_require'] == 'no') {
                                                    echo 'checked="checked"';
                                                } ?> /> No
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <label>Upload Your Image</label>
                              <span class="btn btn-default btn-file upload_img col-md-2">
                              <input type="file" id="imgMember" title="" name="activity_image" class="form-control">
                              </span>

                              <span id="imagediv">
                              <?php
                              if ($detail['activity_image'] != '') {
                                  echo '<img class="upload_side_img2" id="upload_image" src="' . MAIN_DIR_URL . 'uploads/activity/' . $detail['activity_image'] . '" />';
                              }
                              ?>
                              </span>

                                        <div class="clr"></div>
                                    </div>

                                    <div class="col-md-12 mb50 footer-btn">
                                        <hr>

                                        <input type="submit" name="submit" value="Submit Offer"
                                               class="btn btn-primary btn-md submit_btn sb_offer_btn ">
                                        <input type="button" name="submit" value="Save"
                                               class="btn btn-info  btn-md submit_btn sb_offer_btn ">


                                        <input type="button" name="submit" value="Cancel"
                                               class="btn btn-danger btn-md submit_btn sb_offer_btn "
                                               onClick="location.href='<?php echo base_url() . 'activity'; ?>'">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Form Elements -->
</div>
</div>
</div>
<!-- end page-wrapper -->
<style>
    ul {
        list-style: none;
    }

    .checkbox1 {
        float: left;
        margin-top: 10px !important;
        padding-left: 0;
        width: 95px;
    }

    .validate_txt {
        font-size: 11px;
    }
</style>
<!--  Add Datepicker css &  js -->
<link href="<?php echo ASSET_URL; ?>/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>/assets/scripts/moment.js"></script>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>/assets/scripts/bootstrap-datetimepicker.min.js"></script>

<link href="<?php echo ASSET_URL; ?>assets/css/datepicker3.css" rel="stylesheet"/>
<script type="text/javascript" src="<?php echo ASSET_URL; ?>assets/scripts/bootstrap-datepicker.js"></script>
<!--  Validate Text -->
<script src="<?php echo MAIN_DIR_URL; ?>assests/js/jquery.validationEngine.js"></script>
<script src="<?php echo MAIN_DIR_URL; ?>assests/js/jquery.validationEngine-en.js"></script>
<script src="<?php echo MAIN_DIR_URL; ?>assests/js/custom.js"></script>
<link href="<?php echo MAIN_DIR_URL; ?>assests/css/validationEngine.jquery.css" rel="stylesheet">
<!-- Validate Text End -->
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('input[type="checkbox"]').on('change', function () {
            if (this.name != 'activity_type[]') {
                $('input[name="' + this.name + '"]').not(this).prop('checked', false);
            }
        });
    });
    jQuery(document).ready(function () {
        //Set Validation  Based  on  Selection
        jQuery('select[name="activity_lenght_type"]').change(function () {
            var currentDate = jQuery('input[name=activity_start_date]').val('');
        });

        jQuery('input[name=activity_start_date]').focus(function () {
            var tsdate = jQuery('input[name=activity_travelling_date]').val();
            if (tsdate == '') {
                dt = new Date();
                jQuery('input[name=activity_start_date]').datepicker('setStartDate', dt);
            } else {
                jQuery('input[name=activity_start_date]').datepicker('setStartDate', tsdate);
            }

        });
        jQuery('input[name=activity_start_date]').datepicker({
            format: "dd-mm-yyyy",
            startDate: "<?php echo date('d-m-Y'); ?>",
            todayBtn: "linked",
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (selected) {
            var endDate = new Date(selected.date.valueOf());
            var optionSelect = jQuery('select[name="activity_lenght_type"] :selected').val();
            if (optionSelect == '') {
                alert("Please select activity length type.");
                jQuery('input[name=activity_start_date]').val('');
            }
            if (optionSelect == 'part-day' || optionSelect == 'single-day') {
                jQuery('input[name=activity_end_date]').datepicker('setStartDate', endDate);
                jQuery('input[name=activity_end_date]').datepicker('setEndDate', endDate);
                jQuery('input[name=activity_travelling_date]').datepicker('setStartDate', endDate);
                jQuery('input[name=activity_travelling_date]').datepicker('setEndDate', endDate);
            } else {
                jQuery('input[name=activity_end_date]').datepicker('setStartDate', endDate);
                //jQuery('input[name=activity_end_date]').datepicker('setStartDate', "<?php echo date('d-m-Y'); ?>");
                jQuery('input[name=activity_end_date]').datepicker('setEndDate', "");
                jQuery('input[name=activity_travelling_date]').datepicker('setStartDate', endDate);
                jQuery('input[name=activity_travelling_date]').datepicker('setEndDate', '');
            }
            $('#activity_start_date').validationEngine('hide');
        });

        jQuery('input[name=activity_end_date]').datepicker({
            format: "dd-mm-yyyy",
            startDate: "<?php echo date('d-m-Y'); ?>",
            todayBtn: "linked",
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (selected) {
            var selectedstartDate = jQuery('input[name="activity_start_date"]').val();
            var optionSelect = jQuery('select[name="activity_lenght_type"] :selected').val();

            if (optionSelect == '') {
                alert("Please select activity length type.");
                jQuery('input[name="activity_end_date"]').val('');
            }
            if (selectedstartDate == '') {
                alert("Please select start date of activity.");
                jQuery('input[name="activity_end_date"]').val('');
            }
            $('#activity_end_date').validationEngine('hide');
        });

        jQuery('input[name=activity_travelling_date]').datepicker({
            format: "dd-mm-yyyy",
            startDate: "<?php echo date('d-m-Y'); ?>",
            todayBtn: "linked",
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (selected) {
            var selectedstartDate = jQuery('input[name="activity_start_date"]').val();
            var optionSelect = jQuery('select[name="activity_lenght_type"] :selected').val();

            if (optionSelect == '') {
                alert("Please select activity length type.");
                jQuery('input[name="activity_travelling_date"]').val('');
            }
            /*if( selectedstartDate == '' ){
             alert("Please select start date of activity.");
             jQuery('input[name="activity_travelling_date"]').val('');
             }*/

        });
        jQuery('#activity_booking_enddate').datetimepicker({
            format: 'D-M-YYYY, h:mm:ss a'
        });

        jQuery("#imgMember").change("on", function () {

            var filename = jQuery(this).val();
            var extension = filename.replace(/^.*\./, '');

            extension = extension.toLowerCase();

            if (extension == "jpg" || extension == "jpeg" || extension == "png") {
                $('#imgMember').validationEngine('hide');
                $('#imagediv').text('');
                readURL(this);
            } else {
                $('#imagediv').text('');
                $('#imgMember').validationEngine('showPrompt', 'Invalid File Type.', 'fail');
            }
        });

        jQuery("body").on("click", function () {
            $('#imgMember').validationEngine('hide');
        });

        jQuery("body").on("click", "#remove_upload", function () {
            jQuery('#imagediv').html('');
            jQuery('#imagediv').html('');
            jQuery('#imagediv').val('');
            jQuery('#imagediv').text('');
            jQuery('#imgMember').val('');
        });

        /*Leader Suggestion */
        jQuery("input[name=activity_leader1_name]").blur(function () {
            var leader1Text = jQuery(this).val();
            var state_url = '<?php  echo $state_url; ?>';
            jQuery.ajax({
                url: state_url,
                type: "POST",
                dataType: "json",
                data: {textLeader: leader1Text},
                success: function (data) {

                    if (data && data != 'error') {
                        //data = jQuery.parseJSON(data);

                        jQuery(this).validationEngine('hide');
                        jQuery.each(data, function (index, value) {

                            if (index == 'email') {
                                jQuery('input[name="activity_leader1_email"]').val(value);
                            } else if (index == 'memberId') {
                                jQuery('input[name="activity_leader1Id"]').val(value);
                            } else if (index == 'memberId') {
                                jQuery('input[name="activity_leader1_email"]').val(value);
                            } else if (index == 'webview_details') {
                                dataVal = value;
                                if (dataVal != '') {
                                    var temp = new Array();
                                    // this will return an array with strings "1", "2", etc.
                                    temp = dataVal.split(",");
                                    var leader1_whatson_mobile = 0;
                                    var leader1_whatson_phone = 0;
                                    var leader1_whatson_workphone = 0;
                                    var email1 = 0;

                                    for (a in temp) {
                                        attrSelected = temp[a];
                                        if (attrSelected == 'name') {
                                            jQuery('input[name="leader1_whatson_name"]').attr('checked', 'checked');
                                            jQuery('input[name="leader1_whatson_webname"]').removeAttr('checked');

                                        } else if (attrSelected == 'webname') {
                                            jQuery('input[name="leader1_whatson_webname"]').attr('checked', 'checked');
                                            jQuery('input[name="leader1_whatson_name"]').removeAttr('checked');

                                        }
                                        if (attrSelected == 'home_phone' || attrSelected == 'home_phone1') {
                                            leader1_whatson_phone = 1;
                                        }
                                        if (attrSelected == 'mobile' || attrSelected == 'mobile1') {
                                            leader1_whatson_mobile = 1;
                                        }
                                        if (attrSelected == 'work_phone' || attrSelected == 'work_phone1') {
                                            leader1_whatson_workphone = 1;
                                        }
                                        if (attrSelected == 'webview_email1') {
                                            email1 = 1;
                                        }
                                    }
                                    if (leader1_whatson_phone) {
                                        //leader1_whatson_phone
                                        jQuery('input[name="leader1_whatson_phone"][value="yes"]').attr('checked', 'checked');
                                        jQuery('input[name="leader1_whatson_phone"][value="no"]').removeAttr('checked', 'checked');
                                    } else {
                                        jQuery('input[name="leader1_whatson_phone"][value="no"]').attr('checked', 'checked');
                                        jQuery('input[name="leader1_whatson_phone"][value="yes"]').removeAttr('checked', 'checked');
                                    }
                                    if (leader1_whatson_mobile) {
                                        jQuery('input[name="leader1_whatson_mobile"][value="yes"]').attr('checked', 'checked');
                                        jQuery('input[name="leader1_whatson_mobile"][value="no"]').removeAttr('checked', 'checked');
                                    } else {
                                        jQuery('input[name="leader1_whatson_mobile"][value="no"]').attr('checked', 'checked');
                                        jQuery('input[name="leader1_whatson_mobile"][value="yes"]').removeAttr('checked', 'checked');
                                    }
                                    if (leader1_whatson_workphone) {
                                        jQuery('input[name="leader1_whatson_workphone"][value="yes"]').attr('checked', 'checked');
                                        jQuery('input[name="leader1_whatson_workphone"][value="no"]').removeAttr('checked', 'checked');
                                    } else {
                                        jQuery('input[name="leader1_whatson_workphone"][value="no"]').attr('checked', 'checked');
                                        jQuery('input[name="leader1_whatson_workphone"][value="yes"]').removeAttr('checked', 'checked');
                                    }
                                    if (email1) {
                                        //email1
                                        jQuery('input[name="leader1_whatson_email"][value="yes"]').attr('checked', 'checked');
                                        jQuery('input[name="leader1_whatson_email"][value="no"]').removeAttr('checked', 'checked');
                                    } else {
                                        jQuery('input[name="leader1_whatson_email"][value="no"]').attr('checked', 'checked');
                                        jQuery('input[name="leader1_whatson_email"][value="yes"]').removeAttr('checked', 'checked');
                                    }
                                }

                            }


                        });
                    } else {
                        //Error  State
                        jQuery('input[name="activity_leader1_name"]').validationEngine('showPrompt', 'This Leader is not valid.', 'fail');
                        jQuery('input[name="activity_leader1_name"]').val('');
                        jQuery('input[name="activity_leader1_email"]').val('');

                    }
                }
            });

        });
        //Leader2 Validation
        jQuery("input[name=activity_leader2_name]").blur(function () {

            var leader1Text = jQuery(this).val();
            var state_url = '<?php  echo $state_url; ?>';
            jQuery.ajax({
                url: state_url,
                type: "POST",
                dataType: "json",
                data: {textLeader: leader1Text},
                success: function (data) {

                    if (data != 'error') {
                        //data = jQuery.parseJSON(data);
                        jQuery(this).validationEngine('hide');
                        jQuery.each(data, function (index, value) {

                            if (index == 'email') {
                                jQuery('input[name="activity_leader2_email"]').val(value);
                            } else if (index == 'memberId') {
                                jQuery('input[name="activity_leader2Id"]').val(value);
                            } else if (index == 'webview_details') {
                                dataVal = value;
                                if (dataVal != '') {
                                    var temp = new Array();
                                    // this will return an array with strings "1", "2", etc.
                                    temp = dataVal.split(",");

                                    for (a in temp) {
                                        attrSelected = temp[a];
                                        if (attrSelected == 'name') {
                                            jQuery('input[name="leader2_whatson_name"]').attr('checked', 'checked');
                                            jQuery('input[name="leader2_whatson_webname"]').removeAttr('checked');

                                        } else if (attrSelected == 'webname') {
                                            jQuery('input[name="leader2_whatson_webname"]').attr('checked', 'checked');
                                            jQuery('input[name="leader2_whatson_name"]').removeAttr('checked');

                                        }

                                    }

                                }

                            }


                        });
                    } else {
                        //Error  State
                        jQuery('input[name="activity_leader2_name"]').validationEngine('showPrompt', 'This Leader is not valid.', 'fail');
                        jQuery('input[name="activity_leader2_name"]').val('');
                        jQuery('input[name="activity_leader2_email"]').val('');
                    }
                }
            });

        });
        //Leader3 Validation
        jQuery("input[name=activity_leader3_name]").blur(function () {

            var leader1Text = jQuery(this).val();
            var state_url = '<?php  echo $state_url; ?>';
            jQuery.ajax({
                url: state_url,
                type: "POST",
                dataType: "json",
                data: {textLeader: leader1Text},
                success: function (data) {

                    if (data != 'error') {
                        //data = jQuery.parseJSON(data);

                        jQuery(this).validationEngine('hide');
                        jQuery.each(data, function (index, value) {

                            if (index == 'email') {
                                jQuery('input[name="activity_leader3_email"]').val(value);
                            } else if (index == 'memberId') {
                                jQuery('input[name="activity_leader3Id"]').val(value);
                            } else if (index == 'webview_details') {
                                dataVal = value;
                                if (dataVal != '') {
                                    var temp = new Array();
                                    // this will return an array with strings "1", "2", etc.
                                    temp = dataVal.split(",");

                                    for (a in temp) {
                                        attrSelected = temp[a];
                                        if (attrSelected == 'name') {
                                            jQuery('input[name="leader3_whatson_name"]').attr('checked', 'checked');
                                            jQuery('input[name="leader3_whatson_webname"]').removeAttr('checked');

                                        } else if (attrSelected == 'webname') {
                                            jQuery('input[name="leader3_whatson_webname"]').attr('checked', 'checked');
                                            jQuery('input[name="leader3_whatson_name"]').removeAttr('checked');

                                        }

                                    }

                                }

                            }


                        });
                    } else {
                        //Error  State
                        jQuery('input[name="activity_leader3_name"]').validationEngine('showPrompt', 'This Leader is not valid.', 'fail');
                        jQuery('input[name="activity_leader3_name"]').val('');
                        jQuery('input[name="activity_leader3_email"]').val('');
                    }
                }
            });

        });

    });
</script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    window.onload = function () {
        var location = {lat:<?php echo $lat; ?>, lng:<?php echo $long;   ?>};
        var mapOptions = {
            center: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>),
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var infoWindow = new google.maps.InfoWindow();
        var latlngbounds = new google.maps.LatLngBounds();
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
        var marker = new google.maps.Marker({
            position: location,
            map: map,
            draggable: true
        });
        map.setCenter(location);
        var markerPosition = marker.getPosition();
        google.maps.event.addListener(marker, 'dragend', function (mEvent) {
            jQuery('input[name=activity_location_mappoints]').val(mEvent.latLng.lat() + ',' + mEvent.latLng.lng());
            //document.getElementById('current').innerHTML = '<p>Marker dropped: Current Lat: ' + evt.latLng.lat().toFixed(3) + ' Current Lng: ' + evt.latLng.lng().toFixed(3) + '</p>';
        });
    }
</script>
