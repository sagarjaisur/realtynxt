<?php
$page_caption = 'Offered Activities';
$cancel_url = base_url() . 'activity/index';
$add_url = base_url() . 'activity/createactivity/';
?>
<link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/dataTables/dataTables.bootstrap.css">
<!--  page-wrapper -->
<div id="page-wrapper">


    <div class="row">
        <!--  page header -->
        <div class="col-lg-12">
            <h1 class="page-header page_title"><?php echo $page_caption; ?> <input type="button"
                                                                                   class="btn btn-primary caption-btn"
                                                                                   onClick="location.href='<?php echo $add_url; ?>'"
                                                                                   value="ADD Activity offer"
                                                                                   name="submit"></h1>
        </div>
        <!-- end  page header -->
    </div>
    <?php echo $this->session->flashdata('success'); ?>
    <div class="row">
        <div class="col-lg-12">
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <!--div class="panel-heading">
                             <?php //echo  $page_caption;?> List
                        </div-->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($activities)) {
                                foreach ($activities as $activity) {
                                    //Links
                                    $editLink = base_url() . 'activity/edit/' . $activity['activityId'];
                                    $deleteLink = base_url() . 'activity/delete/' . $activity['activityId'];
                                    $imageLink = UPLOAD_URL . '/activity/' . $activity['activity_image'];
                                    $imageLink1 = ADMIN_URL . '/assests/images/default-images.png';


                                    ?>
                                    <tr class="odd gradeX">
                                        <td>
                                            <a href="<?php echo $editLink; ?>"><?php echo $activity['activity_name']; ?></a>
                                        </td>
                                        <td>
                                            <?php if ($activity['activity_image'] != '') { ?>
                                                <img src="<?php echo $imageLink; ?>"
                                                     title="<?php echo $activity['activity_image']; ?>" width="50"
                                                     height="50"/>
                                            <?php } else { ?>
                                                <img src="<?php echo $imageLink1; ?>"
                                                     title="<?php echo $activity['activity_image']; ?>" width="50"
                                                     height="50"/>
                                            <?php } ?>
                                        </td>
                                        <td><?php echo ucfirst($activity['activity_state']); ?></td>
                                        <td class="center">
                                            <a href="<?php echo base_url(); ?>activity/changeactivitystate/<?php echo $activity['activityId'] . '/approved'; ?>">Approve</a>
                                            /
                                            <a href="<?php echo base_url(); ?>activity/changeactivitystate/<?php echo $activity['activityId'] . '/rejected'; ?>">Reject</a>
                                            /
                                            <a href="<?php echo base_url(); ?>activity/activitydetails/<?php echo $activity['activityId']; ?>">View</a>
                                        </td>
                                    </tr>
                                <?php }
                            } else {
                                echo '<tr><td colspan="4">No record found.</td></tr>';
                            }
                            ?>

                            </tbody>
                        </table>

                        <p><?php echo $links; ?></p>
                    </div>

                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>

</div>
<!-- end page-wrapper -->
