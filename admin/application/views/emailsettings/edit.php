<?php
$page_caption = 'Email Template';
$cancel_url = base_url() . 'emailsettings/index';
$submitForm_url = 'emailsettings/edit/';
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">

        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <h1 class="page-header page_title">Edit <?php echo $page_caption; ?></h1>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <!--div class="panel-heading">
                            <?php echo $page_caption; ?> Detail
                        </div-->
                    <?php //echo '<pre>';print_r($emailTemplates);exit;?>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <?php echo form_open('emailsettings/edit/' . $id) ?>

                                <input class="form-control" name="emailId" type="hidden" value="<?php echo $id; ?>"
                                       required>

                                <div class="form-group">
                                    <label>Email Template *</label>
                                    <input class="form-control" name="emailTemplate" type="text"
                                           value="<?php echo $details[0]['emailTemplate']; ?>" required>
                                </div>
                                <div class="form-group">
                                    <label>Email Subject *</label>
                                    <input class="form-control" name="emailSubject" type="text"
                                           value="<?php echo $details[0]['emailSubject']; ?>" required>
                                </div>

                                <div class="form-group">
                                    <label>Email Body *</label>
                                    <?php /*?><textarea class="form-control" name="emailBody" required><?php echo $details[0]['emailBody'];?></textarea><?php */ ?>
                                    <?php echo $this->ckeditor->editor('emailBody', $details[0]['emailBody']); ?>
                                </div>

                                <div class="form-group"> &nbsp;</div>
                                <input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
                                <input type="button" name="Cancel" value="Cancel" class="btn btn-success"
                                       onclick="javascript: window.location.href='<?php echo $cancel_url; ?>'"/>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>


    </div>
</div>