<?php
$page_caption = 'Email Template';
$add_url = base_url() . 'emailsettings/add';
?>
<link href="<?php echo ASSET_URL; ?>assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet"/>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">


        <div class="row">
            <!--  page header -->
            <div class="col-lg-12">
                <h1 class="page-header page_title">
                    <section class="content-header">
                        <h1 class="page-header page_title">&nbsp;<?php echo $page_caption; ?> Management<input
                                type="button" onClick="location.href='<?php echo $add_url; ?>'" name="submit"
                                value="Add <?php echo $page_caption; ?>" class="btn btn-success btn-md submit"
                                style="margin-left: 50px"></h1>

                        <ol class="breadcrumb">
                            <li><a href="<?php echo MAIN_DIR_URL . 'admin/index.php/dashboard/'; ?>"><i
                                        class="fa fa-dashboard"></i> Home</a></li>
                            <li class="active"><a href="#"><?php echo $page_caption; ?> Management</a></li>
                        </ol>
                    </section>
                </h1>
            </div>
            <!-- end  page header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <div class="row">
            <div class="col-lg-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">
                    <!--div class="panel-heading">
                             <?php echo $page_caption; ?> List
                        </div-->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>Email Template</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($details)): foreach ($details as $detail) {
                                    //Links

                                    $editLink = base_url() . 'emailsettings/edit/' . $detail['emailId'];
                                    $deleteLink = base_url() . 'emailsettings/delete/' . $detail['emailId'];
                                    ?>
                                    <tr class="odd gradeX">
                                        <td>
                                            <a href="<?php echo $editLink; ?>"><?php echo $detail['emailTemplate']; ?></a>
                                        </td>
                                        <td class="center">
                                            <button class="btn btn-default" type="button"
                                                    onClick="javascipt:window.location.href='<?php echo $editLink; ?>';"
                                                    title="EDIT">
                                                <i class="fa fa-edit fa-fw"></i>Edit
                                            </button>
                                            <button class="btn btn-danger" type="button"
                                                    onClick="return deleteRecode('<?php echo $deleteLink; ?>','<?php echo $page_caption; ?>');"
                                                    title="DELETE">
                                                <i class="fa fa-edit fa-fw"></i>Delete
                                            </button>
                                        </td>
                                    </tr>
                                <?php } endif; ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>

    </div>
    <!-- end page-wrapper -->
</div>