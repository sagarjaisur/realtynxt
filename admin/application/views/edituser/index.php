<?php
$page_caption = 'Member';
$cancel_url = base_url() . 'users/userrequest' . $user[0]['roleId'];
$submitForm_url = 'users/updateUser/' . $user[0]['memberId'];
$ajax_url = base_url() . 'users/';
$state_url = base_url() . 'users/';
?>
<!--  page-wrapper -->
<div id="page-wrapper">
    <div class="row">
        <!-- Page Header -->
        <div class="col-lg-12">
            <h1 class="page-header page_title"><?php echo $page_caption; ?> Details</h1>
        </div>
        <!--End Page Header -->
    </div>
    <?php echo $this->session->flashdata('success'); ?>
    <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
    <div class="row">
        <div class="col-lg-12">
            <?php echo form_open_multipart($submitForm_url, 'id="edit_user"') ?>
            <input type="hidden" id="ajax_url" name="ajax_url" value="<?php echo $ajax_url; ?>">
            <input type="hidden" id="state_url" name="state_url" value="<?php echo $state_url; ?>">
            <!-- Form Elements -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $page_caption; ?> Detail
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Name
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Full Name *</label>
                                        <input class="form-control validate[required,custom[onlyLetterSp]] fname"
                                               name="name" type="text" value="<?php if (set_value('name')) {
                                            echo set_value('name');
                                        } else {
                                            echo $user[0]['name'];
                                        } ?>">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Postal Information
                            </div>
                            <div class="panel-body">
                                <div class="row">

                                    <div class="col-md-6">
                                        <label>Address Line1 *</label>
                                        <input class="form-control  postal_address1 validate[required]"
                                               name="postal_address1" type="text"
                                               value="<?php echo $user[0]['street']; ?>">
                                    </div>

                                    <div class="col-md-6">
                                        <label>Address Line2</label>
                                        <input class="form-control postal_address2" name="city" type="text"
                                               value="<?php echo $user[0]['city']; ?>">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Suburb</label>
                                        <input class="form-control postal_suburb" name="postal_suburb" type="text"
                                               value="<?php echo $user[0]['suburb']; ?>">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Country</label>
                                        <select id="member_country" name="postal_country"
                                                class="form-control postal_country">
                                            <option value="">Select Country</option>
                                            <?php if (!empty($countries)): foreach ($countries as $country): ?>
                                                <option
                                                    value="<?php echo $country['country']; ?>" <?php if ($country['country'] == $user[0]['country']) echo 'selected = selected'; ?>><?php echo $country['title']; ?></option>
                                            <?php endforeach; endif; ?>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6">
                                        <label>State</label>
                                        <select id="member_state" name="postal_state" class="form-control postal_state">
                                            <option value="">Select State</option>
                                            <?php if (!empty($states)): foreach ($states as $state): ?>
                                                <option
                                                    value="<?php echo $state['state']; ?>" <?php if ($state['state'] == $user[0]['state']) echo 'selected = selected'; ?>><?php echo $state['title']; ?></option>
                                            <?php endforeach; endif; ?>
                                        </select>
                                    </div>

                                    <div class="col-md-6">
                                        <label>Postcode</label>
                                        <input class="form-control validate[custom[number],maxsize[9] pincode"
                                               name="pincode"
                                               type="text" value="<?php echo $user[0]['pincode']; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Pick-up Information <input type="checkbox" id="same_as_postal"
                                                   class="checkbox-inline same_as_postal">Same as postal address
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Address Line1 </label>
                                <input class="form-control pickup_street" name="pickup_street" type="text"
                                       value="<?php echo $user[0]['pickup_street']; ?>">
                            </div>
                            <div class="col-md-6">
                                <label>Address Line2</label>
                                <input class="form-control pickup_city" name="pickup_city" type="text"
                                       value="<?php echo $user[0]['pickup_city']; ?>">
                            </div>
                            <div class="col-md-6">
                                <label>Suburb</label>
                                <input class="form-control pickup_suburb" name="pickup_suburb" type="text"
                                       value="<?php echo $user[0]['pickup_suburb']; ?>">
                            </div>
                            <div class="col-md-6">
                                <label>Country</label>
                                <select id="pickup_country" name="pickup_country" class="form-control pickup_country">
                                    <option value="">Select Country</option>
                                    <?php if (!empty($countries)): foreach ($countries as $country): ?>
                                        <option
                                            value="<?php echo $country['country']; ?>" <?php if ($country['country'] == $user[0]['pickup_country']) echo 'selected = selected'; ?>><?php echo $country['title']; ?></option>
                                    <?php endforeach; endif; ?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">
                                <label>State</label>
                                <select id="pickup_state" name="pickup_state" class="form-control pickup_state">
                                    <option value="">Select State</option>
                                    <?php if (!empty($states)): foreach ($states as $state): ?>
                                        <option
                                            value="<?php echo $state['state']; ?>" <?php if ($state['state'] == $user[0]['pickup_state']) echo 'selected = selected'; ?>><?php echo $state['title']; ?></option>
                                    <?php endforeach; endif; ?>
                                </select>
                            </div>


                            <div class="col-md-6">
                                <label>Postcode</label>
                                <input class="form-control validate[maxsize[9] zip1" name="zip1" type="text"
                                       value="<?php echo $user[0]['pickup_zip']; ?>">
                            </div>

                        </div>
                    </div>

                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Phone and Email
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Home Phone</label>
                                <input class="form-control validate[maxSize[15]]" name="phone_h" type="text"
                                       value="<?php echo $user[0]['phone_h']; ?>">
                            </div>
                            <div class="col-md-6">
                                <label>Mobile *</label>
                                <input class="form-control validate[maxSize[15]]" name="mobileNumber" type="text"
                                       value="<?php echo $user[0]['mobileNumber']; ?>">
                            </div>
                            <div class="col-md-6">
                                <label>Work Phone</label>
                                <input class="form-control validate[maxSize[15]]" name="phone_w" type="text"
                                       value="<?php echo $user[0]['phone_w']; ?>">
                            </div>
                            <div class="col-md-6">
                                <label>Email address1 *</label>
                                <input class="form-control validate[required,email]" name="emailId" type="text"
                                       value="<?php echo $user[0]['email']; ?>">
                            </div>
                            <div class="col-md-6">
                                <label>Email address2 </label>
                                <input class="form-control validate[email]" name="emailId1" type="text"
                                       value="<?php echo $user[0]['email1']; ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Emergency Contact
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Emergency Name *</label>
                                <input class="form-control validate[required]" name="emergencyFullname" type="text"
                                       value="<?php echo $user[0]['emergencyFullname']; ?>">
                            </div>

                            <div class="col-md-6">
                                <label>Emergency Contact *</label>
                                <input class="form-control validate[required,maxSize[15]]" name="emergency_contactno"
                                       type="text" value="<?php echo $user[0]['emergency_contactno']; ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Qualifying walk
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Walk And Date *</label>
                                <input class="form-control validate[required]" name="qualifying_walk" type="text"
                                       value="<?php echo $user[0]['qualifying_walk']; ?>">
                            </div>
                            <div class="col-md-6">
                                <label>Nominated by *</label>
                                <input class="form-control validate[required]" name="nominated_by" type="text"
                                       value="<?php echo $user[0]['nominated_by']; ?>">
                            </div>
                            <div class="col-md-6">
                                <label>Seconded by *</label>
                                <input class="form-control validate[required]" name="seconded_by" type="text"
                                       value="<?php echo $user[0]['seconded_by']; ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        User Account
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label>User Name *</label>
                                <input class="form-control validate[required]" name="login" type="text"
                                       value="<?php echo $user[0]['login']; ?>">
                            </div>
                            <div class="col-md-6">
                                <label>Password *</label>
                                <input class="form-control validate[required,minSize[4]]" name="password"
                                       type="password" id="password" value="<?php echo $user[0]['pass']; ?>">
                            </div>
                            <div class="col-md-6">
                                <label>Confirm Password *</label>
                                <input class="form-control validate[required,equals[password]]" name="password1"
                                       type="password" value="<?php echo $user[0]['pass']; ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Privacy
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="classrelative col-xs-push-0 putright p0">
                                    <label>Web Name *</label>
                                    <i class="validate_txt">Choose your own name or a pseudonym for the Membership List
                                        on the Club web site</i>
                                    <input type="text" class="form-control validate[required] webname" name="webname"
                                           value="<?php echo $user[0]['webname']; ?>">
                                    <span class="webname-suggetion text-success"></span>
                                </div>
                            </div>

                            <div class="col-md-8 col-xs-push-0 padding-l">
                                <div class="radio col-md-3 col-xs-6">
                                    <label><input type="radio" <?php if ($user[0]['webview_type'] == 'full') {
                                            echo 'checked="checked"';
                                        } ?> value="full" class="showfull" name="webview_type">Full Details</label>
                                </div>
                                <div class="radio col-md-3 col-xs-6 ">
                                    <label><input type="radio"
                                                  value="other" <?php if ($user[0]['webview_type'] == 'other') {
                                            echo 'checked="checked"';
                                        } ?> class="showother" name="webview_type">Custom</label>
                                </div>
                            </div>
                            <div class="col-md-12 clr">
                                <div class="padding-l"><i class="validate_txt">Choose how much of your data should
                                        appear in the Membership List on the Club web site. Access to the Membership
                                        List is restricted to Club Members.</i></div>
                            </div>

                            <div class="full_details mb10 <?php if ($user[0]['webview_type'] == 'other') {
                                echo 'hide';
                            } ?>">
                                <div class="col-md-8 col-xs-push-0 padding-l">
                                    <div class="radio col-md-3 col-xs-6 ">
                                        <label>
                                            <input type="radio" checked disabled value="name"
                                                   name="name_type">Name</label>
                                    </div>
                                    <div class="clr"></div>
                                    <div class="radio col-md-3 col-xs-6 ">
                                        <label>
                                            <input type="radio" checked disabled value="postal_address"
                                                   name="address_type">Postal Address</label>
                                    </div>
                                </div>

                                <div class="radio-main">
                                    <ul>
                                        <li>
                                            <div class="col-xs-8">Home Phone</div>
                                            <div class="col-xs-4">
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input type="radio" checked disabled value="yes" id="fd1"
                                                               name="home_phone">
                                                        <label for="fd1">Yes</label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input type="radio" disabled name="home_phone" id="fd2"
                                                               value="no">
                                                        <label for="fd2">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-xs-8">Mobile</div>
                                            <div class="col-xs-4">
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input type="radio" checked disabled id="fd3" name="mobile"
                                                               value="yes">
                                                        <label for="fd3">Yes</label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input type="radio" disabled id="fd4" name="mobile" value="no">
                                                        <label for="fd4">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-xs-8">Work Phone</div>
                                            <div class="col-xs-4">
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input type="radio" checked disabled id="fd5" name="work_phone"
                                                               value="yes">
                                                        <label for="fd5">Yes</label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input type="radio" disabled name="work_phone" id="fd6"
                                                               value="no">
                                                        <label for="fd6">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-xs-8">Email Address1</div>
                                            <div class="col-xs-4">
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input type="radio" checked disabled name="email1" id="fd07"
                                                               value="yes">
                                                        <label for="fd07">Yes</label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input type="radio" disabled name="email1" id="fd8" value="no">
                                                        <label for="fd8">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-xs-8">Leader may access my emergency contact details</div>
                                            <div class="col-xs-4">
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input type="radio" checked disabled
                                                               name="show_emergency_contact_to_leader" id="fd09"
                                                               value="yes">
                                                        <label for="fd09">Yes</label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input type="radio" disabled
                                                               name="show_emergency_contact_to_leader" id="fd10"
                                                               value="no">
                                                        <label for="fd10">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="clr"></div>
                            </div>

                            <div class="mb10 other <?php if ($user[0]['webview_type'] == 'full') {
                                echo 'hide';
                            } ?>">
                                <?php $webviewdetails = explode(',', $user[0]['webview_details']); ?>
                                <?php /*
                    <div class="col-md-6 col-xs-push-0 padding-l">
                      <div class="radio col-md-3 col-xs-6 ">
                        <label>
                          <input type="radio" name="name_type1" value="name" <?php echo (in_array("name", $webviewdetails))?'checked':'';?>>Name</label>
                      </div>
                      <div class="clr"></div>
                      <div class="radio col-md-3 col-xs-6 ">
                        <label>
                          <input type="radio" name="name_type1" value="webname" <?php echo (in_array("webname", $webviewdetails))?'checked':'';?>>Webname</label>
                      </div>
                    </div>
                    <div class="col-md-6 col-xs-push-0 padding-r">
                      <div class="radio col-md-3 col-xs-6 ">
                        <label>
                          <input type="radio" name="address_type1" value="postal_address" <?php echo (in_array("postal_address", $webviewdetails))?'checked':'';?>>Postal address
                          </label>
                      </div>
                      <div class="clr"></div>
                      <div class="radio col-md-3 col-xs-6 ">
                        <label>
                          <input type="radio" name="address_type1" value="pickup_address" <?php echo (in_array("pickup_address", $webviewdetails))?'checked':'';?>>Pick-up address
                          </label>
                      </div>
                      <div class="clr"></div>
                      <div class="radio col-md-3 col-xs-6 ">
                        <label>
                          <input type="radio" name="address_type1" value="None" <?php echo (in_array("none", $webviewdetails))?'checked':'';?>>None
                          </label>
                      </div>
                      <div class="clr"></div>
                    </div>  */ ?>

                                <div class="col-md-12 col-xs-push-0 padding-l radio_set">
                                    <div class="radio col-md-3 col-xs-6 pl0">
                                        <label><input type="radio" name="name_type1"
                                                      value="name" <?php echo (in_array("name", $webviewdetails)) ? 'checked' : ''; ?>>Name</label>
                                    </div>
                                    <div class="radio col-md-3 col-xs-6 pl0">
                                        <label><input type="radio" name="name_type1"
                                                      value="webname" <?php echo (in_array("webname", $webviewdetails)) ? 'checked' : ''; ?>>Webname</label>
                                    </div>
                                </div>

                                <div class="col-md-12 col-xs-push-0 padding-l radio_set">
                                    <div class="radio col-md-3 col-xs-12 pl0">
                                        <label><input type="radio" name="address_type1"
                                                      value="postal_address" <?php echo (in_array("postal_address", $webviewdetails)) ? 'checked' : ''; ?>>Postal
                                            address</label>
                                    </div>
                                    <div class="radio col-md-3 col-xs-12 pl0">
                                        <label><input type="radio" name="address_type1"
                                                      value="pickup_address" <?php echo (in_array("pickup_address", $webviewdetails)) ? 'checked' : ''; ?>>Pick-up
                                            address</label>
                                    </div>
                                    <div class="radio col-md-3 col-xs-12 pl0">
                                        <label>
                                            <input type="radio" name="address_type1"
                                                   value="None" <?php echo (in_array("none", $webviewdetails)) ? 'checked' : ''; ?>>None
                                        </label>
                                    </div>
                                    <div class="clr"></div>
                                </div>

                                <div class="radio-main">
                                    <ul>
                                        <li>
                                            <div class="col-xs-8">Home Phone</div>
                                            <div class="col-xs-4">
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input id="o1" type="radio" name="home_phone1"
                                                               value="yes" <?php echo (in_array("home_phone1", $webviewdetails)) ? 'checked' : ''; ?>>
                                                        <label for="o1">Yes</label>
                                                    </div>
                                                </div>

                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input id="o2" type="radio" name="home_phone1"
                                                               value="no" <?php echo (in_array("home_phone1", $webviewdetails)) ? '' : 'checked'; ?>>
                                                        <label for="o2">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="col-xs-8">Mobile</div>
                                            <div class="col-xs-4">
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input id="o3" type="radio" name="mobile1"
                                                               value="yes" <?php echo (in_array("mobile1", $webviewdetails)) ? 'checked' : ''; ?>>
                                                        <label for="o3">Yes</label>
                                                    </div>
                                                </div>

                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input id="o4" type="radio" name="mobile1"
                                                               value="no" <?php echo (in_array("mobile1", $webviewdetails)) ? '' : 'checked'; ?>>
                                                        <label for="o4">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="col-xs-8">Work Phone</div>
                                            <div class="col-xs-4">
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input id="o5" type="radio" name="work_phone1"
                                                               value="yes" <?php echo (in_array("work_phone1", $webviewdetails)) ? 'checked' : ''; ?>>
                                                        <label for="o5">Yes</label>
                                                    </div>
                                                </div>

                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input id="o6" type="radio" name="work_phone1"
                                                               value="no" <?php echo (in_array("work_phone1", $webviewdetails)) ? '' : 'checked'; ?>>
                                                        <label for="o6">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="col-xs-8">Email adddress 1</div>
                                            <div class="col-xs-4">
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input id="o7" type="radio" name="webview_email1"
                                                               value="yes" <?php echo (in_array("webview_email1", $webviewdetails)) ? 'checked' : ''; ?>>
                                                        <label for="o7">Yes</label>
                                                    </div>
                                                </div>

                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input id="o8" type="radio" name="webview_email1"
                                                               value="no" <?php echo (in_array("webview_email1", $webviewdetails)) ? '' : 'checked'; ?>>
                                                        <label for="o8">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-xs-8">Leader may access my emergency contact details</div>
                                            <div class="col-xs-4">
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input id="o9" type="radio"
                                                               name="show_emergency_contact_to_leader1"
                                                               value="yes" <?php echo (in_array("show_emergency_contact_to_leader1", $webviewdetails)) ? 'checked' : ''; ?>>
                                                        <label for="o9">Yes</label>
                                                    </div>
                                                </div>

                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input id="o10" type="radio"
                                                               name="show_emergency_contact_to_leader1"
                                                               value="no" <?php echo (in_array("show_emergency_contact_to_leader1", $webviewdetails)) ? '' : 'checked'; ?>>
                                                        <label for="o10">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="clr"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Emailed alerts
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="radio-main">
                                <ul>
                                    <li>
                                        <div class="row">

                                            <div class="col-xs-8 pl30 radio_set">Weekly summaries of upcoming
                                                activities
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input
                                                            type="radio" <?php if ($user[0]['upcomming_event'] == 'yes') {
                                                            echo 'checked="checked"';
                                                        } ?> value="yes" id="ed1" name="upcomming_event">
                                                        <label for="ed1">Yes</label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input type="radio" value="no" id="ed2"
                                                               name="upcomming_event" <?php if ($user[0]['upcomming_event'] == 'no') {
                                                            echo 'checked="checked"';
                                                        } ?>>
                                                        <label for="ed2">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-xs-8 pl30 radio_set">Short notice activities</div>
                                            <div class="col-xs-4">
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input
                                                            type="radio" <?php if ($user[0]['short_notice_walks'] == 'yes') {
                                                            echo 'checked="checked"';
                                                        } ?> value="yes" name="short_notice_walks" id="ed3">
                                                        <label for="ed3">Yes</label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input type="radio" value="no" name="short_notice_walks"
                                                               id="ed4" <?php if ($user[0]['short_notice_walks'] == 'no') {
                                                            echo 'checked="checked"';
                                                        } ?>>
                                                        <label for="ed4">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-xs-8 pl30 radio_set">Wednesday walks</div>
                                            <div class="col-xs-4">
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input
                                                            type="radio" <?php if ($user[0]['wednesday_walks'] == 'yes') {
                                                            echo 'checked="checked"';
                                                        } ?> value="yes" name="wednesday_walks" id="ed5">
                                                        <label for="ed5">Yes</label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input type="radio" value="no" id="ed6"
                                                               name="wednesday_walks" <?php if ($user[0]['wednesday_walks'] == 'no') {
                                                            echo 'checked="checked"';
                                                        } ?>>
                                                        <label for="ed6">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-xs-8 pl30 radio_set">Family activities</div>
                                            <div class="col-xs-4">
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input
                                                            type="radio" <?php if ($user[0]['family_activity'] == 'yes') {
                                                            echo 'checked="checked"';
                                                        } ?> value="yes" id="ed7" name="family_activity">
                                                        <label for="ed7">Yes</label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-md-6">
                                                    <div class="radio radio-n">
                                                        <input type="radio" value="no" id="ed8"
                                                               name="family_activity" <?php if ($user[0]['family_activity'] == 'no') {
                                                            echo 'checked="checked"';
                                                        } ?>>
                                                        <label for="ed8">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        About Me
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Year Of Birth</label>
                                <input type="text" id="dob" value="<?php echo $user[0]['dob']; ?>" name="dob"
                                       class="form-control">
                            </div>
                            <div class="col-md-6">
                                <label>First Aid Qualifications</label>
                                <input type="text" class="form-control" name="first_aid_qualification"
                                       value="<?php echo $user[0]['first_aid_qualification']; ?>">
                            </div>
                            <div class="col-md-6">
                                <label class="">Gender</label>
                                <select class="form-control" name="is_male">
                                    <option value="">Select Gender</option>
                                    <option value="1" <?php if ($user[0]['is_male'] == '1') {
                                        echo 'selected="selected"';
                                    } ?> >Male
                                    </option>
                                    <option value="2" <?php if ($user[0]['is_male'] == '2') {
                                        echo 'selected="selected"';
                                    } ?> >Female
                                    </option>
                                    <option value="3" <?php if ($user[0]['is_male'] == '3') {
                                        echo 'selected="selected"';
                                    } ?> >Unspecified
                                    </option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Expiry Date</label>
                                <input type="text" class="form-control" id="datepicker" name="expiry_date"
                                       value="<?php echo $user[0]['first_aid_qualification_expiry_date']; ?>">
                            </div>
                            <div class="col-md-12 vehicle_txt mb10">
                                <div>Vehicle</div>
                            </div>
                            <div class="col-md-6">
                                <label>Registration Number</label>
                                <input type="text" value="<?php echo $user[0]['vehical_number']; ?>"
                                       name="vehical_number" class="form-control">
                            </div>
                            <div class="col-md-6">
                                <label>Passengers</label>
                                <select name="passenger" class="form-control">
                                    <option value="">Select Passengers</option>
                                    <option value="+1" <?php if ($user[0]['passengers'] == '+1') {
                                        echo 'selected="selected"';
                                    } ?>>Driver +1
                                    </option>
                                    <option value="+2" <?php if ($user[0]['passengers'] == '+2') {
                                        echo 'selected="selected"';
                                    } ?>>Driver +2
                                    </option>
                                    <option value="+3" <?php if ($user[0]['passengers'] == '+3') {
                                        echo 'selected="selected"';
                                    } ?>>Driver +3
                                    </option>
                                    <option value="+4" <?php if ($user[0]['passengers'] == '+4') {
                                        echo 'selected="selected"';
                                    } ?>>Driver +4
                                    </option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">
                                <label>Driving License</label>
                                <select name="driving_license" class="form-control">
                                    <option value="">Select License</option>
                                    <option value="2WD" <?php if ($user[0]['driving_license'] == '2WD') {
                                        echo 'selected="selected"';
                                    } ?>>2WD
                                    </option>
                                    <option value="AWD/4WD" <?php if ($user[0]['driving_license'] == 'AWD/4WD') {
                                        echo 'selected="selected"';
                                    } ?>>AWD/4WD
                                    </option>
                                </select>
                            </div>
                            <div class="col-md-12"><label>The person with this User Name may make booking requests on my
                                    behalf</label></div>
                            <div class="col-md-6">
                                <label>Person 1</label>
                                <input type="text" id="booking_responsible_person"
                                       value="<?php echo $user[0]['booking_responsible_person']; ?>"
                                       name="booking_responsible_person" class="form-control">
                            </div>
                            <div class="col-md-6">
                                <label>Person 2</label>
                                <input type="text" id="booking_responsible_person1"
                                       value="<?php echo $user[0]['booking_responsible_person1']; ?>"
                                       name="booking_responsible_person1" class="form-control">
                            </div>
                            <div class="col-md-6">
                                <label>Upload Your Image</label>
            <span class="btn btn-default btn-file upload_img col-md-2">
            	<input type="file" id="imgMember" name="avtar" value="<?php echo $user[0]['avtar']; ?>">
            </span>
                                <?php if ($user[0]['avtar'] != '') { ?>
                                    <span id="imagediv">
                <img id="upload_image" class="upload_side_img2"
                     src="<?php echo UPLOAD_URL . 'users/' . $user[0]['avtar']; ?>">
                <input type="button" value="Remove" id="remove_upload"
                       class="remove_upload btn btn-success btn-md submit_btn">
            </span>
                                <?php } else { ?>
                                    <span id="imagediv"></span>
                                <?php } ?>
                                <div class="clr"></div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">How did you hear about the Club?</div>
                    <div class="panel-body">
                        <div class="row">

                            <?php $know_about_club = explode(',', $user[0]['know_about_club']); ?>

                            <div class="padding_spc">

                                <div class="col-md-2 col-xs-6">
                                    <div class="checkbox">
                                        <label><input
                                                type="checkbox" <?php echo (in_array("website", $know_about_club)) ? 'checked' : ''; ?>
                                                value="website" name="know_about_club[]">Website</label>
                                    </div>
                                </div>
                                <div class="col-md-2 col-xs-6">
                                    <div class="checkbox">
                                        <label><input
                                                type="checkbox" <?php echo (in_array("friend", $know_about_club)) ? 'checked' : ''; ?>
                                                value="friend" name="know_about_club[]">Friends</label>
                                    </div>
                                </div>
                                <div class="col-md-2 col-xs-6">
                                    <div class="checkbox">
                                        <label><input
                                                type="checkbox" <?php echo (in_array("brochure", $know_about_club)) ? 'checked' : ''; ?>
                                                value="brochure" name="know_about_club[]">Brochure</label>
                                    </div>
                                </div>
                                <div class="col-md-2 col-xs-6">
                                    <div class="checkbox">
                                        <label><input
                                                type="checkbox" <?php echo (in_array("display", $know_about_club)) ? 'checked' : ''; ?>
                                                value="display" name="know_about_club[]">Display</label>
                                    </div>
                                </div>
                                <div class="col-md-2 col-xs-6 ">
                                    <div class="checkbox">
                                        <label><input
                                                type="checkbox" <?php echo (in_array("other", $know_about_club)) ? 'checked' : ''; ?>
                                                value="other" name="know_about_club[]">Other</label>
                                    </div>
                                </div>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Payment
                    </div>
                    <div class="panel-body">

                        <div class="col-md-12">

                            <label>Pay By *</label>

                            <div class="radio col-md-3 col-xs-12">
                                <label>
                                    <input type="radio"
                                           value="paypal_creditcard" <?php if ($user[0]['payment_type'] == 'paypal_creditcard') {
                                        echo 'checked="checked"';
                                    } ?> name="payment_type" class="radio-inline validate[required]" disabled>Paypal Or
                                    Credit Card
                                </label>
                            </div>
                            <div class="radio col-md-9 col-xs-12">
                                <label>
                                    <input type="radio" value="offline_payment" name="payment_type"
                                           class="radio-inline" <?php if ($user[0]['payment_type'] == 'offline_payment') {
                                        echo 'checked="checked"';
                                    } ?> disabled>Offline: Cheque OR Bank Transfer
                                </label>
                            </div>
                            <div class="clr"></div>

                            <div class="mt20">
                                <label>Annual Membership Including *</label>

                                <div class="radio col-md-3">
                                    <label>
                                        <input type="radio" value="electronic_it" name="subscription_pack"
                                               class="radio-inline validate[required]" <?php if ($user[0]['subscription_pack'] == 'electronic_it') {
                                            echo 'checked="checked"';
                                        } ?> disabled>Electronic IT $29
                                    </label>
                                </div>
                                <div class="radio col-md-3">
                                    <label>
                                        <input type="radio" value="paper_it" name="subscription_pack"
                                               class="radio-inline " <?php if ($user[0]['subscription_pack'] == 'paper_it') {
                                            echo 'checked="checked"';
                                        } ?> disabled>Paper IT $68
                                    </label>
                                </div>
                                <div class="clr"></div>
                            </div>

                            <div class="col-md-12 mb50">
                                <hr>
                                <div class="col-md-6 col-xs-4">
                                    <div class="col-md-2 putright">
                                        <input type="submit" name="submit" value="Submit"
                                               class="btn btn-success btn-md submit">
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-4">
                                    <div class="col-md-2 putleft">
                                        <input type="button" name="Cancel" value="Cancel" class="btn btn-default btn-md"
                                               onclick="javascript: window.location.href='<?php echo base_url() . 'users/userrequest'; ?>';">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!--End Form Elements -->
            </form>
        </div>
    </div>
</div>
</div>
</div>
<!-- end page-wrapper -->
