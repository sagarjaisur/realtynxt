<!--  page-wrapper -->
<div id="page-wrapper">

    <div class="row">
        <!-- Page Header -->
        <div class="col-lg-12">
            <h1 class="page-header">Change Password.</h1>
        </div>
        <!--End Page Header -->
    </div>
    <?php echo $this->session->flashdata('success'); ?>
    <?php echo validation_errors(); ?>
    <div class="row">
        <div class="col-lg-12">
            <!-- Form Elements -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    Enter Old Password And New Password.
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <?php echo form_open('login/changepassword') ?>
                            <div class="form-group">
                                <label>Old Password</label>
                                <input class="form-control" name="oldpassword" type="password" value="" required>

                            </div>
                            <div class="form-group">
                                <label>New Password</label>
                                <input class="form-control" name="newpassword" type="password" required>
                            </div>
                            <div class="form-group">
                                <label>Re Type Password</label>
                                <input class="form-control" name="retypepassword" type="password" required>
                            </div>
                            <div class="form-group">
                                <label>Enter Email</label>
                                <input class="form-control" name="email" type="text" required>

                                <p class="help-block">Please enter correct email to change password.</p>
                            </div>

                            <input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
                            <input type="reset" name="reset" value="Reset" class="btn btn-success"/>

                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End Form Elements -->
        </div>
    </div>


</div>
<!-- end page-wrapper -->