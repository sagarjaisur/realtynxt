<?php
$page_caption = 'Setting';
$cancel_url = base_url() . 'setting/index';
$add_url = base_url() . 'setting/add/';
?>
<link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/dataTables/dataTables.bootstrap.css">
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">

        <div class="row">
            <!--  page header -->
            <div class="col-lg-12">
                <h1 class="page-header page_title">
                    <section class="content-header">
                        <h1 class="page-header page_title">&nbsp;<?php echo $page_caption; ?>s<input type="button"
                                                                                                     onClick="location.href='<?php echo $add_url; ?>'"
                                                                                                     name="submit"
                                                                                                     value="Add <?php echo $page_caption; ?>"
                                                                                                     class="btn btn-success btn-md submit"
                                                                                                     style="margin-left: 50px">
                        </h1>

                        <ol class="breadcrumb">
                            <li><a href="<?php echo MAIN_DIR_URL . 'admin/index.php/dashboard/'; ?>"><i
                                        class="fa fa-dashboard"></i> Home</a></li>
                            <li class="active"><a href="#"><?php echo $page_caption; ?>s</a></li>
                        </ol>
                    </section>
                </h1>
            </div>
            <!-- end  page header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>

        <div class="row">
            <div class="col-lg-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php echo $page_caption; ?>s List
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading  ">
                                <i class="fa fa-bar-chart-o fa-fw"></i>Home Page Banner Setting
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Setting Name</th>
                                                    <th>Setting Value</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                foreach ($stars as $star) {
                                                    //Check Show link
                                                    $titleCheck = current(explode('_', $star['settingName']));
                                                    if ($titleCheck != 'BANNER') {
                                                        continue;
                                                    }
                                                    //Links
                                                    $editLink = base_url() . 'setting/edit/' . $star['settingId'];
                                                    $deleteLink = base_url() . 'setting/delete/' . $star['settingId'];
                                                    //$statusLink  =  base_url().'setting/status/'.$star['bannerId'].'/'.$status;
                                                    ?>
                                                    <tr class="odd gradeX">
                                                        <td>
                                                            <a href="<?php echo $editLink; ?>"><?php echo $star['settingName']; ?></a>
                                                        </td>
                                                        <td><?php echo $star['settingValue']; ?></td>
                                                        <td class="center">
                                                            <button class="btn btn-default" type="button"
                                                                    onClick="javascipt:window.location.href='<?php echo $editLink; ?>';"
                                                                    title="EDIT">
                                                                <i class="fa fa-edit fa-fw"></i>Edit
                                                            </button>
                                                            <button class="btn btn-danger" type="button"
                                                                    onClick="return deleteRecode('<?php echo $deleteLink; ?>','<?php echo $page_caption; ?>');"
                                                                    title="DELETE">
                                                                <i class="fa fa-edit fa-fw"></i>Delete
                                                            </button>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading  ">
                                <i class="fa fa-bar-chart-o fa-fw"></i>Meeting Setting
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Setting Name</th>
                                                    <th>Setting Value</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                foreach ($stars as $star) {
                                                    //Check Show link
                                                    $titleCheck = current(explode('_', $star['settingName']));
                                                    if ($titleCheck != 'MEETING') {
                                                        continue;
                                                    }
                                                    //Links
                                                    $editLink = base_url() . 'setting/edit/' . $star['settingId'];
                                                    $deleteLink = base_url() . 'setting/delete/' . $star['settingId'];
                                                    //$statusLink  =  base_url().'setting/status/'.$star['bannerId'].'/'.$status;
                                                    ?>
                                                    <tr class="odd gradeX">
                                                        <td>
                                                            <a href="<?php echo $editLink; ?>"><?php echo $star['settingName']; ?></a>
                                                        </td>
                                                        <td><?php echo $star['settingValue']; ?></td>
                                                        <td class="center">
                                                            <button class="btn btn-default" type="button"
                                                                    onClick="javascipt:window.location.href='<?php echo $editLink; ?>';"
                                                                    title="EDIT">
                                                                <i class="fa fa-edit fa-fw"></i>Edit
                                                            </button>
                                                            <button class="btn btn-danger" type="button"
                                                                    onClick="return deleteRecode('<?php echo $deleteLink; ?>','<?php echo $page_caption; ?>');"
                                                                    title="DELETE">
                                                                <i class="fa fa-edit fa-fw"></i>Delete
                                                            </button>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading  ">
                                <i class="fa fa-bar-chart-o fa-fw"></i>Site Setting
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Setting Name</th>
                                                    <th>Setting Value</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                foreach ($stars as $star) {
                                                    //Check Show link
                                                    $titleCheck = current(explode('_', $star['settingName']));
                                                    if ($titleCheck != 'SITE') {
                                                        continue;
                                                    }
                                                    //Links
                                                    $editLink = base_url() . 'setting/edit/' . $star['settingId'];
                                                    $deleteLink = base_url() . 'setting/delete/' . $star['settingId'];
                                                    //$statusLink  =  base_url().'setting/status/'.$star['bannerId'].'/'.$status;
                                                    ?>
                                                    <tr class="odd gradeX">
                                                        <td>
                                                            <a href="<?php echo $editLink; ?>"><?php echo $star['settingName']; ?></a>
                                                        </td>
                                                        <td><?php echo $star['settingValue']; ?></td>
                                                        <td class="center">
                                                            <button class="btn btn-default" type="button"
                                                                    onClick="javascipt:window.location.href='<?php echo $editLink; ?>';"
                                                                    title="EDIT">
                                                                <i class="fa fa-edit fa-fw"></i>Edit
                                                            </button>
                                                            <button class="btn btn-danger" type="button"
                                                                    onClick="return deleteRecode('<?php echo $deleteLink; ?>','<?php echo $page_caption; ?>');"
                                                                    title="DELETE">
                                                                <i class="fa fa-edit fa-fw"></i>Delete
                                                            </button>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading  ">
                                <i class="fa fa-bar-chart-o fa-fw"></i>General Setting
                            </div>

                            <div class="panel-body">
                                <div class="row">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover"
                                               id="dataTables-example">
                                            <thead>
                                            <tr>
                                                <th>Setting Name</th>
                                                <th>Setting Value</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($stars as $star) {
                                                //Check Show link
                                                $titleCheck = current(explode('_', $star['settingName']));
                                                if ($titleCheck == 'MEETING' || $titleCheck == 'BANNER' || $titleCheck == 'SITE' || $titleCheck == 'EMAIL') {
                                                    continue;
                                                }

                                                //Links
                                                $editLink = base_url() . 'setting/edit/' . $star['settingId'];
                                                $deleteLink = base_url() . 'setting/delete/' . $star['settingId'];
                                                //$statusLink  =  base_url().'setting/status/'.$star['bannerId'].'/'.$status;
                                                ?>
                                                <tr class="odd gradeX">
                                                    <td>
                                                        <a href="<?php echo $editLink; ?>"><?php echo $star['settingName']; ?></a>
                                                    </td>
                                                    <td><?php echo $star['settingValue']; ?></td>
                                                    <td class="center">
                                                        <button class="btn btn-default" type="button"
                                                                onClick="javascipt:window.location.href='<?php echo $editLink; ?>';"
                                                                title="EDIT">
                                                            <i class="fa fa-edit fa-fw"></i>Edit
                                                        </button>
                                                        <button class="btn btn-danger" type="button"
                                                                onClick="return deleteRecode('<?php echo $deleteLink; ?>','<?php echo $page_caption; ?>');"
                                                                title="DELETE">
                                                            <i class="fa fa-edit fa-fw"></i>Delete
                                                        </button>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading  ">
                                <i class="fa fa-bar-chart-o fa-fw"></i>Email Setting
                            </div>

                            <div class="panel-body">
                                <div class="row">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover"
                                               id="dataTables-example">
                                            <thead>
                                            <tr>
                                                <th>Setting Name</th>
                                                <th>Setting Value</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($stars as $star) {
                                                //Check Show link
                                                $titleCheck = current(explode('_', $star['settingName']));
                                                if ($titleCheck != 'EMAIL') {
                                                    continue;
                                                }
                                                //Links
                                                $editLink = base_url() . 'setting/edit/' . $star['settingId'];
                                                $deleteLink = base_url() . 'setting/delete/' . $star['settingId'];
                                                ?>
                                                <tr class="odd gradeX">
                                                    <td>
                                                        <a href="<?php echo $editLink; ?>"><?php echo $star['settingName']; ?></a>
                                                    </td>
                                                    <td><?php echo $star['settingValue']; ?></td>
                                                    <td class="center">
                                                        <button class="btn btn-default" type="button"
                                                                onClick="javascipt:window.location.href='<?php echo $editLink; ?>';"
                                                                title="EDIT">
                                                            <i class="fa fa-edit fa-fw"></i>Edit
                                                        </button>
                                                        <button class="btn btn-danger" type="button"
                                                                onClick="return deleteRecode('<?php echo $deleteLink; ?>','<?php echo $page_caption; ?>');"
                                                                title="DELETE">
                                                            <i class="fa fa-edit fa-fw"></i>Delete
                                                        </button>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->
</div>
<!--<style type="text/css">
    .col-lg-12 {
        padding: 10px 0px 0px 0px !important;
    }
</style>-->
