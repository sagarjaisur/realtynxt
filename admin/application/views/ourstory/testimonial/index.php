<?php
$page_caption = 'Testimonial';
$cancel_url = base_url() . 'ostestimonial/index';
$add_url = base_url() . 'ostestimonial/add/';
?>
<link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/dataTables/dataTables.bootstrap.css">
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">

        <div class="row">
            <!--  page header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title">&nbsp;<?php echo $page_caption; ?> Management<input type="button"
                                                                                                           onClick="location.href='<?php echo $add_url; ?>'"
                                                                                                           name="submit"
                                                                                                           value="Add <?php echo $page_caption; ?>"
                                                                                                           class="btn btn-success btn-md submit"
                                                                                                           style="margin-left: 50px">
                    </h1>

                    <ol class="breadcrumb">
                        <li><a href="<?php echo MAIN_DIR_URL . 'admin/index.php/dashboard/'; ?>"><i
                                    class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active"><a href="#"><?php echo $page_caption; ?> Management</a></li>
                    </ol>
                </section>
            </div>
            <!-- end  page header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>

        <div class="row">
            <div class="col-lg-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>Testimonial Title</th>
                                    <th width="20%">Short Description</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (isset($allostestimonial) && $allostestimonial != "") {
                                    foreach ($allostestimonial as $ostestimonial) {

                                        $editLink = base_url() . 'ostestimonial/edit/' . $ostestimonial['testimonialId'];
                                        $deleteLink = base_url() . 'ostestimonial/delete/' . $ostestimonial['testimonialId'];

                                        $status = $ostestimonial['testimonialStatus'];
                                        $statusLink = base_url() . 'ostestimonial/status/' . $ostestimonial['testimonialId'] . '/' . $status;

                                        ?>
                                        <tr class="odd gradeX">
                                            <td>
                                                <?php echo $ostestimonial['testimonialTitle']; ?>
                                            </td>
                                            <td>
                                                <?php echo $ostestimonial['testimonialShortDesc']; ?>
                                            </td>
                                            <td class="center">
                                                <button
                                                    class="<?php echo $status ? 'btn btn-success' : 'btn btn-danger'; ?>"
                                                    type="button"
                                                    onClick="javascipt:window.location.href='<?php echo $statusLink; ?>';"
                                                    title="Click here to Toggle (Active / Inactive)">
                                                    <?php echo $status ? 'Active' : 'Inactive'; ?>
                                                </button>

                                                <button class="btn btn-default" type="button"
                                                        onClick="javascipt:window.location.href='<?php echo $editLink; ?>';"
                                                        title="EDIT">
                                                    <i class="fa fa-edit fa-fw"></i>Edit
                                                </button>

                                                <button class="btn btn-danger" type="button"
                                                        onClick="return deleteRecode('<?php echo $deleteLink; ?>','<?php echo $page_caption; ?>');"
                                                        title="DELETE">
                                                    <i class="fa fa-edit fa-fw"></i>Delete
                                                </button>
                                            </td>
                                        </tr>
                                    <?php }
                                } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.panel-body -->

                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->
</div>