<?php
$page_caption = 'Media';
$cancel_url = base_url() . 'osmedia/index';
$submitForm_url = 'osmedia/edit/' . $id;
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title">Edit <?php echo $page_caption; ?></h1>
                </section>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <?php if (isset($error)) echo '<div class="alert alert-danger">' . $error . '</div>'; ?>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8">
                                <?php echo form_open_multipart($submitForm_url) ?>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label>Media Title *</label>
                                        <input class="form-control validate[required] mediaTitle"
                                               name="mediaTitle"
                                               type="text"
                                               value="<?php echo set_value("mediaTitle") ? set_value("mediaTitle") : $osmedia[0]['mediaTitle']; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <label>Media Link *</label>
                                        <input class="form-control validate[required] mediaLink"
                                               name="mediaLink"
                                               type="text"
                                               value="<?php echo set_value("mediaLink") ? set_value("mediaLink") : $osmedia[0]['mediaLink']; ?>"
                                               required>
                                        <!--                                        <textarea class="form-control" name="pageShortDesc" required>-->
                                        <?php //echo set_value('pageShortDesc'); ?><!--</textarea>-->
                                        <!--                                        --><?php //echo $this->ckeditor->editor('pageShortDesc', (set_value('pageShortDesc')) ? set_value('pageShortDesc') : $osmedia[0]['pageShortDesc']); ?>
                                    </div>
                                    <div class="clearfix"></div>

                                </div>

                                <input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
                                <input type="button" name="Cancel" value="Cancel" class="btn btn-success"
                                       onclick="javascript: window.location.href='<?php echo $cancel_url; ?>';"/>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->
</div>