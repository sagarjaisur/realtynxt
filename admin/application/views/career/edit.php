<?php
$page_caption = 'Career Management';
$cancel_url = base_url() . 'career/index';
$submitForm_url = 'career/edit/' . $id;
$cmspage =  $cmspage[0];
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title">Edit <?php echo $page_caption; ?></h1>
                </section>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <?php if (isset($error)) echo '<div class="alert alert-danger">' . $error . '</div>'; ?>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <?php echo form_open_multipart($submitForm_url) ?>
                                 <div class="form-group">
                                    <div class="col-md-4">
                                        <label>Career Title *</label>
                                        <input class="form-control validate[required] careerTitle"
                                               name="careerTitle"
                                               type="text"
                                               value="<?php echo set_value("careerTitle") ? set_value("careerTitle") : $cmspage['careerTitle']; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-12">
                                        <br/>
                                        <label>Career Experience *</label>
                                        <?php echo $this->ckeditor->editor('careerExperience', $cmspage['careerExperience']); ?>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-12">
                                        <br/>
                                        <label>Career Locations *</label>
                                        <?php echo $this->ckeditor->editor('careerLocations',  $cmspage['careerLocations']); ?>
                                    </div>
                                     <div class="col-md-12">
                                        <br/>
                                        <label>Career Responsibilities *</label>
                                        <?php echo $this->ckeditor->editor('careerResponsibilities', $cmspage['careerResponsibilities']); ?>
                                    </div>
                                    <div class="col-md-12">
                                        <br/>
                                        <label>Career Skills *</label>
                                        <?php echo $this->ckeditor->editor('careerSkils', $cmspage['careerSkils']); ?>
                                    </div>
                                    <div class="clearfix"></div>

                                </div>


                                <input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
                                <input type="button" name="Cancel" value="Cancel" class="btn btn-success"
                                       onclick="javascript: window.location.href='<?php echo $cancel_url; ?>';"/>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->
</div>