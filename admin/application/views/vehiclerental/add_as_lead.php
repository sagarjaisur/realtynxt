<?php
$page_caption = 'Add As Lead';
$cancel_url = base_url() . 'property/index';
$submitForm_url = 'property/add/';
$addAsLeadLink = base_url() . 'property/addAsLead/' . $propertyId . '/' . $propertyOwnerId;
?>
<script>
    $(document).ready(function () {
        $("#addAsLead").trigger('click');
    });
</script>
<div class="daterangepicker dropdown-menu show-calendar opensleft"
     style="top: 565px; right: 25.5px; left: auto; display: none;">
    <div class="calendar first left">
        <div class="calendar-date">
            <table class="table-condensed">
                <thead>
                <tr>
                    <th class="prev available"><i
                            class="fa fa-arrow-left icon icon-arrow-left glyphicon glyphicon-arrow-left"></i></th>
                    <th colspan="5" class="month">Dec 2015</th>
                    <th class="next available"><i
                            class="fa fa-arrow-right icon icon-arrow-right glyphicon glyphicon-arrow-right"></i></th>
                </tr>
                <tr>
                    <th>Su</th>
                    <th>Mo</th>
                    <th>Tu</th>
                    <th>We</th>
                    <th>Th</th>
                    <th>Fr</th>
                    <th>Sa</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="available off" data-title="r0c0">29</td>
                    <td class="available off" data-title="r0c1">30</td>
                    <td class="available" data-title="r0c2">1</td>
                    <td class="available" data-title="r0c3">2</td>
                    <td class="available" data-title="r0c4">3</td>
                    <td class="available" data-title="r0c5">4</td>
                    <td class="available" data-title="r0c6">5</td>
                </tr>
                <tr>
                    <td class="available" data-title="r1c0">6</td>
                    <td class="available" data-title="r1c1">7</td>
                    <td class="available" data-title="r1c2">8</td>
                    <td class="available" data-title="r1c3">9</td>
                    <td class="available" data-title="r1c4">10</td>
                    <td class="available" data-title="r1c5">11</td>
                    <td class="available" data-title="r1c6">12</td>
                </tr>
                <tr>
                    <td class="available" data-title="r2c0">13</td>
                    <td class="available" data-title="r2c1">14</td>
                    <td class="available active start-date end-date" data-title="r2c2">15</td>
                    <td class="available" data-title="r2c3">16</td>
                    <td class="available" data-title="r2c4">17</td>
                    <td class="available" data-title="r2c5">18</td>
                    <td class="available" data-title="r2c6">19</td>
                </tr>
                <tr>
                    <td class="available" data-title="r3c0">20</td>
                    <td class="available" data-title="r3c1">21</td>
                    <td class="available" data-title="r3c2">22</td>
                    <td class="available" data-title="r3c3">23</td>
                    <td class="available" data-title="r3c4">24</td>
                    <td class="available" data-title="r3c5">25</td>
                    <td class="available" data-title="r3c6">26</td>
                </tr>
                <tr>
                    <td class="available" data-title="r4c0">27</td>
                    <td class="available" data-title="r4c1">28</td>
                    <td class="available" data-title="r4c2">29</td>
                    <td class="available" data-title="r4c3">30</td>
                    <td class="available" data-title="r4c4">31</td>
                    <td class="available off" data-title="r4c5">1</td>
                    <td class="available off" data-title="r4c6">2</td>
                </tr>
                <tr>
                    <td class="available off" data-title="r5c0">3</td>
                    <td class="available off" data-title="r5c1">4</td>
                    <td class="available off" data-title="r5c2">5</td>
                    <td class="available off" data-title="r5c3">6</td>
                    <td class="available off" data-title="r5c4">7</td>
                    <td class="available off" data-title="r5c5">8</td>
                    <td class="available off" data-title="r5c6">9</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="calendar-time"><select class="hourselect">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12" selected="selected">12</option>
            </select> : <select class="minuteselect">
                <option value="0" selected="selected">00</option>
                <option value="30">30</option>
            </select> <select class="ampmselect">
                <option value="AM" selected="selected">AM</option>
                <option value="PM">PM</option>
            </select></div>
    </div>
    <div class="calendar second right">
        <div class="calendar-date">
            <table class="table-condensed">
                <thead>
                <tr>
                    <th></th>
                    <th colspan="5" class="month">Dec 2015</th>
                    <th class="next available"><i
                            class="fa fa-arrow-right icon icon-arrow-right glyphicon glyphicon-arrow-right"></i></th>
                </tr>
                <tr>
                    <th>Su</th>
                    <th>Mo</th>
                    <th>Tu</th>
                    <th>We</th>
                    <th>Th</th>
                    <th>Fr</th>
                    <th>Sa</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="off disabled" data-title="r0c0">29</td>
                    <td class="off disabled" data-title="r0c1">30</td>
                    <td class="off disabled" data-title="r0c2">1</td>
                    <td class="off disabled" data-title="r0c3">2</td>
                    <td class="off disabled" data-title="r0c4">3</td>
                    <td class="off disabled" data-title="r0c5">4</td>
                    <td class="off disabled" data-title="r0c6">5</td>
                </tr>
                <tr>
                    <td class="off disabled" data-title="r1c0">6</td>
                    <td class="off disabled" data-title="r1c1">7</td>
                    <td class="off disabled" data-title="r1c2">8</td>
                    <td class="off disabled" data-title="r1c3">9</td>
                    <td class="off disabled" data-title="r1c4">10</td>
                    <td class="off disabled" data-title="r1c5">11</td>
                    <td class="off disabled" data-title="r1c6">12</td>
                </tr>
                <tr>
                    <td class="off disabled" data-title="r2c0">13</td>
                    <td class="off disabled" data-title="r2c1">14</td>
                    <td class="available active start-date end-date" data-title="r2c2">15</td>
                    <td class="available" data-title="r2c3">16</td>
                    <td class="available" data-title="r2c4">17</td>
                    <td class="available" data-title="r2c5">18</td>
                    <td class="available" data-title="r2c6">19</td>
                </tr>
                <tr>
                    <td class="available" data-title="r3c0">20</td>
                    <td class="available" data-title="r3c1">21</td>
                    <td class="available" data-title="r3c2">22</td>
                    <td class="available" data-title="r3c3">23</td>
                    <td class="available" data-title="r3c4">24</td>
                    <td class="available" data-title="r3c5">25</td>
                    <td class="available" data-title="r3c6">26</td>
                </tr>
                <tr>
                    <td class="available" data-title="r4c0">27</td>
                    <td class="available" data-title="r4c1">28</td>
                    <td class="available" data-title="r4c2">29</td>
                    <td class="available" data-title="r4c3">30</td>
                    <td class="available" data-title="r4c4">31</td>
                    <td class="available off" data-title="r4c5">1</td>
                    <td class="available off" data-title="r4c6">2</td>
                </tr>
                <tr>
                    <td class="available off" data-title="r5c0">3</td>
                    <td class="available off" data-title="r5c1">4</td>
                    <td class="available off" data-title="r5c2">5</td>
                    <td class="available off" data-title="r5c3">6</td>
                    <td class="available off" data-title="r5c4">7</td>
                    <td class="available off" data-title="r5c5">8</td>
                    <td class="available off" data-title="r5c6">9</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="calendar-time"><select class="hourselect">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11" selected="selected">11</option>
                <option value="12">12</option>
            </select> : <select class="minuteselect">
                <option value="0">00</option>
                <option value="30">30</option>
            </select> <select class="ampmselect">
                <option value="AM">AM</option>
                <option value="PM" selected="selected">PM</option>
            </select></div>
    </div>
    <div class="ranges">
        <div class="range_inputs">
            <div class="daterangepicker_start_input"><label for="daterangepicker_start">From</label><input
                    class="input-mini" type="text" name="daterangepicker_start" value=""></div>
            <div class="daterangepicker_end_input"><label for="daterangepicker_end">To</label><input class="input-mini"
                                                                                                     type="text"
                                                                                                     name="daterangepicker_end"
                                                                                                     value=""></div>
            <button class="applyBtn btn btn-small btn-sm btn-success">Apply</button>
            &nbsp;
            <button class="cancelBtn btn btn-small btn-sm btn-default">Cancel</button>
        </div>
    </div>
</div>

<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="example-modal">
            <div class="modal" id="addAsLead">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">�</span></button>
                            <h4 class="modal-title"><?php echo $page_caption; ?></h4>
                        </div>
                        <div class="modal-body">
                            <?php echo form_open_multipart($submitForm_url) ?>
                            <div class="form-group">
                                <!-- Form Elements -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Lead Details
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">

                                            <div class="col-lg-6">
                                                <label>Property Name : </label>
                                                <?php echo $propertyName; ?>
                                            </div>

                                            <div class="col-md-6">
                                                <label>Lead Name *</label>
                                                <?php
                                                //                                                $selected = (isset($this->input->post))
                                                echo form_dropdown('customerId', $brokersList, reset($brokersList), ' class="form-control"');
                                                ?>
                                            </div>
                                            <!--<div class="clearfix"></div>-->

                                            <!--<div class="clearfix"></div>-->

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Duration</label>

                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                        <input type="text" class="form-control pull-right"
                                                               id="reservationtime"
                                                               value="<?php if (set_value('duration')) {
                                                                   echo set_value('duration');
                                                               } else {
                                                                   echo "";
                                                               } ?>" name="duration">
                                                    </div>
                                                </div>

                                                <!--                                                <input type="date" id="dob" value="<?php /*if (set_value('dob')) {
                                                    echo set_value('dob');
                                                } else {
                                                    echo "";
                                                } */ ?>" name="dob" class="form-control">
-->
                                            </div>
                                            <!--<div class="clearfix"></div>-->

                                            <div class="col-md-6">
                                                <label>Appointment Time</label>

                                                <div class="container">
                                                    <div class="row">
                                                        <div class='col-sm-6'>
                                                            <input type='text' class="form-control" id='datetimepicker4'
                                                                   value="<?php if (set_value('appointmentTime')) {
                                                                       echo set_value('appointmentTime');
                                                                   } else {
                                                                       echo "";
                                                                   } ?>" name="appointmentTime"/>
                                                        </div>
                                                        <script type="text/javascript">
                                                            $(function () {
                                                                $('#datetimepicker4').datetimepicker();
                                                            });
                                                        </script>
                                                    </div>
                                                </div>

                                                <!--                                                <input type="date" id="appointmentTime" value="<?php /*if (set_value('appointmentTime')) {
                                                    echo set_value('appointmentTime');
                                                } else {
                                                    echo "";
                                                } */ ?>" name="appointmentTime" class="form-control">
-->
                                            </div>
                                            <!--<div class="clearfix"></div>-->
                                        </div>

                                        <!--<div class="col-md-12 mb50">
                                            <hr>
                                            <div class="col-md-6 col-xs-4">
                                                <div class="col-md-2 putright">
                                                    <input type="submit" name="submit" value="Submit"
                                                           class="btn btn-success btn-md submit">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xs-4">
                                                <div class="col-md-2 putleft">
                                                    <input type="button" name="Cancel" value="Cancel" class="btn btn-default btn-md"
                                                           onclick="javascript: window.location.href='<?php /*echo $cancel_url; */ ?>';">
                                                </div>
                                            </div>
                                        </div>-->
                                    </div>
                                </div>
                                <!-- End Form Elements -->
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary"
                                    onclick="javascript: window.location.href='<?php echo $addAsLeadLink; ?>';">Add As
                                Lead
                            </button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        </div>
    </div>
</div>