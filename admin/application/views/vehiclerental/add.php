<?php
$page_caption = 'Vehicle Rental';
$cancel_url = base_url() . 'vehiclerental/index';
$submitForm_url = 'vehiclerental/add/';
$ajax_url = base_url() . 'vehiclerental/';
$area_url = base_url() . 'vehiclerental/';
//$area_url = 'http://localhost/realtynxt/admin/index.php/property/';
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <h1 class="page-header page_title">Add <?php echo $page_caption; ?></h1>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <div class="row">
            <div class="col-lg-12">
                <?php echo form_open_multipart($submitForm_url) ?>
                <input type="hidden" id="ajax_url" name="ajax_url" value="<?php echo $ajax_url; ?>">
                <input type="hidden" id="area_url" name="state_url" value="<?php echo $area_url; ?>">

                <div class="form-group">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo $page_caption; ?> Details
                        </div>
                        <div class="panel-body">
                            <div class="row">

                                <div class="col-md-6">
                                    <label>Owner Name *</label>
                                    <?php
                                    $selected = ($this->input->post('memberId')) ? $this->input->post('memberId') : reset($usersList);
                                    echo form_dropdown('memberId', $usersList, $selected, ' class="form-control"');
                                    ?>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-6">
                                    <label>Vehicle Type *</label>
                                    <?php
                                    $options = array(
                                        'car' => 'Car',
                                        'bike' => 'Bike'
                                    );
                                    $selected = ($this->input->post('vehicleType')) ? $this->input->post('vehicleType') : reset($options);
                                    echo form_dropdown('vehicleType', $options, $selected, ' class="form-control"');
                                    ?>
                                </div>
                                <!--                                <div class="clearfix"></div>-->

                                <div class="col-md-6">
                                    <label>Vehicle Model *</label>
                                    <?php
                                    $options = array(
                                        'Residential' => 'Residential',
                                        'Commercial' => 'Commercial',
                                        'Agricultural' => 'Agricultural'
                                    );
                                    $selected = ($this->input->post('vehicleModel')) ? $this->input->post('vehicleModel') : reset($options);
                                    echo form_dropdown('vehicleModel', $options, $selected, ' class="form-control"');
                                    ?>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-6">
                                    <label>City *</label>
                                    <?php
                                    $selected = ($this->input->post('city')) ? $this->input->post('city') : 'Pune';
                                    echo form_dropdown('city', $CitiesList, $selected, ' class="form-control" id="property_city"');
                                    ?>
                                </div>
                                <!--                                <div class="clearfix"></div>-->

                                <div class="col-md-6">
                                    <label>Area *</label>
                                    <?php
                                    $selected = ($this->input->post('area')) ? $this->input->post('area') : reset($AreasList);
                                    echo form_dropdown('area', $AreasList, $selected, ' class="form-control" id="property_area"');
                                    ?>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-6">
                                    <label>Landmark *</label>
                                    <input class="form-control area validate[required]"
                                           name="landmark" type="text" value="<?php if (set_value('landmark')) {
                                        echo set_value('landmark');
                                    } else {
                                        echo "";
                                    } ?>" required>
                                </div>
                                <div class="clearfix"></div>


                                <div class="col-md-6">
                                    <label>Number Of vehicle you have *</label>
                                    <input class="form-control vehicleHaving validate[required]"
                                           name="vehicleHaving" type="text"
                                           value="<?php if (set_value('vehicleHaving')) {
                                               echo set_value('vehicleHaving');
                                           } else {
                                               echo "";
                                           } ?>" required>
                                </div>
                                <!--                                <div class="clearfix"></div>-->

                                <div class="col-md-6">
                                    <label>No Of Avilable *</label>
                                    <input class="form-control vehicleAvilable validate[required]"
                                           name="vehicleAvilable" type="text"
                                           value="<?php if (set_value('vehicleAvilable')) {
                                               echo set_value('vehicleAvilable');
                                           } else {
                                               echo "";
                                           } ?>" required>
                                </div>


                                <div class="col-md-6">
                                    <label>No Of Unavilable *</label>
                                    <span id="vehicleUnavilable"></span>
                                </div>
                                <div class="col-md-6">
                                    <label>Vehicle Price</label>
                                    <input class="form-control vehicleAvilable validate[required]"
                                           name="vehiclePrice" type="text" value="<?php if (set_value('vehiclePrice')) {
                                        echo set_value('vehiclePrice');
                                    } else {
                                        echo "";
                                    } ?>" required>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <label>Number Of Days *</label>
                                </div>
                                <div class="col-md-12">
                                    <input class="validate[required] vehicleDays"
                                           name="vehicleDays[]" type="checkbox"
                                           value="1">Sunday
                                    <input class="validate[required] vehicleDays"
                                           name="vehicleDays[]" type="checkbox"
                                           value="2">Monday
                                    <input class="validate[required] vehicleDays"
                                           name="vehicleDays[]" type="checkbox"
                                           value="3">Tuesday
                                    <input class="validate[required] vehicleDays"
                                           name="vehicleDays[]" type="checkbox"
                                           value="4">Wendsday
                                    <input class="validate[required] vehicleDays"
                                           name="vehicleDays[]" type="checkbox"
                                           value="5">Thursday
                                    <input class="validate[required] vehicleDays"
                                           name="vehicleDays[]" type="checkbox"
                                           value="6">Friday
                                    <input class="validate[required] vehicleDays"
                                           name="vehicleDays[]" type="checkbox"
                                           value="7">Saturday

                                </div>

                                <div class="col-md-6">
                                    <label>Vehicle Note</label>
                                    <textarea class="form-control vehicleNote validate[required]"
                                              name="vehicleNote"><?php if (set_value('vehicleNote')) {
                                            echo set_value('vehicleNote');
                                        } else {
                                            echo "";
                                        } ?></textarea>
                                </div>


                                <div class="col-md-6">
                                    <label>Vehicle Number *</label>
                                    <textarea class="form-control vehicleNumber validate[required]"
                                              name="vehicleNumber"><?php if (set_value('vehicleNumber')) {
                                            echo set_value('vehicleNumber');
                                        } else {
                                            echo "";
                                        } ?></textarea>
                                </div>
                                <div class="clearfix"></div>


                            </div>

                            <div class="col-md-12 mb50">
                                <hr>
                                <div class="col-md-6 col-xs-4">
                                    <div class="col-md-2 putright">
                                        <input type="submit" name="submit" value="Submit"
                                               class="btn btn-success btn-md submit">
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-4">
                                    <div class="col-md-2 putleft">
                                        <input type="button" name="Cancel" value="Cancel" class="btn btn-default btn-md"
                                               onclick="javascript: window.location.href='<?php echo $cancel_url; ?>';">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Form Elements -->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>