<?php
$user = $this->session->all_userdata();
//echo '<pre>';print_r($user);die();
//$linkUser = base_url() . 'edituser/userdetails/' . $user['user'];

$segment = $this->uri->segment('1');
$segment2 = $this->uri->segment('2');
//Get URI segment for Roles
$roleOf = $this->uri->segment('3');
$regionTypeId = $this->uri->segment('3');

?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo $user['image']; ?>" class="img-circle"
                     alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $user['name']; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">

            <li <?php if ($segment == 'dashboard'){ ?>class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>dashboard/">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>

            <li <?php if ($segment == 'propertyMapList'){ ?>class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>propertyMapList/">
                    <i class="fa fa-edit"></i> <span>Property Map-List</span>
                    <small class="label pull-right bg-green">new</small>
                </a>
            </li>

            <!--
            <li <?php /*if ($segment == 'users'){ */ ?>class="treeview active" <?php /*} */ ?> class="treeview">
                <a href="<?php /*echo base_url(); */ ?>users/userrequest/1">
                    <i class="fa fa-files-o"></i>
                    <span>User Management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                    <!--                    <span class="label label-primary pull-right">4</span>--><!--
                </a>
                <ul class="treeview-menu">
                    <?php
            /*                    foreach ($user['activeRoles'] as $value) {
                                    if ($roleOf == $value['roleId'] && $segment == 'users') {
                                        $classOf = 'class="active"';
                                    } else {
                                        $classOf = '';
                                    }
                                    echo '<li ' . $classOf . '><a href=' . base_url() . 'users/userrequest/' . $value['roleId'] . '><i class="fa fa-circle-o"></i> ' . ucwords(strtolower($value['roleTitle'])) . '</a></li>';
                                }
                                */ ?>
                </ul>
            </li>

            <li <?php /*if ($segment == 'adventure'){ */ ?>class="treeview active" <?php /*} */ ?> class="treeview">
                <a href="<?php /*echo base_url(); */ ?>adventure/adventurerequest/1">
                    <i class="fa fa-table"></i>
                    <span>Adventure Management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <?php
            /*                    if ($segment == 'adventure' && ($segment2 == 'add' || $segment2 == 'edit' || $segment2 == ''  || $segment2 == 'index')) {
                                    $classEvents = 'class="active"';
                                    $classTypes = '';
                                } else {
                                    $classEvents = '';
                                    $classTypes = 'class="active"';
                                }
                                echo '<li ' . $classEvents . '><a href=' . base_url() . 'adventure/><i class="fa fa-circle-o"></i>Manage Events</a></li>';
                                echo '<li ' . $classTypes . '><a href=' . base_url() . 'adventure/types/><i class="fa fa-circle-o"></i>Manage Event Types</a></li>';
                                */ ?>
                </ul>
            </li>

            <li <?php /*if ($segment == 'regions'){ */ ?>class="treeview active" <?php /*} */ ?> class="treeview">
                <a href="<?php /*echo base_url(); */ ?>regions/regionrequest/1">
                    <i class="fa fa-table"></i>
                    <span>Region Management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                    <!--                    <span class="label label-primary pull-right">4</span>--><!--
                </a>
                <ul class="treeview-menu">
                    <?php
            /*                    foreach ($user['regionTypes'] as $key => $value) if ($key != 4) {
                                    if ($regionTypeId == $key && $segment == 'regions') {
                                        $classOf = 'class="active"';
                                    } else {
                                        $classOf = '';
                                    }
                                    echo '<li ' . $classOf . '><a href=' . base_url() . 'regions/regionrequest/' . $key . '><i class="fa fa-circle-o"></i> ' . ucwords($value) . '</a></li>';
                                }
                                */ ?>
                </ul>
            </li>
-->
            <li>
                <a href="<?php echo base_url(); ?>emailsettings/">
                    <i class="fa fa-envelope"></i> <span>Email Templates</span>
                    <!--                    <small class="label pull-right bg-green">new</small>-->
                </a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>setting/">
                    <i class="fa fa-gears"></i> <span>Settings</span>
                    <!--                    <small class="label pull-right bg-green">new</small>-->
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>