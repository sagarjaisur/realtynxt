<?php
$user = $this->session->all_userdata();
//echo '<pre>';print_r($user);die();
//$linkUser = base_url() . 'edituser/userdetails/' . $user['user'];

$segment = $this->uri->segment('1');
$segment2 = $this->uri->segment('2');
//Get URI segment for Roles
$roleOf = $this->uri->segment('3');
$regionTypeId = $this->uri->segment('3');

?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo $user['image']; ?>" class="img-circle"
                     alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $user['name']; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">

            <li <?php if ($segment == 'dashboard'){ ?>class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>dashboard/">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>

           <!-- <li <?php if ($segment == 'home_page'){ ?>class="active" <?php } ?>>
                <a href="<?php echo base_url() ?>home_page/">
                    <i class="fa fa-th"></i> <span>Home Page Management</span>
                    <small class="label pull-right bg-green">new</small>
                </a>
            </li> -->

            <li <?php if ($segment == 'users'){ ?>class="treeview active" <?php } ?> class="treeview">
                <a href="<?php echo base_url(); ?>users/userrequest/1">
                    <i class="fa fa-files-o"></i>
                    <span>User Management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                    <!--                    <span class="label label-primary pull-right">4</span>-->
                </a>
                <ul class="treeview-menu">
                    <?php
                    foreach ($user['activeRoles'] as $value) {
                        if($value['roleId'] >=  6){
                            continue;
                        }
                        if ($roleOf == $value['roleId'] && $segment == 'users') {
                            $classOf = 'class="active"';
                        } else {
                            $classOf = '';
                        }
                        echo '<li ' . $classOf . '><a href=' . base_url() . 'users/userrequest/' . $value['roleId'] . '><i class="fa fa-circle-o"></i> ' . ucwords(strtolower($value['roleTitle'])) . '</a></li>';
                    }
                    ?>
                </ul>
            </li>
             <li <?php if ($segment == 'team'){ ?>class="active" <?php } ?>>
                <a href="<?php echo base_url() ?>u/">
                    <i class="fa fa-th"></i> <span>Team Management</span>                    
                   <i class="fa fa-angle-left pull-right"></i>
                   <small class="label pull-right bg-green">new</small>


                </a>
                <ul class="treeview-menu">
                    <?php
                    foreach ($user['activeRoles'] as $value) {
                        if($value['roleId'] < 6){
                            continue;
                        }
                        if ($roleOf == $value['roleId'] && $segment == 'team') {
                            $classOf = 'class="active"';
                        } else {
                            $classOf = '';
                        }
                        echo '<li ' . $classOf . '><a href=' . base_url() . 'team/userrequest/' . $value['roleId'] . '><i class="fa fa-circle-o"></i> ' . ucwords(strtolower($value['roleTitle'])) . '</a></li>';
                    }
                    ?>
                </ul>
            </li>
            <li <?php if ($segment == 'builder'){ ?>class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>builder/">
                    <i class="fa fa-fw fa-building-o"></i> <span>Builder Management</span>
                    <!-- <small class="label pull-right bg-green">new</small> -->
                </a>
            </li>

            <li <?php if ($segment == 'document'){ ?>class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>document/">
                    <i class="fa fa-files-o"></i> <span>Document Management</span>
                    <!-- <small class="label pull-right bg-green">new</small> -->
                </a>
            </li>
             <li <?php if ($segment == 'plan'){ ?>class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>plan/">
                    <i class="fa fa-edit"></i> <span>Membership Plan Management</span>
                     <small class="label pull-right bg-green">new</small>
                </a>
            </li>
            <li <?php if ($segment == 'role'){ ?>class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>role/">
                    <i class="fa fa-edit"></i> <span>Role Management</span>
                </a>
            </li>

            <!-- <li <?php if ($segment == 'propertyMapList'){ ?>class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>propertyMapList/">
                    <i class="fa fa-edit"></i> <span>Property Map-List</span>
                    <small class="label pull-right bg-green">new</small>
                </a>
            </li> -->

            <li <?php if ($segment == 'currency'){ ?>class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>currency/">
                    <i class="fa fa-fw fa-rupee"></i> <span>Currency Management</span>
                    <small class="label pull-right bg-green">new</small>
                </a>
            </li>

            <li <?php if ($segment == 'osmedia' || $segment == 'ostestimonial'){ ?>class="treeview active" <?php } ?>
                class="treeview">
                <a href="<?php echo base_url(); ?>osmedia/">
                    <i class="fa fa-table"></i>
                    <span>Our Story Management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <?php
                    $classMedia = $classTestimonial = "";
                    if ($segment == 'osmedia') {
                        $classMedia = 'class="active"';
                        $classTestimonial = '';
                    } elseif ($segment == 'ostestimonial') {
                        $classMedia = '';
                        $classTestimonial = 'class="active"';
                    }
                    echo '<li ' . $classMedia . '><a href=' . base_url() . 'osmedia/><i class="fa fa-circle-o"></i>Media Content</a></li>';
                    echo '<li ' . $classTestimonial . '><a href=' . base_url() . 'ostestimonial/><i class="fa fa-circle-o"></i>Testimonials</a></li>';
                    ?>
                </ul>
            </li>
             <li <?php if ($segment == 'cms'){ ?>class="active" <?php } ?>>
                <a href="<?php echo base_url() ?>cms/">
                    <i class="fa fa-th"></i> <span>CMS Pages Management</span>
                    <small class="label pull-right bg-green">new</small>
                </a>
            </li>
            <li <?php if ($segment == 'career'){ ?>class="active" <?php } ?>>
                <a href="<?php echo base_url() ?>career/">
                    <i class="fa fa-th"></i> <span>Career Management</span>
                    <small class="label pull-right bg-green">new</small>
                </a>
            </li>
            
            <!--
                        <li <?php /*if ($segment == 'regions'){ */ ?>class="treeview active" <?php /*} */ ?> class="treeview">
                            <a href="<?php /*echo base_url(); */ ?>regions/regionrequest/1">
                                <i class="fa fa-table"></i>
                                <span>Region Management</span>
                                <i class="fa fa-angle-left pull-right"></i>
                                <!--                    <span class="label label-primary pull-right">4</span>--><!--
                </a>
                <ul class="treeview-menu">
                    <?php
            /*                    foreach ($user['regionTypes'] as $key => $value) if ($key != 4) {
                                    if ($regionTypeId == $key && $segment == 'regions') {
                                        $classOf = 'class="active"';
                                    } else {
                                        $classOf = '';
                                    }
                                    echo '<li ' . $classOf . '><a href=' . base_url() . 'regions/regionrequest/' . $key . '><i class="fa fa-circle-o"></i> ' . ucwords($value) . '</a></li>';
                                }
                                */ ?>
                </ul>
            </li>
-->
            <li <?php if ($segment == 'emailsettings'){ ?>class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>emailsettings/">
                    <i class="fa fa-envelope"></i> <span>Email Templates</span>                   
                </a>
            </li>
            <li <?php if ($segment == 'smssettings'){ ?>class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>smssettings/">
                    <i class="fa fa-envelope"></i> <span>SMS Templates</span>                   
                </a>
            </li>
           

            <li <?php if ($segment == 'setting'){ ?>class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>setting/">
                    <i class="fa fa-gears"></i> <span>Settings</span>                  
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>