<?php
$user = $this->session->all_userdata();
$role = $user['activeRoles'][array_search($user['roleId'], $user['activeRoles'])]['roleTitle'];
//echo '<pre>';print_r($user);die();
?>
<!DOCTYPE html>
<html>
<head>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo MAIN_DIR_URL . 'admin/index.php/dashboard/'; ?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>GS</b> Admin</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>RealtyNxt</b> Admin Panel</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Notifications: style can be found in dropdown.less -->
                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-warning"><?php echo count($user['newUsers']); ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have <?php echo count($user['newUsers']); ?> notifications</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    <?php
                                    foreach ($user['newUsers'] as $email) { ?>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-user text-green"></i> New User
                                                Added<br/>[<?php echo $email; ?>]
                                            </a>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                    <!--
                                                                        <li>
                                                                            <a href="#">
                                                                                <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#">
                                                                                <i class="fa fa-warning text-yellow"></i> Very long description here that
                                                                                may not fit into the page and may cause design problems
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#">
                                                                                <i class="fa fa-users text-red"></i> 5 new members joined
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#">
                                                                                <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                                                                            </a>
                                                                        </li>

                                    -->                                </ul>
                            </li>
                            <li class="footer"><a href="#">View all</a></li>
                        </ul>
                    </li>

                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo $user['image']; ?>"
                                 class="user-image" alt="User Image">
                            <span class="hidden-xs"><?php echo $user['name']; ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?php echo $user['image']; ?>"
                                     class="img-circle"
                                     alt="User Image">

                                <p>
                                    <?php echo $user['name'] . ' - ' . $role; ?>
                                    <small>Last Login : <?php
                                        $date = date_create($user['lastLogin']);
                                        //echo date_format($date,"Y/m/d H:i:s");
                                        echo date_format($date, "d F, Y"); ?>
                                    </small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <!--                            <li class="user-body">
                                                            <div class="col-xs-4 text-center">
                                                                <a href="#">Followers</a>
                                                            </div>
                                                            <div class="col-xs-4 text-center">
                                                                <a href="#">Sales</a>
                                                            </div>
                                                            <div class="col-xs-4 text-center">
                                                                <a href="#">Friends</a>
                                                            </div>
                                                        </li>-->
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <!--                                    <a href="#" class="btn btn-default btn-flat">Profile</a>-->
                                    <a href="<?php echo base_url(); ?>users/userdetails/<?php echo $user['roleId'] . '/' . $user['memberId'] ?>"
                                       class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?php echo base_url(); ?>dashboard/logout"
                                       class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>

                    <!-- Control Sidebar Toggle Button -->
                    <!--
                                        <li>
                                            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                                        </li>
                    -->
                </ul>
            </div>
        </nav>
    </header>

    <!-- Miiddle  Container Start end At Footer -->
    <!--<div class="containe-middal">-->