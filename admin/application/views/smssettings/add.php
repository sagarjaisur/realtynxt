<?php
$page_caption = 'SMS Template';
$cancel_url = base_url() . 'smssettings/index';
$submitForm_url = 'smssettings/add/';
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">

        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <h1 class="page-header page_title">Add <?php echo $page_caption; ?></h1>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <!--div class="panel-heading">
                            <?php echo $page_caption; ?> Detail
                        </div-->
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <?php echo form_open_multipart($submitForm_url) ?>
                                <div class="form-group">
                                    <label>Template Name *</label>
                                    <input class="form-control" name="smsTemplate" type="text"
                                           value="<?php echo set_value('smsTemplate'); ?>" required>
                                </div>
                                <div class="form-group">
                                    <label>SMS Subject *</label>
                                    <input class="form-control" name="smsSubject" type="text"
                                           value="<?php echo set_value('smsSubject'); ?>" required>
                                </div>

                                <div class="form-group">
                                    <label>SMS Body *</label>
                                    <textarea class="form-control" name="smsBody"
                                              required><?php echo set_value('smsBody'); ?></textarea>
                                    <?php //echo $this->ckeditor->editor('smsBody', set_value('smsBody')); ?>
                                </div>

                                <input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
                                <input type="button" name="Cancel" value="Cancel" class="btn btn-success"
                                       onclick="javascript: window.location.href='<?php echo $cancel_url; ?>';"/>

                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>


    </div>
    <!-- end page-wrapper -->
</div>