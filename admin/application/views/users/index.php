<?php
$page_caption = 'User';
$cancel_url = base_url() . 'dashboard/index';
$add_url = base_url() . 'users/addUser/' . $roleId;
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!--  page header -->
            <div class="col-lg-12">
                <!--                <h1 class="page-header page_title">&nbsp;-->
                <?php //echo $page_caption; ?><!-- Dashboard</h1>-->
                <section class="content-header">
                    <h1 class="page-header page_title">&nbsp;<?php echo $page_caption; ?> Management<input type="button"
                                                                                                           onClick="location.href='<?php echo $add_url; ?>'"
                                                                                                           name="submit"
                                                                                                           value="Add <?php echo ucwords(strtolower($default_roles[$roleId])); ?>"
                                                                                                           class="btn btn-success btn-md submit"
                                                                                                           style="margin-left: 50px">
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo MAIN_DIR_URL . 'admin/index.php/dashboard/'; ?>"><i
                                    class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#"><?php echo $page_caption; ?> Management</a></li>
                        <li class="active"><?php echo ucwords(strtolower($default_roles[$roleId])); ?></li>
                    </ol>
                </section>
            </div>
            <!-- end  page header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <div class="row">
            <div class="col-lg-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>User Role</th>
                                    <th>Full name</th>
                                    <th>Mobile Number</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (!empty($users)) {
                                    foreach ($users as $user) {
                                        $editLink = base_url() . 'users/userdetails/' . $roleId . '/' . $user['memberId'];
                                        $deleteLink = base_url() . 'users/delete/' . $user['memberId'] . '/' . $roleId;
                                        $submitForm_url = 'users/updateRoleMember/' . $user['memberId'];
                                        ?>
                                        <tr class="odd gradeX">
                                            <td><?php echo form_dropdown('user_type', $default_roles, $user['roleId'], 'class="user_role" id=' . $user["memberId"]); ?></td>
                                            <td><?php echo $user['firstName'] . " " . $user['lastName']; ?></td>
                                            <td><?php echo $user['mobileNumber']; ?></td>
                                            <td><?php echo $user['email']; ?></td>
                                            <td class="center">
                                                <?php if ($user['status'] == '0') { ?>
                                                    <a href="<?php echo base_url(); ?>users/changeuserstatus/<?php echo $user['memberId'] . '/1'; ?>">Approve</a> /
                                                    <a href="<?php echo base_url(); ?>users/changeuserstatus/<?php echo $user['memberId'] . '/2'; ?>">Reject</a>
                                                <?php } else if ($user['status'] == '1') { ?>
                                                    Approved /
                                                    <a href="<?php echo base_url(); ?>users/changeuserstatus/<?php echo $user['memberId'] . '/2'; ?>">Reject</a>
                                                <?php } else if ($user['status'] == '2') { ?>
                                                    <a href="<?php echo base_url(); ?>users/changeuserstatus/<?php echo $user['memberId'] . '/1'; ?>">Approve</a> /
                                                    Rejected
                                                <?php } ?>
                                            </td>
                                            <!--                                            <td><a href="-->
                                            <?php //echo base_url(); ?><!--users/userdetails/-->
                                            <?php //echo $user['memberId']; ?><!--">Edit</a></td>-->
                                            <td>
                                                <button class="btn btn-default" type="button"
                                                        onClick="javascipt:window.location.href='<?php echo $editLink; ?>';"
                                                        title="EDIT">
                                                    <i class="fa fa-edit fa-fw"></i>Edit
                                                </button>

                                                <button class="btn btn-danger" type="button"
                                                        onClick="return deleteRecode('<?php echo $deleteLink; ?>','<?php echo $page_caption; ?>');"
                                                        title="DELETE">
                                                    <i class="fa fa-edit fa-fw"></i>Delete
                                                </button>
                                            </td>
                                        </tr>
                                    <?php }
                                } else { ?>
                                    <tr>
                                        <td colspan="5">Record Not Available.</td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <p><?php echo $links; ?></p>
                        </div>
                    </div>
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .odd.gradeX a {
        float: none !important;
    }
</style>
