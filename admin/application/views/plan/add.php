<?php
$page_caption = 'Membership Plan';
$cancel_url = base_url() . 'plan/index';
$submitForm_url = 'plan/add/';
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title">Add <?php echo $page_caption; ?></h1>
                </section>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <?php if (isset($error)) echo '<div class="alert alert-danger">' . $error . '</div>'; ?>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php echo $page_caption; ?> Detail
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-10">
                                <?php echo form_open_multipart($submitForm_url) ?>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label>Plan Title *</label>
                                        <input class="form-control validate[required] planTitle"
                                               name="planTitle"
                                               type="text"
                                               value="<?php echo set_value("planTitle") ? set_value("planTitle") : ""; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-12">
                                        <br/>
                                        <label>Inventory Price Request *</label>
                                        <input class="form-control validate[required] inventoryPriceRequest"
                                               name="inventoryPriceRequest"
                                               type="text"
                                               value="<?php echo set_value("inventoryPriceRequest") ? set_value("inventoryPriceRequest") : ""; ?>"
                                               required>
                                    </div>
                                     <div class="clearfix"></div>

                                    <div class="col-md-12">
                                        <br/>
                                        <label>Plan Price *</label>
                                        <input class="form-control validate[required] planPrice"
                                               name="planPrice"
                                               type="text"
                                               value="<?php echo set_value("planPrice") ? set_value("planPrice") : ""; ?>"
                                               required>
                                    </div>
                                     <div class="clearfix"></div>
                                    <div class="col-md-3">
                                     <br/>
                                        <label>Builder Documents *</label>
                                         <br/>
                                        <select   name="builderDocuments" required>
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </div>

                                    <div class="col-md-9">
                                        <br/>
                                        <label>Number Of Builder Documents </label>
                                         <input class="form-control validate[required] numberBuilderDocuments"
                                               name="numberBuilderDocuments"
                                               type="text"
                                               value="<?php echo set_value("numberBuilderDocuments") ? set_value("numberBuilderDocuments") : ""; ?>"
                                               required>
                                       
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-3">
                                        <br/>
                                        <label>Skype Call Allowed *</label>
                                        <br/>
                                        <select   name="skypeCall" required>
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </div>
                                    <div class="col-md-9">
                                        <br/>
                                        <label>Number Of Skype Call Allowed </label>
                                         <input class="form-control validate[required] numberSkypeCall"
                                               name="numberSkypeCall"
                                               type="text"
                                               value="<?php echo set_value("numberSkypeCall") ? set_value("numberSkypeCall") : ""; ?>"
                                               required>
                                       
                                    </div>
                                    <div class="col-md-3">
                                        <br/>
                                        <label>Actual Visit Project *</label>
                                        <br/>
                                        <select   name="actualVisitProject" required>
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </div>
                                     <div class="col-md-9">
                                        <br/>
                                        <label>Number Of Actual Visit Project </label>
                                         <input class="form-control validate[required] numberActualVisitProject"
                                               name="numberActualVisitProject"
                                               type="text"
                                               value="<?php echo set_value("numberActualVisitProject") ? set_value("numberActualVisitProject") : ""; ?>"
                                               required>
                                       
                                    </div>
                                    <div class="col-md-4">
                                        <br/>
                                        <label>Legal Support Services(days) *</label>
                                      <input class="form-control validate[required] legalSupportServices"  name="legalSupportServices"  type="text"  value="<?php echo set_value("legalSupportServices") ? set_value("legalSupportServices") : ""; ?>"
                                               required>
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <br/>
                                        <label>Registry Support *</label>
                                        <br/>
                                        <select   name="registrySupport" required>
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </div>
                                     <div class="col-md-2">                                        
                                        <label>Project Updates *</label>
                                        <input class="form-control validate[required] projectUpdates"  name="projectUpdates"  type="text"  value="<?php echo set_value("projectUpdates") ? set_value("projectUpdates") : ""; ?>"
                                               required>
                                    </div>
                                    <div class="col-md-6">        
                                        <label>&nbsp;</label> <br/>                              
                                        <select  name="projectUpdatesType" required>
                                            <option value="months">Months</option>                                                                                        
                                            <option value="years">Years</option>
                                        </select>
                                    </div>
                                     <div class="clearfix"></div>
                                    
                                     <div class="col-md-2">
                                            <label >RM Support *</label>
                                            <input class="form-control validate[required] rmSupport col-md-2"  name="rmSupport"  type="text"  value="<?php echo set_value("rmSupport") ? set_value("rmSupport") : ""; ?>"
                                                   required>
                                    </div>
                                    <div class="col-md-6">
                                        <br/>
                                            <select  name="rmSupportType" required>
                                                <option value="months">Months</option>                                                                                        
                                                <option value="years">Years</option>
                                            </select>                                        
                                    </div>
                                     <div class="col-md-12">
                                        <br/>
                                        <label>Plan Status *</label>
                                        <select   name="planStatus" required>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>
                                    </div>

                                    <div class="clearfix"></div>

                                  

                                </div>

                                <input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
                                <input type="button" name="Cancel" value="Cancel" class="btn btn-success"
                                       onclick="javascript: window.location.href='<?php echo $cancel_url; ?>';"/>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
</div>
