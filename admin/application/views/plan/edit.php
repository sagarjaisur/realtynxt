<?php
$page_caption = 'Membership Plan';
$cancel_url = base_url() . 'plan/index';
$submitForm_url = 'plan/edit/' . $id;
$cmsPage = $cmsPage[0];
?>
<div class="content-wrapper" style="min-height: 916px;">
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <section class="content-header">
                    <h1 class="page-header page_title">Edit <?php echo $page_caption; ?></h1>
                </section>
            </div>
            <!--End Page Header -->
        </div>
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <?php if (isset($error)) echo '<div class="alert alert-danger">' . $error . '</div>'; ?>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-10">
                                <?php echo form_open_multipart($submitForm_url) ?>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <label>Plan Title *</label>
                                        <input class="form-control validate[required] planTitle"
                                               name="planTitle"
                                               type="text"
                                               value="<?php echo  $cmsPage["planTitle"] ; ?>"
                                               required>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-4">
                                        <br/>
                                        <label>Inventory Price Request *</label>
                                        <input class="form-control validate[required] inventoryPriceRequest"
                                               name="inventoryPriceRequest"
                                               type="text"
                                               value="<?php echo  $cmsPage["inventoryPriceRequest"] ; ?>"
                                               required>
                                    </div>
                                     <div class="clearfix"></div>

                                    <div class="col-md-12">
                                        <br/>
                                        <label>Plan Price *</label>
                                        <input class="form-control validate[required] planPrice"
                                               name="planPrice"
                                               type="text"
                                               value="<?php echo $cmsPage["planPrice"]; ?>"
                                               required>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <br/>
                                        <label>Builder Documents *</label>
                                        <br/>
                                        <select   name="builderDocuments" required>
                                            <option value="0" <?php if($cmsPage['builderDocuments'] == 0){ echo "selected='selected'"; }  ?> >No</option>
                                            <option value="1" <?php if($cmsPage['builderDocuments'] == 1){ echo "selected='selected'"; }  ?>>Yes</option>
                                        </select>
                                    </div>
                                    <div class="col-md-9">
                                        <br/>
                                        <label>Number Of Builder Documents </label>
                                         <input class="form-control validate[required] numberBuilderDocuments"
                                               name="numberBuilderDocuments"
                                               type="text"
                                               value="<?php if($cmsPage['numberBuilderDocuments'] != 0){ echo $cmsPage['numberBuilderDocuments']; }  ?>"
                                               required>
                                       
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-3">
                                        <br/>
                                        <label>Skype Call Allowed *</label>
                                        <br/>
                                        <select   name="skypeCall" required>
                                            <option value="0" <?php if($cmsPage['skypeCall'] == 0){ echo "selected='selected'"; }  ?>>No</option>
                                            <option value="1"  <?php if($cmsPage['skypeCall'] == 1){ echo "selected='selected'"; }  ?>>Yes</option>
                                        </select>
                                    </div>
                                    <div class="col-md-9">
                                        <br/>
                                        <label>Number Of Skype Call Allowed </label>
                                         <input class="form-control validate[required] numberSkypeCall"
                                               name="numberSkypeCall"
                                               type="text"
                                               value="<?php if($cmsPage['numberSkypeCall'] != 0){ echo $cmsPage['numberSkypeCall']; }  ?>"
                                               required>
                                       
                                    </div>
                                     <div class="clearfix"></div>
                                    <div class="col-md-3">
                                        <br/>
                                        <label>Actual Visit Project *</label>
                                        <br/>
                                        <select   name="actualVisitProject" required>
                                            <option value="0"  <?php if($cmsPage['actualVisitProject'] == 0){ echo "selected='selected'"; }  ?>>No</option>
                                            <option value="1" <?php if($cmsPage['actualVisitProject'] == 1){ echo "selected='selected'"; }  ?>>Yes</option>
                                        </select>
                                    </div>
                                     <div class="col-md-9">
                                        <br/>
                                        <label>Number Of Actual Visit Project </label>
                                         <input class="form-control validate[required] numberActualVisitProject"
                                               name="numberActualVisitProject"
                                               type="text"
                                               value="<?php if($cmsPage['numberActualVisitProject'] != 0){ echo $cmsPage['numberActualVisitProject']; }  ?>"
                                               required>
                                       
                                    </div>
                                    <div class="col-md-4">
                                        <br/>
                                        <label>Legal Support Services(days) *</label>
                                      <input class="form-control validate[required] legalSupportServices"  name="legalSupportServices"  type="text"  value="<?php echo $cmsPage["legalSupportServices"]; ?>" required />
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <br/>
                                        <label>Registry Support *</label>
                                        <select   name="registrySupport" required>
                                            <option value="0" <?php if($cmsPage['registrySupport'] == 0){ echo "selected='selected'"; }  ?>>No</option>
                                            <option value="1" <?php if($cmsPage['registrySupport'] == 1){ echo "selected='selected'"; }  ?>>Yes</option>
                                        </select>
                                    </div>
                                     <div class="col-md-2">                                        
                                        <label>Project Updates *</label>
                                        <input class="form-control validate[required] projectUpdates"  name="projectUpdates"  type="text"  value="<?php echo $cmsPage["projectUpdates"]; ?>"
                                               required>
                                    </div>
                                    <div class="col-md-6">        
                                        <label>&nbsp;</label> <br/>                              
                                        <select  name="projectUpdatesType" required>
                                            <option value="months" <?php if($cmsPage['projectUpdatesType'] == 'months'){ echo "selected='selected'"; }  ?>>Months</option>                                                                                        
                                            <option value="years" <?php if($cmsPage['projectUpdatesType'] == 'years'){ echo "selected='selected'"; }  ?> >Years</option>
                                        </select>
                                    </div>
                                     <div class="clearfix"></div>
                                    
                                     <div class="col-md-2">
                                            <label >RM Support *</label>
                                            <input class="form-control validate[required] rmSupport col-md-2"  name="rmSupport"  type="text"  value="<?php echo $cmsPage["rmSupport"]; ?>"
                                                   required>
                                    </div>
                                    <div class="col-md-6">
                                        <br/>
                                            <select  name="rmSupportType" required>
                                              <option value="months" <?php if($cmsPage['rmSupportType'] == 'months'){ echo "selected='selected'"; }  ?>>Months</option>                                                                                        
                                            <option value="years" <?php if($cmsPage['rmSupportType'] == 'years'){ echo "selected='selected'"; }  ?> >Years</option></select>                                        
                                    </div>
                                     <div class="col-md-12">
                                       
                                        <label>Plan Status *</label>
                                         <br/>
                                        <select   name="planStatus" required>
                                            <option value="1" <?php if($cmsPage['planStatus'] == '1'){ echo "selected='selected'"; }  ?>>Active</option>
                                            <option value="0" <?php if($cmsPage['planStatus'] == '0'){ echo "selected='selected'"; }  ?>>InActive</option>
                                        </select>
                                    </div>

                                    <div class="clearfix"></div>

                                  

                                </div>

                                <input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
                                <input type="button" name="Cancel" value="Cancel" class="btn btn-success"
                                       onclick="javascript: window.location.href='<?php echo $cancel_url; ?>';"/>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->
</div>