<?php

class Restaurant_model extends CI_Model
{
    private $tablename;

    function __construct()
    {
        $this->tablename = 'tbl_restaurant';
        parent::__construct();
    }

    // Get all restaurants details
    function get_restaurant($where = '')
    {
        $this->load->database();
        if (empty($where)) {
            $query = $this->db->get($this->tablename);
        } else {
            $query = $this->db->get_where($this->tablename, $where);
        }

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    // Get specific restaurant's details by restaurantId
    function get_detail($restaurantId = '')
    {
        $where = array('restaurantId' => $restaurantId);
        return $this->db->get_where($this->tablename, $where)->result_array();
    }

    // Add New Restaurant
    public function add($data)
    {
        $res = $this->db->insert($this->tablename, $data);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Update Restaurant Details
    public function update($data, $where)
    {
        $res = $this->db->update($this->tablename, $data, $where);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Delete Restaurant
    public function delete($restaurantId)
    {
        $where = array('restaurantId' => $restaurantId);
        $res = $this->db->delete($this->tablename, $where);
        if ($res) {
            return TRUE;
        }
    }

    // Update status of Restaurant
    function changeStatus($id, $status)
    {
        if (isset($status) && $status == 1) {
            $changeStatus = 0;
        }
        if (isset($status) && $status == 0) {
            $changeStatus = 1;
        }
        $data = array('status' => $changeStatus);
        $where = array('restaurantId' => $id);

        $result = $this->db->update($this->tablename, $data, $where);

        return $result;
    }
}