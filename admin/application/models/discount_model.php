<?php

class Discount_model extends CI_Model
{
    private $tablename;

    function __construct()
    {
        $this->tablename = 'tbl_special_discounts';
        parent::__construct();
    }

    // Get all documents details
    function get_document($where = '')
    {
        $this->load->database();
        if (empty($where)) {
            $query = $this->db->get($this->tablename);
        } else {
            $query = $this->db->get_where($this->tablename, $where);
        }

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    // Get specific document's details by documentId
    function get_detail($documentId = '')
    {
        $where = array('documentId' => $documentId);
        return $this->db->get_where($this->tablename, $where)->result_array();
    }

    // Add New Document
    public function add($data)
    {
        $res = $this->db->insert($this->tablename, $data);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Update Document Details
    public function update($data, $where)
    {
        $res = $this->db->update($this->tablename, $data, $where);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Delete Document
    public function delete($documentId)
    {
        $where = array('documentId' => $documentId);
        $res = $this->db->delete($this->tablename, $where);
        if ($res) {
            return TRUE;
        }
    }

    // Update status of Document
    function changeStatus($id, $status)
    {
        if (isset($status) && $status == 1) {
            $changeStatus = 0;
        }
        if (isset($status) && $status == 0) {
            $changeStatus = 1;
        }
        $data = array('status' => $changeStatus);
        $where = array('documentId' => $id);

        $result = $this->db->update($this->tablename, $data, $where);

        return $result;
    }
}