<?php

class OsMedia_model extends CI_Model
{
    private $tablename;

    function __construct()
    {
        $this->tablename = 'tbl_os_media';
        parent::__construct();
    }

    function getAllOsMedia($limit, $start, $where = '')
    {
        $this->load->database();
        if (empty($where)) {
            $this->db->from($this->tablename);
            $this->db->limit($limit, $start);
            $this->db->order_by("mediaId", "desc");
            $query = $this->db->get();
        } else {
            $this->db->from($this->tablename);
            $this->db->where($where);
            $this->db->limit($limit, $start);
            $this->db->order_by("mediaId", "desc");
            $query = $this->db->get();
        }

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //  Get Count  Of Recodes
    function recode_count()
    {
        $this->db->select('*');
        $this->db->from($this->tablename);
        $query = $this->db->get();
        if ($query->num_rows()) {
            return count($query->result_array());
        } else {
            return false;
        }
    }

    function changeStatus($id, $status)
    {
        $changeStatus = $status ? 0 : 1;
        $data = array('mediaStatus' => $changeStatus);
        $where = array('mediaId' => $id);
        $this->db->update($this->tablename, $data, $where);
        return $this->db->get_where($this->tablename, array('mediaId' => $id))->result_array();
    }

    //Get Single Media Details
    function getOsMediaById($id = '')
    {
        $where = array('mediaId' => $id);
        return $this->db->get_where($this->tablename, $where)->result_array();
    }

    //Update Media
    public function update($data, $where)
    {
        $res = $this->db->update($this->tablename, $data, $where);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Insert Media
    public function add($data)
    {
        $res = $this->db->insert($this->tablename, $data);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Delete Media
    public function delete($id)
    {
        $where = array('mediaId' => $id);
        $res = $this->db->delete($this->tablename, $where);
        if ($res) {
            return true;
        }
    }
}