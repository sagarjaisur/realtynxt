<?php

class Amenity_model extends CI_Model
{
    private $tablename;

    function __construct()
    {
        $this->tablename = 'tbl_amenity';
        parent::__construct();
    }

    // Get all amenitys details
    function get_amenity($where = '')
    {
        $this->load->database();
        if (empty($where)) {
            $query = $this->db->get($this->tablename);
        } else {
            $query = $this->db->get_where($this->tablename, $where);
        }

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    // Get specific amenity's details by amenityId
    function get_detail($amenityId = '')
    {
        $where = array('amenityId' => $amenityId);
        return $this->db->get_where($this->tablename, $where)->result_array();
    }

    // Add New Amenity
    public function add($data)
    {
        $res = $this->db->insert($this->tablename, $data);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Update Amenity Details
    public function update($data, $where)
    {
        $res = $this->db->update($this->tablename, $data, $where);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Delete Amenity
    public function delete($amenityId)
    {
        $where = array('amenityId' => $amenityId);
        $res = $this->db->delete($this->tablename, $where);
        if ($res) {
            return TRUE;
        }
    }

    // Update status of Amenity
    function changeStatus($id, $status)
    {
        if (isset($status) && $status == 1) {
            $changeStatus = 0;
        }
        if (isset($status) && $status == 0) {
            $changeStatus = 1;
        }
        $data = array('status' => $changeStatus);
        $where = array('amenityId' => $id);

        $result = $this->db->update($this->tablename, $data, $where);

        return $result;
    }
}