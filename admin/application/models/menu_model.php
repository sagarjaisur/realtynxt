<?php

class Menu_model extends CI_Model
{
    private $tablename;

    function __construct()
    {
        $this->tablename = 'tbl_cms_menu';
        parent::__construct();
    }

    function getMenus($limit, $start, $where = '')
    {
        $this->load->database();
        if (empty($where)) {
            $this->db->from($this->tablename);
            $this->db->limit($limit, $start);
            $query = $this->db->get();
        } else {
            $this->db->from($this->tablename);
            $this->db->where($where);
            $this->db->limit($limit, $start);
            $query = $this->db->get();
        }

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //  Get Count  Of Recodes
    function recode_count()
    {
        $this->db->select('*');
        $this->db->from($this->tablename);
        $query = $this->db->get();
        if ($query->num_rows()) {
            return count($query->result_array());
        } else {
            return false;
        }
    }

    function changeStatus($id, $status)
    {
        $data = array('menuStatus' => $status);
        $where = array('menuId' => $id);
        $this->db->update($this->tablename, $data, $where);

        return $this->db->get_where($this->tablename, array('menuId' => $starId))->result_array();
    }

    //Get Single Cms Page Details
    function getMenuById($id = '')
    {
        $where = array('menuId' => $id);
        return $this->db->get_where($this->tablename, $where)->result_array();
    }

    //Update Cms Page
    public function update($data, $where)
    {
        $res = $this->db->update($this->tablename, $data, $where);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Insert Cms Page
    public function add($data)
    {
        $res = $this->db->insert($this->tablename, $data);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Delete Cms Page
    public function delete($id)
    {
        $where = array('menuId' => $id);
        $res = $this->db->delete($this->tablename, $where);
        if ($res) {
            return true;
        }
    }

    //  Menu List  All

    function getParentChildCombo2($table, $id, $printId, $parentId, $parent_value, $ORDERBY = '', $selectedId = '', $selectIdSet = '', $eventSet = '')
    {
        if ($selectIdSet == '')
            $selectIdSet = "CategoryList";
//        $tree = '<select name="'.$selectIdSet.'" id="'.$selectIdSet.'" '.$eventSet.' class="span4 m-wrap" >';


        if ($ORDERBY != '') {
            $result = $this->db->query("SELECT * FROM $table WHERE $parentId = $parent_value  ORDER BY $ORDERBY ASC")->result_array();

        } else {
            $result = $this->db->query("SELECT * FROM $table WHERE $parentId = $parent_value  ")->result_array();

        }


        $tree .= '<option value="">Choose a category</option>';

        foreach ($result as $key => $row) {
            $checkSub = $this->db->query("SELECT * FROM $table WHERE $parentId =" . $row[$id] . " ")->result_array();
            $num_rows = count($checkSub);
            if ($num_rows > 0) {
                $disable = 'disabled="disabled"';
            } else {
                $disable = '';
            }
            //  Selected Value  Check
            if ($selectedId != '') {
                if (is_array($selectedId)) {
                    if (in_array($row[$id], $selectedId)) {
                        $selectedText = 'selected="selected"';
                    } else {
                        $selectedText = '';
                    }
                } else {
                    if ($selectedId == $row[$id])
                        $selectedText = 'selected="selected"';
                    else
                        $selectedText = '';
                }
            } else {
                $selectedText = '';
            }
            //  Selected Check End

            $tree .= '<option value="' . $row[$id] . '" ' . $selectedText . '  ' . $disable . ' >';
            $tree .= $row[$printId];
            $tree .= '</option>';
            $tree .= $this->getParentChildCombo2($table, $id, $printId, $parentId, $row[$id], 1, $ORDERBY, $selectedId);
        }


        $tree .= '</select>';

        return ($tree);

    }

    // All  Mennu List
    public function getAllMenus()
    {
        $this->load->database();
        $this->db->from($this->tablename);
        $query = $this->db->get();

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }
}