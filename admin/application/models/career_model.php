<?php

class Career_model extends CI_Model
{
    private $tablename;

    function __construct()
    {
        $this->tablename = 'tbl_career_management';
        parent::__construct();
    }

    function getCmsPages($limit, $start, $where = '')
    {
        $this->load->database();
        if (empty($where)) {
            $this->db->from($this->tablename);
            $this->db->limit($limit, $start);
            $this->db->order_by("careerId", "desc");
            $query = $this->db->get();
        } else {
            $this->db->from($this->tablename);
            $this->db->where($where);
            $this->db->limit($limit, $start);
            $this->db->order_by("careerId", "desc");
            $query = $this->db->get();
        }

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //  Get Count  Of Recodes
    function recode_count()
    {
        $this->db->select('*');
        $this->db->from($this->tablename);
        $query = $this->db->get();
        if ($query->num_rows()) {
            return count($query->result_array());
        } else {
            return false;
        }
    }

    function changeStatus($id, $status)
    {
        $changeStatus = $status ? 0 : 1;
        $data = array('careerStatus' => $changeStatus);
        $where = array('careerId' => $id);
        $this->db->update($this->tablename, $data, $where);
        return $this->db->get_where($this->tablename, array('careerId' => $id))->result_array();
    }

    //Get Single Cms Page Details
    function getCmsPageById($id = '')
    {
        $where = array('careerId' => $id);
        return $this->db->get_where($this->tablename, $where)->result_array();
    }

    //Update Cms Page
    public function update($data, $where)
    {
        $res = $this->db->update($this->tablename, $data, $where);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Insert Cms Page
    public function add($data)
    {
        $res = $this->db->insert($this->tablename, $data);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Delete Cms Page
    public function delete($id)
    {
        $where = array('careerId' => $id);
        $res = $this->db->delete($this->tablename, $where);
        if ($res) {
            return true;
        }
    }
}