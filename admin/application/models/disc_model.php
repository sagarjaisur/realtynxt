<?php

class Disc_model extends CI_Model
{
    private $tablename;

    function __construct()
    {
        $this->tablename = 'tbl_disc';
        parent::__construct();
    }

    // Get Disc details
    function get_disc($where = '')
    {
        $this->load->database();
        if (empty($where)) {
            $query = $this->db->from($this->tablename)
                ->join('tbl_disc_details', 'tbl_disc.discId = tbl_disc_details.discId')
                ->get();
        } else {
            $query = $this->db->select('tbl_disc.*, tbl_disc_details.*')
                ->from($this->tablename)
                ->where($where)
                ->join('tbl_disc_details', 'tbl_disc.discId = tbl_disc_details.discId')
                ->get();
        }

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    // Get Disc details by discID
    function get_detail($discId = '')
    {
        $where = array('tbl_disc.discId' => $discId);

        $result = $this->db->get_where($this->tablename, $where)
            ->result_array();

        return $result;
    }

    // Get Disc details by discID
    function get_disc_details($discId = '')
    {
        $where = array('tbl_disc.discId' => $discId, 'tbl_disc.status' => 1);

        $result = $this->db->select('tbl_disc_details.*')
            ->from($this->tablename)
            ->where($where)
            ->join('tbl_disc_details', 'tbl_disc_details.discId = tbl_disc.discId')
            ->get()
            ->result_array();

        return $result;
    }

    // Add New Disc
    public function add($data, $discDetails)
    {
        $this->db->insert($this->tablename, $data);

        $discId = $this->db->insertId();

        foreach ($discDetails as $key => $discDetail) {
            $discDetail += array(
                'discId' => $discId
            );
            $discDetails[$key] = $discDetail;
        }

        $res = $this->db->insert_batch('tbl_disc_details', $discDetails);

        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Update Disc
    public function update($data, $discDetails, $discId)
    {
        $this->db->update($this->tablename, $data, "discId = $discId");

//        echo "<pre>";
//        print_r($discDetails);

        foreach ($discDetails AS $discDetail) {
            $where = array(
                'discId' => $discId,
                'entryType' => $discDetail['entryType']
            );

            $recordExists = $this->db->get_where('tbl_disc_details', $where)->num_rows();
            if ($recordExists == 0) {
                $res = $this->db->insert('tbl_disc_details', $discDetail);
            } else {
                $id = @$this->db->select('id')
                    ->get_where('tbl_disc_details', $where)->result_array()[0]['id'];
//                print_r($id);
//                echo "<pre>";
//                print_r($discDetail);
                $res = $this->db->update('tbl_disc_details', $discDetail, "id = $id");
            }
        }

//        die();
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Delete Disc
    public function delete($discId)
    {
        $where = array('discId' => $discId);
        $this->db->delete($this->tablename, $where);
        $res = $this->db->delete('tbl_disc_details', $where);
        if ($res) {
            return TRUE;
        }
    }

    // Change Disc Status
    function changeStatus($discId, $status)
    {
        if (isset($status) && $status == 1) {
            $changeStatus = 0;
        }
        if (isset($status) && $status == 0) {
            $changeStatus = 1;
        }
        $data = array('status' => $changeStatus);
        $where = array('discId' => $discId);

        $result = $this->db->update($this->tablename, $data, $where);

        return $result;
    }
}