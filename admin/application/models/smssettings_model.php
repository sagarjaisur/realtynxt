<?php

class Smssettings_model extends CI_Model
{
    private $tablename;

    function __construct()
    {
        $this->tablename = 'tbl_smssettings';
        parent::__construct();
    }

    function getSmssTemplates($where = '')
    {
        $this->load->database();
        if (empty($where)) {
            $this->db->select('*');
            $this->db->from($this->tablename);
            $this->db->order_by("smsTemplate", "desc");
            $query = $this->db->get();
        } else {
            $this->db->select('*');
            $this->db->from($this->tablename);
            $this->db->where($where);
            $this->db->order_by("smsTemplate", "desc");
            $query = $this->db->get();
        }

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //Get Single Sms Template Details
    function getSmsTemplateById($id = '')
    {
        $where = array('smsId' => $id);
        return $this->db->get_where($this->tablename, $where)->result_array();
    }

    //Update Cms Page
    public function update($data, $where)
    {
        $res = $this->db->update($this->tablename, $data, $where);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Insert Sms Settings Page
    public function add($data)
    {
        $res = $this->db->insert($this->tablename, $data);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Delete Cms Page
    public function delete($id)
    {
        $where = array('smsId' => $id);
        $res = $this->db->delete($this->tablename, $where);
        if ($res) {
            return true;
        }
    }

}