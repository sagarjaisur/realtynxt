<?php

class Setting_model extends CI_Model
{
    private $tablename;

    function __construct()
    {
        $this->tablename = 'tbl_setting';
        parent::__construct();
    }

    function get_banners($where = '')
    {
        $this->load->database();
        if (empty($where)) {
            $query = $this->db->get($this->tablename);
        } else {
            $query = $this->db->get_where($this->tablename, $where);
        }

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function changeStatus($starId, $status)
    {
        $data = array('bannerStatus' => $status);
        $where = array('bannerId' => $starId);
        $this->db->update($this->tablename, $data, $where);

        return $this->db->get_where($this->tablename, array('bannerId' => $starId))->result_array();
    }

    function updatePassword($data = array())
    {
        if (empty($data[0]['vStar_secret_key'])) {
            $starId = $data[0]['iStarId'];
            $data = array('vStar_secret_key' => '123456');
            $where = array('iStarId' => $starId);
            $this->db->update($this->tablename, $data, $where);
            return $this->db->get_where($this->tablename, array('iStarId' => $starId))->result_array();
        }
    }

    function get_single_star_detail($starId = '')
    {
        $where = array('iStarId' => $starId);
        return $this->db->get_where($this->tablename, $where)->result_array();
    }

    //Get Single Banner Detail
    function get_single_banner_detail($settingId = '')
    {
        $where = array('settingId' => $settingId);
        return $this->db->get_where($this->tablename, $where)->result_array();
    }

    //Update Banner
    public function update($data, $where)
    {
        $res = $this->db->update($this->tablename, $data, $where);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Insert Banner
    public function add($data)
    {
        $res = $this->db->insert($this->tablename, $data);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Delete Banner
    public function delete($settingId)
    {
        /*$availableId = $this->db->get_where($this->tablename, array('settingId' => $bannerId))->num_rows();
        if($availableId > 0)
        {
            return FALSE;
        }*/

        $where = array('settingId' => $settingId);
        $res = $this->db->delete($this->tablename, $where);
        if ($res) {
            return TRUE;
        }
    }

}