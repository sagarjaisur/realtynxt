<?php

class Team_model extends CI_Model
{
    private $tablename;

    function __construct()
    {
        $this->tablename = 'tbl_team';

        parent::__construct();
        $this->load->database();
    }

    function addUser($data)
    {
        $res_in = $this->db->insert('tbl_team', $data);

        if ($res_in) {
            return true;
        } else {
            return false;
        }
    }


    function addAdmin($data, $adminData)
    {
        $this->db->insert('tbl_team', $data);
        $res_in = $this->db->insert('tbl_admin', $adminData);
        if ($res_in) {
            return true;
        } else {
            return false;
        }
    }


    function getUsersRequest($limit, $start, $where = '')
    {
        if (empty($where)) {
            $this->db->select('*');
            $this->db->order_by('memberId', 'desc');
            $this->db->from($this->tablename);
            $this->db->limit($limit, $start);
            $query = $this->db->get();
        } else {
            $this->db->select('*');
            $this->db->order_by('memberId', 'desc');
            $this->db->from($this->tablename);
            $this->db->where($where);
            $this->db->limit($limit, $start);
            $query = $this->db->get();
        }

        /*
        $this->db->select('*');
        $this->db->order_by('memberId','desc');
        $this->db->from($this->tablename);
        $query = $this->db->get();
        */
        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //  Get Count  Of Recodes
    function recode_count()
    {
        $this->db->select('*');
        $this->db->from($this->tablename);
        $query = $this->db->get();
        if ($query->num_rows()) {
            return count($query->result_array());
        } else {
            return false;
        }
    }

    function get_User($limit, $start, $where = '')
    {
        if (empty($where)) {
            $this->db->select('*');
            //$this->db->where('user_type',$user_typeId);
//        $this->db->order_by('memberId', 'desc');
            $this->db->order_by('memberId');
            $this->db->from($this->tablename);
            $query = $this->db->get();
        } else {
            $query = $this->db->select('*')
                ->order_by('memberId')
                ->from($this->tablename)
                ->where($where)
                ->get();
        }

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

//  function get_User_Record($id,$column_name){
    function get_User_Record($id)
    {
        if ($id > 0) {
//          $this->db->select($column_name);
            $this->db->where('memberId', $id);
            $this->db->from($this->tablename);
            $query = $this->db->get();
            if ($query->num_rows()) {
                return $query->result_array();
            } else {
                return false;
            }
        }
    }

    function getGuestUser()
    {
        $this->db->select('*');
        $this->db->where('user_type', '2');
        $this->db->order_by('memberId', 'desc');
        $this->db->from($this->tablename);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getMemberUser()
    {
        $this->db->select('*');
        $this->db->where('user_type', '1');
        $this->db->order_by('memberId', 'desc');
        $this->db->from($this->tablename);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getUserById($id)
    {
//      $this->db->select('m.*,pm.addressId,pm.address1 as "pickup_street",pm.address1 as "pickup_city",pm.suburb as "pickup_suburb",pm.country as "pickup_country",pm.state as "pickup_state",pm.pincode as "pickup_zip"');
//      $this->db->select('m.*,pm.addressId,pm.address1 as "pickup_street",pm.address1 as "pickup_city",pm.suburb as "pickup_suburb",pm.country as "pickup_country",pm.state as "pickup_state",pm.pincode as "pickup_zip"');
        $this->db->where('m.memberId', $id);
        $this->db->from($this->tablename . ' as m');
//      $this->db->join('tbl_team_pickup_address as pm','m.memberId = pm.memberId','left');
        $query = $this->db->get();


        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getUserByEmail($email)
    {
//        $this->db->where('email', $email);
//        $this->db->from($this->tablename);
        $query = $this->db->get_where($this->tablename, array('email' => $email));

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getAdminByEmail($email)
    {
        $query = $this->db->get_where('tbl_admin', array('email' => $email));

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getAdminById($uid)
    {
        $query = $this->db->get_where('tbl_admin', array('memberId' => $uid));

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }


    function changeUserStatus($userId, $status)
    {
        $date = date('Y-m-d H:i:s');
        if ($status == '1')
            $verificationFlag = 1;//Approved
        else if ($status == '0')
            $verificationFlag = 3;//Rejected

        $data = array('status' => $status, 'modifiedDate' => $date, 'verificationFlag' => $verificationFlag);
        $where = array('memberId' => $userId);

        return $this->db->update($this->tablename, $data, $where);

        //return $this->db->get_where($this->tablename,array('iUserId' => $userId))->result_array();
    }

    public function updateRoleMember($id, $role)
    {
        $data = array('roleId' => $role);
        $where = array('memberId' => $id);

        return $this->db->update($this->tablename, $data, $where);
    }

    /*function record_count($search = ''){
      $this->db->select('*');
      if(!empty($search)){
         $this->db->or_like('vFirst_name', $search);
         $this->db->or_like('vLast_name', $search);
         $this->db->or_like('vPhone_no', $search);
         $this->db->or_like('vEmailId', $search);
         $this->db->or_like('country', $search);
         $this->db->or_like('state', $search);
         $this->db->or_like('city', $search);
      }

          $this->db->from($this->tablename);
      $query = $this->db->get();
      if($query->num_rows()){
           return $query->result_array();
          }else{
               return false;
          }
    }
  */
    // Old Function
    /* function updateMemberDetails($id,$data){
        $this->db->where('memberId', $id);
        return $this->db->update($this->tablename, $data);
    }

    function updatePickupAddress($id,$data){
        $this->db->where('memberId', $id);
        return $this->db->update('tbl_team_pickup_address', $data);
    }
    */
    // End here


    function updateMemberDetails($uid, $data)
    {
//        $this->db->where('memberId', $uid);
        $result = $this->db->update($this->tablename, $data, array('memberId' => $uid));
        $affectedRows = $this->db->affected_rows();
        if ($affectedRows > 0) {
            return $result;
        } else {
            return false;
        }
    }

    function updateAdminDetails($email, $data, $dataAdmin)
    {
        $this->db->update($this->tablename, $data, array('email' => $email));
        $result = $this->db->update('tbl_admin', $dataAdmin, array('email' => $email));
//        die($result);
//        $affectedRows = $this->db->affected_rows();
//        var_dump($affectedRows);
//        die();
//        if ($affectedRows > 0) {
        if ($result == 1) {
            return $result;
        } else {
            return false;
        }
    }


    function updatePickupAddress($id, $data)
    {
        $this->db->select('memberId');
        $this->db->where('memberId', $id);
        $this->db->from('tbl_team_pickup_address');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $this->db->where('memberId', $id);
            $res = $this->db->update('tbl_team_pickup_address', $data);
        } else {
            $data['memberId'] = $id;
            $res_in = $this->db->insert('tbl_team_pickup_address', $data);
            if ($res_in) {
                return true;
            } else {
                return false;
            }
        }
    }

    function checkForUniquewsername($str = '', $uid = '')
    {
        $this->db->select('*');
        $this->db->where('webname', $str);
        if ($uid != '') {
            $this->db->where('uid != "' . $uid . '"');
        }
        $this->db->from('tbl_team');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $str = $str . rand(1, 30);
            $this->checkForUniqueWsername($str, $uid);
        } else {
            echo $str;
        }
    }

    function checkForNomineeList($str)
    {
        //$query = $this->db->query('SELECT webname FROM `tbl_team` WHERE webname LIKE "%'.$str.'%"');
        $query = $this->db->query('SELECT webname FROM `tbl_team` WHERE webname = "' . $str . '"');

        if ($query->num_rows() > 0) {
            //return $query->result_array();
            return $query->num_rows();
        } else {
            return false;
        }
    }

    function checkForState($countryId)
    {
        $query = $this->db->query('SELECT `state`,`title` FROM `tbl_state` WHERE country = "' . $countryId . '"');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function checkForCity($stateId)
    {
        $query = $this->db->query('SELECT `cityName` FROM `tbl_city` WHERE stateId = "' . $stateId . '"');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function check_login($where)
    {
        $query = $this->db->get_where('tbl_admin', $where);
        if ($query->num_rows()) {
            $result = $query->result_array();

            //To update last login information
            date_default_timezone_set('Asia/Calcutta');
            $lastLogin = array('lastLogin' => date('Y-m-d h:i:s'));
            $this->db->update('tbl_admin', $lastLogin, $where);

            return $result;
        } else {
            return false;
        }
    }

    function changepassword()
    {
        $where = array('pass' => $_POST['oldpassword']);
        $data = array('pass' => $_POST['newpassword']);
        $this->db->update('tbl_admins', $data, $where);
        return TRUE;;
    }

    function check_pasword($passwd)
    {
        $data = $this->db->get_where('tbl_admins', array('pass' => $passwd))->result_array();
        return $data;
    }

    // Delete User
    public function delete($userId)
    {
        $this->db->delete('tbl_admin', array('memberId' => $userId));

        $res = $this->db->delete($this->tablename, array('memberId' => $userId));

        if ($res) {
            return TRUE;
        }
    }


}