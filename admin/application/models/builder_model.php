<?php

class Builder_model extends CI_Model
{
    private $tablename;

    function __construct()
    {
        $this->tablename = 'tbl_builder';
        parent::__construct();
    }

    // Get all builders details
    function get_builder($where = '')
    {
        $this->load->database();
        if (empty($where)) {
            $query = $this->db->get($this->tablename);
        } else {
            $query = $this->db->get_where($this->tablename, $where);
        }

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    // Get specific builder's details by builderId
    function get_detail($builderId = '')
    {
        $where = array('builderId' => $builderId);
        return $this->db->get_where($this->tablename, $where)->result_array();
    }

    // Add New Builder
    public function add($data)
    {
        $res = $this->db->insert($this->tablename, $data);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Update Builder Details
    public function update($data, $where)
    {
        $res = $this->db->update($this->tablename, $data, $where);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Delete Builder
    public function delete($builderId)
    {
        $where = array('builderId' => $builderId);
        $res = $this->db->delete($this->tablename, $where);
        if ($res) {
            return TRUE;
        }
    }

    // Update status of Builder
    function changeStatus($id, $status)
    {
        if (isset($status) && $status == 1) {
            $changeStatus = 0;
        }
        if (isset($status) && $status == 0) {
            $changeStatus = 1;
        }
        $data = array('status' => $changeStatus);
        $where = array('builderId' => $id);

        $result = $this->db->update($this->tablename, $data, $where);

        return $result;
    }
}