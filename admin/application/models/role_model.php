<?php

class Role_model extends CI_Model
{
    private $tablename;

    function __construct()
    {
        $this->tablename = 'tbl_members_roles';
        parent::__construct();
    }


    function get_role($where = '')
    {
        $this->load->database();
        if (empty($where)) {
            $query = $this->db->get($this->tablename);
        } else {
            $query = $this->db->get_where($this->tablename, $where);
        }

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function changeStatus($id, $status)
    {

        if (isset($status) && $status == "enabled") {
            $changeStatus = "disabled";
        }
        if (isset($status) && $status == "disabled") {
            $changeStatus = "enabled";
        }
        $data = array('rolePermission' => $changeStatus);
        $where = array('roleId' => $id);

        $result = $this->db->update($this->tablename, $data, $where);

        //Update Session data as well
        $activeRoles = $this->get_role(array('rolePermission' => 'Enabled'));
        $this->session->set_userdata('activeRoles', $activeRoles);

        return $this->db->get_where($this->tablename, array('roleId' => $id))->result_array();
    }


    function get_detail($roleId = '')
    {
        $where = array('roleId' => $roleId);
        return $this->db->get_where($this->tablename, $where)->result_array();
    }

    //Update Role
    public function update($data, $where)
    {
        $res = $this->db->update($this->tablename, $data, $where);
        if ($res) {
            //Update Session data as well
            $activeRoles = $this->get_role(array('rolePermission' => 'Enabled'));
            $this->session->set_userdata('activeRoles', $activeRoles);

            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Insert Role
    public function add($data)
    {
        $res = $this->db->insert($this->tablename, $data);
        if ($res) {
            //Update Session data as well
            $activeRoles = $this->get_role(array('rolePermission' => 'Enabled'));
            $this->session->set_userdata('activeRoles', $activeRoles);

            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Delete Role
    public function delete($roleId)
    {
        $where = array('roleId' => $roleId);
        $res = $this->db->delete($this->tablename, $where);
        if ($res) {
            //Update Session data as well
            $activeRoles = $this->get_role(array('rolePermission' => 'Enabled'));
            $this->session->set_userdata('activeRoles', $activeRoles);

            return TRUE;
        }
    }

}