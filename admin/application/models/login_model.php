<?php

class Login_model extends CI_Model
{
    private $tablename;

    function __construct()
    {
        $this->tablename = 'tbl_member';
        parent::__construct();
        $this->load->database();
    }

    function check_login($where)
    {
        $query = $this->db->get_where($this->tablename, $where);
        //echo  'test<pre>';print_r($query);die();
        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function changepassword()
    {
        $where = array('pass' => $_POST['oldpassword']);
        $data = array('pass' => $_POST['newpassword']);
        $this->db->update($this->tablename, $data, $where);
        return TRUE;;
    }

    public function check_pasword($passwd)
    {
        $data = $this->db->get_where($this->tablename, array('pass' => $passwd))->result_array();
        return $data;
    }
}