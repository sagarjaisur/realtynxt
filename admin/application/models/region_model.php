<?php

class Region_model extends CI_Model
{
//    private $tablename;

    function __construct()
    {
//        $tableName = 'tbl_member';

        parent::__construct();
        $this->load->database();
    }

    function addRegion($tableName, $data)
    {
        $res_in = $this->db->insert($tableName, $data);

        if ($res_in) {
            return true;
        } else {
            return false;
        }
    }


    function getRegionsRequest($limit, $start, $where = '', $tableName, $id, $joinTable = '', $column)
    {
        if (empty($where)) {
            $this->db->select('*');
            $this->db->order_by($id);
            $this->db->from($tableName);
            $this->db->limit($limit, $start);
            $query = $this->db->get();
        } elseif (!empty($joinTable)) {
            $this->db->select("$tableName.*,$joinTable.$column");
            $this->db->order_by($id);
            $this->db->from($tableName);
//            $this->db->where($where);
            $this->db->join($joinTable, $where);
            $this->db->limit($limit, $start);
            $query = $this->db->get();
        }

        /*
        $this->db->select('*');
        $this->db->order_by('memberId','desc');
        $this->db->from($tableName);
        $query = $this->db->get();
        */
        if ($query->num_rows()) {
            return $query->result_array();

        } else {
            return false;
        }
    }

    //  Get Count  Of Recodes
    function recode_count($tableName, $id)
    {
        $this->db->select('*');
        $this->db->from($tableName);
        $query = $this->db->get();
        if ($query->num_rows()) {
            return count($query->result_array());
        } else {
            return false;
        }
    }

    function get_Region($tableName, $limit, $start, $where = '')
    {
        $this->db->select('*');

        if (!empty($where)) {
            $this->db->where($where);
        }

//        $this->db->order_by('id', 'desc');
//        $this->db->order_by('id');
        $this->db->from($tableName);
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

//  function get_Region_Record($id,$column_name){
    function get_Region_Record($id)
    {
        if ($id > 0) {
//          $this->db->select($column_name);
            $this->db->where('memberId', $id);
            $this->db->from($tableName);
            $query = $this->db->get();
            if ($query->num_rows()) {
                return $query->result_array();
            } else {
                return false;
            }
        }
    }


    public function getRegionById($id)
    {
//      $this->db->select('m.*,pm.addressId,pm.address1 as "pickup_street",pm.address1 as "pickup_city",pm.suburb as "pickup_suburb",pm.country as "pickup_country",pm.state as "pickup_state",pm.pincode as "pickup_zip"');
//      $this->db->select('m.*,pm.addressId,pm.address1 as "pickup_street",pm.address1 as "pickup_city",pm.suburb as "pickup_suburb",pm.country as "pickup_country",pm.state as "pickup_state",pm.pincode as "pickup_zip"');
        $this->db->where('m.memberId', $id);
        $this->db->from($tableName . ' as m');
//      $this->db->join('tbl_member_pickup_address as pm','m.memberId = pm.memberId','left');
        $query = $this->db->get();


        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function changeRegionStatus($regionId, $status, $tableName, $id)
    {
        if (isset($status) && $status == 1) {
            $changeStatus = 0;
        }
        if (isset($status) && $status == 0) {
            $changeStatus = 1;
        }
        $data = array('status' => $changeStatus);
        $where = array($id => $regionId);

        return $this->db->update($tableName, $data, $where);

        //return $this->db->get_where($tableName,array('iRegionId' => $regionId))->result_array();
    }

    function update($data, $where, $tableName)
    {
//        $this->db->where('memberId', $uid);
        $result = $this->db->update($tableName, $data, $where);
        $affectedRows = $this->db->affected_rows();
        if ($affectedRows > 0) {
            return $result;
        } else {
            return false;
        }
    }

    function checkForUniqueRegionName($str = '', $uid = '')
    {
        $this->db->select('*');
        $this->db->where('webname', $str);
        if ($uid != '') {
            $this->db->where('uid != "' . $uid . '"');
        }
        $this->db->from('tbl_member');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $str = $str . rand(1, 30);
            $this->checkForUniqueWsername($str, $uid);
        } else {
            echo $str;
        }
    }

    function checkForState($countryId)
    {
        $query = $this->db->query('SELECT `state`,`title` FROM `tbl_state` WHERE country = "' . $countryId . '"');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function get_detail($where = '', $tableName, $id, $joinTable = '', $column)
    {
        if (empty($where)) {
            $this->db->select('*');
            $this->db->from($tableName);
            $query = $this->db->get();
        } elseif (!empty($joinTable)) {
            $this->db->select("$tableName.*,$joinTable.$column");
            $this->db->from($tableName);
            $this->db->join($joinTable, $where);
            $query = $this->db->get();
        }

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }


    // Delete Region
    public function delete($regionId, $tableName, $id)
    {
        if ($tableName == 'tbl_area') {
            $res = $this->db->delete($tableName, array($id => $regionId));
        } elseif ($tableName == 'tbl_city') {
            $this->db->delete($tableName, array($id => $regionId));
            $res = $this->db->delete('tbl_area', array($id => $regionId));
        }

        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}