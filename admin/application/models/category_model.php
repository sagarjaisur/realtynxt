<?php

class Category_model extends CI_Model
{
    private $tablename;

    function __construct()
    {
        $this->tablename = 'tbl_business_category';
        parent::__construct();
    }


    function get_category($where = '')
    {
        $this->load->database();
        if (empty($where)) {
            $query = $this->db->get($this->tablename);
        } else {
            $query = $this->db->get_where($this->tablename, $where);
        }

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function changeStatus($id, $status)
    {
        if (isset($status) && $status == 1) {
            $changeStatus = 0;
        }
        if (isset($status) && $status == 0) {
            $changeStatus = 1;
        }
        $data = array('status' => $changeStatus);
        $where = array('id' => $id);

        $result = $this->db->update($this->tablename, $data, $where);

        return $result;
    }


    function get_detail($id = '')
    {
        $where = array('id' => $id);
        return $this->db->get_where($this->tablename, $where)->result_array();
    }

    //Update Category
    public function update($data, $where)
    {
        $res = $this->db->update($this->tablename, $data, $where);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Insert Category
    public function add($data)
    {
        $res = $this->db->insert($this->tablename, $data);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Delete Category
    public function delete($id)
    {
        $where = array('id' => $id);
        $res = $this->db->delete($this->tablename, $where);
        if ($res) {
            return TRUE;
        }
    }

}