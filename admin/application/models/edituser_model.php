<?php

class Edituser_model extends CI_Model
{
    private $tablename;

    function __construct()
    {
        $this->tablename = 'tbl_member';
        parent::__construct();
        $this->load->database();
    }

    function getUsersRequest($limit, $start, $where = '')
    {

        if (empty($where)) {
            $this->db->select('*');
            $this->db->order_by('memberId', 'desc');
            $this->db->from($this->tablename);
            $this->db->limit($limit, $start);
            $query = $this->db->get();
        } else {
            $this->db->select('*');
            $this->db->order_by('memberId', 'desc');
            $this->db->from($this->tablename);
            $this->db->where($where);
            $this->db->limit($limit, $start);
            $query = $this->db->get();
        }

        /*
        $this->db->select('*');
        $this->db->order_by('memberId','desc');
        $this->db->from($this->tablename);
        $query = $this->db->get();
        */
        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //  Get Count  Of Recodes
    function recode_count()
    {
        $this->db->select('*');
        $this->db->from($this->tablename);
        $query = $this->db->get();
        if ($query->num_rows()) {
            return count($query->result_array());
        } else {
            return false;
        }
    }

    function get_User($limit, $start, $where = '')
    {
        $this->db->select('*');
        //$this->db->where('user_type',$user_typeId);
        $this->db->order_by('memberId', 'desc');
        $this->db->from($this->tablename);
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function get_User_Record($id, $column_name)
    {
        if ($id > 0) {
            $this->db->select($column_name);
            $this->db->where('memberId', $id);
            $this->db->from($this->tablename);
            $query = $this->db->get();
            if ($query->num_rows()) {
                return $query->result_array();
            } else {
                return false;
            }
        }
    }

    function getGuestUser()
    {
        $this->db->select('*');
        $this->db->where('user_type', '2');
        $this->db->order_by('memberId', 'desc');
        $this->db->from($this->tablename);
        $query = $this->db->get();


        //echo $this->db->last_query();exit;
        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getMemberUser()
    {
        $this->db->select('*');
        $this->db->where('user_type', '1');
        $this->db->order_by('memberId', 'desc');
        $this->db->from($this->tablename);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getAdminUserById($id)
    {


        $condition = array('m.memberId' => $id);
        $this->db->select('m.*,pm.addressId,pm.address1 as "pickup_street",pm.address1 as "pickup_city",pm.suburb as "pickup_suburb",pm.country as "pickup_country",pm.state as "pickup_state",pm.pincode as "pickup_zip"');
        $this->db->where($condition);
        $this->db->from($this->tablename . ' as m');
        $this->db->join('tbl_member_pickup_address as pm', 'm.memberId = pm.memberId', 'left');
        $query = $this->db->get();
        //echo $this->db->last_query();

        // exit;


        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getUserById
    ($id)
    {
        $this->db->select('m.*,pm.addressId,pm.address1 as "pickup_street",pm.address1 as "pickup_city",pm.suburb as "pickup_suburb",pm.country as "pickup_country",pm.state as "pickup_state",pm.pincode as "pickup_zip"');
        $this->db->where('m.memberId', $id);
        $this->db->from($this->tablename . ' as m');
        $this->db->join('tbl_member_pickup_address as pm', 'm.memberId = pm.memberId', 'left');
        $query = $this->db->get();


        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }


    function changeUserStatus($userId, $status)
    {
        $date = date('Y-m-d H:i:s');
        if ($status == '1')
            $col = 'date_of_approve';
        else if ($status == '2')
            $col = 'date_of_reject';

        $data = array('status' => $status, $col => $date);
        $where = array('memberId' => $userId);

        return $this->db->update($this->tablename, $data, $where);

        //return $this->db->get_where($this->tablename,array('iUserId' => $userId))->result_array();
    }


    function updateMemberDetails($id, $data)
    {
        $this->db->where('memberId', $id);
        $result = $this->db->update($this->tablename, $data);
        $afftectedRows = $this->db->affected_rows();
        if ($afftectedRows > 0) {
            return $result;
        } else {
            return false;
        }
    }

    function updatePickupAddress($id, $data)
    {
        $this->db->select('memberId');
        $this->db->where('memberId', $id);
        $this->db->from('tbl_member_pickup_address');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $this->db->where('memberId', $id);
            $res = $this->db->update('tbl_member_pickup_address', $data);
        } else {
            $data['memberId'] = $id;
            $res_in = $this->db->insert('tbl_member_pickup_address', $data);
            if ($res_in) {
                return true;
            } else {
                return false;
            }
        }
    }

    function checkForUniquewsername($str)
    {
        $this->db->select('*');
        $this->db->where('webname', $str);
        $this->db->from('tbl_member');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $str = $str . rand(1, 30);
            $this->checkForUniqueWsername($str);
        } else {
            echo $str;
        }
    }

    function checkForNomineeList($str)
    {
        //$query = $this->db->query('SELECT webname FROM `tbl_member` WHERE webname LIKE "%'.$str.'%"');
        $query = $this->db->query('SELECT webname FROM `tbl_member` WHERE webname = "' . $str . '"');

        if ($query->num_rows() > 0) {
            //return $query->result_array();
            return $query->num_rows();
        } else {
            return false;
        }
    }

    function checkForState($countryId)
    {
        $query = $this->db->query('SELECT `state`,`title` FROM `tbl_state` WHERE country = "' . $countryId . '"');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

}

?>