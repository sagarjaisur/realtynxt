<?php

class OsTestimonial_model extends CI_Model
{
    private $tablename;

    function __construct()
    {
        $this->tablename = 'tbl_os_testimonial';
        parent::__construct();
    }

    function getAllOsTestimonial($limit, $start, $where = '')
    {
        $this->load->database();
        if (empty($where)) {
            $this->db->from($this->tablename);
            $this->db->limit($limit, $start);
            $this->db->order_by("testimonialId", "desc");
            $query = $this->db->get();
        } else {
            $this->db->from($this->tablename);
            $this->db->where($where);
            $this->db->limit($limit, $start);
            $this->db->order_by("testimonialId", "desc");
            $query = $this->db->get();
        }

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //  Get Count  Of Recodes
    function recode_count()
    {
        $this->db->select('*');
        $this->db->from($this->tablename);
        $query = $this->db->get();
        if ($query->num_rows()) {
            return count($query->result_array());
        } else {
            return false;
        }
    }

    function changeStatus($id, $status)
    {
        $changeStatus = $status ? 0 : 1;
        $data = array('testimonialStatus' => $changeStatus);
        $where = array('testimonialId' => $id);
        $this->db->update($this->tablename, $data, $where);
        return $this->db->get_where($this->tablename, array('testimonialId' => $id))->result_array();
    }

    //Get Single Testimonial Details
    function getOsTestimonialById($id = '')
    {
        $where = array('testimonialId' => $id);
        return $this->db->get_where($this->tablename, $where)->result_array();
    }

    //Update Testimonial
    public function update($data, $where)
    {
        $res = $this->db->update($this->tablename, $data, $where);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Insert Testimonial
    public function add($data)
    {
        $res = $this->db->insert($this->tablename, $data);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Delete Testimonial
    public function delete($id)
    {
        $where = array('testimonialId' => $id);
        $res = $this->db->delete($this->tablename, $where);
        if ($res) {
            return true;
        }
    }
}