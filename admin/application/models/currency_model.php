<?php

class Currency_model extends CI_Model
{
    private $tablename;

    function __construct()
    {
        $this->tablename = 'tbl_currency';
        parent::__construct();
    }

    // Get all currencys details
    function get_currency($where = '')
    {
        $this->load->database();
        if (empty($where)) {
            $query = $this->db->get($this->tablename);
        } else {
            $query = $this->db->get_where($this->tablename, $where);
        }

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    // Get specific currency's details by currencyId
    function get_detail($currencyId = '')
    {
        $where = array('currencyId' => $currencyId);
        return $this->db->get_where($this->tablename, $where)->result_array();
    }

    // Add New Currency
    public function add($data)
    {
        $res = $this->db->insert($this->tablename, $data);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Update Currency Details
    public function update($data, $where)
    {
        $res = $this->db->update($this->tablename, $data, $where);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Delete Currency
    public function delete($currencyId)
    {
        $where = array('currencyId' => $currencyId);
        $res = $this->db->delete($this->tablename, $where);
        if ($res) {
            return TRUE;
        }
    }

    // Update status of Currency
    function changeStatus($id, $status)
    {
        if (isset($status) && $status == 1) {
            $changeStatus = 0;
        }
        if (isset($status) && $status == 0) {
            $changeStatus = 1;
        }
        $data = array('status' => $changeStatus);
        $where = array('currencyId' => $id);

        $result = $this->db->update($this->tablename, $data, $where);

        return $result;
    }
}