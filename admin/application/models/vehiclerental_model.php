<?php

class Vehiclerental_model extends CI_Model
{
    private $tablename;

    function __construct()
    {
        $this->tablename = 'tbl_vehicle_rental';
        parent::__construct();
    }


    function get_property($where = '')
    {
        $this->load->database();
        if (empty($where)) {
            $query = $this->db->get($this->tablename);
        } else {
            $query = $this->db->get_where($this->tablename, $where);
        }

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function changeStatus($propertyId, $status)
    {
        $data = array('status' => $status);
        $where = array('id' => $propertyId);

        $result = $this->db->update($this->tablename, $data, $where);

        return $result;
//        return $this->db->get_where($this->tablename, array('id' => $propertyId))->result_array();
    }


    function get_detail($id = '')
    {
        $where = array('id' => $id);
        return $this->db->get_where($this->tablename, $where)->result_array();
    }

    //Update Property
    public function update($data, $where)
    {
        $res = $this->db->update($this->tablename, $data, $where);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Insert Property
    public function add($data)
    {
        $res = $this->db->insert($this->tablename, $data);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Delete Property
    public function delete($id)
    {
        $where = array('id' => $id);
        $res = $this->db->delete($this->tablename, $where);
        if ($res) {
            return TRUE;
        }
    }

    function checkForArea($cityId)
    {
        $query = $this->db->query('SELECT `areaName` FROM `tbl_area` WHERE cityId = "' . $cityId . '"');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
}