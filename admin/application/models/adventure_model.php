<?php

class Adventure_model extends CI_Model
{
    private $tablename;

    function __construct()
    {
        $this->tablename = 'tbl_adventure';
        parent::__construct();
    }

    // Get all adventures details
    function get_adventure($where = '')
    {
        $this->load->database();
        if (empty($where)) {
            $query = $this->db->get($this->tablename);
        } else {
            $query = $this->db->get_where($this->tablename, $where);
        }

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    // Get all adventures types
    function get_adventure_types($where = '')
    {
        if (empty($where)) {
            $query = $this->db->query('SELECT ChildUserType.id, ChildUserType.eventName, ChildUserType.parentId, ChildUserType.status, ParentUserType.eventName AS adventureType FROM tbl_adventure_type AS ChildUserType LEFT JOIN tbl_adventure_type AS ParentUserType ON ChildUserType.parentId = ParentUserType.id');
        } else {
            $query = $this->db->get_where('tbl_adventure_type', $where);
        }

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    // Get specific adventure's details by adventureId
    function get_detail($adventureId = '')
    {
        $where = array('adventureId' => $adventureId);
        return $this->db->get_where($this->tablename, $where)->result_array();
    }

    // Add New Adventure
    public function add($data)
    {
        $res = $this->db->insert($this->tablename, $data);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Update Adventure Details
    public function update($data, $where)
    {
        $res = $this->db->update($this->tablename, $data, $where);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Delete Adventure
    public function delete($adventureId)
    {
        $where = array('adventureId' => $adventureId);
        $res = $this->db->delete($this->tablename, $where);
        if ($res) {
            return TRUE;
        }
    }

    // Update status of Adventure
    function changeStatus($id, $status)
    {
        if (isset($status) && $status == 1) {
            $changeStatus = 0;
        }
        if (isset($status) && $status == 0) {
            $changeStatus = 1;
        }
        $data = array('status' => $changeStatus);
        $where = array('adventureId' => $id);

        $result = $this->db->update($this->tablename, $data, $where);

        return $result;
    }


    // Manage Adventure Type
    // Get specific adventure type's details by id
    function get_detail_type($id = '')
    {
        $where = array('id' => $id);
        return $this->db->get_where('tbl_adventure_type', $where)->result_array();
    }

    // Add New Adventure Type
    public function addType($data)
    {
        $res = $this->db->insert('tbl_adventure_type', $data);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Update Adventure Type
    public function updateType($data, $where)
    {
        $res = $this->db->update('tbl_adventure_type', $data, $where);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Delete Adventure Type
    public function deleteType($id, $isParent)
    {
        if ($isParent) {
            $where = "id = $id || parentId = $id";
        } else {
            $where = "id = $id";
        }

        $res = $this->db->delete('tbl_adventure_type', $where);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Update status of Adventure Type
    function changeStatusType($id, $status, $isParent)
    {
        if (isset($status) && $status == 1) {
            $changeStatus = 0;
        }
        if (isset($status) && $status == 0) {
            $changeStatus = 1;
        }
        $data = array('status' => $changeStatus);

        if ($isParent) {
            $where = "id = $id || parentId = $id";
        } else {
            $where = "id = $id";
        }

        $result = $this->db->update('tbl_adventure_type', $data, $where);

        return $result;
    }


    // Manage Event Name
    // Add New Event Name
    public function addEventName($data)
    {
        $res = $this->db->insert('tbl_adventure_type', $data);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}