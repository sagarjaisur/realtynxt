<?php

class User_model extends CI_Model
{
    private $tablename;

    function __construct()
    {
        $this->tablename = 'login';
        parent::__construct();
        $this->load->database();
    }

    function get_users($limit, $start, $search = '')
    {
        $this->db->select('*');
        if (!empty($search)) {
            $this->db->or_like('vFirst_name', $search);
            $this->db->or_like('vLast_name', $search);
            $this->db->or_like('vPhone_no', $search);
            $this->db->or_like('vEmailId', $search);
            $this->db->or_like('country', $search);
            $this->db->or_like('state', $search);
            $this->db->or_like('city', $search);
        }
        if (!empty($limit)) {
            $this->db->limit($limit, $start);
        }

        $this->db->from($this->tablename);
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function changeStatus($userId, $status)
    {
        $data = array('eStatus' => $status);
        $where = array('iUserId' => $userId);
        $this->db->update($this->tablename, $data, $where);

        return $this->db->get_where($this->tablename, array('iUserId' => $userId))->result_array();
    }

    function record_count($search = '')
    {
        $this->db->select('*');
        if (!empty($search)) {
            $this->db->or_like('vFirst_name', $search);
            $this->db->or_like('vLast_name', $search);
            $this->db->or_like('vPhone_no', $search);
            $this->db->or_like('vEmailId', $search);
            $this->db->or_like('country', $search);
            $this->db->or_like('state', $search);
            $this->db->or_like('city', $search);
        }

        $this->db->from($this->tablename);
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }
}