jQuery(document).ready(function () {

    jQuery('#datepicker').datepicker({
        "format": "yyyy-mm-dd",
        "autoclose": true,
        "startDate": '+1d'
    });

    jQuery('.showother').click(function () {
        jQuery('.full_details').addClass(' hide');
        jQuery('.other').removeClass('hide');
    });

    jQuery('.showfull').click(function () {
        jQuery('.other').addClass(' hide');
        jQuery('.full_details').removeClass('hide');
    });
    jQuery('body').on('click', '#same_as_postal', function () {
        if (this.checked) {
            $("#pickup_state").text('');

            $('.pickup_street').val($('.postal_address1').val());
            $('.pickup_city').val($('.postal_address2').val());
            $('.pickup_suburb').val($('.postal_suburb').val());
            $(".pickup_country").val($('.postal_country').val());

            var stateName = $('#member_state').find(":selected").text();
            var stateValue = $('#member_state').find(":selected").attr('value');
            $('#pickup_state').append('<option value="' + stateValue + '">' + stateName + '</option>');

            $('.zip1').val($('.zip').val());
        } else {
            $('.pickup_street').val('');
            $('.pickup_city').val('');
            $('.pickup_suburb').val('');
            $(".pickup_country").val('');
            $(".pickup_state").val('');
            $('.zip1').val('');
        }
    });

    jQuery('body').on('blur', '.lname', function () {
        jQuery('.webname').val(jQuery('.fname').val() + ' ' + jQuery('.lname').val());
    });

    // For Webname functionality in Member registration

    jQuery('body').on('blur', '.webname', function () {
        var text = $(this).val();
        var weburl = $('#ajax_url').val();
        // to avoid empty webname
        if (text == '') {
            text = $('.fname').val();
        }

        if (text != '') {
            $.ajax({
                url: weburl + 'checkForWebname',
                type: "POST",
                data: {text: text},
                success: function (data) {
                    if (data == text) {
                        $('.webname-suggetion').html('');
                        return true;
                    } else {
                        $('.webname').validationEngine('showPrompt', '*Please choose another Webname.', 'fail');
                        $('.webname').val('');
                        $('.webname-suggetion').html('Suggestion : ' + data);
                        return false;
                    }
                }
            });
        }
    });

    //jQuery("#edit_user").validationEngine();

    //Start Country - State management

    jQuery('#member_state').click('on', function () {
        // For get selected option text
        var countryName = jQuery('#member_country').find(":selected").text();
        // For get selected value
        var countryValue = jQuery('#member_country').find(":selected").attr('value');
        // If country not selected
        if (!countryValue || countryValue == '') {
            jQuery('#member_state').text('');
            jQuery('#member_state').append('<option selected="selected" value="">Please Select Country</option>');
        }
    });

    jQuery('#member_country').change('on', function () {
        jQuery('#member_state').text('');
        var state_url = jQuery('#state_url').val();
        var val_country = jQuery(this).val();

        if (val_country != '') {
            jQuery.ajax({
                url: state_url + 'stateFind',
                type: "POST",
                dataType: "json",
                data: {countryId: val_country},
                success: function (data) {
                    if (data && data != 'error') {
                        jQuery('#member_state').text('');
                        jQuery('#member_state').append('<option selected="selected" value="">Select State</option>');
                        jQuery.each(data, function (i, v) {
                            jQuery('#member_state').append('<option value="' + v.state + '">' + v.title + '</option>');
                        });
                    } else {
                        jQuery('#member_state').append('<option selected="selected" value="">No state exists.</option>');
                    }
                }
            });
        } else {
            jQuery('#member_state').append('<option selected="selected" value="">Please Select Country</option>');
        }
    });

    //On City Change, Display areas accordingly
    jQuery('#property_city').change('live', function () {

        jQuery('#property_area').text('');
        var area_url = jQuery('#area_url').val();
        var val_city = jQuery(this).val();

        if (val_city != '') {
            jQuery.ajax({
                url: area_url + 'areaFind',
                type: "POST",
                dataType: "json",
                data: {cityName: val_city},
                success: function (data) {
                    if (data && data != 'error') {
                        jQuery('#property_area').text('');

                        jQuery('#property_area').append('<option selected="selected" value="">Select Area</option>');
                        jQuery.each(data, function (i, v) {
                            jQuery('#property_area').append('<option value="' + v.areaName + '">' + v.areaName + '</option>');
                        });
                    } else {
                        jQuery('#property_area').append('<option selected="selected" value="">No area exists.</option>');
                    }
                }
            });
        } else {
            jQuery('#property_area').append('<option selected="selected" value="">Please Select City</option>');
        }
    });


    //On State Change, Display Cities accordingly
    jQuery('#user_state').change('live', function () {

        jQuery('#user_city').text('');
        var city_url = jQuery('#city_url').val();
        var val_state = jQuery(this).val();

        if (val_state != '') {
            jQuery.ajax({
                url: city_url + 'cityFind',
                type: "POST",
                dataType: "json",
                data: {stateName: val_state},
                success: function (data) {
                    if (data && data != 'error') {
                        jQuery('#user_city').text('');

                        jQuery('#user_city').append('<option selected="selected" value="">Select City</option>');
                        jQuery.each(data, function (i, v) {
                            jQuery('#user_city').append('<option value="' + v.cityName + '">' + v.cityName + '</option>');
                        });
                    } else {
                        jQuery('#user_city').append('<option selected="selected" value="">No city exists.</option>');
                    }
                }
            });
        } else {
            jQuery('#user_city').append('<option selected="selected" value="">Please Select State</option>');
        }
    });

    //On Adventure Type Change, Display Event Names accordingly
    jQuery('#adventureType').change('live', function () {

        jQuery('#eventName').text('');
        var eventName_url = jQuery('#eventName_url').val();
        var val_adventureType = jQuery(this).val();

        if (val_adventureType != '') {
            jQuery.ajax({
                url: eventName_url + 'eventNameFind',
                type: "POST",
                dataType: "json",
                data: {adventureType: val_adventureType},
                success: function (data) {
                    if (data && data != 'error') {
                        jQuery('#eventName').text('');

                        jQuery('#eventName').append('<option selected="selected" value="">Select Event Name</option>');
                        jQuery.each(data, function (i, v) {
                            jQuery('#eventName').append('<option value="' + v.eventName + '">' + v.eventName + '</option>');
                        });
                    } else {
                        jQuery('#eventName').append('<option selected="selected" value="">No Event exists.</option>');
                    }
                }
            });
        } else {
            jQuery('#eventName').append('<option selected="selected" value="">Please Select Adventure Type</option>');
        }
    });


    // End Country State
    // For Pick UP address functionality
    jQuery('#pickup_state').click('on', function () {
        // For get selected option text
        var countryName = jQuery('#pickup_country').find(":selected").text();
        // For get selected value
        var countryValue = jQuery('#pickup_country').find(":selected").attr('value');
        // If country not selected
        if (!countryValue || countryValue == '') {
            jQuery('#pickup_state').text('');
            jQuery('#pickup_state').append('<option selected="selected" value="">Please Select Country</option>');
        }
    });

    jQuery('#pickup_country').change('on', function () {
        jQuery('#pickup_state').text('');
        var state_url = jQuery('#state_url').val();
        var val_country = jQuery(this).val();

        if (val_country != '') {
            jQuery.ajax({
                url: state_url + 'stateFind',
                type: "POST",
                dataType: "json",
                data: {countryId: val_country},
                success: function (data) {
                    if (data && data != 'error') {
                        jQuery('#pickup_state').text('');
                        jQuery('#pickup_state').append('<option selected="selected" value="">Select State</option>');
                        jQuery.each(data, function (i, v) {
                            jQuery('#pickup_state').append('<option value="' + v.state + '">' + v.title + '</option>');
                        });
                    } else {
                        jQuery('#pickup_state').append('<option selected="selected" value="">No state exists.</option>');
                    }
                }
            });
        } else {
            jQuery('#pickup_state').append('<option selected="selected" value="">Please Select Country</option>');
        }
    });
    // End Pickup country state
});

// For Image upload functionality in Member registration
jQuery(document).ready(function () {

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                var imageResult = e.target.result;
                var old_image = jQuery('#upload_image').attr('src');

                if (old_image) {
                    jQuery('#imagediv').html('');
                    var img = jQuery('<img class="upload_side_img2" id="upload_image">').attr({
                        src: imageResult,
                    });
                    var btn = jQuery('<input type="button" class="remove_upload  btn btn-success btn-md submit_btn" id="remove_upload">');
                    img.appendTo('#imagediv');
                    btn.appendTo('#imagediv');
                } else {
                    var img = jQuery('<img class="upload_side_img2" id="upload_image">').attr({
                        src: imageResult,
                    });
                    var btn = jQuery('<input type="button" class="remove_upload btn btn-success btn-md submit_btn" id="remove_upload" value="Remove">');

                    img.appendTo('#imagediv');
                    btn.appendTo('#imagediv');
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    jQuery("#imgMember").change("on", function () {

        var filename = jQuery(this).val();
        var extension = filename.replace(/^.*\./, '');

        extension = extension.toLowerCase();

        if (extension == "jpg" || extension == "jpeg" || extension == "png") {
            $('#imgMember').validationEngine('hide');
            $('#imagediv').text('');
            readURL(this);
        } else {
            $('#imagediv').text('');
            $('#imgMember').validationEngine('showPrompt', 'Invalid File Type.', 'fail');
        }
    });

    jQuery("body").on("click", "#remove_upload", function () {
        jQuery('#imagediv').html('');
        jQuery('#imagediv').html('');
        jQuery('#imagediv').val('');
        jQuery('#imagediv').text('');
        jQuery('#imgMember').val('');
    });

    // Auto Search functionality for Nominee

    jQuery("#booking_responsible_person").change('on', function () {
        var weburl = jQuery('#ajax_url').val();
        var current_field = jQuery(this).val();

        jQuery.ajax({
            url: weburl + 'checkForNominee',
            type: "POST",
            data: 'keyword=' + jQuery(this).val(),
            success: function (data) {
                if (data == 'error') {
                    jQuery('#booking_responsible_person').validationEngine('showPrompt', 'This Nominee is not valid.', 'fail');
                } else if (data == 'success') {
                    var brp = jQuery('#booking_responsible_person1').val();

                    if (brp != '' && brp == current_field) {
                        jQuery('#booking_responsible_person').val('');
                        jQuery('#booking_responsible_person').validationEngine('showPrompt', 'Please choose another Nominee.', 'fail');
                    } else if (brp == '' && current_field != '') {
                        jQuery('#booking_responsible_person').validationEngine('hide');
                    } else {
                        jQuery('#booking_responsible_person').validationEngine('hide');
                    }
                }

            }
        });
    });

    jQuery("#booking_responsible_person1").change('on', function () {
        var weburl = jQuery('#ajax_url').val();
        var current_field = jQuery(this).val();

        jQuery.ajax({
            url: weburl + 'checkForNominee',
            type: "POST",
            data: 'keyword=' + jQuery(this).val(),
            success: function (data) {
                if (data == 'error') {
                    jQuery('#booking_responsible_person1').validationEngine('showPrompt', 'This Nominee is not valid.', 'fail');
                } else if (data == 'success') {
                    var brp = jQuery('#booking_responsible_person').val();

                    if (brp != '' && brp == current_field) {
                        jQuery('#booking_responsible_person1').val('');
                        jQuery('#booking_responsible_person1').validationEngine('showPrompt', 'Please choose another Nominee.', 'fail');
                    } else if (brp == '' && current_field != '') {
                        jQuery('#booking_responsible_person').validationEngine('hide');
                    } else {
                        jQuery('#booking_responsible_person1').validationEngine('hide');
                    }
                }
            }
        });
    });

    jQuery("body").on("change", "#booking_responsible_person1", function () {
        var brp = jQuery('#booking_responsible_person').val();
        var current_field = jQuery(this).val();
        if (brp != '' && brp == current_field) {
            jQuery('#booking_responsible_person1').val('');
            jQuery('#booking_responsible_person1').validationEngine('showPrompt', 'Please choose another Nominee.', 'fail');
        }
    });

    jQuery("body").on("change", "#booking_responsible_person", function () {
        var brp = jQuery('#booking_responsible_person1').val();
        var current_field = jQuery(this).val();
        if (brp != '' && brp == current_field) {
            jQuery('#booking_responsible_person').val('');
            jQuery('#booking_responsible_person').validationEngine('showPrompt', 'Please choose another Nominee.', 'fail');
        }
    });
});