// On Delete Button Click
function deleteRecode(url, caption) {
    if (confirm("Are you sure you want to delete this " + caption + "?")) {
        window.location.href = url;
    }
    else {
        return false;
    }
}
