jQuery(function($) {
    "use strict";

    $('.ale_demopreview_container .close-preview .icon').on('click',function(){
       $('.ale_demopreview_container').fadeOut(300);
        $('.show-modal-demo-selector').fadeIn(300);
        $.cookie('demopreview', 'yes', { path: '/' });
    });


    $('.ale_demopreview_container .body').jScrollPane({
        animateScroll:true,
        autoReinitialise : true
    });

    $('.show-modal-demo-selector').on('click',function(){
        $('.ale_demopreview_container').fadeIn(300);
        $(this).fadeOut(0);
    });
});
