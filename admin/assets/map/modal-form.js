jQuery(function($) {
    "use strict";

    //Hide loading gif
    $(".ale-ajax").hide();

    //Submit the form
    $("#ale_modal_form" ).submit(function(event) {

        //Show loading gif
        $(".ale-ajax").show();
        var posting = $.post( ale.ajax_load_url, $("#ale_modal_form :input").serialize() )
            .done(function() {

                //hide loading and show success
                $( ".ale-ajax").hide();
                $(".form-message p").html('<span class="success">Thanks. Your Message Sent Successfully.');
                $("#ale_modal_form :input[type='text'],#ale_modal_form :input[type='email'],#ale_modal_form textarea").val('');
            })
            .fail(function() {

                //hide loading and show error
                $( ".ale-ajax").hide();
                $(".form-message p").html('<span class="error">Oops, something went wrong.');
            });
        event.preventDefault();
    });
});