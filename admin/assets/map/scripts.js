jQuery(function($) {
    "use strict";

    //Header Builder
    //==============

        //Open search form "from top"
        $('.ale_search_button_header').on('click',function(){

            var ale_form_position = $('.header-builder .ale_search_form').css('margin-top');

            if(ale_form_position == '-50px') {
                $('.header-builder .ale_search_form').css('margin-top', '0px');

                //Close search form
                $('.ale_search_close .ale_close_icon').on('click', function () {
                    $('.header-builder .ale_search_form').css('margin-top', '-50px');
                });
            } else {
                $('.header-builder .ale_search_form').css('margin-top', '-50px');
            }
        });

        //Open "cover" search form
        $('.ale_cover_search_button_header').on('click',function(){
            $('.ale_cover_search_form').fadeIn(300);

            $('.ale_cover_search_close .ale_close_icon').on('click',function(){
                $('.ale_cover_search_form').fadeOut(300);
            });
        });


        //Open Right Side Section
        $('.ale_open_right_side').on('click',function(){

            var move_value = $('.ale_right_side_container').data('move');

            if($(this).hasClass('opened')){
                $('.ale_side_wrapper,.builder-fixed,.builder-sticky').css('left', '0px');
                $(this).removeClass('opened');
            } else {
                $('.ale_right_side_container').css('visibility', 'visible');
                $('.ale_side_wrapper,.builder-fixed,.builder-sticky').css('left', '-' + move_value);
                $(this).addClass('opened');

                $('.close_right_side i').on('click',function(){
                    $('.ale_side_wrapper,.builder-fixed,.builder-sticky').css('left', '0px');
                    $('.ale_open_right_side').removeClass('opened');
                });
            }
        });

        //Scroll for right side
        if($('.side-scroll-pane').length){
            $('.side-scroll-pane').jScrollPane({
                mouseWheelSpeed: 3,
                animateScroll:true,
                autoReinitialise : true
            });
        }

        //Open Left side
        $('.ale_left_side_open').on('click',function(){
            var opened_side = $('.left_builder_hidden_side').css('left');
            var value_left = $('.left_header_builder').data('width');
            if(opened_side != '0px'){
                $('.left_builder_hidden_side').css('left','0');
                $('.ale_open_leftside').css('display','none');
                $('.ale_close_leftside').css('display','inline-block');
            } else {
                $('.left_builder_hidden_side').css('left',value_left);
                $('.ale_open_leftside').css('display','inline-block');
                $('.ale_close_leftside').css('display','none');
            }
        });

        //Scroll for left side
        if($('.left-side-scroll-pane').length){
            $('.left-side-scroll-pane').jScrollPane({
                mouseWheelSpeed: 3,
                animateScroll:true,
                autoReinitialise : true
            });
        }

        //Show mobile dropdown menu
        $('.builder-dropdown-style').on('click',function(){
           $('nav.builder-dropdown-menu').toggle();
        });


    //Sliders
    if($('.home3-gallery .slider,.home3-gallery .slider,.slider-box .wrapper, .agents-block .wrapper, .home2-agents .wrapper, .clients-block, .slider-box-2 .wrapper, .slider-box-3, .home3-agencies .slider, .home3-testimonials .slider').length){
        $('.home3-gallery .slider,.slider-box .wrapper, .agents-block .wrapper, .home2-agents .wrapper, .clients-block, .slider-box-2 .wrapper, .slider-box-3, .home3-agencies .slider, .home3-testimonials .slider').flexslider({
            controlNav: false,
            animation: "slide",
            pauseOnHover: true
        });
    }
    if($('.featured-slider').length){
        $('.featured-slider').flexslider({
            controlNav: false,
            animation: "slide",
            itemWidth: 480,
            move: 1
        });
    }
    if($('.single-slider').length){
        $('.single-slider').flexslider({
            animation: "slide",
            controlNav: false,
            directionNav: false,
            animationLoop: false,
            slideshow: false,
            itemMargin: 0,
            sync: ".thumbnails"
        });
    }
    if($('.thumbnails').length){
        $('.thumbnails').flexslider({
            animation: "slide",
            direction: "vertical",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            directionNav: true,
            maxItems: 3,
            minItems: 1,
            asNavFor: ".single-slider",
            move: 1,
            itemWidth: 140
        });
    }
    if($('.bot-property-flexslider').length){
        $('.bot-property-flexslider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemMargin: 0,
            sync: ".bot-property-thumbnails"
        });
    }
    if($('.bot-property-thumbnails').length){
        $('.bot-property-thumbnails').flexslider({
            animation: "slide",
            animationLoop: false,
            slideshow: false,
            controlNav: false,
            directionNav: false,
            maxItems: 7,
            minItems: 1,
            asNavFor: ".bot-property-flexslider",
            itemWidth: 138
        });
    }
    if($('.shop-thumbnails ul.slides li').length){
        $('.shop-thumbnails').flexslider({
            animation: "slide",
            controlNav: false,
            maxItems: 4,
            minItems: 1,
            move: 1,
            itemWidth: 90
        });
    }
    if($('.skills .slider').length){
        $('.skills .slider').flexslider({
            animation: "slide",
            controlNav: false,
        });
    }
    if($('.simple-slider').length){
        $('.simple-slider').flexslider({
            animation: "slide",
            controlNav: false
        });
    }
    if($('.mobile-gallery-slider').length){
        $('.mobile-gallery-slider').flexslider({
            animation: "slide",
            controlNav: false
        });
    }
    if($('.recent-properties-2 .slider').length){
        $('.recent-properties-2 .slider').flexslider({
            animation: "slide",
            controlNav: false,
            maxItems: 8,
            minItems: 1,
            itemWidth: 240,
            move: 1,
            pauseOnHover: true
        });
    }

	//Pop up window
	if($('.pop-up-slider').length){
		var windowWidth = $(window).width();
		if(windowWidth >= 960){
			var boxWidth = windowWidth - 400;
		} else {
			var boxWidth = windowWidth;
		}
		var boxHeight = boxWidth / 2;
		var windowHeight = ($(window).height() - boxHeight) / 2;
		$('.pop-up-slider .box').css({
			width: boxWidth,
			height: boxHeight,
			marginTop: windowHeight
		});

		//Exit
		$('.pop-up-slider .exit, .pop-up-slider .exit-box').live('click', function(){
			$('.pop-up-slider').fadeOut(300);
		});

		//Show
		$('.single-page.single-gallery .top .single-slider .flex-active-slide').on('click', function(){
			$('.pop-up-slider').fadeIn(300);
			$('.pop-up-single-slider').flexslider({
				animation: "slide",
				controlNav: false
			});
		});
	}

    $( window ).load(function() {
        if($('.grid-system-gallery-3').length){
            $('.grid-system-gallery-3 .slider').flexslider({
                animation: "slide",
                controlNav: false
            });
        }
    });

    //UI Price
    if($('.rangeSldier_scale').length){
        var min_count = $('.sliderRange_first').val();
        var max_count = $('.sliderRange_last').val();
        $( ".rangeSldier_scale" ).slider({
            range: true,
            min: $('body').data('min_price'),
            max: $('body').data('max_price'),
            values: [ min_count, max_count ],
            slide: function( event, ui ) {
                $( ".sliderRange_first" ).val( ui.values[ 0 ] );
                $( ".sliderRange_last" ).val( ui.values[ 1 ] );
            }
        });
        $( ".sliderRange_first" ).val( $( ".rangeSldier_scale" ).slider( "values", 0 ));

        $( ".sliderRange_last" ).val( $( ".rangeSldier_scale" ).slider( "values", 1 ) );
    }
    //All categories list
    if($('.all-categories').length){
        $('.all-categories > ul > li > span').toggle(function(){
            $('.all-categories ul li > ul').fadeIn(300);
        }, function(){
            $('.all-categories ul li > ul').fadeOut(300);
        });
    }
    //Scroll
    if($('.scroll-pane').length){
        $('.scroll-pane').jScrollPane();
    }

    //Select
    function isTouchDevice(){
        return typeof window.ontouchstart !== 'undefined';
    }

    if($('.dropdown').length && isTouchDevice() == false){
        $('.dropdown').easyDropDown();
    }

    //Change grid
    if($('.grid-change').length){
        $('.grid-change .fullwidth').on('click', function(){
            $('.archive-page article').animate({
                opacity: 0
            }, 300, function(){
                $('.archive-page').removeClass('grid');
            });
            $('.archive-page article').animate({
                opacity:1
            }, 300);
        });

        $('.grid-change .partwidth').on('click', function(){
            $('.archive-page article').animate({
                opacity: 0
            }, 300, function(){
                $('.archive-page').addClass('grid');
            });
            $('.archive-page article').animate({
                opacity:1
            }, 300);
        })
    }

    //Accordion
    if($(".accordion").length){
        $(".accordion").accordion({
            collapsible: true,
            heightStyle: "content"
        });
    }

    //Select Navigation on Mobile Devices
    $('select.mobilemenu').change(function(){
        var url = $(this).val();
        if (url) {
            window.location = url;
        }
        return false;
    });

    $('select.builder-mobile-menu').change(function(){
        var url = $(this).val();
        if (url) {
            window.location = url;
        }
        return false;
    });

    //Acocordion on home page 4
    if($($('.home4-blog .text'))){
        var text2 = $('.home4-blog .text .string');
        var text = $('.home4-blog .text');
        text.on('click', function(){
            $('.home4-blog .text .string').slideUp('slow');
            $('.home4-blog .text').removeClass('active');
            $(this).addClass('active');
            $(this).children().slideDown('slow');
            $(this).parent().parent().find('.image');
            var active_post = $(this).data('attr');
            //Disable all images
            $('.home4-blog .image').css('opacity','0');
            //Enable Active Emage
            $('.home4-blog .image[data-attr="' + active_post +'"]').css({'opacity': '1','z-index': '++1'});

        });
    }
    if($($('.home5-blog .text'))){
    //Acocordion on home page 5
        var text2 = $('.home5-blog .text .string');
        var text = $('.home5-blog .text');
        text.on('click', function(){
            text2.slideUp('slow');
            text.removeClass('active');
            $(this).addClass('active');
            $(this).children().slideDown('slow');
            $(this).parent().parent().find('.image');
            var active_post = $(this).data('attr');
            //Disable all images
            $('.home5-blog .image').css('opacity','0');
            //Enable Active Emage
            $('.home5-blog .image[data-attr="' + active_post +'"]').css({'opacity': '1','z-index': '++1'});

        });
    }

    //Tabs
    if($(".registration-form").length){
        $(".registration-form .inner").tabs({
            collapsible: true
        });

        $('.forgot-password-box .forgot-password-link').on('click',function(){
            $('#forgot-password').toggle();
            $(".user-auth,.forgot-password-box").css('display','none');

            $('span.return-to-login').on('click',function(){
                $(".user-auth,.forgot-password-box").css('display','block');
                $('#forgot-password').css('display','none');
            });
        });
    }

    //Forgot Password
    $('#ale-forgot').on('click',function(e){
        e.preventDefault();

        var forgot_email = $('#forgot_email').val();
        var security_forgot = $('#security-forgot').val();
        var ajaxurl = ale.ajax_load_url;


        jQuery.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                'action'            :   'ale_ajax_forgot_password',
                'forgot_email'      :   forgot_email,
                'security-login'    :   security_forgot
            },

            success: function (data) {
                jQuery('#forgot_email').val('');
                jQuery('#forgot_password_message').empty().text(data);
            },
            error: function (errorThrown) {
            }
        });
    });

    if($(".exit").length){
        $(".exit").on('click', function(){
            $('.pop-up').fadeOut(300);
        });
    }
    if($(".registration").length){
        $(".registration span,.registration a.profile").on('click', function(e){
            $('.registration-form').fadeIn(300);
            e.preventDefault();
        });
    }

    //Add new Property fields
    var images_num = $('.gal-fields #property_gimg').length;
    $('#add-another').on('click',function(){
        images_num++;
        $('.gal-fields').append('<input type="file" name="property_gimg_'+images_num+'" id="property_gimg" tabindex="4" />');
    });

    //Enquire Popup
    $('#open_enquire').on('click',function(){
        $('#enquire-box').fadeIn(300);
    });

    //Submit
    $( '.article_filter' ).on( 'change', 'select.orderby', function() {
        $( this ).closest( 'form' ).submit();
    });

    // Properties Sorting
    function insertParam(key, value) {
        key = encodeURI(key);
        value = encodeURI(value);

        var kvp = document.location.search.substr(1).split('&');

        var i = kvp.length;
        var x;
        while (i--) {
            x = kvp[i].split('=');

            if (x[0] == key) {
                x[1] = value;
                kvp[i] = x.join('=');
                break;
            }
        }

        if (i < 0) {
            kvp[kvp.length] = [key, value].join('=');
        }

        //this will reload the page, it's likely better to store this until finished
        document.location.search = kvp.join('&');
    }

    $('#ale-sort-properties').on('change', function() {
        var key = 'orderby';
        var value = $(this).val();
        insertParam( key, value );
    });

    //Isotope
    $( window ).load(function() {
        if($('.grid-system').length){
            $('.grid-system').isotope({
                itemSelector: '.grid-item',
                layoutMode: 'packery'
            });
        }
        if($('.grid-system2').length){
            $('.grid-system2').isotope({
                itemSelector: '.grid-item',
                layoutMode: 'packery'
            });
        }
        if($('.grid-system-gallery-1').length){
            $('.grid-system-gallery-1').isotope({
                itemSelector: '.grid-item',
                layoutMode: 'packery',
                packery: {
                    gutter: '.gutter'
                }
            });
        }
        if($('.grid-system-gallery-2').length){
            $('.grid-system-gallery-2').isotope({
                itemSelector: '.grid-item',
                layoutMode: 'packery'
            });
        }
        if($('.grid-system-gallery-3').length){
            $('.grid-system-gallery-3').isotope({
                itemSelector: '.grid-item',
                layoutMode: 'packery',
                packery: {
                    gutter: '.gutter'
                }
            });
        }
        if($('.grid-system-gallery-5').length){
            $('.grid-system-gallery-5').isotope({
                itemSelector: '.grid-item',
                layoutMode: 'packery',
                packery: {
                    gutter: '.gutter'
                }
            });
        }
    });

    //Popup video
    $('.video-box-section .item').on('click', function(){
        $('.video-box-section .popup').fadeIn(300);
        $('.popup .close').on('click', function(){
            $('.video-box-section .popup').fadeOut(300);
        })
    });
    //Popup video on home page 4
    $('.video-box').on('click', function(){
        $('.video-popup').fadeIn(300);
        $('.popup .close').on('click', function(){
            $('.popup').fadeOut(300);
        })
    });

    //Progress bar
    $(window).scroll(function() {
        if($('.progress').length){
            var progress = $('.progress');
            var position = progress.offset().top;
            var topOfWindow = $(window).scrollTop();

            if (position < topOfWindow + 400)
            {
                progress.css({visibility: "visible"});
                progress.removeClass("zero");
                progress.addClass("toright");
            }
        }
    });
    $('.home3-map .map-overlay span').toggle(function(){
        $('.home3-map').css('height', '450px');
        $('.home3-map .map-overlay').css({
            'top': 'auto',
            'bottom': '0',
            'height': '18%'
        });
        $('.home3-map .map-overlay span').css({
            'margin-top': '13px'
        }).text('Hide map');
    }, function(){
        $('.home3-map').css('height', '169px');
        $('.home3-map .map-overlay').css({
            'height': '100%'
        });
        $('.home3-map .map-overlay span').css({
            'margin-top': '56px'
        }).text('Open map');
    });

    //Filter hide
    $('.hide-filter,.map-overlay-background').on('click', function(){
        $('.home4-map .filter').animate({
            'left': '-100%'
        },1000);
        $('.home4-map .filter-shadow').animate({
            'left': '-100%'
        },1000);
        $('.home4-map .show-filter').animate({
            'left': '0'
        },1000);
        $('.map-overlay-background').fadeOut(1000);
    });
    $('.show-filter').on('click', function(){
        $('.home4-map .filter').animate({
            'left': '50%'
        },1000);
        $('.home4-map .filter-shadow').animate({
            'left': '50%'
        },1000);
        $('.home4-map .show-filter').animate({
            'left': '-100%'
        },1000);
        $('.map-overlay-background').fadeIn(1000);
    });

    //Contact page
    if($('.contact1 .ale_map_canvas')){
        $('.contact1 .ale_map_canvas').hover(function(){
            $('.contact1 .info').css('top', '-207px');
        }, function(){
            $('.contact1 .info').css('top', '0');
        });
    }

    $('a.remove-image').click(function(event){
        event.preventDefault();

        var $this = $(this);
        var gallery_thumb = $this.closest('.gallery-thumb');
        var loader = $this.siblings('.loader');

        loader.show();

        var removal_request = $.ajax({
            url: $this.attr('href'),
            type: "POST",
            data: {
                gallery_img_id : $this.data('gallery-img-id'),
                action : "remove_gallery_image"
            },
            dataType: "html"
        });

        removal_request.done(function( msg ) {
            var code = parseInt(msg);
            loader.hide();

            if(code == 3){
                gallery_thumb.remove();
            }else if( code == 2){
                alert( "Failed to remove!" );
            }else if( code == 1){
                alert( "Invalid Parameters!" );
            }else{
                alert( "Unexpected response: " + msg );
            }

        });

        removal_request.fail(function( jqXHR, textStatus ) {
            alert( "Request failed: " + textStatus );
        });
    });

    $('a.remove-featured-image').click(function(event){
        event.preventDefault();

        var $this = $(this);
        var gallery_thumb = $this.closest('.gallery-thumb');
        var loader = $this.siblings('.loader');

        loader.show();

        var removal_request = $.ajax({
            url: $this.attr('href'),
            type: "POST",
            data: {
                property_id : $this.data('property-id'),
                action : "remove_featured_image"
            },
            dataType: "html"
        });

        removal_request.done(function( msg ) {
            var code = parseInt(msg);
            loader.hide();

            if(code == 3){
                gallery_thumb.remove();
                $("#featured-file-container").removeClass('hidden');
            }else if( code == 2){
                alert( "Failed to remove!" );
            }else if( code == 1){
                alert( "Invalid Parameters!" );
            }else{
                alert( "Unexpected response: " + msg );
            }

        });

        removal_request.fail(function( jqXHR, textStatus ) {
            alert( "Request failed: " + textStatus );
        });
    });


    // Add to favorites
    $('a#ale-add-to-favorite').click(function(e){
        e.preventDefault();
        var $star = $(this).find('i');
        var ale_add_to_favorite = {
            target:        '#favorite_target',   // target element(s) to be updated with server response
            beforeSubmit:  function(){
                $star.addClass('fa-spin');
            },  // pre-submit callback
            success:       function(){
                $star.removeClass('fa-spin');
                $('#ale-add-to-favorite').hide(0,function(){
                    $('#favorite_output').delay(200).show();
                });
            }
        };

        $('#add-to-favorite-form').ajaxSubmit( ale_add_to_favorite );
    });

    //Remove from favorites
    $('a.ale-remove-from-favorite').click(function(event){
        event.preventDefault();
        var $this = $(this);
        var property_item = $this.parent().parent();
        var loader = $this.siblings('.loader');
        var ajax_response = property_item.find('.ajax-response');

        $this.hide();
        loader.show();

        var ale_remove_favorite_request = $.ajax({
            url: $this.attr('href'),
            type: "POST",
            data: {
                ale_property_id : $this.data('property-id'),
                ale_user_id : $this.data('user-id'),
                action : "ale_remove_from_favorites"
            },
            dataType: "html"
        });


        ale_remove_favorite_request.done(function( msg ) {
            var code = parseInt(msg);
            loader.hide();

            if(code == 3){
                property_item.remove();
            }else if( code == 2){
                ajax_response.text("Failed to remove!");
            }else if( code == 1){
                ajax_response.text("Invalid Data.");
            }else{
                ajax_response.text("Unexpected Response: "+ msg);
            }

        });

        ale_remove_favorite_request.fail(function( jqXHR, textStatus ) {
            ajax_response.text( "Request failed: " + textStatus );
        });
    });

    //Header scroll
    $(window).bind('scroll', function() {
        var scroll_top = $(document).scrollTop();
        if($('header.light').length){
            var header = $('header.light div.header');
            var menubox = $('header.light .header-menu');
        }else if($('header .info').length){
            var header = $('header.dark .info');
            var menubox = $('header.dark div.header');
        }

        if($('header.builder-regular,header.builder-fixed,header.builder-sticky,aside.left_header_builder ').length == 0){
            var headerheight = header.outerHeight();

            if(scroll_top >= headerheight ){
                menubox.css({
                    'position': 'fixed',
                    'top': '0',
                    'z-index':'9999',
                    'padding': '0'
                });
                menubox.addClass('shadow-header');
                if($('.absolute-header').length){
                    header.css('margin-bottom', '');
                } else {
                    header.css('margin-bottom', '66px');
                }
            } else {
                menubox.css({
                    'position': '',
                    'top': '',
                    'z-index':'',
                    'padding': ''
                });
                menubox.removeClass('shadow-header');
                header.css('margin-bottom', '');
            }
        }
    });

    if($('.video-section .mask').length){
        $('.video-section .mask').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });
    }
    if($('.linkmap').length){
        $('.linkmap').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });
    }
});

Modernizr.addTest('ipad', function () {
    return !!navigator.userAgent.match(/iPad/i);
});

Modernizr.addTest('iphone', function () {
    return !!navigator.userAgent.match(/iPhone/i);
});

Modernizr.addTest('ipod', function () {
    return !!navigator.userAgent.match(/iPod/i);
});

Modernizr.addTest('appleios', function () {
    return (Modernizr.ipad || Modernizr.ipod || Modernizr.iphone);
});

Modernizr.addTest('positionfixed', function () {
    var test    = document.createElement('div'),
        control = test.cloneNode(false),
        fake = false,
        root = document.body || (function () {
            fake = true;
            return document.documentElement.appendChild(document.createElement('body'));
        }());

    var oldCssText = root.style.cssText;
    root.style.cssText = 'padding:0;margin:0';
    test.style.cssText = 'position:fixed;top:42px';
    root.appendChild(test);
    root.appendChild(control);

    var ret = test.offsetTop !== control.offsetTop;

    root.removeChild(test);
    root.removeChild(control);
    root.style.cssText = oldCssText;

    if (fake) {
        document.documentElement.removeChild(root);
    }

    /* Uh-oh. iOS would return a false positive here.
     * If it's about to return true, we'll explicitly test for known iOS User Agent strings.
     * "UA Sniffing is bad practice" you say. Agreeable, but sadly this feature has made it to
     * Modernizr's list of undectables, so we're reduced to having to use this. */
    return ret && !Modernizr.appleios;
});


// Modernizr.load loading the right scripts only if you need them
Modernizr.load([
    {
        // Let's see if we need to load selectivizr
        test : Modernizr.borderradius,
        // Modernizr.load loads selectivizr and Respond.js for IE6-8
        nope : [ale.template_dir + '/js/libs/selectivizr.min.js', ale.template_dir + '/js/libs/respond.min.js']
    },{
        test: Modernizr.touch,
        yep:ale.template_dir + '/css/touch.css'
    }
]);



jQuery(function($) {
    var is_single = $('#post').length;
    var posts = $('article.post');
    var is_mobile = parseInt(ale.is_mobile);

    var slider_settings = {
        animation: "slide",
        slideshow: false,
        controlNav: false
    }

    $(document).ajaxComplete(function(){
        try{
            if (!posts.length) {
                return;
            }
            FB.XFBML.parse();
            gapi.plusone.go();
            twttr.widgets.load();
            pin_load();
        }catch(ex){}
    });

    // open external links in new window
    $("a[rel$=external]").each(function(){
        $(this).attr('target', '_blank');
    });

    $.fn.init_posts = function() {
        var init_post = function(data) {
            // close other posts
            data.post.siblings('.open-post').find('a.toggle').trigger('click', {
                hide:true
            });

            var loading = data.post.find('span.loading');

            if (data.more.is(':empty')) {
                data.post.addClass('post-loading');
                loading.css('visibility', 'visible');
                data.more.load(ale.ajax_load_url, {
                    'action':'aletheme_load_post',
                    'id':data.post.data('post-id')
                }, function(){
                    loading.remove();
                    data.more.slideDown(400, function(){
                        data.post.addClass('open-post');
                        data.toggler.text('Close Post');
                        $('.video', data.more).fitVids();
                        if (data.scroll) {
                            data.scroll.scrollTo('fast');
                        }
                    });
                    init_comments(data.post);
                });
            } else {
                data.more.slideDown(400, function(){
                    data.post.addClass('open-post');
                    data.toggler.text('Close Post');
                    if (data.scroll) {
                        data.scroll.scrollTo('fast');
                    }
                });
            }
        }

        var load_post = function(e, _opts) {
            e.preventDefault();
            var data  = {
                toggler:$(this),
                scroll:false
            };
            var opts = $.extend({
                comments:false,
                hide:false,
                add_comment:false
            }, _opts);
            data.post = data.toggler.parents('article.post');
            data.more = data.post.find('.full');

            if (data.more.is(':visible')) {
                if (opts.hide == true) {
                    // quick hide for multiple posts
                    data.more.hide();
                } else {
                    data.more.slideUp(400);
                }
                data.toggler.text('Open Post');
                data.post.removeClass('open-post');
            } else {
                if (typeof(e.originalEvent) != 'undefined' ) {
                    data.scroll = data.post;
                }
                init_post(data);
            }
        }

        var init_comments = function(post) {
            var respond = $('section.respond', post);
            var respond_form = $('form', respond);
            var respond_form_error = $('p.error', respond_form);
            //var respond_cancel = $('.cancel-comment-reply a', respond);
            var comments = $('section.comments', post);

            /*$('a.comment-reply-link', post).on('click', function(e){
             e.preventDefault();
             var comment = $(this).parents('li.comment');
             comment.find('>div').append(respond);
             respond_cancel.show();
             respond.find('input[name=comment_post_ID]').val(post.data('post-id'));
             respond.find('input[name=comment_parent]').val(comment.data('comment-id'));
             respond.find('input:first').focus();
             }).attr('onclick', '');

             respond_cancel.on('click', function(e){
             e.preventDefault();
             comments.after(respond);
             respond.find('input[name=comment_post_ID]').val(post.data('post-id'));
             respond.find('input[name=comment_parent]').val(0);
             $(this).hide();
             });
             */
            respond_form.ajaxForm({
                'beforeSubmit':function(){
                    respond_form_error.text('').hide();
                },
                'success':function(_data){
                    var data = $.parseJSON(_data);
                    if (data.error) {
                        respond_form_error.html(data.msg).slideDown('fast');
                        return;
                    }
                    var comment_parent_id = respond.find('input[name=comment_parent]').val();
                    var _comment = $(data.html);
                    var list;
                    _comment.hide();

                    if (comment_parent_id == 0) {
                        list = comments.find('ol');
                        if (!list.length) {
                            list = $('<ol class="commentlist "></ol>');
                            comments.find('.scrollbox .jspContainer .jspPane').append(list);
                        }
                    } else {
                        list = $('#comment-' + comment_parent_id).parent().find('ul');
                        if (!list.length) {
                            list = $('<ul class="children"></ul>');
                            $('#comment-' + comment_parent_id).parent().append(list);
                        }
                        respond_cancel.trigger('click');
                    }
                    list.append(_comment);
                    _comment.fadeIn('fast');
                    //.scrollTo();

                    respond.find('textarea').clearFields();
                },
                'error':function(response){
                    var error = response.responseText.match(/\<p\>(.*)<\/p\>/)[1];
                    if (typeof(error) == 'undefined') {
                        error = 'Something went wrong. Please reload the page and try again.';
                    }
                    respond_form_error.html(error).slideDown('fast');
                }
            });
        }
        $(this).each(function(){
            var post = $(this);
            // init post galleries
            $(window).load(function(){
                $('.preview .flexslider', post).flexslider(slider_settings);
            });
            // do not init ajax posts & comments for mobile
            if (!is_mobile) {
                // ajax posts enabled
                if (ale.ajax_posts) {
                    $('a.toggle', post).click(load_post);
                    $('.video', post).fitVids();
                    $('.preview figure a', post).click(function(e){
                        e.preventDefault();
                        $(this).parents('article.post').find('a.toggle').trigger('click');
                    });
                }
            }
        });
        // init ajax comments on a single post if ajax comments are enabled
        if (is_single && parseInt(ale.ajax_comments)) {
            init_comments(posts);
        }
        // open single post on page
        if (parseInt(ale.ajax_open_single) && !is_single && posts.length == 1) {
            posts.find('a.toggle').trigger('click');
        }
    }
    posts.init_posts();

    $.fn.init_gallery = function() {
        $(this).each(function(){
            var gallery = $(this);
            $(window).load(function(){
                $('.flexslider', gallery).flexslider(slider_settings);
            });

        })
    }
    $('#gallery').init_gallery();

    $.fn.init_archives = function()
    {
        $(this).each(function(){
            var archives = $(this);
            var year = $('#archives-active-year');
            var months = $('div.months div.year-months', archives);
            var arrows = $('a.up, a.down', archives);
            var activeMonth;
            var current, active;
            var animated = false;
            if (months.length == 1) {
                arrows.remove();
                activeMonth = months.filter(':first').addClass('year-active').show();
                year.text(activeMonth.attr('id').replace(/[^0-9]*/, ''));
                return;
            }
            arrows.click(function(e){
                e.preventDefault();
                if (animated) {
                    return;
                }
                var fn = $(this);
                animated = true;
                arrows.css('visibility', 'visible');
                var current = months.filter('.year-active');
                if (fn.hasClass('up')) {
                    active = current.prev();
                    if (!active.length) {
                        active = months.filter(':last');
                    }
                } else {
                    active = current.next();
                    if (!active.length) {
                        active = months.filter(':first');
                    }
                }
                current.removeClass('year-active').fadeOut(150, function(){
                    active.addClass('year-active').fadeIn(150, function(){
                        animated = false;
                    });
                    year.text(active.attr('id').replace(/[^0-9]*/, ''));

                    if (fn.hasClass('up')) {
                        if (!active.prev().length) {
                            arrows.filter('.up').css('visibility', 'hidden');
                        }
                    } else {
                        if (!active.next().length) {
                            arrows.filter('.down').css('visibility', 'hidden');
                        }
                    }
                });
            });
            activeMonth = months.filter(':first').addClass('year-active').show();
            year.text(activeMonth.attr('id').replace(/[^0-9]*/, ''));
            arrows.filter('.up').css('visibility', 'hidden');
        });
    }
    $('#archives .ale-archives').init_archives();






});

// HTML5 Fallbacks for older browsers
jQuery(function($) {
    // check placeholder browser support
    if (!Modernizr.input.placeholder) {
        // set placeholder values
        $(this).find('[placeholder]').each(function() {
            $(this).val( $(this).attr('placeholder') );
        });

        // focus and blur of placeholders
        $('[placeholder]').focus(function() {
            if ($(this).val() == $(this).attr('placeholder')) {
                $(this).val('');
                $(this).removeClass('placeholder');
            }
        }).blur(function() {
                if ($(this).val() == '' || $(this).val() == $(this).attr('placeholder')) {
                    $(this).val($(this).attr('placeholder'));
                    $(this).addClass('placeholder');
                }
            });

        // remove placeholders on submit
        $('[placeholder]').closest('form').submit(function() {
            $(this).find('[placeholder]').each(function() {
                if ($(this).val() == $(this).attr('placeholder')) {
                    $(this).val('');
                }
            });
        });
    }
});

